package starter.salesorder;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class SalesOvertimeChartBuilder {

	private static final String DAY = "day";
	private static final String MONTH = "month";
	private static final String YEAR = "year";
	private static final String QUARTER = "quarter";
	
	public static List<String> getXAxis(String startDate, String endDate, String viewBy) throws ParseException {
		switch (viewBy.toLowerCase()) {
		case YEAR:
			int startYear = Integer.valueOf(DateTimeHelper.format(startDate, AdvoticsDateFormat.DATE_INDEX_FORMAT,
					AdvoticsDateFormat.YEAR_FORMAT));
			int endYear = Integer.valueOf(DateTimeHelper.format(endDate, AdvoticsDateFormat.DATE_INDEX_FORMAT,
					AdvoticsDateFormat.YEAR_FORMAT));
			List<String> year = new ArrayList<>();
			while (startYear <= endYear) {
				year.add(String.valueOf(startYear));
				startYear++;
			}
			return year;
		case QUARTER:
			int startYearQ = Integer.valueOf(DateTimeHelper.format(startDate, AdvoticsDateFormat.DATE_INDEX_FORMAT,
					AdvoticsDateFormat.YEAR_FORMAT));
			int endYearQ = Integer.valueOf(DateTimeHelper.format(endDate, AdvoticsDateFormat.DATE_INDEX_FORMAT,
					AdvoticsDateFormat.YEAR_FORMAT));
			List<String> yearQs = new ArrayList<>();
			while (startYearQ <= endYearQ) {
				yearQs.add(String.valueOf(startYearQ));
				startYearQ++;
			}

			List<String> quarter = new ArrayList<>();
			int firstMonthQuarter = Integer
					.valueOf(DateTimeHelper.convertDateIndexToSpecifiedDateFormat(Integer.valueOf(startDate), "MM"));
			int lastMonthQuarter = Integer
					.valueOf(DateTimeHelper.convertDateIndexToSpecifiedDateFormat(Integer.valueOf(endDate), "MM"));

			int firstQuarter = findQuarterByMonth(firstMonthQuarter);
			int lastQuarter = findQuarterByMonth(lastMonthQuarter);

			if (yearQs.size() == 1) {
				while (firstQuarter <= lastQuarter) {
					quarter.add("Q" + firstQuarter + " " + yearQs.get(0));
					firstQuarter++;
				}
			} else {
				while (firstQuarter <= 4) {
					quarter.add("Q" + firstQuarter + " " + yearQs.get(0));
					firstQuarter++;
				}

				if (yearQs.size() > 2) {
					for (int i = 1; i < yearQs.size() - 1; i++) {
						for (int j = 1; j < 5; j++) {
							quarter.add("Q" + j + " " + yearQs.get(i));
						}
					}
				}

				int q = 1;
				while (q <= lastQuarter) {
					quarter.add("Q" + q + " " + yearQs.get(yearQs.size() - 1));
					q++;
				}

			}

			return quarter;
		case MONTH:
			String startMonth = DateTimeHelper.format(startDate, AdvoticsDateFormat.DATE_INDEX_FORMAT,
					AdvoticsDateFormat.MONTH_FORMAT);
			String endMonth = DateTimeHelper.format(endDate, AdvoticsDateFormat.DATE_INDEX_FORMAT,
					AdvoticsDateFormat.MONTH_FORMAT);
			List<String> month = DateTimeHelper.getMonthsBetweenMonthRangeInclusive(startMonth, endMonth);
			return month;
		case DAY:
			
			 String startDay = DateTimeHelper.format(startDate,
			 AdvoticsDateFormat.ISO_8601_UTC_DAY_FORMAT, AdvoticsDateFormat.DAY_TIME_FORMAT);
			 String endDay = DateTimeHelper.format(endDate,
			 AdvoticsDateFormat.ISO_8601_UTC_DAY_FORMAT, AdvoticsDateFormat.DAY_TIME_FORMAT);
			 System.out.println("DAY START DAY CHANGE : " + startDay);
			 System.out.println("DAY END DAY CHANGE : " + endDay);
			 
			System.out.println("DAY START DAY : " + startDate);
			System.out.println("DAY END DAY : " + endDate);
			List<String> dates = DateTimeHelper.getDatesBetweenDateRangeInclusive(startDay, endDay);
			return dates;
		default:
			return new ArrayList<>();
		}
	}
	
	public static Integer findQuarterByMonth(int month) {
		return ((month - 1) / 3) + 1;
	}
	
}
