package starter.salesorder;

/*import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;*/

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cucumber.api.java.Before;

//@Slf4j
public class DemoTest {

	/*
	 * private Map<Integer, Integer> map = new HashMap<>();
	 * 
	 * @Before void beforeEach() { map.put(1, 1); map.put(2, 1); map.put(3, 1);
	 * map.put(4, 6);
	 * 
	 * map.put(5, 2); map.put(6, 2); map.put(7, 2); map.put(8, 3); map.put(9, 3);
	 * map.put(10, 4); map.put(11, 4);
	 * 
	 * map.put(12, 1); map.put(14, 17); map.put(15, 14); }
	 * 
	 * private List<Integer> findByParentList(Integer parent) { return
	 * map.entrySet().stream() .filter(entry -> entry.getValue().equals(parent))
	 * .map(Map.Entry::getKey) .collect(Collectors.toList()); }
	 */

//  @Test
  public void test1() {
    System.out.println("INI LIST DARI LANGSUNG JALANIN JAVA {}" + getResultList(1));
  }

  public List<Integer> getResultList(Integer startFrom) {
    Deque<Integer> finalQueue = new ArrayDeque<>();
    Deque<Integer> queue = findByParent(startFrom);

    finalQueue.push(startFrom);
    
    while (!queue.isEmpty()) {
      Integer value = queue.pop();
      finalQueue.push(value);

      Deque<Integer> byParent = findByParent(value);
      while (!byParent.isEmpty()) {
        queue.push(byParent.pop());
      }
    }

    // result untuk loop as criteria query param
    List<Integer> result = new ArrayList<>(finalQueue);
    Collections.sort(result);

    // buang yang perlu
    // result.remove(99978);

    return result;
  }
  
  public Map<Integer, Integer> getMAp(){
	  Map<Integer, Integer> map = new HashMap<>();
	  map.put(1, 1);
	    map.put(2, 1);
	    map.put(3, 1);
	    map.put(4, 6);

	    map.put(5, 2);
	    map.put(6, 2);
	    map.put(7, 2);
	    map.put(8, 3);
	    map.put(9, 3);
	    map.put(10, 4);
	    map.put(11, 4);

	    map.put(12, 1);
	    map.put(14, 17);
	    map.put(15, 14);
	  return map;
  }

  public Deque<Integer> findByParent(Integer parent) {
    Deque<Integer> queue = new ArrayDeque<>();
    
    Map<Integer, Integer> mapPP = new HashMap<>();
    mapPP = getMAp();
    
   

    for (Map.Entry<Integer, Integer> entry : mapPP.entrySet()) {
      if (entry.getValue().equals(parent) && !entry.getKey().equals(parent)) {
        queue.push(entry.getKey());
      }
    }

    queue.forEach(mapPP::remove);

    System.out.println("QUEQUE DEMO TEST : " + queue);
    return queue;
  }
}