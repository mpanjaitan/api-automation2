package starter.salesorder;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SalesOrdersController {

	/*@Autowired
	SalesOrdersRepository salesOrdersRepository;*/
	
	@Autowired
	SalesOrdersRepository salesOrdersRepRepository;

	@Autowired
	SalesOvertimeCalculatorService salesOvertimeService;
	/*@GetMapping("/all")
	public List<SalesOrders> index(){
		return salesOrdersRepository.findAll();
	}
	
	@GetMapping("/so/{soNo}")
	public SalesOrders show(@PathVariable String soNo) {
		
		 * List<SalesOrders> salesOrders = salesOrdersRepository.findBySONo(soNo);
		 * if(salesOrders==null || salesOrders.isEmpty()) { return null; }
		 
		
		return salesOrdersRepository.findBySONo(soNo);
	}
	
	@GetMapping("/soNew/{soNo}")
	public SalesOrdersNew showNew(@PathVariable String soNo) {
		return salesOrdersRepRepository.findBySONo(soNo);
	}*/
	
	@GetMapping("/soNew/all")
	public List<SalesOrderTransaction> showSOTransaction() {
		return salesOvertimeService.calculate();
		//return salesOrdersRepRepository.findByDefaultDate();
	}
	
	@GetMapping("/soByDate")
	public List<SalesOrderTransaction> showSOBydate(@RequestParam(value="idClient") Integer idClient, @RequestParam(value="startTime") Long startTime, @RequestParam(value="endTime") Long endTime) throws ParseException {
		return salesOvertimeService.calculateFindByDate(idClient, startTime, endTime);
//		return salesOrdersRepRepository.findByDate(idClient, startTime, endTime);
	}
	
	public List<SalesOrderTransaction> showSOTransactionMaria() {
		return salesOrdersRepRepository.findByDefaultDate();
	}
	
	//NEWW
	//maria
	@GetMapping("/soByDateString")
	public List<SalesOrderTransaction> showSOBydateString(@RequestParam(value="idClient") String idClient, @RequestParam(value="startTime") String startTime, @RequestParam(value="endTime") String endTime) throws ParseException {
		return salesOvertimeService.calculateFindByDateString(idClient, startTime, endTime);
//		return salesOrdersRepRepository.findByDate(idClient, startTime, endTime);
	}
	
	@GetMapping("/soRevenueBySOAndDate")
	public Map<String, Double> getSORevenueBySO(@RequestParam(value="idClient") String idClient, @RequestParam(value="username") String username, @RequestParam(value="startTime") String startTime, @RequestParam(value="endTime") String endTime) throws ParseException {
//		return salesOvertimeService.getRevenueBySOString(idClient, startTime, endTime);
		System.out.println("INI TOTAL ALL SALES REVENUE : " + salesOvertimeService.calculateFindByFilterStringHirarki(idClient, username, startTime, endTime));
		return salesOvertimeService.getRevenueBySOStringHirarki(idClient, username, startTime, endTime);
//		return salesOrdersRepRepository.findByDate(idClient, startTime, endTime);
	}
	
	@GetMapping("/soRevenueTotalBySOAndDate")
	public String getSORevenueTotalBySO(@RequestParam(value="idClient") String idClient, @RequestParam(value="username") String username, @RequestParam(value="startTime") String startTime, @RequestParam(value="endTime") String endTime) throws ParseException {
//		return salesOvertimeService.getRevenueBySOString(idClient, startTime, endTime);
		String salesOvertimeTotal = salesOvertimeService.calculateFindByFilterStringHirarki(idClient, username, startTime, endTime);
//		System.out.println("INI TOTAL ALL SALES REVENUE : " + salesOvertimeService.calculateFindByFilterStringHirarki(idClient, username, startTime, endTime));
//		return salesOvertimeService.calculateFindByFilterStringHirarki(idClient, username, startTime, endTime);
		System.out.println("INI TOTAL ALL SALES REVENUE : " + salesOvertimeTotal);
		return salesOvertimeTotal;
//		return salesOrdersRepRepository.findByDate(idClient, startTime, endTime);
	}
	
	@GetMapping("/soRevenueByProductInsight")
	public Map<String, Double> getSORevenueByProductInsight(@RequestParam(value="idClient") String idClient, @RequestParam(value="username") String username, @RequestParam(value="startTime") String startTime, @RequestParam(value="endTime") String endTime, @RequestParam(value="viewBy") String viewBy) throws ParseException {
		return salesOvertimeService.getRevenueByProductInsightHirarki(idClient, username, startTime, endTime, viewBy);
	}
	
	@GetMapping("/soRevenueTotalByProductInsight")
	public String getSORevenueTotalByProductInsight(@RequestParam(value="idClient") String idClient, @RequestParam(value="username") String username, @RequestParam(value="startTime") String startTime, @RequestParam(value="endTime") String endTime, @RequestParam(value="viewBy") String viewBy) throws ParseException {
		String revenueProductInsightTotal = salesOvertimeService.calculateRevenueByProductInsightHirarki(idClient, username, startTime, endTime, viewBy);
		System.out.println("INI TOTAL ALL SALES REVENUE PRODUCT INSIGHT : " + revenueProductInsightTotal);
		return revenueProductInsightTotal;
	}
	
	@GetMapping("/soProductInsightQtyFulfilled")
	public Map<String, Integer> getSOByProductInsightQtyFulfilled(@RequestParam(value="idClient") String idClient, @RequestParam(value="username") String username, @RequestParam(value="startTime") String startTime, @RequestParam(value="endTime") String endTime, @RequestParam(value="viewBy") String viewBy) throws ParseException {
		return salesOvertimeService.getProductInsightByQtyFulfilledHirarki(idClient, username, startTime, endTime, viewBy);
	}
	
	@GetMapping("/soTotalByProductInsightQtyFulfilled")
	public String getSOTotalByProductInsightQtyFulfilled(@RequestParam(value="idClient") String idClient, @RequestParam(value="username") String username, @RequestParam(value="startTime") String startTime, @RequestParam(value="endTime") String endTime, @RequestParam(value="viewBy") String viewBy) throws ParseException {
		String productInsightQtyFulfilledTotal = salesOvertimeService.calculateProductInsightQtyFulfilledHirarki(idClient, username, startTime, endTime, viewBy);
		System.out.println("INI TOTAL ALL SALES REVENUE PRODUCT INSIGHT QTY FULFILLED : " + productInsightQtyFulfilledTotal);
		return productInsightQtyFulfilledTotal;
	}
	
	@GetMapping("/soProductInsightScarcityLevel")
	public Map<String, Double> getSOByProductInsightScarcityLevel(@RequestParam(value="idClient") String idClient, @RequestParam(value="username") String username, @RequestParam(value="startTime") String startTime, @RequestParam(value="endTime") String endTime, @RequestParam(value="viewBy") String viewBy) throws ParseException {
		salesOvertimeService.getCustomerOverview(idClient, username, startTime, endTime);
		return salesOvertimeService.getProductInsightByScarcityLevelHirarkiString(idClient, username, startTime, endTime, viewBy);
	}
	
	@GetMapping("/soTotalByProductInsightScarcityLevel")
	public String getSOTotalByProductInsightScarcityLevel(@RequestParam(value="idClient") String idClient, @RequestParam(value="username") String username, @RequestParam(value="startTime") String startTime, @RequestParam(value="endTime") String endTime, @RequestParam(value="viewBy") String viewBy) throws ParseException {
		String productInsightScarcityLevelTotal = salesOvertimeService.calculateProductInsightScarcityLevelHirarki(idClient, username, startTime, endTime, viewBy);
		System.out.println("INI TOTAL ALL SALES REVENUE PRODUCT INSIGHT SCARCITY LEVEL : " + productInsightScarcityLevelTotal);
		return productInsightScarcityLevelTotal;
	}
	
	@GetMapping("/dashboardCustomerOverview")
	public Map<String, Integer> getDashboardCustomerOverview(@RequestParam(value="idClient") String idClient, @RequestParam(value="username") String username, @RequestParam(value="startTime") String startTime, @RequestParam(value="endTime") String endTime) throws ParseException {
		return salesOvertimeService.getCustomerOverview(idClient, username, startTime, endTime);
	}
	
	@GetMapping("/dashboardCustomerOverviewWorkgroup")
	public Map<String, String> getDashboardCustomerOverviewWorkgroup(@RequestParam(value="idClient") String idClient, @RequestParam(value="username") String username, @RequestParam(value="startTime") String startTime, @RequestParam(value="endTime") String endTime) throws ParseException {
		return salesOvertimeService.getCustomerOverviewWorkgroup(idClient, username, startTime, endTime);
	}
	
	@GetMapping("/dashboardCustomerOverviewArea")
	public Map<String, String> getDashboardCustomerOverviewArea(@RequestParam(value="idClient") String idClient, @RequestParam(value="username") String username, @RequestParam(value="startTime") String startTime, @RequestParam(value="endTime") String endTime) throws ParseException {
		return salesOvertimeService.getCustomerOverviewArea(idClient, username, startTime, endTime);
	}
}
