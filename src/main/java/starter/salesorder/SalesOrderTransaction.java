package starter.salesorder;

public interface SalesOrderTransaction {

	String getSalesOrderNo();
	Integer getClientId();
	String getStatus();
	String getCreationDate();
	String getSalesChannel();
	Integer getSoiSeq();
	String getProductCode();
	Integer getQuantity();
	Integer getFulfilledQuantity();
	Double getPrice();
	Character getIsActive();
	String getProductName();
	Integer getProductGroupId();
	Integer getBrandId();
	String getProductGroupName();
	String getBrandName();
	Integer getSalesOrderItemSeq();
	String getDiscountLevel();
	Integer getSoidDiscountSeq();
	Double getSoidValue();
	String getSoidValueType();
	Character getSoidIsActive();
	/*Integer getSodDiscountSeq();
	Double getSodValue();
	String getSodValueType();
	Character getSoodIsActive();*/
}
