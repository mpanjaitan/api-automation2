package starter.salesorder;

public interface SalesOrder {

	String getSalesOrderNo();
	Integer getClientId();
	String getStatus();
	Long getSubmittedTime();
	Double getGlobalSODiscount();
	Double getGlobalSOTax();
}
