package starter.salesorder;

public interface RegisteredStoreArea {

	String getProvinceName();
	Integer getRegisteredStore();
}
