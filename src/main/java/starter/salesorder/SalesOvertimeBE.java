package starter.salesorder;

import java.util.StringJoiner;

public class SalesOvertimeBE {

	private Integer statusCode;
	
	private String status;
	
	private Double value;
	
	private String valueLabel;

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public String getValueLabel() {
		return valueLabel;
	}

	public void setValueLabel(String valueLabel) {
		this.valueLabel = valueLabel;
	}

	@Override
	public String toString() {
		return "SalesOvertimeBE [statusCode=" + statusCode + ", status=" + status + ", value=" + value + ", valueLabel="
				+ valueLabel + "]";
	}
	
}
