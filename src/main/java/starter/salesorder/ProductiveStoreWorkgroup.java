package starter.salesorder;

public interface ProductiveStoreWorkgroup {

	String getSalesGroupName();
	Integer getProductiveStore();
	
}
