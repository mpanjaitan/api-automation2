package starter.salesorder;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.springframework.scheduling.support.CronSequenceGenerator;

/**
 * A class for all the date time manipulation/display/calculation/etc Convention
 * for the method name: 1. if returning 'long' (ms from epoch), use
 * 'get...InMillis()' 2. if returning string, use 'print...()' 3. if returning
 * date, use 'get...InDate()'
 * 
 * @author hendi
 *
 */
public class DateTimeHelper {

	enum DayOfWeekString {
		SUNDAY(Calendar.SUNDAY, "Sunday"), 
		MONDAY(Calendar.MONDAY, "Monday"), 
		TUESDAY(Calendar.TUESDAY, "Tuesday"), 
		WEDNESDAY(Calendar.WEDNESDAY, "Wednesday"), 
		THURSDAY(Calendar.THURSDAY, "Thursday"), 
		FRIDAY(Calendar.FRIDAY, "Friday"), 
		SATURDAY(Calendar.SATURDAY, "Saturday");

		private int dayOfWeek;
		private String label;

		DayOfWeekString(int dayOfWeek, String label) {
			this.dayOfWeek = dayOfWeek;
			this.label = label;
		}

		public int getDayOfWeek() {
			return dayOfWeek;
		}

		public String getLabel() {
			return label;
		}

		public static DayOfWeekString fromDayOfWeek(int dayOfWeek) {
			for (DayOfWeekString dows : DayOfWeekString.values()) {
				if (dows.getDayOfWeek() == dayOfWeek) {
					return dows;
				}
			}

			return null;
		}
	};

	enum MonthString {
		JANUARY(Calendar.JANUARY, "January"), 
		FEBRUARY(Calendar.FEBRUARY, "February"), 
		MARCH(Calendar.MARCH, "March"), 
		APRIL(Calendar.APRIL, "April"), 
		MAY(Calendar.MAY, "May"), 
		JUNE(Calendar.JUNE, "June"), 
		JULY(Calendar.JULY, "July"),
		AUGUST(Calendar.AUGUST, "August"), 
		SEPTEMBER(Calendar.SEPTEMBER, "September"), 
		OCTOBER(Calendar.OCTOBER, "October"), 
		NOVEMBER(Calendar.NOVEMBER, "November"), 
		DECEMBER(Calendar.DECEMBER, "December");

		private int month;
		private String label;

		MonthString(int month, String label) {
			this.month = month;
			this.label = label;
		}

		public int getMonth() {
			return month;
		}

		public String getLabel() {
			return label;
		}

		public static MonthString fromMonth(int month) {
			for (MonthString ms : MonthString.values()) {
				if (ms.getMonth() == month) {
					return ms;
				}
			}

			return null;
		}
	};

	public static final Integer MILISECOND = 1;
	public static final Integer SECOND = 2;
	public static final Integer MINUTE = 3;
	public static final Integer HOUR_OF_DAY = 4;
	public static final Integer DATE = 5;
	public static final Integer DAY_OF_WEEK = 6;
	public static final Integer DAY_OF_WEEK_IN_STRING = 7;
	public static final Integer MONTH = 8;
	public static final Integer MONTH_IN_STRING = 9;
	public static final Integer YEAR = 10;

	public static final int LOKI_INDEX_LENGTH = 8;
	public static final int ZERO_LENGTH = 0;

	private static final DateFormat fullTimeFormatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
	private static final DateFormat bareFullTimeFormatter = new SimpleDateFormat("ddMMyyyyHHmmssSSS");
	private static final DateFormat secondTimeFormatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	private static final DateFormat secondTimeWithMonthNameFormatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
	private static final DateFormat minuteTimeFormatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	private static final DateFormat dayTimeFormatter = new SimpleDateFormat("dd-MM-yyyy");
	private static final DateFormat dayMonthNameFormatter = new SimpleDateFormat("dd-MMM-yyyy");
	private static final DateFormat monthTimeFormatter = new SimpleDateFormat("MM-yyyy");
	private static final DateFormat monthFormatter = new SimpleDateFormat("MM-yyyy");
	private static final DateFormat yearFormatter = new SimpleDateFormat("yyyy");
	private static final DateFormat monthYearFormatter = new SimpleDateFormat("yyyyMM");
	private static final DateFormat rfc2822TimeFormatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");
	private static final DateFormat fileTimestampFormatter = new SimpleDateFormat("ddMMyyyyHHmmssSSS");
	private static final DateFormat milisecondFormatter = new SimpleDateFormat("HHmmssSSS");
	private static final DateFormat secondFormatter = new SimpleDateFormat("HHmmss");
	private static final DateFormat iso8601UtcTimeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

	private static final SimpleDateFormat formatterDate = new SimpleDateFormat("dd-MM-yyyy");
	private static final SimpleDateFormat formatterDateTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

	private static final DateFormat iso8601UtcDayFormatter = new SimpleDateFormat("yyyy-MM-dd");
	private static final DateFormat dateIndexFormatter = new SimpleDateFormat("yyyyMMdd");

	private static final DateFormat iso8601FullTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final DateFormat iso8601DayTimeFormatter = new SimpleDateFormat("yyyy-MM-dd");
	private static final DateFormat iso8601MonthTimeFormatter = new SimpleDateFormat("yyyy-MM");
	public static final DateFormat lokiIndexFormatter = new SimpleDateFormat("yyyyMMddHHmmss");
	private static final DateFormat yearMilisecondFormatter = new SimpleDateFormat("yyyyMMddHHmmssSSS");
	private static final DateFormat monthDateYearFormatter = new SimpleDateFormat("MM-dd-yyyy");
	private static final DateFormat bareFullTimeYYYYMMDDFormatter = new SimpleDateFormat("yyyyMMddHHmmssSSS");

	public static final String DAILY = "daily";
	public static final String ISO_8601_DAY_TIME_FORMAT = "yyyy-MM-dd"; 

	public static final String ISO_8601_FULLTIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String SECOND_TIME_FORMAT = "dd-MM-yyyy HH:mm:ss";
	public static final String DAY_TIME_FORMAT = "dd-MM-yyyy";
	public static final String DATE_INDEX_FORMAT = "yyyyMMdd";
	public static final String LOKI_INDEX_FORMAT = "yyyyMMddHHmmss";

	public static long getCurrentTimeInMillis() {
		Calendar now = new GregorianCalendar();
		return now.getTimeInMillis();
	}

	/**
	 * Helper utility to get the current hour in the 24-hour format i.e. 0-23. E.g.
	 * 10 PM will return 22 and noon time will return 12 See Calendar.HOUR_OF_DAY
	 * 
	 * @return
	 */
	public static int getCurrentHourOfDay() {
		Calendar now = new GregorianCalendar();

		return now.get(Calendar.HOUR_OF_DAY);
	}

	public static int getHourOfDayFromBaseTime(long timeInMillis) {
		Calendar now = new GregorianCalendar();
		now.setTimeInMillis(timeInMillis);

		return now.get(Calendar.HOUR_OF_DAY);
	}

	/**
	 * Helper utility to get the current hour in the 12-hour format i.e. 0-11. E.g.:
	 * noon and midnight will return 0. see Calendar.HOUR
	 * 
	 * @return
	 */
	public static int getCurrentHourOfAMPM() {
		Calendar now = new GregorianCalendar();

		return now.get(Calendar.HOUR);
	}

	/**
	 * Helper utility to get the current minute within the hour i.e. 0-59. See
	 * Calendar.MINUTE
	 * 
	 * @return
	 */
	public static int getCurrentMinuteOfHour() {
		Calendar now = new GregorianCalendar();

		return now.get(Calendar.MINUTE);
	}

	public static int getMinuteFromBaseTime(long timeInMillis) {
		Calendar now = new GregorianCalendar();
		now.setTimeInMillis(timeInMillis);

		return now.get(Calendar.MINUTE);
	}

	/**
	 * Helper utility to get the current day of week i.e. SUNDAY...SATURDAY. See
	 * Calendar.DAY_OF_WEEK
	 * 
	 * @return
	 */
	public static int getCurrentDayOfWeek() {
		Calendar now = new GregorianCalendar();

		return now.get(Calendar.DAY_OF_WEEK);
	}

	public static int getDayOfWeekFromBaseTime(long timeInMillis) {
		Calendar now = new GregorianCalendar();
		now.setTimeInMillis(timeInMillis);

		return now.get(Calendar.DAY_OF_WEEK);
	}

	public static int getCurrentDate() {
		Calendar now = new GregorianCalendar();

		return now.get(Calendar.DATE);
	}

	public static int getCurrentMonth() {
		Calendar now = new GregorianCalendar();

		// hendi 04-Feb-2018, the month here seems to be 1-off
		// e.g January is 0 and December is 11
		return now.get(Calendar.MONTH) + 1;
	}

	public static int getCurrentYear() {
		Calendar now = new GregorianCalendar();

		return now.get(Calendar.YEAR);
	}

	public static Map<Integer, String> getCurrentTimeFragments() {
		Map<Integer, String> fragments = new HashMap<Integer, String>();

		Calendar now = new GregorianCalendar();

		fragments.put(DateTimeHelper.MILISECOND, String.valueOf(now.get(Calendar.MILLISECOND)));
		fragments.put(DateTimeHelper.SECOND, String.valueOf(now.get(Calendar.SECOND)));
		fragments.put(DateTimeHelper.MINUTE, String.valueOf(now.get(Calendar.MINUTE)));
		fragments.put(DateTimeHelper.HOUR_OF_DAY, String.valueOf(now.get(Calendar.HOUR_OF_DAY)));
		fragments.put(DateTimeHelper.DATE, String.valueOf(now.get(Calendar.DATE)));

		int dayOfWeek = now.get(Calendar.DAY_OF_WEEK);
		fragments.put(DateTimeHelper.DAY_OF_WEEK, String.valueOf(dayOfWeek));
		fragments.put(DateTimeHelper.DAY_OF_WEEK_IN_STRING, DayOfWeekString.fromDayOfWeek(dayOfWeek).getLabel());

		int month = now.get(Calendar.MONTH);
		fragments.put(DateTimeHelper.MONTH, String.valueOf(month));
		fragments.put(DateTimeHelper.MONTH_IN_STRING, MonthString.fromMonth(month).getLabel());
		fragments.put(DateTimeHelper.YEAR, String.valueOf(now.get(Calendar.YEAR)));

		return fragments;
	}

	/**
	 * Get the midnight time of a certain day from the current time in epoch time
	 * 
	 * @param diffDays
	 *            - the day difference, - for past, + for future, 0 for today's
	 *            midnight
	 * @return
	 */
	public static long getMidnightTimeInMillis(int diffDays) {
		return getMidnightTimeInMillisFromBaseTime((new Date()).getTime(), diffDays);
	}

	/**
	 * Get the midnight time of a certain day from the given time in epoch time
	 * 
	 * @param timeInMillis
	 *            - the time stamp in millis of the base time
	 * @param diffDays
	 *            - the day difference, - for past, + for future, 0 for today's
	 *            midnight
	 * @return
	 */
	public static long getMidnightTimeInMillisFromBaseTime(long timeInMillis, int diffDays) {
		Calendar now = new GregorianCalendar();
		now.setTimeInMillis(timeInMillis);

		// reset hour, minutes, seconds and millis to get the needed midnight
		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);

		now.add(Calendar.DAY_OF_MONTH, diffDays);

		return now.getTimeInMillis();
	}

	/**
	 * Given hour of day and minute, return that time of today in millis e.g. given
	 * hour = 9 and minute 30, return today's 9.30 in epoch time
	 * 
	 * @param hourOfDay
	 * @param minute
	 * @return
	 */
	public static long getTodayHourMinuteTimeInMillis(int hourOfDay, int minute) {
		Calendar now = new GregorianCalendar();

		now.set(Calendar.HOUR_OF_DAY, hourOfDay);
		now.set(Calendar.MINUTE, minute);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);

		return now.getTimeInMillis();
	}

	public static long getHourMinuteTimeInMillisFromBaseTime(long timeInMillis, int hourOfDay, int minute) {
		Calendar now = new GregorianCalendar();
		now.setTimeInMillis(timeInMillis);

		now.set(Calendar.HOUR_OF_DAY, hourOfDay);
		now.set(Calendar.MINUTE, minute);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);

		return now.getTimeInMillis();
	}

	/**
	 * Method to add miliseconds to a given time and return the resulting epoch time
	 * 
	 * @param timeInMillis
	 * @param diffSeconds
	 *            - + for future, - for past
	 * @return
	 */
	public static long getMsecsAdditionInMillis(long timeInMillis, int diffMsecs) {
		Calendar now = new GregorianCalendar();
		now.setTimeInMillis(timeInMillis);

		now.add(Calendar.MILLISECOND, diffMsecs);

		return now.getTimeInMillis();
	}

	/**
	 * Method to add seconds to a given time and return the resulting epoch time
	 * 
	 * @param timeInMillis
	 * @param diffSeconds
	 *            - + for future, - for past
	 * @return
	 */
	public static long getSecondAdditionInMillis(long timeInMillis, int diffSeconds) {
		Calendar now = new GregorianCalendar();
		now.setTimeInMillis(timeInMillis);

		// Do the addition of seconds
		now.add(Calendar.SECOND, diffSeconds);

		return now.getTimeInMillis();
	}

	/**
	 * Method to add days to given day and return the resulting epoch time
	 * 
	 * @param timeInMillis
	 * @param diffDays
	 *            - + for future, - for past
	 * @return
	 */
	public static long getHoursAdditionInMillis(long timeInMillis, int diffHours) {
		Calendar now = new GregorianCalendar();
		now.setTimeInMillis(timeInMillis);

		now.add(Calendar.HOUR_OF_DAY, diffHours);

		return now.getTimeInMillis();

	}

	/**
	 * Method to add days to given day and return the resulting epoch time
	 * 
	 * @param timeInMillis
	 * @param diffDays
	 *            - + for future, - for past
	 * @return
	 */
	public static long getDayAdditionInMillis(long timeInMillis, int diffDays) {
		Calendar now = new GregorianCalendar();
		now.setTimeInMillis(timeInMillis);

		now.add(Calendar.DAY_OF_MONTH, diffDays);

		return now.getTimeInMillis();

	}

	/**
	 * Method to add months to a given time and return the resulting epoch time
	 * 
	 * @param timeInMillis
	 * @param diffMonths
	 *            - + for future, - for past
	 * @return
	 */
	public static long getMonthAdditionInMillis(long timeInMillis, int diffMonths) {
		Calendar now = new GregorianCalendar();
		now.setTimeInMillis(timeInMillis);

		// Do the addition of months
		now.add(Calendar.MONTH, diffMonths);

		return now.getTimeInMillis();
	}

	/**
	 * Method to add years to a given time and return the resulting epoch time
	 * 
	 * @param timeInMillis
	 * @param diffYears
	 *            - + for future, - for past
	 * @return
	 */
	public static long getYearAdditionInMillis(long timeInMillis, int diffYears) {
		Calendar now = new GregorianCalendar();
		now.setTimeInMillis(timeInMillis);

		// Do the addition of years
		now.add(Calendar.YEAR, diffYears);

		return now.getTimeInMillis();
	}

	public static long getBareFullStringInMillis(String timeString) throws ParseException {
		Date date = bareFullTimeFormatter.parse(timeString);
		return date.getTime();
	}

	public static long getBareFullYYYYMMDDStringInMillis(String timeString) throws ParseException {
		Date date = bareFullTimeYYYYMMDDFormatter.parse(timeString);
		return date.getTime();
	}

	/**
	 * Get a milliseconds from epoch time representation given a time format of
	 * DD-MM-YYYY HH:MI:SS
	 * 
	 * @param timeString
	 * @throws ParseException
	 */
	public static long getSecondStringInMillis(String timeString) throws ParseException {
		Date date = secondTimeFormatter.parse(timeString);
		return date.getTime();
	}

	public static long getMinuteStringInMillis(String timeString) throws ParseException {
		Date date = minuteTimeFormatter.parse(timeString);
		return date.getTime();
	}

	/**
	 * Get a milliseconds from epoch time representation given a time format of
	 * DD-MM-YYYY
	 * 
	 * @param timeString
	 * @throws ParseException
	 */
	public static long getDayStringInMillis(String timeString) throws ParseException {
		return getDayStringInMillis(timeString, true);
	}

	public static long getDayStringInMillis(String timeString, boolean lenient) throws ParseException {

		DateFormat dateFormat = new SimpleDateFormat(AdvoticsDateFormat.FORMATTER_DATE.getDateFormat());
		dateFormat.getCalendar().setLenient(lenient);
		Date date = dateFormat.parse(timeString);
		return date.getTime();
	}

	/**
	 * Get a milliseconds from epoch time representation given a time format of
	 * MM-YYYY
	 * 
	 * @param timeString
	 * @throws ParseException
	 */
	public static long getMonthStringInMillis(String monthString) throws ParseException {
		Date date = monthFormatter.parse(monthString);
		return date.getTime();
	}

	/**
	 * Get a milliseconds from epoch time representation given a time format of YYYY
	 * 
	 * @param timeString
	 * @throws ParseException
	 */
	public static long getYearStringInMillis(String yearString) throws ParseException {
		Date date = yearFormatter.parse(yearString);
		return date.getTime();
	}

	/**
	 * Get a milliseconds from epoch time representation given a time format of EEE,
	 * d MMM yyyy HH:mm:ss Z
	 * 
	 * @param timeString
	 * @throws ParseException
	 */

	public static long getRfc2822StringInMillis(String timeString) throws ParseException {
		Date date = rfc2822TimeFormatter.parse(timeString);
		return date.getTime();
	}

	/**
	 * Get a milliseconds from epoch time representation given a time format of
	 * ddMMyyyyhhmmss
	 * 
	 * @param timeString
	 * @throws ParseException
	 */

	public static long getFileTimestampInMillis(String timeString) throws ParseException {
		Date date = fileTimestampFormatter.parse(timeString);
		return date.getTime();
	}

	/**
	 * Get a milliseconds from epoch time representation given a time format of
	 * yyyy-MM-dd'T'HH:mm:ssX, eg: 2015-10-02T07:01:49Z
	 * 
	 * @param timeString
	 * @throws ParseException
	 */
	public static long getIso8601Utc(String timeString) throws ParseException {
		Date date = iso8601UtcTimeFormatter.parse(timeString);
		return date.getTime();
	}

	/**
	 * Get a milliseconds from epoch time representation given a time format of
	 * yyyy-MM-dd, eg: 2015-10-02
	 * 
	 * @param timeString
	 * @throws ParseException
	 */
	public static long getIso8601Day(String timeString) throws ParseException {
		Date date = iso8601UtcDayFormatter.parse(timeString);
		return date.getTime();
	}

	/**
	 * Get a milliseconds from epoch time representation given a time format of
	 * yyyy-MM-dd HH:mm:ss, eg: 2015-10-02 07:01:49
	 * 
	 * @param timeString
	 * @throws ParseException
	 */
	public static long getIso8601FullTimeFormat(String timeString) throws ParseException {
		DateFormat iso8601FullTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Date date = iso8601FullTimeFormatter.parse(timeString);
		
		return date.getTime();
	}

	/**
	 * Get a pretty print of "YYYY-MM" format
	 */
	public static String printCurrentTimeInIso8601MonthTimeFormatter() {
		return iso8601MonthTimeFormatter.format(new Date());
	}
	
	/**
	 * Get a pretty print of "yyyy-MM-dd'T'HH:mm:ssZ" format
         */
	public static String printCurrentTimeInIso8601UtcTimeFormatter() {
		return iso8601UtcTimeFormatter.format(new Date());
	}

	/**
	 * Get a pretty print of current time in dd-MM-yyyy hh:mm:ss.SSS eg.:
	 * "14-08-2014 8:02:53.000"
	 */
	public static String printCurrentTimeInFullFormat() {
		return fullTimeFormatter.format(new Date());
	}

	/**
	 * Get a pretty print of current time in DDMMYYYYHHMISSSSS eg.:
	 * "15092014080253000"
	 */
	public static String printCurrentTimeInBareFullFormat() {
		return bareFullTimeFormatter.format(new Date());
	}

	/**
	 * Get a pretty print of current time in dd-MM-yyyy hh:mm:ss eg.: "14-08-2014
	 * 8:02"
	 */
	public static String printCurrentTimeInMinuteFormat() {
		return minuteTimeFormatter.format(new Date());
	}

	/**
	 * Get a pretty print of current time in yyyyMMdd eg.: "20180719"
	 */
	public static String printCurrentTimeInDateIndexFormat() {
		return dateIndexFormatter.format(new Date());
	}

	/**
	 * Get a pretty print of current time in dd-MM-yyyy hh:mm:ss eg.: "14-08-2014
	 * 8:02:53"
	 */
	public static String printCurrentTimeInSecondFormat() {
		return secondTimeFormatter.format(new Date());
	}

	/**
	 * Get a pretty print of current time in dd-MM-yyyy hh:mm:ss eg.: "14-Aug-2014
	 * 8:02:53"
	 */
	public static String printCurrentTimeInSecondWithMonthNameFormat() {
		return secondTimeWithMonthNameFormatter.format(new Date());
	}

	/**
	 * Get a pretty print of current time in dd-MM-yyyy eg.: "14-08-2014"
	 */
	public static String printCurrentTimeInDayFormat() {
		return dayTimeFormatter.format(new Date());
	}

	/**
	 * Get a pretty print of current time in HHmmssSSS eg.: "105912777"
	 */
	public static String printCurrentTimeInMilisecondFormat() {
		return milisecondFormatter.format(new Date());
	}

	/**
	 * Get a pretty print of current time in MM-yyyy eg.: "08-2014"
	 */
	public static String printCurrentTimeInMonthFormat() {
		return monthTimeFormatter.format(new Date());
	}

	/**
	 * Get a pretty print of specified time in dd-MM-yyyy hh:mm:ss.SSS given a time
	 * eg.: "14-08-2014 8:02:53.000"
	 * 
	 * @param time
	 *            - number of milliseconds (NOT seconds) since epoch time
	 */
	public static String printTimeInFullFormat(long time) {
		return fullTimeFormatter.format(new Date(time));
	}

	/**
	 * Get a pretty print of specified time in DDMMYYHHMISSSSS given a time eg.:
	 * "15092014080253000"
	 * 
	 * @param time
	 *            - number of milliseconds (NOT seconds) since epoch time
	 */
	public static String printTimeInBareFullFormat(long time) {
		return bareFullTimeFormatter.format(new Date(time));
	}

	/**
	 * print of current time in HHMMssSSS
	 */
	public static String printCurrentTimeInBareHourMinuteSecondMilisecondFormat() {
		return (milisecondFormatter.format(getCurrentTimeInMillis()));
	}

	/**
	 * print of current time in HHMMss
	 */
	public static String printCurrentTimeInBareHourMinuteSecondFormat() {
		return (secondFormatter.format(getCurrentTimeInMillis()));
	}

	/**
	 * Get a pretty print of current time in Loki index format
	 */
	public static String printCurrentTimeInLokiIndexFormat() {
		return (lokiIndexFormatter.format(getCurrentTimeInMillis()));
	}

	/**
	 * Get a pretty print of specified time in DD-MM-YYYY HH:MI given a time eg.:
	 * "14-08-2014 8:02"
	 * 
	 * @param time
	 *            - number of milliseconds (NOT seconds) since epoch time
	 */
	public static String printTimeInMinuteFormat(long time) {
		return minuteTimeFormatter.format(new Date(time));
	}

	/**
	 * Get a pretty print of specified time in DD-MM-YYYY HH:MI:SS given a time eg.:
	 * "14-08-2014 8:02:53"
	 * 
	 * @param time
	 *            - number of milliseconds (NOT seconds) since epoch time
	 */
	public static String printTimeInSecondFormat(long time) {
		return secondTimeFormatter.format(new Date(time));
	}

	/**
	 * Get a pretty print of specified time in DD-MM-YYYY HH:MI:SS given a time eg.:
	 * "14-Aug-2014 8:02:53"
	 * 
	 * @param time
	 *            - number of milliseconds (NOT seconds) since epoch time
	 */
	public static String printTimeInSecondWithMonthNameFormat(long time) {
		return secondTimeWithMonthNameFormatter.format(new Date(time));
	}

	/**
	 * Get a pretty print of specified time in DD-MM-YYYY given a time eg.:
	 * "14-08-2014"
	 */
	public static String printTimeInDayFormat(long time) {
		return dayTimeFormatter.format(new Date(time));
	}

	/**
	 * Get a pretty print of specified time in MM-YYYY given a time eg.: "08-2014"
	 */
	public static String printTimeInMonthFormat(long time) {
		return monthTimeFormatter.format(new Date(time));
	}

	/**
	 * Get a pretty print of specified time in YYYY-MM-DD HH:mm:ss given a time eg.:
	 * "2014-08-21 19:20:00"
	 */
	public static String printTimeISO8601FullFormat(long time) {
		return iso8601FullTimeFormatter.format(new Date(time));
	}

	/**
	 * Get a pretty print of specified time in YYYY-MM-DD given a time eg.:
	 * "2014-08-21"
	 */
	public static String printTimeISO8601DayFormat(long time) {
		return iso8601DayTimeFormatter.format(new Date(time));
	}

	/**
	 * Get a pretty print of specified time in YYYY given a time eg.: "2014"
	 */
	public static String printTimeInYearFormat(long time) {
		return yearFormatter.format(new Date(time));
	}

	/**
	 * Get a pretty print of specified time in yyyyMMdd given a time eg.: "20140810"
	 */
	public static String printTimeInDateIndexFormat(long time) {
		return dateIndexFormatter.format(new Date(time));
	}

	/**
	 * Get a milliseconds from epoch time representation with given dateFormat
	 * 
	 * @param timeString
	 * @throws ParseException
	 */
	public static long getDayStringInMillis(String timeString, DateFormat dateFormat) throws ParseException {
		return getDayStringInMillis(timeString, dateFormat, true);
	}

	public static long getDayStringInMillis(String timeString, DateFormat dateFormat, boolean lenient)
			throws ParseException {
		dateFormat.getCalendar().setLenient(lenient);

		Date date = dateFormat.parse(timeString);
		return date.getTime();
	}

	public static int getWeekOfYearFromBaseTime(long timeInMillis) {
		Calendar now = new GregorianCalendar();
		now.setTimeInMillis(timeInMillis);

		return now.get(Calendar.WEEK_OF_YEAR);
	}

	public static long getLokiIndexInMillis(String timeString) throws ParseException {
		Date date = lokiIndexFormatter.parse(timeString);
		return date.getTime();
	}

	public static String printTimeInLokiIndexFormat(long time) {
		return lokiIndexFormatter.format(new Date(time));
	}

	public static long printCurrentTimeInYearMillisecondIndexFormat() {
		return Long.parseLong(yearMilisecondFormatter.format(System.currentTimeMillis()));
	}

	public static long printTimeInYearMillisecondIndexFormat(long millis) {
		return Long.parseLong(yearMilisecondFormatter.format(millis));
	}

	public static String printDayInDayIndexFormat(long time) {
		return dateIndexFormatter.format(new Date(time));
	}

	public static long getYearMilisecondIndexFormatInMilis(String timeString) throws ParseException {
		Date date = yearMilisecondFormatter.parse(timeString);
		return date.getTime();
	}

	/**
	 * Get a list of all dates between a start date and end date End date is
	 * EXCLUSIVE
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws ParseException
	 */
	public static List<String> getDatesBetweenDateRange(String startDate, String endDate) throws ParseException {
		List<String> dates = new ArrayList<String>();

		Calendar lastDate = Calendar.getInstance();
		lastDate.setTimeInMillis(getDayStringInMillis(endDate));

		Calendar fromDate = Calendar.getInstance();
		fromDate.setTimeInMillis(getDayStringInMillis(startDate));

		while (fromDate.getTime().before(lastDate.getTime())) {
			dates.add(printTimeInDayFormat(fromDate.getTimeInMillis()));
			fromDate.add(Calendar.DATE, 1);
		}

		return dates;
	}

	public static List<String> getDatesBetweenDateRangeNotIncluseFuture(String startDate, String endDate)
			throws ParseException {
		List<String> dates = new ArrayList<String>();

		Calendar lastDate = Calendar.getInstance();
		lastDate.setTimeInMillis(getDayStringInMillis(endDate));

		Calendar today = Calendar.getInstance();

		if (lastDate.after(today)) {
			lastDate = today;
		}

		Calendar fromDate = Calendar.getInstance();
		fromDate.setTimeInMillis(getDayStringInMillis(startDate));

		while (fromDate.getTime().before(lastDate.getTime())) {
			dates.add(printTimeInDayFormat(fromDate.getTimeInMillis()));
			fromDate.add(Calendar.DATE, 1);
		}

		return dates;
	}

	public static boolean isFutureDate(String endDate) throws ParseException {

		Calendar lastDate = Calendar.getInstance();
		lastDate.setTimeInMillis(getDayStringInMillis(endDate));

		Calendar today = Calendar.getInstance();

		if (lastDate.after(today)) {
			return true;
		}
		return false;
	}

	/**
	 * Get a list of all dates between a start date and end date End date is
	 * EXCLUSIVE
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws ParseException
	 */
	public static List<String> getDatesBetweenDateRange(int diffDays) throws ParseException {
		String nowDate = printCurrentTimeInDayFormat();
		String otherDate = printTimeInDayFormat(getMidnightTimeInMillis(diffDays));

		return (diffDays < 0) ? getDatesBetweenDateRange(otherDate, nowDate)
				: getDatesBetweenDateRange(nowDate, otherDate);
	}

	public static long getLastDayOfMonthInMillisFromBaseTime(long timeInMillis) {
		Calendar now = new GregorianCalendar();
		now.setTimeInMillis(timeInMillis);

		now.set(Calendar.DAY_OF_MONTH, now.getActualMaximum(Calendar.DAY_OF_MONTH));

		return now.getTimeInMillis();
	}

	public static String getInputStartEndDate(String startDate, String endDate, String dateFormat) {
		Date start = null;
		Date end = null;
		DateFormat formatter = new SimpleDateFormat(dateFormat);
		String output = "";
		if ((startDate == null) || (endDate == null)) {
			end = new Date();
			start = addDatesWithNDays(end, -DAY_OF_WEEK_IN_STRING);
		} else {
			try {
				end = formatter.parse(endDate);
			} catch (ParseException e1) {
				end = new Date();
			}
			try {
				start = formatter.parse(startDate);
			} catch (ParseException e1) {
				start = addDatesWithNDays(end, -DAY_OF_WEEK_IN_STRING);
			}

			if (end.before(start)) {
				end = new Date();
				start = addDatesWithNDays(end, -DAY_OF_WEEK_IN_STRING);
			}

			SimpleDateFormat newFormat = new SimpleDateFormat(dateFormat);
			String finalStartDate = newFormat.format(start);
			String finalEndDate = newFormat.format(end);
			output = finalStartDate + "," + finalEndDate;
		}
		return output;

	}

	public static int countDurationInMinute(Timestamp startTime, Timestamp endTime) {

		return (int) (countDurationInSecond(startTime, endTime) / 60.0);

	}

	public static String countDurationInHourMinuteSecond(Timestamp timestamp, Timestamp timestamp2) {

		// create a calendar and assign it the same time
		Calendar cal = Calendar.getInstance();

		// add a bunch of seconds to the calendar
		cal.add(Calendar.SECOND, 98765);

		// get time difference in seconds
		long milliseconds = timestamp2.getTime() - timestamp.getTime();
		int seconds = (int) milliseconds / 1000;

		// calculate hours minutes and seconds
		int hours = seconds / 3600;
		int minutes = (seconds % 3600) / 60;
		seconds = (seconds % 3600) % 60;

		String hoursSuffix = "";
		String minutesSuffix = "";
		String secondsSuffix = "";

		if (hours == 1) {
			hoursSuffix = " hour, ";
		} else {
			hoursSuffix = " hours, ";
		}

		if (minutes == 1) {
			minutesSuffix = " minute, ";
		} else {
			minutesSuffix = " minutes, ";
		}

		if (seconds == 1) {
			secondsSuffix = " second";
		} else {
			secondsSuffix = " seconds";
		}

		String result = hours + hoursSuffix + minutes + minutesSuffix + seconds + secondsSuffix;

		return result;
	}

	public static String convertMinutesInHourMinute(long minutes) {
		int seconds = (int) minutes * 60;

		// calculate hours and minutes
		int convertedHours = seconds / 3600;
		int convertedMinutes = (seconds % 3600) / 60;

		String hoursSuffix = "";
		String minutesSuffix = "";

		if (convertedHours == 1) {
			hoursSuffix = " hour ";
		} else {
			hoursSuffix = " hours ";
		}

		if (convertedMinutes == 1) {
			minutesSuffix = " minute";
		} else {
			minutesSuffix = " minutes";
		}

		String result = convertedHours + hoursSuffix + convertedMinutes + minutesSuffix;

		if (convertedHours == 0) {
			result = convertedMinutes + minutesSuffix;
		}

		if (convertedMinutes == 0) {
			result = convertedHours + hoursSuffix;
		}

		return result;
	}

	public static String countDurationInHourMinute(Timestamp timestamp, Timestamp timestamp2) {

		// create a calendar and assign it the same time
		Calendar cal = Calendar.getInstance();

		// add a bunch of seconds to the calendar
		cal.add(Calendar.SECOND, 98765);

		// get time difference in seconds
		long milliseconds = timestamp2.getTime() - timestamp.getTime();
		int seconds = (int) milliseconds / 1000;

		// calculate hours minutes and seconds
		int hours = seconds / 3600;
		int minutes = (seconds % 3600) / 60;
		seconds = (seconds % 3600) % 60;
		if (seconds > 30) {
			minutes += 1;
			if (minutes >= 60) {
				minutes = minutes % 60;
				hours += 1;
			}
		}

		String hoursSuffix = "";
		String minutesSuffix = "";

		if (hours == 1) {
			hoursSuffix = " hour, ";
		} else {
			hoursSuffix = " hours, ";
		}

		if (minutes == 1) {
			minutesSuffix = " minute";
		} else {
			minutesSuffix = " minutes";
		}

		String result = hours + hoursSuffix + minutes + minutesSuffix;

		return result;
	}

	public static int countDurationInMinuteCeiling(Timestamp timestamp, Timestamp timestamp2) {

		// HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String dateStart = String.valueOf(timestamp);
		String dateStop = String.valueOf(timestamp2);

		Date d1 = null;
		Date d2 = null;

		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);

			// in milliseconds
			long diff = d2.getTime() - d1.getTime();

			double diffMinutes = diff / 1000.0 / 60.0;

			int result = NumberHelper.roundToInteger(Math.ceil(diffMinutes));
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public static int countDurationInSecond(Timestamp startTime, Timestamp endTime) {

		// create a calendar and assign it the same time
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(startTime.getTime());

		// add a bunch of seconds to the calendar
		cal.add(Calendar.SECOND, 98765);

		// get time difference in seconds
		long milliseconds = endTime.getTime() - startTime.getTime();
		
		return (int)milliseconds / 1000;
		
	}

	public static boolean isTheDateAfterToday(String dateStr) {
		boolean isAfterToday = true;
		Date date = null;
		Date today = new Date();
		try {
			date = dayTimeFormatter.parse(dateStr);
		} catch (ParseException e) {
			date = new Date();
		}

		if (today.after(date)) {
			isAfterToday = false;
		}
		return isAfterToday;
	}

	/**
	 * Return true if given date is equal or after today
	 * 
	 * @param dateStr
	 * @param dateTimeFormatter
	 * @return
	 */
	public static boolean isTheDateEqualOrAfterToday(String dateStr, DateFormat dateTimeFormatter) {

		String todayStr = dateTimeFormatter.format(new Date());
		Date date = null;
		Date today = null;

		try {
			today = dateTimeFormatter.parse(todayStr);
			date = dateTimeFormatter.parse(dateStr);
		} catch (ParseException e) {
			date = new Date();
		}

		return (date.compareTo(today) >= 0);
	}

	public static boolean isTheDateBeforeToday(Timestamp time) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time.getTime());

		Calendar temp = Calendar.getInstance();
		Calendar today = (Calendar) cal.clone();
		today.set(temp.get(Calendar.YEAR), temp.get(Calendar.MONTH), temp.get(Calendar.DATE));
		return cal.before(today);
	}

	public static Timestamp addDatesWithNDays(Timestamp timestamp, int numberDays) {

		Date date = new Date(timestamp.getTime());

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, numberDays);

		return new Timestamp(calendar.getTimeInMillis());
	}

	public static Date addDatesWithNDays(Date date, int numberDays) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, numberDays);
		return calendar.getTime();
	}

	public static Date getNMonthsAgoFromCurrentDate(int numberMonths) {
		Calendar calender = Calendar.getInstance();
		calender.add(Calendar.MONTH, -numberMonths);
		return calender.getTime();
	}

	public static String getNMonthsAgoFromCurrentDateWithDateIndexFormat(int numberMonths) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -numberMonths);

		return convertDateToString(new SimpleDateFormat("yyyyMMdd"), calendar.getTime());
	}

	public static String getNMonthsAgoFromCurrentDateWithDateIndexHourFormat(int numberMonths) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -numberMonths);

		return convertDateToString(new SimpleDateFormat("yyyyMMddHHmmss"), calendar.getTime());
	}

	public static String getNMonthsAgoFromCurrentDateWithSpecificFormat(int numberMonths, String dateFormat) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -numberMonths);

		return convertDateToString(new SimpleDateFormat(dateFormat), calendar.getTime());
	}
	
	public static String getNMonthsAgo(String dateStr, int numberMonths, AdvoticsDateFormat currentDateFormat,
			AdvoticsDateFormat targetDateFormat) throws ParseException {
		
		SimpleDateFormat sdf = new SimpleDateFormat(currentDateFormat.getDateFormat());
		Date date = sdf.parse(dateStr);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -numberMonths);

		return convertDateToString(new SimpleDateFormat(targetDateFormat.getDateFormat()), calendar.getTime());
	}

	public static Date addDatesWithNMonths(Date date, int numberMonths) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, numberMonths);
		return calendar.getTime();
	}

	public static String generateDefaultRangeDate(String typeDailyOrMonthly, int differences, String dateFormat) {
		SimpleDateFormat newFormat = new SimpleDateFormat(dateFormat);

		Date end = new Date();
		Date start = new Date();

		if (typeDailyOrMonthly.equalsIgnoreCase(DAILY)) {
			start = addDatesWithNDays(end, -differences);
		} else {
			start = addDatesWithNMonths(end, -differences);
		}

		String startDate = newFormat.format(start);
		String endDate = newFormat.format(end);
		return startDate + "," + endDate;

	}

	public static boolean isDateValid(String dateToValidate, String dateFormat) {

		if (dateToValidate == null) {
			return false;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		sdf.setLenient(false);

		try {
			// if not valid, it will throw ParseException
			sdf.parse(dateToValidate);

		} catch (ParseException e) {
			return false;
		}

		return true;
	}

	public static boolean isDateValid(String dateToValidate, SimpleDateFormat sdf) {

		if (dateToValidate == null) {
			return false;
		}

		sdf.setLenient(false);

		try {
			// if not valid, it will throw ParseException
			sdf.parse(dateToValidate);

		} catch (ParseException e) {
			return false;
		}

		return true;
	}

	public static boolean isDateTimeValid(String dateToValidate, String dateFormat) {

		if (dateToValidate == null) {
			return false;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

		try {
			// if not valid, it will throw ParseException
			sdf.parse(dateToValidate);

		} catch (ParseException e) {
			return false;
		}

		return true;
	}

	public static boolean checkTodayInRangeDates(String startDate, String endDate) {

		Date start = null;
		try {
			start = dayTimeFormatter.parse(startDate);
		} catch (ParseException e) {
		}
		Date end = null;
		try {
			end = dayTimeFormatter.parse(endDate);
		} catch (ParseException e) {
		}
		Date date = new Date();
		String dateStr = formatterDate.format(date);
		try {
			date = dayTimeFormatter.parse(dateStr);
		} catch (ParseException e) {
		}
		boolean inRange = false;
		if ((start != null) && (end != null)) {
			if ((date.before(end)) && (date.after(start))) {
				inRange = true;
			}

			if ((date.equals(start)) || (date.equals(end))) {
				inRange = true;
			}
		}

		return inRange;
	}

	public static boolean checkTodayInRangeDatesWithTimeFormat(String startDate, String endDate) {

		Date start = null;
		try {
			start = secondTimeFormatter.parse(startDate);
		} catch (ParseException e) {
		}
		Date end = null;
		try {
			end = secondTimeFormatter.parse(endDate);
		} catch (ParseException e) {
		}
		Date date = new Date();
		String dateStr = formatterDateTime.format(date);
		try {
			date = secondTimeFormatter.parse(dateStr);
		} catch (ParseException e) {
		}
		boolean inRange = false;
		if ((start != null) && (end != null)) {
			if ((date.before(end)) && (date.after(start))) {
				inRange = true;
			}

			if ((date.equals(start)) || (date.equals(end))) {
				inRange = true;
			}
		}

		return inRange;
	}

	public static boolean checkDateInRangeDates(String startDate, String endDate, String checkDate) {

		Date start = null;
		try {
			start = dayTimeFormatter.parse(startDate);
		} catch (ParseException e) {
		}
		Date end = null;
		try {
			end = dayTimeFormatter.parse(endDate);
		} catch (ParseException e) {
		}
		Date date = null;
		try {
			date = dayTimeFormatter.parse(checkDate);
		} catch (ParseException e) {
		}
		boolean inRange = false;
		if ((start != null) && (end != null)) {
			if ((date.before(end)) && (date.after(start))) {
				inRange = true;
			}

			if ((date.equals(start)) || (date.equals(end))) {
				inRange = true;
			}
		}

		return inRange;
	}

	public static String convertDateToString(AdvoticsDateFormat targetDateFormat, Date date) {

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(targetDateFormat.getDateFormat());
		String dateString = null;
		try {
			dateString = simpleDateFormat.format(date);
		} catch (Exception ex) {
			Date today = new Date();
			dateString = simpleDateFormat.format(today);
		}
		return dateString;

	}

	public static String convertDateToString(DateFormat dateFormat, Date date) {

		String dateString = null;
		try {
			dateString = dateFormat.format(date);
		} catch (Exception ex) {
			Date today = new Date();
			dateString = dateFormat.format(today);
		}
		return dateString;

	}

	public static Date convertDateStringToDate(AdvoticsDateFormat currentDateFormat, String dateStr) {

		Date date = null;
		try {
			date = parseDateWithAdvoticsDateFormat(currentDateFormat, dateStr);
		} catch (ParseException e) {
			date = new Date();
		}

		return date;

	}

	private static Date parseDateWithAdvoticsDateFormat(AdvoticsDateFormat dateFormat, String date)
			throws ParseException {
		return new SimpleDateFormat(dateFormat.getDateFormat()).parse(date);
	}

	public static Date convertDateStringToDate(DateFormat dateFormat, String dateStr) {
		Date date = null;
		try {
			date = dateFormat.parse(dateStr);
		} catch (ParseException e) {
			date = new Date();
		}

		return date;

	}

	public static Date addDayWithNMonths(Date date, int months) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(Calendar.MONTH, months);

		return cal.getTime();
	}

	public static Date addDayWithNDays(Date date, int days) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);

		return cal.getTime();
	}

	/**
	 * Truncate YYYYMMDDHHMISS to YYYYMMDD
	 * 
	 * @param lokiIndex
	 * @return
	 */
	public static int translateLokiIndexToDateIndex(long lokiIndex) {
		String lokiString = String.valueOf(lokiIndex);
		lokiString = (lokiString.length() >= LOKI_INDEX_LENGTH) ? lokiString.substring(ZERO_LENGTH, LOKI_INDEX_LENGTH)
				: lokiString;

		return Integer.parseInt(lokiString);
	}

	public static String translateDayTimeToDateIndex(String dayTime) {
		Date date = convertDateStringToDate(new SimpleDateFormat("dd-MM-yyyy"), dayTime);

		return convertDateToString(new SimpleDateFormat("yyyyMMdd"), date);
	}

	public static String translateDateTimeToDateTimeIndex(String dayTime) {
		Date date = convertDateStringToDate(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"), dayTime);

		return convertDateToString(new SimpleDateFormat("yyyyMMddHHmmss"), date);
	}
	
	/**
	 * Convert date time index yyyyMMddhhmmss into date time with format yyyy-MM-dd hh:mm:ss
	 * 
	 * @param String
	 *            yyyyMMddHHmmss
	 * @return int yyyy-MM-dd HH:mm:ss
	 **/
	public static String translateDateTimeIndexToDateTime(String dateTimeIndex) {
		Date date = convertDateStringToDate(new SimpleDateFormat("yyyyMMddHHmmss"), dateTimeIndex);
		
		return convertDateToString(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), date);
	}

	public static String translateDateTimeToDateTimeIndexWithStartHour(String dayTime) {
		Date date = convertDateStringToDate(new SimpleDateFormat("dd-MM-yyyy"), dayTime);

		String newDateFormat = convertDateToString(new SimpleDateFormat("yyyy-MM-dd"), date).concat(" 00:00:00");

		Date dateWithTime = convertDateStringToDate(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"), newDateFormat);

		return convertDateToString(new SimpleDateFormat("yyyyMMddHHmmss"), dateWithTime);
	}

	public static String translateDateTimeToDateTimeIndexWithEndHour(String dayTime) {
		Date date = convertDateStringToDate(new SimpleDateFormat("dd-MM-yyyy"), dayTime);

		String newDateFormat = convertDateToString(new SimpleDateFormat("yyyy-MM-dd"), date).concat(" 23:59:59");

		Date dateWithTime = convertDateStringToDate(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"), newDateFormat);

		return convertDateToString(new SimpleDateFormat("yyyyMMddHHmmss"), dateWithTime);
	}

	public static Timestamp getCurrentTimeStamp() {
		Calendar calendar = Calendar.getInstance();
		
		Date now = calendar.getTime();

		Timestamp currentTimestamp = new Timestamp(now.getTime());
		
		return currentTimestamp;
	}
	
	public static Timestamp getCurrentTimeStampByTimeZone(String zoneId) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone(zoneId));
		
		Date now = calendar.getTime();

		Timestamp currentTimestamp = new Timestamp(now.getTime());
		
		return currentTimestamp;
	}

	/**
	 * Function to get date string from current date with parameter N months and the
	 * desired date format.
	 * 
	 * @param numberMonths
	 * @param dateFormat
	 * @param isEnd
	 * @return
	 */
	public static String getNMonthsAgoFromCurrentDateWithSpecificFormat(int numberMonths, String dateFormat,
			boolean isEnd) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -numberMonths);

		if (isEnd) {
			return convertDateToString(new SimpleDateFormat(dateFormat), calendar.getTime()).concat(" 23:59:59");
		}

		return convertDateToString(new SimpleDateFormat(dateFormat), calendar.getTime()).concat(" 00:00:00");
	}

	/**
	 * Translate date string to datetime string with specified source date format
	 * and desired date format. The source date will be concatenated with either
	 * "23:59:59" or "00:00:00" according to the value of isEnd.
	 * 
	 * @param dateString
	 * @param sourceDateFormat
	 * @param specifiedDateFormat
	 * @param isEnd
	 * @return
	 */
	public static String translateDateToSpecifiedFormatWithHour(String dateString, String sourceDateFormat,
			String specifiedDateFormat, boolean isEnd) {
		Date date = convertDateStringToDate(new SimpleDateFormat(sourceDateFormat), dateString);

		String newDateFormat = "";
		if (isEnd) {
			newDateFormat = convertDateToString(new SimpleDateFormat(specifiedDateFormat), date).concat(" 23:59:59");
		} else {
			newDateFormat = convertDateToString(new SimpleDateFormat(specifiedDateFormat), date).concat(" 00:00:00");
		}

		return newDateFormat;
	}

	/**
	 * Convert date string of dd-MM-yyyy into dateIndex of integer with format
	 * yyyyMMdd
	 * 
	 * @param String
	 *            dd-MM-yyyy
	 * @return int yyyyMMdd
	 **/
	public static int convertDateStringIntoDateIndex(String dateString) {
		Date date = null;
		try {
			date = dayTimeFormatter.parse(dateString);
		} catch (ParseException e1) {
			date = new Date();
		}
	
		return Integer.valueOf(iso8601DayTimeFormatter.format(date).replace("-", ""));
	}

	public static int getCurrentDateInt() {
		Date date = new Date();
		return Integer.valueOf(iso8601DayTimeFormatter.format(date).replace("-", ""));
	}

	/**
	 * Convert month string of MM-yyyy into monthIndex of integer with format yyyyMM
	 * 
	 * @param String
	 *            MM-yyyy
	 * @return int yyyyMM
	 **/
	public static int convertMonthStringIntoMonthIndex(String monthString) {
		Date date = null;
		try {
			date = monthFormatter.parse(monthString);
		} catch (ParseException e1) {
			date = new Date();
		}

		return Integer.valueOf(monthYearFormatter.format(date));
	}

	/**
	 * Convert date integer of yyyyMMdd into monthyear of integer with format yyyyMM
	 * 
	 * @param int
	 *            yyyyMMdd
	 * @return int yyyyMM
	 **/
	public static int convertDateIndexIntoMonthIndex(int dateInt) {

		final DateFormat dateIndexFormatter = new SimpleDateFormat("yyyyMMdd");
		final DateFormat monthYearFormatter = new SimpleDateFormat("yyyyMM");

		Date date = null;
		try {
			date = dateIndexFormatter.parse(String.valueOf(dateInt));
		} catch (ParseException e1) {
			date = new Date();
		}

		return Integer.valueOf(monthYearFormatter.format(date));
	}

	/**
	 * Convert Timestamp into dateIndex of integer with format yyyyMMdd
	 * 
	 * @param Timestamp
	 *            yyyy-MM-dd hh:mm:ss
	 * @return int yyyyMMdd
	 **/
	public static int convertStringTimestampIntoDateIndex(String timestamp) {
		Date date = null;
		try {
			DateFormat iso8601FullTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			date = iso8601FullTimeFormatter.parse(timestamp);
		} catch (ParseException e1) {
			date = new Date();
		}

		return Integer.valueOf(iso8601DayTimeFormatter.format(date).replace("-", ""));
	}

	/**
	 * Convert date index of yyyyMMdd into String date with format MM-dd-yyyy
	 * 
	 * @param int
	 *            yyyyMMdd
	 * @return String MM-dd-yyyy
	 **/
	public static String convertDateIndexIntoDateString(int dateInt) {
		DateFormat dateIndexFormatter = new SimpleDateFormat("yyyyMMdd");
		DateFormat monthDateYearFormatter = new SimpleDateFormat("MM-dd-yyyy");
		Date date = null;
		try {
			date = dateIndexFormatter.parse(String.valueOf(dateInt));
		} catch (ParseException e1) {
			date = new Date();
		}

		return String.valueOf(monthDateYearFormatter.format(date));
	}

	/**
	 * Convert Timestamp of dd-mm-yyyy hh:mm:ss into String date with format
	 * MM-dd-yyyy
	 * 
	 * @param Timestamp
	 *            dd-mm-yyyy hh:mm:ss
	 * @return String MM-dd-yyyy
	 **/
	public static String convertTimestampIntoDateStringMonthDateYear(Timestamp dateTimestamp) {
		Date date = null;
		try {
			date = iso8601FullTimeFormatter.parse(String.valueOf(dateTimestamp));
		} catch (ParseException e1) {
			date = new Date();
		}

		return String.valueOf(monthDateYearFormatter.format(date));
	}
	
	/**
	 * Convert Timestamp of dd-mm-yyyy hh:mm:ss into String date with format
	 * MM-dd-yyyy
	 * 
	 * @param Timestamp
	 *            dd-mm-yyyy hh:mm:ss
	 * @return String MM-dd-yyyy
	 **/
	public static String convertTimestampIntoDateStringDateMonthYear(Timestamp dateTimestamp) {
		Date date = null;
		try {
			date = iso8601FullTimeFormatter.parse(String.valueOf(dateTimestamp));
		} catch (ParseException e1) {
			date = new Date();
		}

		return String.valueOf(dayTimeFormatter.format(date));
	}


	/**
	 * Convert date index of yyyyMMdd into String date with format dd-MM-yyyy
	 * 
	 * @param int
	 *            yyyyMMdd
	 * @return String dd-MM-yyyy
	 **/
	public static String convertDateIndexIntoMonthDateYearDateString(int dateInt) {
		Date date = null;
		try {
			date = dateIndexFormatter.parse(String.valueOf(dateInt));
		} catch (ParseException e1) {
			date = new Date();
		}

		return String.valueOf(monthDateYearFormatter.format(date));
	}

	public static String convertDateIndexYearMonthDayString(int dateInt) {
		Date date = null;
		try {
			date = dateIndexFormatter.parse(String.valueOf(dateInt));
		} catch (ParseException e1) {
			date = new Date();
		}

		return String.valueOf(iso8601UtcDayFormatter.format(date));
	}

	/**
	 * Convert date index of yyyyMMdd into String date with format dd-MM-yyyy
	 * 
	 * @param int
	 *            yyyyMMdd
	 * @return String dd-MM-yyyy
	 **/
	public static String convertDateIndexIntoDateMonthYearDateString(int dateInt) {
		Date date = null;
		try {
			date = dateIndexFormatter.parse(String.valueOf(dateInt));
		} catch (ParseException e1) {
			date = new Date();
		}

		return String.valueOf(dayTimeFormatter.format(date));
	}

	/**
	 * Convert date string of "dd-MM-yyyy" into date string of "MM-dd-yyyy"
	 * 
	 * @param String
	 *            "dd-MM-yyyy"
	 * @return String "MM-dd-yyyy"
	 **/
	public static String convertDateMonthYearStringIntoMonthDateYearString(String dateString) {
		Date date = null;
		DateFormat dayTimeFormatter = new SimpleDateFormat("dd-MM-yyyy");
		try {
			date = dayTimeFormatter.parse(dateString);
		} catch (ParseException e) {
			date = new Date();
		}
		DateFormat monthDateYearFormatter = new SimpleDateFormat("MM-dd-yyyy");
		return monthDateYearFormatter.format(date);
	}
	
	/**
	 * Convert date string of "dd-MM-yyyy" into date string of "yyyy-MM-dd"
	 * 
	 * @param String
	 *            "dd-MM-yyyy"
	 * @return String "yyyy-MM-dd"
	 **/
	public static String convertDateMonthYearStringIntoYearMonthDateString(String dateString) {
		Date date = null;
		DateFormat dayTimeFormatter = new SimpleDateFormat("dd-MM-yyyy");
		try {
			date = dayTimeFormatter.parse(dateString);
		} catch (ParseException e) {
			date = new Date();
		}
		DateFormat monthDateYearFormatter = new SimpleDateFormat("yyyy-MM-dd");
		return monthDateYearFormatter.format(date);
	}

	/**
	 * Convert date string of "dd-MM-yyyy" into date string of "dd-MMM-yyyy"
	 * 
	 * @param String
	 *            "dd-MM-yyyy"
	 * @return String "dd-MMM-yyy"
	 **/
	public static String convertDateMonthYearStringIntoDateMonthNameYearString(String dateString) {
		Date date = null;
		try {
			date = dayTimeFormatter.parse(dateString);
		} catch (ParseException e) {
			date = new Date();
		}
		return dayMonthNameFormatter.format(date);
	}

	/**
	 * Get inclusively date difference between two event dates with format
	 * dd-MM-yyyy
	 * 
	 * @return int inclusiveDayDifferences, -1 if fail in parsing the dateString
	 *         (bad format)
	 **/
	public static int getInclusiveDayDifferenceBetweenTwoEventDates(String startDateStr, String endDateStr) {
		Date startDate;
		Date endDate;
		try {
			startDate = dayTimeFormatter.parse(startDateStr);
			endDate = dayTimeFormatter.parse(endDateStr);
		} catch (ParseException e) {
			return -1;
		}
		long dayDiff = Math.abs(TimeUnit.MILLISECONDS.toDays(startDate.getTime() - endDate.getTime()));
		int inclusiveDayDiff = (int) (dayDiff + 1);

		return inclusiveDayDiff;
	}
	
	/**
	 * Get exclusively date difference between two event dates with format
	 * dd-MM-yyyy HH:mm:ss
	 * 
	 * @return int inclusiveDayDifferences, -1 if fail in parsing the dateString
	 *         (bad format)
	 **/
	public static int getExclusiveDayDifferenceBetweenTwoEventDates
		(String startDateStr, String endDateStr) {
		DateFormat secondTimeFormatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date startDate;
		Date endDate;
		try {
			startDate = secondTimeFormatter.parse(startDateStr);
			endDate = secondTimeFormatter.parse(endDateStr);
		} catch (ParseException e) {
			return -1;
		}
		long dayDiff = Math.abs(TimeUnit.MILLISECONDS.toDays(startDate.getTime() - endDate.getTime()));
		int exclusiveDayDiff = (int) (dayDiff);

		return exclusiveDayDiff;
	}

	/**
	 * Convert date format from dd-mm-yyyy into dd/mm/yyyy
	 * 
	 * @param date
	 * @return
	 */
	public static String convertDateFormatPeriod(String date) {
		String[] parts = date.split("-");

		String dateStr = "";

		if (parts.length == 3) {
			String dd = parts[0];
			String mm = parts[1];
			String yyyy = parts[2];
			dateStr = dd + "/" + mm + "/" + yyyy;
		}

		return dateStr;

	}

	/**
	 * Convert timestamp into string date with format
	 */
	public static String convertTimestampToDateStringWithSpecifyFormat(Timestamp time, String dateFormat) {
		SimpleDateFormat strFormat = new SimpleDateFormat(dateFormat);

		Date date = new Date(time.getTime());

		String dateString = strFormat.format(date);

		return dateString;

	}

	/***
	 * 
	 * @param time
	 * @param
	 * @return String date that already formatted with param advoticsDateFormat
	 */
	public static String format(long time, AdvoticsDateFormat advoticsDateFormat) {
		return new SimpleDateFormat(advoticsDateFormat.getDateFormat()).format(new Date(time));
	}

	/***
	 * 
	 * @param date
	 * @param
	 * @return String date that already formatted with param advoticsDateFormat
	 */
	public static String format(String date, AdvoticsDateFormat advoticsDateFormat) {
		return new SimpleDateFormat(advoticsDateFormat.getDateFormat()).format(new Date(date));
	}

	public static String format(String date, AdvoticsDateFormat sourceDateFormat, AdvoticsDateFormat targetDateFormat) {
		Date currentDate = convertDateStringToDate(sourceDateFormat, date);
		return convertDateToString(targetDateFormat, currentDate);
	}
	
	
	public static Timestamp convertToTimestamp(String date, AdvoticsDateFormat targetDateFormat) {
		Date currentDate = convertDateStringToDate(targetDateFormat, date);
		return new Timestamp(currentDate.getTime());
	}

	/***
	 * 
	 * @param dateString
	 * @param advoticsDateFormat
	 * @return
	 * @throws ParseException
	 */
	public static Date getDate(String dateString, AdvoticsDateFormat advoticsDateFormat) throws ParseException {
		return new SimpleDateFormat(advoticsDateFormat.getDateFormat()).parse(dateString);
	}

	public static int getDateIndex(String date, AdvoticsDateFormat currentDateFormat) {
		String dateIndex = DateTimeHelper.format(date, currentDateFormat, AdvoticsDateFormat.DATE_INDEX_FORMAT);
		return Integer.valueOf(dateIndex);
	}

	/**
	 * Convert timestamp into string date with formatDate and Locale
	 */
	public static String convertTimestampToDateStringWithSpecifyFormatAndLocale(Timestamp time, String dateFormat,
			Locale locale) {

		SimpleDateFormat strFormat = new SimpleDateFormat(dateFormat, locale);

		Date date = new Date(time.getTime());

		String dateString = strFormat.format(date);

		return dateString;

	}

	public static Timestamp convertDateStringToTimestamp(String date, String inputDateFormat) {
		Timestamp time = null;
		SimpleDateFormat strFormat = new SimpleDateFormat(inputDateFormat);
		SimpleDateFormat timestampFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date d;
		try {
			d = strFormat.parse(date);
			time = Timestamp.valueOf(timestampFormat.format(d));
		} catch (Exception e) {
			return null;
		}
		return time;
	}

	public static String printDayFormatDateIndex(int dateIndex) {
		String date = String.valueOf(dateIndex);
		try {
			return iso8601UtcDayFormatter.format(dateIndexFormatter.parse(date));
		} catch (ParseException e) {
			return "";
		}
	}

	/**
	 * Convert date format from int yyyMMdd into dd[limiter]mm[limiter]yyyy
	 * 
	 * @param date
	 *            , limiter
	 * @return string
	 */

	public static String convertDateInNumberToDateStringWithSpecifyFormat(int date, String limiter) {

		String dateInString = String.valueOf(date);
		// dateInString.getChars(

		if (dateInString.length() != 8) {
			return "";
		}

		String dd = dateInString.substring(6, 8);
		String mm = dateInString.substring(4, 6);
		String yyyy = dateInString.substring(0, 4);

		return dd + limiter + mm + limiter + yyyy;
	}

	/**
	 * Get a list of all dates between a start date and end date End date is
	 * INCLUSIVE
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws ParseException
	 */
	public static List<String> getDatesBetweenDateRangeInclusive(String startDate, String endDate)
			throws ParseException {
		List<String> dates = new ArrayList<String>();

		Calendar lastDate = Calendar.getInstance();
		lastDate.setTimeInMillis(getDayStringInMillis(endDate));

		Calendar fromDate = Calendar.getInstance();
		fromDate.setTimeInMillis(getDayStringInMillis(startDate));

		while (fromDate.getTime().before(addDatesWithNDays(lastDate.getTime(), 1))) {
			dates.add(printTimeInDayFormat(fromDate.getTimeInMillis()));
			fromDate.add(Calendar.DATE, 1);
		}

		return dates;
	}
	
	/**
	 * Get a list of all dates between a start date and end date End date is
	 * INCLUSIVE
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws ParseException
	 */
	public static List<String> getDatesBetweenDateRangeInclusive(Date startDate, Date endDate)
			throws ParseException {
		List<String> dates = new ArrayList<String>();

		Calendar lastDate = Calendar.getInstance();
		lastDate.setTime(endDate);

		Calendar fromDate = Calendar.getInstance();
		fromDate.setTime(startDate);

		while (fromDate.getTime().before(lastDate.getTime())) {
			dates.add(printTimeInDayFormat(fromDate.getTimeInMillis()));
			fromDate.add(Calendar.DATE, 1);
		}

		return dates;
	}

	/**
	 * Get DayOfWeek from date (String dd-MM-yyyy) input WITH MONDAY = 1
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws ParseException
	 */

	public static int getDayOfWeekStartFromMonday(String date) {

		Calendar cal = Calendar.getInstance();

		try {
			cal.setTimeInMillis(getDayStringInMillis(date));
		} catch (Exception e) {
			return 0;
		}

		if (cal.get(Calendar.DAY_OF_WEEK) == 1) {
			return 7; // IF the day is SUNDAY
		} else {
			return (cal.get(Calendar.DAY_OF_WEEK)) - 1;
		}
	}

	/**
	 * Get WeekOfMonth from date (integer) input FOR EACH MONTH Example: 16-02-2018
	 * is on the 7th week of the year. To get the week of month of that day, get the
	 * remainder from WeekOfYear (in this case: 7) by 4, as 1 month has 4 weeks.
	 * Hence, WeekOfMonth = 7 % 4 = 3.
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws ParseException
	 */
	public static int getWeekOfMonthFromJanuaryFirst(String date) {

		Calendar cal = Calendar.getInstance();

		try {
			cal.setTimeInMillis(getDayStringInMillis(date));
		} catch (Exception e) {
			return 0;
		}

		if ((cal.get(Calendar.WEEK_OF_YEAR)) % 4 == 0) {
			return 4;
		} else {
			return (cal.get(Calendar.WEEK_OF_YEAR)) % 4;
		}
	}
	
	/**
	 * Get Week Of Year 
	 * @param date format 25-04-2019
	 * @retun 17
	 */
	public static int getWeekOfYear(String date) {
		
		Calendar cal = Calendar.getInstance();
		
		try {
			cal.setTimeInMillis(getDayStringInMillis(date));
		} catch (Exception e) {
			return 0;
		}

		return cal.get(Calendar.WEEK_OF_YEAR);
	}
	
	/**
	 * Get Day Of Week
	 * @param date format 25-04-2019
	 * @return 5
	 */
	public static int getDayOfWeek(String date) {

		Calendar cal = Calendar.getInstance();

		try {
			cal.setTimeInMillis(getDayStringInMillis(date));
		} catch (Exception e) {
			return 0;
		}

		return cal.get(Calendar.DAY_OF_WEEK);
	}
	
	/**
	 * Convert date format from dd/mm/yyyy into yyyymmdd
	 * 
	 * @param date
	 * @return
	 */
	public static int convertDateFormatPeriodtoInt(String date) {
		String[] parts = date.split("/");

		String dateStr = "";
		String dd = "";
		String mm = "";

		if (parts.length == 3) {
			if (parts[0].length() == 1) {
				dd = "0" + parts[0];
			} else {
				dd = parts[0];
			}
			if (parts[1].length() == 1) {
				mm = "0" + parts[1];
			} else {
				mm = parts[1];
			}

			String yyyy = parts[2];
			dateStr = yyyy + mm + dd;
		}
		return Integer.valueOf(dateStr);
	}

	/**
	 * Convert date format from mm[operator]dd[operator]yyyy into yyyymmdd
	 * 
	 * @param date
	 * @return
	 */
	public static int convertCustomOperatorDateFormatPeriodtoInt(String date, String operator) {
		String[] parts = date.split(operator);

		String dateStr = "";
		String dd = "";
		String mm = "";

		if (parts.length == 3) {
			if (parts[0].length() == 1) {
				mm = "0" + parts[0];
			} else {
				mm = parts[0];
			}
			if (parts[1].length() == 1) {
				dd = "0" + parts[1];
			} else {
				dd = parts[1];
			}
			String yyyy = parts[2];
			dateStr = yyyy + mm + dd;
		}
		return Integer.valueOf(dateStr);
	}

	/**
	 * Convert date format from mm[operator]dd[operator]yyyy into yyyymmdd
	 * 
	 * @param date
	 * @return
	 */
	public static int convertCustomOperatorDateFormatPeriodWithShortYeartoInt(String date, String operator) {
		String[] parts = date.split(operator);

		String dateStr = "";

		if (parts.length == 3) {
			String dd = parts[1];
			String mm = parts[0];
			String yyyy = parts[2];
			dateStr = "20" + yyyy + mm + dd;
		}
		return Integer.valueOf(dateStr);
	}

	/**
	 * Get current date with custom format
	 * 
	 * @param String
	 *            format. Ex. "MM-dd-yyyy" or "dd/MM/yyyy"
	 * @return date in string
	 */
	public static String getCuretDateWithCustomFormat(String format) {
		return new SimpleDateFormat(format).format(new Date());
	}
	
	public static int getCuretDateIndex() {
		AdvoticsDateFormat.DATE_INDEX_FORMAT.getDateFormat();
		return Integer.valueOf(getCuretDateWithCustomFormat(AdvoticsDateFormat.DATE_INDEX_FORMAT.getDateFormat()));
	}

	public static int countOVDFromDueDateSchedule(int dueDateSchedule) {
		int currentDate = DateTimeHelper.convertDateStringIntoDateIndex(DateTimeHelper.printCurrentTimeInDayFormat());

		Integer overDue = 0; // set default 0

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date now = null, due = null;
		try {
			now = sdf.parse(String.valueOf(currentDate));
			due = sdf.parse(String.valueOf(dueDateSchedule));
		} catch (ParseException e) {
			return 0;
		}

		long diffInMillies = Math.abs(now.getTime() - due.getTime());

		overDue = (int) TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

		if (overDue > 0) {
			return overDue;
		} else {
			return 0;
		}

	}

	public static String convertTimestampFormat(String timestamp, String inputDateFormat, String outputDateFormat) {
		SimpleDateFormat input = new SimpleDateFormat(inputDateFormat);
		SimpleDateFormat output = new SimpleDateFormat(outputDateFormat);
		Date d;
		String result = null;
		try {
			d = input.parse(timestamp);
			result = output.format(d);
		} catch (ParseException e) {
			return null;
		}
		return result;
	}

	/**
	 * Get a pretty print of current time in dd-MM-yyyy eg.: "14-08-2014" if offset
	 * negatif, set to 0
	 * 
	 * @param offset
	 *            in milisecond
	 */
	public static String printCurrentTimeInDayFormatMinOffset(Integer offset) {
		Calendar cal = Calendar.getInstance();
		if (offset != null)
			cal.add(Calendar.MILLISECOND, Math.max(0, offset) * -1);
		return dayTimeFormatter.format(cal.getTime());
	}

	/**
	 * Get last day of a month by string date
	 * 
	 * @param String
	 *            date, String format e.g. dd-MM-yyyy, 21-01-2018
	 * @return String of date, IN THE SAME FORMAT AS THE INPUT, consisting the last
	 *         day of the month, e.g. 31-01-2018
	 */
	public static String getLastDayOfMonth(String date, String format) {

		LocalDate convertedDate = LocalDate.parse(date, DateTimeFormatter.ofPattern(format));
		convertedDate = convertedDate.withDayOfMonth(convertedDate.getMonth().length(convertedDate.isLeapYear()));
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		return convertedDate.format(formatter);

	}

	/**
	 * Get all months between month range, and the range is INCLUSIVE
	 * 
	 * @param String
	 *            startMonth, String format e.g. MM-yyyy, 01-2018
	 * @param String
	 *            endMonth, String format e.g. MM-yyyy, 06-2018
	 * @return List of strings, consisting the months between the range (the range
	 *         is included in the list)
	 */
	public static List<String> getMonthsBetweenMonthRangeInclusive(String startMonth, String endMonth) {

		List<String> months = new ArrayList<String>();
		Calendar beginCalendar = Calendar.getInstance();
		Calendar finishCalendar = Calendar.getInstance();

		try {
			beginCalendar.setTime(monthTimeFormatter.parse(startMonth));
			finishCalendar.setTime(monthTimeFormatter.parse(endMonth));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		while (beginCalendar.before(finishCalendar) || beginCalendar.equals(finishCalendar)) {
			String date = monthTimeFormatter.format(beginCalendar.getTime());
			months.add(date);
			beginCalendar.add(Calendar.MONTH, 1);
		}

		return months;
	}

	/**
	 * Get a milliseconds of end day from epoch time representation given a time
	 * format of dd-MM-yyyy
	 * 
	 * @param timeString
	 * @throws ParseException
	 */

	public static long getEndTimestampInMillisByDayString(String timeString) throws ParseException {
		String dateWithTime = timeString.concat(" 23:59:59");
		Date date = formatterDateTime.parse(dateWithTime);
		return date.getTime();
	}
	
	/**
	 * 
	 * @param timeString. Sample "dd-MM-yyyy"
	 * @return
	 * @throws ParseException
	 */
	public static Timestamp getEndOfDayTimestampByDayTime(String timeString) throws ParseException {
		String dateWithTime = timeString.concat(" 23:59:59");
		return convertToTimestamp(dateWithTime, AdvoticsDateFormat.FORMATTER_DATE_TIME);
	}

	public static Timestamp getSubmittedTime(int requestTimeOffset) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MILLISECOND, Math.max(0, requestTimeOffset) * -1);
		return new Timestamp(cal.getTime().getTime());
	}

	public static Timestamp getSubmittedTime(String requestTimeOffsetParam) {
		int requestTimeOffset = 0;
		try {
			requestTimeOffset = Integer.parseInt(requestTimeOffsetParam);
		} catch (Exception e) {
			// ignore
		}
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MILLISECOND, Math.max(0, requestTimeOffset) * -1);
		return new Timestamp(cal.getTime().getTime());
	}

	public static String printTomorrowDateInDayFormat() {
		String todayString = printCurrentTimeInDayFormat();
		Date today = convertDateStringToDate(formatterDate, todayString);
		String tomorrow = convertDateToString(formatterDate, addDatesWithNDays(today, 1));
		return tomorrow;
	}

	/**
	 * Convert Timestamp of yyyy-mm-dd hh:mm:ss into String date with format
	 * dd/mm/yyyy
	 * 
	 * @param Timestamp
	 *            yyyy-mm-dd hh:mm:ss
	 * @return String dd/mm/yyyy
	 **/
	public static String convertTimestampStringIntoDateString(String dateTimestamp) {
		int dateIndex = convertStringTimestampIntoDateIndex(dateTimestamp);
		String date = convertDateInNumberToDateStringWithSpecifyFormat(dateIndex, "/");
		return date;
	}

	public static int countDurationInDays(Timestamp timestamp, Timestamp timestamp2) {

		// HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String dateStart = String.valueOf(timestamp);
		String dateStop = String.valueOf(timestamp2);

		Date d1 = null;
		Date d2 = null;

		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);

			// in milliseconds
			long different = d2.getTime() - d1.getTime();

			long secondsInMilli = 1000;
			long minutesInMilli = secondsInMilli * 60;
			long hoursInMilli = minutesInMilli * 60;
			long daysInMilli = hoursInMilli * 24;

			// calculate days
			int days = (int) (different / daysInMilli);
			return days;

		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public static String translateDateTimeWithSpecifiedFormatToDateTimeIndex(String dayTime, String dateFormat) {
		Date date = convertDateStringToDate(new SimpleDateFormat(dateFormat), dayTime);

		return convertDateToString(new SimpleDateFormat("yyyyMMddHHmmss"), date);
	}

	public static String intTimeToString(int seconds) {

		if (seconds < 0)
			return "0s";

		String daysString = "";
		String hoursString = "";
		String minutesString = "";
		String secondString = "";
		String monthString = "";
		String yearString = "";

		int days = (int) TimeUnit.SECONDS.toDays(seconds);
		long hours = TimeUnit.SECONDS.toHours(seconds) - (days * 24);
		long minutes = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60);
		long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60);

		if (days > 0) {
			int year = days / 365;
			int month = (days % 365) / 30;
			days = (days % 365) % 30;
			if (days > 0)
				daysString = days + "d ";

			if (month > 0)
				monthString = month + "mo ";

			if (year > 0)
				yearString = year + "y ";
		}

		if (hours > 0) {
			hoursString = hours + "h ";
		}

		if (minutes > 0)
			minutesString = minutes + "m ";

		if (hoursString.isEmpty() && minutesString.isEmpty())
			secondString = second + "s ";

		if (!daysString.isEmpty())
			secondString = "";

		if (!monthString.isEmpty()) {
			secondString = "";
			minutesString = "";
		}

		if (!yearString.isEmpty()) {
			secondString = "";
			minutesString = "";
		}

		return yearString + monthString + daysString + hoursString + minutesString + secondString;

	}

	public static int convertDateToDateIndex(String dateInString, DateFormat dateFotmat) {

		if (StringUtils.isNullOrEmpty(dateInString)) {
			return 0;
		}

		Date date = null;
		try {
			date = dateFotmat.parse(dateInString);
		} catch (ParseException e1) {
			date = new Date();
		}

		return Integer.valueOf(iso8601DayTimeFormatter.format(date).replace("-", ""));
	}

	public static int countDiffDaysBetweenTwoRangeDates(String startDate, String endDate) {
		int days = 0;
		try {
			Date date1 = formatterDate.parse(startDate);
			Date date2 = formatterDate.parse(endDate);
			long diff = date2.getTime() - date1.getTime();
			days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
		} catch (ParseException e) {

		}
		return days;

	}

	/**
	 * Convert date index of yyyyMMdd into String date with specified format from
	 * user
	 * 
	 * @param dateInt
	 * @param dateFormat
	 * @return
	 */
	public static String convertDateIndexToSpecifiedDateFormat(int dateInt, String dateFormat) {

		final DateFormat dateIndexFormatter = new SimpleDateFormat("yyyyMMdd");
		Date date = null;

		DateFormat dayTimeFormatter = new SimpleDateFormat(dateFormat);

		try {
			date = dateIndexFormatter.parse(String.valueOf(dateInt));
		} catch (ParseException e1) {
			date = new Date();
		}

		return String.valueOf(dayTimeFormatter.format(date));
	}

	public static int getDayOfWeekFromDate(int currentDate) {
		Date date = null;
		int dayOfWeek = 0;

		try {
			date = dateIndexFormatter.parse(String.valueOf(currentDate));
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

		} catch (ParseException e1) {
			date = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		}

		if (dayOfWeek == 1) {
			return 7; // IF the day is SUNDAY
		} else {
			return (dayOfWeek - 1);
		}
	}
	
	public static int getDayOfWeekFromDateInt(int currentDate) {
		Date date = null;
		int dayOfWeek = 0;

		try {
			date = dateIndexFormatter.parse(String.valueOf(currentDate));
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

		} catch (ParseException e1) {
			date = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		}
		
		return (dayOfWeek );
	}

	public static int getWeekOfMonthFromDate(int currentDate) {
		Date date = null;
		int weekOfMonth = 0;
		int dayOfWeek = 0;
		try {
			date = dateIndexFormatter.parse(String.valueOf(currentDate));
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
			weekOfMonth = c.get(Calendar.WEEK_OF_MONTH);
		} catch (ParseException e1) {
			date = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			weekOfMonth = c.get(Calendar.WEEK_OF_MONTH);
		}

		if (dayOfWeek == 1) {
			weekOfMonth = weekOfMonth - 1;
		}

		return weekOfMonth;
	}

	public static List<String> getDatesBetweenDateRangeInclusiveWithDateIndexFormat(String startDate, String endDate)
			throws ParseException {
		List<String> dates = new ArrayList<String>();

		Calendar lastDate = Calendar.getInstance();
		lastDate.setTimeInMillis(getDayStringInMillis(endDate, dateIndexFormatter));

		Calendar fromDate = Calendar.getInstance();
		fromDate.setTimeInMillis(getDayStringInMillis(startDate, dateIndexFormatter));

		while ((fromDate.getTime().before(lastDate.getTime())) || fromDate.getTime().equals(lastDate.getTime())) {
			dates.add(printTimeInDateIndexFormat(fromDate.getTimeInMillis()));
			fromDate.add(Calendar.DATE, 1);
		}

		return dates;
	}

	public static int getWeekOfYearFromDateStartFromMonday(int currentDate) {

		Date date = null;
		int weekOfYear = 0;

		try {
			date = dateIndexFormatter.parse(String.valueOf(currentDate));
			Calendar c = Calendar.getInstance();
			c.setFirstDayOfWeek(Calendar.MONDAY);
			c.setTime(date);
			weekOfYear = c.get(Calendar.WEEK_OF_YEAR);
		} catch (ParseException e1) {
// hendi 25/04/2019, commenting out this block since it could lead to wrong behavior
// if we get parse exception. The exception handling is to find the current time's week-of-year???
//			date = new Date();
//			Calendar c = Calendar.getInstance();
//			c.setTime(date);
//			weekOfYear = c.get(Calendar.WEEK_OF_YEAR);
		}

		return weekOfYear;
	}
	
	/**
	 * Method to get ISO 8601's definition of week-of-year
	 * Per https://docs.oracle.com/javase/8/docs/api/java/util/GregorianCalendar.html#week_year,
	 * The getFirstDayOfWeek() and getMinimalDaysInFirstWeek() values are initialized using 
	 * locale-dependent resources when constructing a GregorianCalendar. 
	 * The week determination is compatible with the ISO 8601 standard 
	 * when getFirstDayOfWeek() is MONDAY and getMinimalDaysInFirstWeek() is 4, 
	 * which values are used in locales where the standard is preferred.
	 * @param currentDate
	 * @return
	 */
	public static int getISO8601WeekOfYear(int currentDate) {

		Date date = null;
		int weekOfYear = 0;

		try {
			date = dateIndexFormatter.parse(String.valueOf(currentDate));
			Calendar c = Calendar.getInstance();
			c.setFirstDayOfWeek(Calendar.SUNDAY);
			c.setMinimalDaysInFirstWeek(4);
			c.setTime(date);
			weekOfYear = c.get(Calendar.WEEK_OF_YEAR);
		} catch (ParseException e1) {
			
		}

		return weekOfYear;
	}

	public static Timestamp addMinutesToTimestamp(Timestamp timestamp, long minutes) {
		Timestamp newTimestamp = timestamp;
		long totalMillis = timestamp.getTime() + TimeUnit.MINUTES.toMillis(minutes);
		newTimestamp.setTime(totalMillis);
		return newTimestamp;
	}
	
	/*
	 * Add minutes to time
	 * @param time="HH:mm" ex: "08:00", minutes = 65
	 * @result "09:05"
	 */
	
	public static String addMinutesToTime(String time, int minutes) {
		 
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
		 Date date = null;
		 
		 try {
			 date = dateFormat.parse(time);
		 } catch (ParseException e) {
			e.printStackTrace();
		 }
		 
		 Calendar cal = Calendar.getInstance();
		 cal.setTime(date);
		 
		 cal.add(Calendar.MINUTE, minutes);
		 
		 return dateFormat.format(cal.getTime());
		
	}

	public static int getNumberOfWeeksBetweenTwoDates(int startDateIndex, int endDateIndex) {

		Date startDate = null;
		Date endDate = null;
		int startDateWeek = 0;
		int endDateWeek = 0;

		try {
			startDate = dateIndexFormatter.parse(String.valueOf(startDateIndex));
			endDate = dateIndexFormatter.parse(String.valueOf(endDateIndex));

			Calendar startDateCalendar = Calendar.getInstance();
			startDateCalendar.setFirstDayOfWeek(Calendar.MONDAY);
			startDateCalendar.setTime(startDate);

			Calendar endDateCalendar = Calendar.getInstance();
			endDateCalendar.setFirstDayOfWeek(Calendar.MONDAY);
			endDateCalendar.setTime(endDate);

			startDateWeek = startDateCalendar.get(Calendar.WEEK_OF_YEAR);
			endDateWeek = endDateCalendar.get(Calendar.WEEK_OF_YEAR);

		} catch (ParseException e) {
			e.printStackTrace();
			return 0;
		}

		int numberOfWeeksBetweenDates = 0;
		if (endDateWeek < startDateWeek) {

			Calendar calendar = Calendar.getInstance();
			calendar.setFirstDayOfWeek(Calendar.MONDAY);
			calendar.setTime(startDate);
			Integer weeksOfYear = calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);

			int numberOfWeeksToLastWeek = (weeksOfYear - startDateWeek) + 1;
			numberOfWeeksBetweenDates = numberOfWeeksToLastWeek + endDateWeek;

			return numberOfWeeksBetweenDates;
		}

		numberOfWeeksBetweenDates = (endDateWeek - startDateWeek) + 1;
		return numberOfWeeksBetweenDates;
	}

	public static int getFirstDateInYear(int year) {
		String dateString = year + "0101";
		return Integer.valueOf(dateString);
	}

	public static int getLastDateInYear(int year) {
		String dateString = year + "1231";
		return Integer.valueOf(dateString);
	}

	public static int getYearMonthInt(int dateInt) {
		String date = "" + dateInt;
		if (date.length() > 5)
			return Integer.parseInt(date.substring(0, 6));
		else
			return dateInt;
	}

	public static String getFirstDateInYear(String year) {
		return "01-01-" + year;
	}

	public static String getLastDateInYear(String year) {
		return "31-12-" + year;
	}

	public static int getNumberOfDaysBetweenDates(String startDate, String endDate) {

		int numberOfDays = 0;

		Calendar endDateCalendar = Calendar.getInstance();
		Calendar startDateCalendar = Calendar.getInstance();

		try {
			endDateCalendar.setTimeInMillis(getDayStringInMillis(endDate, dateIndexFormatter));
			startDateCalendar.setTimeInMillis(getDayStringInMillis(startDate, dateIndexFormatter));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		while ((startDateCalendar.getTime().before(endDateCalendar.getTime()))
				|| startDateCalendar.getTime().equals(endDateCalendar.getTime())) {
			numberOfDays++;
			startDateCalendar.add(Calendar.DATE, 1);
		}

		return numberOfDays;
	}

	public static boolean isBetweenTwoHours(String hourToValidate, String startHour, String endHour) {
		try {
			Date time1 = new SimpleDateFormat("HH:mm:ss").parse(startHour);
			Calendar calendar1 = Calendar.getInstance();
			calendar1.setTime(time1);

			Date time2 = new SimpleDateFormat("HH:mm:ss").parse(endHour);
			Calendar calendar2 = Calendar.getInstance();
			calendar2.setTime(time2);

			Date d = new SimpleDateFormat("HH:mm:ss").parse(hourToValidate);
			Calendar calendar3 = Calendar.getInstance();
			calendar3.setTime(d);

			Date x = calendar3.getTime();
			if ((x.after(calendar1.getTime()) || x.equals(calendar1.getTime())) && x.before(calendar2.getTime())) {
				return true;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return false;
	}

	public static String getHourFromTimestamp(Timestamp timestamp) {
		try {

			Date date = new Date(timestamp.getTime());
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			String formattedDate = sdf.format(date);
			return formattedDate;

		} catch (Exception e) {
			Timestamp stamp = new Timestamp(System.currentTimeMillis());
			Date date = new Date(stamp.getTime());
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			String formattedDate = sdf.format(date);
			return formattedDate;
		}
	}

	public static String convertTimeStampIntoHourFormat(Timestamp time) {
		String timeString = String.valueOf(time);
		DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String hours = "";

		try {
			Date dateTime = inputFormat.parse(timeString);
			DateFormat outputFormat = new SimpleDateFormat("hh:mm:ss a");
			hours = outputFormat.format(dateTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return hours;
	}

	public static String convertDateFromSpecifiedFormatToDateIndex(String dateInString, String dateFormat) {

		Date date = convertDateStringToDate(new SimpleDateFormat(dateFormat), dateInString);
		return convertDateToString(new SimpleDateFormat("yyyyMMdd"), date);
	}

	public static String convertDateFromSpecifiedFormatToOtherFormat(String dateInString, 
			String inputDateFormat, String outputDateFormat) {

		Date date = convertDateStringToDate(new SimpleDateFormat(inputDateFormat), dateInString);
		return convertDateToString(new SimpleDateFormat(outputDateFormat), date);
	}

	public static boolean twoDatesAreWithinOneMonth(int startDateIndex, int endDateIndex) {

		Date startDate = null;
		Date endDate = null;

		try {
			startDate = dateIndexFormatter.parse(String.valueOf(startDateIndex));
			endDate = dateIndexFormatter.parse(String.valueOf(endDateIndex));

			Calendar startDateCalendar = Calendar.getInstance();
			startDateCalendar.setFirstDayOfWeek(Calendar.MONDAY);
			startDateCalendar.setTime(startDate);

			Calendar endDateCalendar = Calendar.getInstance();
			endDateCalendar.setFirstDayOfWeek(Calendar.MONDAY);
			endDateCalendar.setTime(endDate);

			LocalDateTime from = LocalDateTime.ofInstant(startDate.toInstant(), ZoneId.systemDefault());
			LocalDateTime to = LocalDateTime.ofInstant(endDate.toInstant(), ZoneId.systemDefault());

			long durationInclusive = Duration.between(from, to).toDays() + 1;
			if (durationInclusive >= 30) {
				return true;
			} else {
				if (startDateCalendar.get(Calendar.MONTH) == 2 && endDateCalendar.get(Calendar.MONTH) == 2) {
					return true;
				}
			}

			return false;
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static String getNMonthsAgoFromCurrentDateWithDayTimeFormat(int numberMonths) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -numberMonths);

		return convertDateToString(new SimpleDateFormat("dd-MM-yyyy"), calendar.getTime());
	}

	/**
	 * Convert dateTimeIndex of yyyyMMddHHmmss into String date with specify format
	 * @param datetimeIndex
	 * @param specifyFormat
	 * @return
	 */
	public static String convertDateTimeIndexToDateStringFormat(Long datetimeIndex, String specifyFormat) {
		String date = "";
		try {
			String dateTimeIndexString = String.valueOf(datetimeIndex);
			if (dateTimeIndexString != null && dateTimeIndexString.length() == 14) {
				DateFormat lokiIndexFormatter = new SimpleDateFormat(
						AdvoticsDateFormat.LOKI_INDEX_FORMAT.getDateFormat());
				Date dateFormated = lokiIndexFormatter.parse(dateTimeIndexString);

				DateFormat dateFormatter = new SimpleDateFormat(specifyFormat);

				date = dateFormatter.format(dateFormated);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return date;
	}
	
	public static String getNMonthsAgoFromCurrentDateWithDateTimeFormat(int numberMonths) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -numberMonths);

		return convertDateToString(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), calendar.getTime());
	}
	
	/**
	 * Converts 5 digit CRON expression into Date object
	 * @param cronExpression a valid CRON Expression
	 * @return the appropriate Date object containing the next run date
	 */
	public static Date convertCRONExpressionIntoNextDate(String cronExpression) {
		try {
			cronExpression = "* " + cronExpression;
			CronSequenceGenerator generator = new CronSequenceGenerator(cronExpression);			
			Date nextRunDate = generator.next(new Date());
			
			return nextRunDate;
			
		} catch (IllegalArgumentException iae) {
			iae.printStackTrace();
			return null;
		}
	}
	public static Long getCurrentDateTimeLokiIndexFormat() {
		SimpleDateFormat sdfDate = new SimpleDateFormat(LOKI_INDEX_FORMAT);
	    Date now = new Date();
	    
	    return Long.valueOf(sdfDate.format(now));
	}

	public static String getNMinutesAgoFromCurrentDateTimeWithDateTimeFormat(int numberMinutes) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, -numberMinutes);

		return convertDateToString(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), calendar.getTime());
	}

	public static Timestamp subtractTimestampBySeconds(Timestamp timestamp, int subtractValue) {

		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(timestamp.getTime());

		cal.add(Calendar.SECOND, -1 * (subtractValue));
		return new Timestamp(cal.getTime().getTime());
	}
	
	//NEWW
	//maria
	public static String formattedDate(String date) throws ParseException {
		SimpleDateFormat sdfFirst = new SimpleDateFormat("yyyy-MM-dd");
		Date currentDate = sdfFirst.parse(date);

		SimpleDateFormat sdfNew = new SimpleDateFormat("dd-MM-yyyy");
		String dateFormat = sdfNew.format(currentDate);
		System.out.println("date format : " + dateFormat);
		return dateFormat;
	}
}

