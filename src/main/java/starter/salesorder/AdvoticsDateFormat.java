package starter.salesorder;

public enum AdvoticsDateFormat {
	
	FULL_TIME_FORMAT("dd-MM-yyyy HH:mm:ss.SSS"),
	BARE_FULLTIME_FORMAT("ddMMyyyyHHmmssSSS"),
	BARE_FULLTIME_FORMAT_YYYYMMDD("yyyyMMddHHmmssSSS"),
	SECOND_TIME_FORMAT("dd-MM-yyyy HH:mm:ss"),
	SECOND_TIME_WITH_MONTH_NAME_FORMAT("dd-MMM-yyyy HH:mm:ss"),
	MINUTE_TIME_FORMAT("dd-MM-yyyy HH:mm"),
	MINUTE_TIME_WITH_MONTH_NAME_FORMAT("dd MMM yyyy HH:mm"),
	MINUTE_TIME_WITH_MONTH_NAME_COMMA_SEPARATED_FORMAT("dd MMM yyyy, HH:mm"),
	DAY_TIME_FORMAT("dd-MM-yyyy"),
	DAY_MONTH_NAME_FORMAT("dd-MMM-yyyy"),
	DAY_MONTH_FORMAT("dd MMM yyyy"),
	DAY_FULL_MONTH_NAME_FORMAT("dd MMMMM yyyy"),
	MONTH_TIME_FORMAT("MM-yyyy"),
	MONTH_FORMAT("MM-yyyy"),
	YEAR_FORMAT("yyyy"),
	MONTH_YEAR_FORMAT("yyyyMM"),
	RFC_2822_TIME_FORMAT("EEE, d MMM yyyy HH:mm:ss Z"),
	FILE_TIMESTAMP_FORMAT("ddMMyyyyHHmmssSSS"),
	MILISECOND_FORMAT("HHmmssSSS"),
	SECOND_FORMAT("HHmmss"),
	ISO_8601_UTC_TIME_FORMAT("yyyy-MM-dd'T'HH:mm:ssZ"),
	FORMATTER_DATE("dd-MM-yyyy"),
	FORMATTER_DATE_TIME("dd-MM-yyyy HH:mm:ss"),
	ISO_8601_UTC_DAY_FORMAT("yyyy-MM-dd"),
	DATE_INDEX_FORMAT("yyyyMMdd"),
	ISO_8601_FULLTIME_FORMAT("yyyy-MM-dd HH:mm:ss"),
	ISO_8601_DAYTIME_FORMAT("yyyy-MM-dd"),
	LOKI_INDEX_FORMAT("yyyyMMddHHmmss"),
	YEAR_MILISECOND_FORMAT("yyyyMMddHHmmssSSS"),
	MONTH_DATE_YEAR_FORMAT("MM-dd-yyyy"),
	FORMATTER_DATE_WITH_SLASH("dd/MM/yyyy"),
	FORMATTER_DATE_WITH_SLASH_AND_SECOND("dd/MM/yyyy HH:mm:ss");
	
	String dateFormat;
	
	AdvoticsDateFormat(String dateFormat){
		this.dateFormat = dateFormat;
	}

	public String getDateFormat() {
		return dateFormat;
	}
	
}
