package starter.salesorder;

public interface SalesOrderDiscounts {

	String getSalesOrderNo();
	Integer getClientId();
	String getStatus();
	Long getSubmittedTime();
	Integer getSodDiscountSeq();
	Double getSodValue();
	String getSodValueType();
	Character getSodIsActive();
}
