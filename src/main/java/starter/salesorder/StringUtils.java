package starter.salesorder;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class StringUtils {

	private static final Logger alog = LogManager.getLogger(StringUtils.class.getName());
	private final static ObjectMapper mapper = new ObjectMapper();
	
	public static String joinStrings(String[] stringsToBeJoined) {
		String result = "";
		boolean firstDest = true;
		for (String str : stringsToBeJoined) {
			if (firstDest) {
				firstDest = false;
			} else {
				result += ",";
			}
			result += str;
		}
		return result;
	}

	public static String joinStringsWithDelimiter(String[] stringsToBeJoined, String delimiter) {
		String result = "";
		boolean firstDest = true;
		for (String str : stringsToBeJoined) {
			if (firstDest) {
				firstDest = false;
			} else {
				result += delimiter;
			}
			result += str;
		}
		return result;
	}

	
	public static String joinStrings(List<String> stringsToBeJoined) {
		String result = "";
		boolean firstDest = true;
		for (String str : stringsToBeJoined) {
			if (firstDest) {
				firstDest = false;
			} else {
				result += ",";
			}
			result += str;
		}
		return result;
	}
	
	public static String joinStringsWithDelimiter(List<String> stringsToBeJoined, String delimiter) {
		String result = "";
		boolean firstDest = true;
		for (String str : stringsToBeJoined) {
			if (firstDest) {
				firstDest = false;
			} else {
				result += delimiter;
			}
			result += str;
		}
		return result;
	}
	
	public static String joinIntegersWithDelimiter(List<Integer> integersToBeJoined, String delimiter) {
		String result = "";
		boolean firstDest = true;
		for (Integer str : integersToBeJoined) {
			if (firstDest) {
				firstDest = false;
			} else {
				result += delimiter;
			}
			result += str;
		}
		return result;
	}

	public static String joinIntegers(List<Integer> integersToBeJoined) {
		String result = "";
		boolean firstDest = true;
		for (Integer str : integersToBeJoined) {
			if (firstDest) {
				firstDest = false;
			} else {
				result += ",";
			}
			result += str;
		}
		return result;
	}

	public static boolean isNotNullAndNotEmpty(String token) {
		boolean isNOE = true;

		isNOE = (token == null || token.isEmpty() || token.equals("null") || token.equals("[null]")) ? false : true;

		return isNOE;
	}

	public static boolean isNullOrEmpty(String token) {
		return token == null || token.isEmpty() || token.equals("null");
	}

	public static boolean isNotNull(Object object) {
		return (object != null);
	}

	public static String getStringValue(String token) {

		String stringValue = "";
		if (token != null && !token.isEmpty()) {

			stringValue = token;

		}
		return stringValue;
	}

	public static boolean isValidInt(String token) {
		boolean isInt = true;
		try {
			Integer.parseInt(token);
		} catch (Exception e) {
			isInt = false;
		}
		return isInt;
	}

	public static boolean isValidDouble(String token) {
		boolean isDouble = true;
		try {
			Double.parseDouble(token);
		} catch (Exception e) {
			isDouble = false;
		}
		return isDouble;
	}

	public static boolean isValidLong(String token) {
		boolean isLong = true;
		try {
			Long.parseLong(token);
		} catch (Exception e) {
			isLong = false;
		}
		return isLong;
	}
	
	public static boolean isValidTimestamp(String token) {
		boolean isTimestamp = true;
		try {
			Timestamp.valueOf(token);
		} catch (Exception e) {
			isTimestamp = false;
		}
		return isTimestamp;
	}
	
	public static boolean isValidBoolean(String token) {
		boolean isBoolean = true;
		try {
			Boolean.parseBoolean(token);
		} catch (Exception e) {
			isBoolean = false;
		}
		return isBoolean;
	}
	
	public static boolean isValidCharacter(String token) {
		boolean isBoolean = true;
		try {
			isBoolean = isValidCharacter(token.charAt(0));
		} catch (Exception e) {
			isBoolean = false;
		}
		return isBoolean;
	}
	
	public static boolean isValidCharacter(Character token) {
		boolean isBoolean = true;
		try {
			isBoolean = token == 'Y' || token == 'N';
		} catch (Exception e) {
			isBoolean = false;
		}
		return isBoolean;
	}

	public static String getDigitOnly(String token) {
		return token.replaceAll("[^\\d.]", "");
	}

	public static boolean isValidCommaSeparatedInteger(String token) {
		return token.matches("^([0-9]|(\\-[0-9]+)*,*)+$");
	}

	public static boolean isValidCommaSeparatedPositiveInteger(String token) {
		return token.matches("^([0-9]*,*)+$");
	}

	public static String cleanNonAlphanumeric(String token) {

		String cleanString = "";

		if ((token == null) || (token.isEmpty())) {
			alog.warn("Null or empty string input detected: " + token);
		} else {
			cleanString = token.replaceAll("[^a-zA-Z0-9.\\/\\-\\:@ ]", "");
		}
		return cleanString;
	}

	public static String cleanCR(String token) {

		String cleanString = "";

		if ((token == null) || (token.isEmpty())) {
			alog.warn("Null or empty string input detected: " + token);
		} else {
			cleanString = token.replaceAll("\n", "");
		}
		return cleanString;
	}

	public static String cleanLF(String token) {

		String cleanString = "";

		if ((token == null) || (token.isEmpty())) {
			alog.warn("Null or empty string input detected: " + token);
		} else {
			cleanString = token.replaceAll("\r", "");
		}

		return cleanString;
	}

	public static String cleanNonAlphanumericCRLF(String token) {

		String cleanString = "";

		if ((token == null) || token.isEmpty()) {
			alog.warn("Null or empty string input detected: " + token);
		} else {
			cleanString = cleanLF(cleanCR(cleanNonAlphanumeric(token))).trim();
		}

		return cleanString;
	}

	public static String cleanCRLF(String token) {
		String cleanString = "";
		if ((token == null) || (token.isEmpty())) {
			alog.warn("Null or empty string input detected: " + token);
		} else {
			cleanString = cleanLF(cleanCR(token)).trim();
		}

		return cleanString;
	}

	public static String[] cleanNonAlphanumericCRLF(String[] token) {
		int length = token == null ? 0 : token.length;

		String[] cleanStrings = new String[length];

		if ((token == null) || (token.length == 0)) {
			alog.warn("Null or empty string input detected: " + token);
		} else {
			for (int i = 0; i < token.length; i++) {
				cleanStrings[i] = cleanNonAlphanumericCRLF(token[i]);
			}
		}
		return cleanStrings;
	}

	public static boolean isAlphaNumericWithDot(String str) {
		return str.matches("^[a-zA-Z0-9.]*$");
	}

	/**
	 * Get string between two characters
	 * @param str
	 * @param firstCharacter
	 * @param lastCharacter
	 * @return 
	 */
	public static String extractStringBetweenTwoCharacters(String str, char firstCharacter, char lastCharacter) {

		if (str != null) {
			try {
				int positionFirstChar = str.indexOf(firstCharacter);
				int positionSecondChar  = str.indexOf(lastCharacter);
				
				if((positionFirstChar > 0) && (positionSecondChar > 0) && (positionFirstChar < positionSecondChar)){
					str = str.substring(str.indexOf(firstCharacter) + 1);
					str = str.substring(0, str.indexOf(lastCharacter));
					
					return str;
				}
				
			} catch (Exception e) {
				alog.warn("String between " + firstCharacter + " and " + lastCharacter + " cannot be extracted from "+str);

			}
		}
		return null;

	}

	public static boolean isNumber (String value) {
		String regex = "^[0-9]*$";
		return value.matches(regex);
	}
	
	public static boolean isFloatingPointNumber(String value) {
		String regexPoint = "^[-+]?[0-9]*(\\.|\\,)[0-9]+$";
		return value.matches(regexPoint);
	}

	public static boolean isPhoneNumber (String value) {
		String regex = "\\+?([ -]?\\d+)+|\\(\\d+\\)([ -]\\d+)";
		return value.matches(regex);
	}

	public static boolean validatePhoneNumber (String phoneNumber) {
		Pattern pattern = Pattern.compile("\\(?(?:\\+62|62|[0-9])(?:\\d{2,4})?\\)?[ .-]?\\d{1,4}[ .-]?\\d{1,4}[ .-]?\\d{1,4}");
		return pattern.matcher(phoneNumber).matches();
	}
	
	public static List<Integer> stringToIntegerList(String str, String delimiter) {
		List<Integer> result = new ArrayList<>();
		if (str == null || str.isEmpty()) {
			return result;
		}
		String[] splitted = str.split("\\s*" + delimiter + "\\s*");
		for (String s : splitted) {
			result.add(Integer.parseInt(s));
		}
		return result;
	}
	
	public static List<String> stringToList(String str, String delimiter) {
		List<String> result = new ArrayList<>();
		if (str == null || str.isEmpty()) {
			return result;
		}
		String[] splitted = str.split("\\s*" + delimiter + "\\s*");
		result = Arrays.asList(splitted);
		return result;
	}	
	
	public static List<String> stringToLinkedList(String str, String delimiter) {
		List<String> result = new LinkedList<>();
		if (str == null || str.isEmpty()) {
			return result;
		}
		String[] splitted = str.split("\\s*" + delimiter + "\\s*");
		return new LinkedList<>(Arrays.asList(splitted));
	}
	
	/**
	 * 
	 * @param string JSON
	 * @return Map<String, Object> constructed from JSON string
	 */
	public static Map<String, Object> getMapFromJson(String jsonString) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = mapper.readValue(jsonString, new TypeReference<Map<String, String>>(){});
		} catch (Exception e) {
			alog.warn("Failed to map json string: " + jsonString + " to Map<String, Object>"); 
		}
		
		return map;
	}
	
	public static String mapToString(Map<?, ?> map) {
		return Arrays.toString(map.entrySet().toArray());
	}
	
	public static boolean isSingleWord(String word) {
		return (word != null && word.length() > 0 && word.split("\\s+").length == 1);
	}
	
	public static String toTitleCase(String input) {
		
		if(StringUtils.isNullOrEmpty(input)) {
			return input;
		}
		
		input = input.toLowerCase();
	    StringBuilder titleCase = new StringBuilder(input.length());
	    boolean nextTitleCase = true;

	    for (char c : input.toCharArray()) {
	        if (Character.isSpaceChar(c)) {
	            nextTitleCase = true;
	        } else if (nextTitleCase) {
	            c = Character.toUpperCase(c);
	            nextTitleCase = false;
	        }

	        titleCase.append(c);
	    }

	    return titleCase.toString();
	}
	
	public static String getWholeNumberFromFloatString(String floatString) {
		String returnValue = floatString;
		try {
			// will ONLY catch float, e.g. accepts 1.1 but not 1.2.3
			if (isFloatingPointNumber(floatString)) { 
				String[] temp = floatString.split("(\\.|\\,)");
				returnValue = temp[0];
			} else {
				throw new Exception("not a floating point");
			}
		} catch (Exception e) {
			alog.debug(e.getMessage());
		}
		return returnValue;
	}
	
	//NEWW
	//maria
	public static String removeSixLastChar(String str) {
		return str.substring(0, str.length() - 6);
	}
	
	public static String getSplit(String key, int i) {
		String[] parts = key.split("|s");
		List<String> strings = Pattern.compile("\\|").splitAsStream(key).collect(Collectors.toList());
//		System.out.println("split nol : " + strings.get(0));
//		System.out.println("split satu : " + strings.get(1));
//		System.out.println("split dua : " + strings.get(2));
		return strings.get(i);
	}
}
