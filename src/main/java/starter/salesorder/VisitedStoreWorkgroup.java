package starter.salesorder;

public interface VisitedStoreWorkgroup {

	String getSalesGroupName();
	Integer getVisitedStore();
}
