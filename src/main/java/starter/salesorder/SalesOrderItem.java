package starter.salesorder;

public interface SalesOrderItem {

	//"soi.seq AS soiSeq, soi.quantity AS quantity, soi.price AS price, soi.is_active AS isActive,"
	String getSalesOrderNo();
	Integer getSoiSeq();
	Integer getQuantity();
	Double getPrice();
	Double getDiscount();
	Character getIsActive();
}
