package starter.salesorder;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Table(name = "sales_orders")
@Entity
public class SalesOrders implements Serializable {

	@Id
	private String sales_order_no;
	
	private int sales_id;
	private int client_id;
	private String client_ref_id;
	private String sales_channel;
	private String sales_channel_ref_id;
	private int customer_id;
	private int fulfiller_id;
	private String status;
	private Timestamp fulfilled_time;
	private Long submitted_time;
	private double discount;
	private double tax;
	private String creation_date;
	private Long loki_index;
	private Timestamp creation_time;
	private String created_by;
	private Timestamp last_updated_time;
	private String last_updated_by;
	
	protected SalesOrders() {}

	public SalesOrders(String sales_order_no, int sales_id, int client_id, String client_ref_id, String sales_channel,
			String sales_channel_ref_id, int customer_id, int fulfiller_id, String status, Timestamp fulfilled_time,
			Long submitted_time, double discount, double tax, String creation_date, Long loki_index,
			Timestamp creation_time, String created_by, Timestamp last_updated_time, String last_updated_by) {
		super();
		this.sales_order_no = sales_order_no;
		this.sales_id = sales_id;
		this.client_id = client_id;
		this.client_ref_id = client_ref_id;
		this.sales_channel = sales_channel;
		this.sales_channel_ref_id = sales_channel_ref_id;
		this.customer_id = customer_id;
		this.fulfiller_id = fulfiller_id;
		this.status = status;
		this.fulfilled_time = fulfilled_time;
		this.submitted_time = submitted_time;
		this.discount = discount;
		this.tax = tax;
		this.creation_date = creation_date;
		this.loki_index = loki_index;
		this.creation_time = creation_time;
		this.created_by = created_by;
		this.last_updated_time = last_updated_time;
		this.last_updated_by = last_updated_by;
	}

	public String getSales_order_no() {
		return sales_order_no;
	}

	public void setSales_order_no(String sales_order_no) {
		this.sales_order_no = sales_order_no;
	}

	public int getSales_id() {
		return sales_id;
	}

	public void setSales_id(int sales_id) {
		this.sales_id = sales_id;
	}

	public int getClient_id() {
		return client_id;
	}

	public void setClient_id(int client_id) {
		this.client_id = client_id;
	}

	public String getClient_ref_id() {
		return client_ref_id;
	}

	public void setClient_ref_id(String client_ref_id) {
		this.client_ref_id = client_ref_id;
	}

	public String getSales_channel() {
		return sales_channel;
	}

	public void setSales_channel(String sales_channel) {
		this.sales_channel = sales_channel;
	}

	public String getSales_channel_ref_id() {
		return sales_channel_ref_id;
	}

	public void setSales_channel_ref_id(String sales_channel_ref_id) {
		this.sales_channel_ref_id = sales_channel_ref_id;
	}

	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}

	public int getFulfiller_id() {
		return fulfiller_id;
	}

	public void setFulfiller_id(int fulfiller_id) {
		this.fulfiller_id = fulfiller_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getFulfilled_time() {
		return fulfilled_time;
	}

	public void setFulfilled_time(Timestamp fulfilled_time) {
		this.fulfilled_time = fulfilled_time;
	}

	public Long getSubmitted_time() {
		return submitted_time;
	}

	public void setSubmitted_time(Long submitted_time) {
		this.submitted_time = submitted_time;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public String getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(String creation_date) {
		this.creation_date = creation_date;
	}

	public Long getLoki_index() {
		return loki_index;
	}

	public void setLoki_index(Long loki_index) {
		this.loki_index = loki_index;
	}

	public Timestamp getCreation_time() {
		return creation_time;
	}

	public void setCreation_time(Timestamp creation_time) {
		this.creation_time = creation_time;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public Timestamp getLast_updated_time() {
		return last_updated_time;
	}

	public void setLast_updated_time(Timestamp last_updated_time) {
		this.last_updated_time = last_updated_time;
	}

	public String getLast_updated_by() {
		return last_updated_by;
	}

	public void setLast_updated_by(String last_updated_by) {
		this.last_updated_by = last_updated_by;
	}

	@Override
	public String toString() {
		return "SalesOrders [sales_order_no=" + sales_order_no + ", sales_id=" + sales_id + ", client_id=" + client_id
				+ ", client_ref_id=" + client_ref_id + ", sales_channel=" + sales_channel + ", sales_channel_ref_id="
				+ sales_channel_ref_id + ", customer_id=" + customer_id + ", fulfiller_id=" + fulfiller_id + ", status="
				+ status + ", fulfilled_time=" + fulfilled_time + ", submitted_time=" + submitted_time + ", discount="
				+ discount + ", tax=" + tax + ", creation_date=" + creation_date + ", loki_index=" + loki_index
				+ ", creation_time=" + creation_time + ", created_by=" + created_by + ", last_updated_time="
				+ last_updated_time + ", last_updated_by=" + last_updated_by + "]";
	}


}
