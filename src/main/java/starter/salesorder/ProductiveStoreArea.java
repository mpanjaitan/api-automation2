package starter.salesorder;

public interface ProductiveStoreArea {

	String getProvinceName();
	Integer getProductiveStore();
}
