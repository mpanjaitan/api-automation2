package starter.salesorder;

public interface RegisteredStoreWorkgroup {

	String getSalesGroupName();
	Integer getRegisteredStore();
	
}
