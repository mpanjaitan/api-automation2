package starter.salesorder;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

@Service
public class SalesOvertimeCalculatorService {

	@Autowired
	SalesOrdersRepository salesOrderRepository;

	DemoTest demoTest = new DemoTest();

	Map<Integer, Double> revenueByDates = new HashMap<>();
	Map<String, Double> revenueBySO = new HashMap<>();

	String principal = "PCP";
	String timeToStart = " 00:00:00";
	String timeToEnd = " 23:59:59";

	public List<SalesOrderTransaction> calculate() {
		/*
		 * //get SalesOrderItem List<SalesOrderItem> salesOrderItems =
		 * SalesOrderItem.getSalesOrderItemsBySalesOrderNumber(salesOrderNoKey,
		 * session);
		 * 
		 * 
		 * List<SalesOrderTransaction> salesOrders =
		 * salesOrderRepository.findByDefaultDate();
		 */

		return salesOrderRepository.findByDefaultDate();
	}

	public List<SalesOrderTransaction> calculateFindByDate(Integer idClient, Long startTime, Long endTime)
			throws ParseException {
		// TODO Auto-generated method stub
//		getRevenueBySO(idClient, startTime, endTime);
		getRevenueByDate(getRevenueBySO(idClient, startTime, endTime));

		String totalRevenue = calculateFindByFilter(idClient, startTime, endTime);

		System.out.println("Total revenue in calculate find by date : " + totalRevenue);
		return salesOrderRepository.findByClientAndDate(idClient, startTime, endTime);
	}

	public String calculateFindByFilter(Integer idClient, Long startTime, Long endTime) throws ParseException {
		// TODO Auto-generated method stub
//		getRevenueBySO(idClient, startTime, endTime);
//		getRevenueByDate(getRevenueBySO(idClient, startTime, endTime));
		return totalSalesRevenuePerFilter(getRevenueByDate(getRevenueBySO(idClient, startTime, endTime)));
	}

	private double getTotalSalesOrder(SalesOrder salesOrder, List<SalesOrderItem> salesOrderItems) {

		return getTotalSOItem(salesOrderItems) + getSOGlobalTax(salesOrder) - getSOGlobalDiscount(salesOrder);
	}

	// YOU CAN :)
	private Map<String, SalesOrder> constructChangedSalesOrderMap(Integer clientId, Long startTime, Long endTime) {
		// scan sales order related data
		List<SalesOrder> salesOrderScan = new ArrayList<SalesOrder>();
		List<String> distinctSalesOrderNoOnsalesOrderItemScan = new ArrayList<String>();

		salesOrderScan = salesOrderRepository.getSalesOrdersByClientAndDate(clientId, startTime, endTime);
		distinctSalesOrderNoOnsalesOrderItemScan = salesOrderRepository
				.getDistinctSalesOrderNumberByClientAndDate(clientId, startTime, endTime);

		// constructing map
		Map<String, SalesOrder> salesOrderMap = new HashMap<String, SalesOrder>();
		if ((salesOrderScan != null) && (!salesOrderScan.isEmpty())) {
			for (SalesOrder salesOrder : salesOrderScan) {
				salesOrderMap.put(salesOrder.getSalesOrderNo(), salesOrder);
			}
		}
		if ((distinctSalesOrderNoOnsalesOrderItemScan != null)
				&& (!distinctSalesOrderNoOnsalesOrderItemScan.isEmpty())) {
			for (String salesOrderNo : distinctSalesOrderNoOnsalesOrderItemScan) {
				if (!salesOrderMap.containsKey(salesOrderNo)) {
					SalesOrder salesOrder = fetchSalesOrderBySalesOrderNumber(salesOrderNo);
					salesOrderMap.put(salesOrderNo, salesOrder);
				}
			}
		}
		return salesOrderMap;
	}

	public SalesOrder fetchSalesOrderBySalesOrderNumber(String salesOrderNumber) {
		/*
		 * List<SalesOrder> salesOrderr = new ArrayList<SalesOrder>(); salesOrderr =
		 * salesOrderRepository.findSalesOrderBySalesOrderNumber(salesOrderNumber);
		 * List<SalesOrder> salesOrder = salesOrderr.li
		 */
		List<SalesOrder> salesOrder = salesOrderRepository.findSalesOrderBySalesOrderNumber(salesOrderNumber);

		if (salesOrder != null && !salesOrder.isEmpty()) {
			return salesOrder.get(0);
		}

		return null;
	}

	// afsdas
	private Double getTotalSOItem(List<SalesOrderItem> salesOrderItems) {

		Double totalAmount = 0.0;
		for (SalesOrderItem soItem : salesOrderItems) {
			if (isSOActive(soItem)) {
				totalAmount += (soItem.getQuantity() * soItem.getPrice()) - soItem.getDiscount();
			}

		}
		return totalAmount;
	}

	private boolean isSOActive(SalesOrderItem soItem) {
		return soItem.getIsActive() == 'Y';
	}

	private Double getSOGlobalTax(SalesOrder so) {
		return so.getGlobalSOTax();
	}

	private double getSOGlobalDiscount(SalesOrder so) {
		return so.getGlobalSODiscount();
	}

	// yes
	private Map<String, Double> getRevenueBySO(Integer clientId, Long startTime, Long endTime) {
		Map<String, Double> revenueBySO = new HashMap<>();
		Map<Integer, Double> revenueByDates = new HashMap<>();
		List<SalesOrderTransaction> salesOrders = salesOrderRepository.findByClientAndDate(clientId, startTime,
				endTime);

		System.out.println("SIZE RESULT MARIAAAA : " + salesOrders.size());

		// Print the name from the list....
		for (SalesOrderTransaction model : salesOrders) {
			System.out.println(
					"SONo from HASIL QUERY NOT STRING : " + model.getSalesOrderNo() + "|" + model.getCreationDate());
		}
		System.out.println("HASIL QUERY FIND BY CLIENT AND DATE : " + salesOrders.toString());

		Map<String, List<SalesOrderTransaction>> soMap = salesOrders.stream()
				.collect(Collectors.groupingBy(SalesOrderTransaction::getSalesOrderNo, Collectors.toList()));

		Double salesOrderTotal = 0D;

		for (Map.Entry<String, List<SalesOrderTransaction>> soEntry : soMap.entrySet()) {
			Map<Integer, List<SalesOrderTransaction>> soiSeqMap = soEntry.getValue().stream()
					.collect(Collectors.groupingBy(SalesOrderTransaction::getSoiSeq, Collectors.toList()));

			Double subtotalAllItem = 0D;

			System.out.println("soiSeqMap size : " + soiSeqMap.size());
			if (soiSeqMap.equals(null) && soiSeqMap.isEmpty()) {
				System.out.println("Soiseqmap null");
			}

			for (Map.Entry<Integer, List<SalesOrderTransaction>> entry : soiSeqMap.entrySet()) {
				Integer soiSeq = entry.getKey();
				System.out.println("soiSeq now: " + soiSeq);
				List<SalesOrderTransaction> siuSalesOrders = entry.getValue().stream()
						.filter(a -> "SIU".equals(a.getDiscountLevel())).collect(Collectors.toList());

				BigDecimal siuDiscount = BigDecimal.ZERO;
				Double finalPrice = 0D;
				Integer qty = 0;
				Double subTotal = 0D;

				System.out.println("siuSalesOrders size : " + siuSalesOrders.size());
				if (!siuSalesOrders.equals(null) && !siuSalesOrders.isEmpty()) {
					// calculate discount - sub total level
					Pair<Double, Integer> siuAfterDiscountPair = calculateSiuAfterDiscount(siuSalesOrders);
					subTotal = siuAfterDiscountPair.getFirst() * siuAfterDiscountPair.getSecond();
				} else {
					System.out.println("siuSalesOrders null");
					SalesOrderTransaction a = entry.getValue().get(0);
					finalPrice = a.getPrice();
//					qty = a.getQuantity();
					qty = a.getFulfilledQuantity();
					System.out.println("finalPrice before cal : " + finalPrice);
					System.out.println("Quantity before cal : " + qty);
					System.out.println("subtotal before cal : " + subTotal);
					subTotal = finalPrice * qty;
					System.out.println("subtotal after cal : " + subTotal);
				}

				List<SalesOrderTransaction> sisSalesOrders = entry.getValue().stream()
						.filter(a -> "SIS".equals(a.getDiscountLevel())).collect(Collectors.toList());

				System.out.println("sisSalesOrders size : " + sisSalesOrders.size());
				if (!sisSalesOrders.equals(null) && !sisSalesOrders.isEmpty()) {
					Double sisAfterDiscount = calculateSisAfterDiscount(subTotal, sisSalesOrders);
					subtotalAllItem = subtotalAllItem + sisAfterDiscount;
				} else {
					System.out.println("sisSalesOrders null");
					System.out.println("subtotalAll item prev : " + subtotalAllItem);
					subtotalAllItem = subtotalAllItem + subTotal;
					System.out.println("subtotalAll item curr : " + subtotalAllItem);
				}
			}

			// get sales order discount
			String salesOrderNo = soEntry.getKey();
			System.out.println("SALES ORDER NUMBER MARIA: " + salesOrderNo);
			System.out.println("Sub total All Item SO [ " + salesOrderNo + " ] : " + subtotalAllItem);
			List<SalesOrderDiscounts> salesOrderDiscounts = salesOrderRepository.findSODiscountSONo(salesOrderNo);
			// for each, calculate discount based on subtotalAllItem

			salesOrderTotal = calculateSOTotalAfterDiscount(subtotalAllItem, salesOrderDiscounts);
			System.out.println("Sales Order Total SO [ " + salesOrderNo + " ] : " + salesOrderTotal);

			// masukin ke format SONumber|SODate, SOTotal
//			SalesOrderTransaction a = entry.getValue().get(0);
			SalesOrderTransaction formatResult = soEntry.getValue().get(0);
			String soDate = formatResult.getCreationDate();
			Double revenues = salesOrderTotal;
			String key = salesOrderNo + "|" + soDate;
			if (revenueBySO.containsKey(key)) {
				Double prevRev = revenueBySO.get(key);
				Double currRev = prevRev + revenues;
				revenueBySO.put(key, currRev);
			} else {
				revenueBySO.put(key, revenues);
			}
			System.out.println("NANANANANA : " + revenueBySO.toString());
		}

//		return soTotalFinal;

		// calculate revenue per sales order
		/*
		 * for(SalesOrderTransaction salesOrder : salesOrders) { Long soDate =
		 * salesOrder.getSubmittedTime(); String soNo = salesOrder.getSalesOrderNo();
		 * Double revenue = salesOrderTotal; // Double revenue =
		 * calculateRevenue(salesOrder.getClientId(), salesOrder.); String key = soNo +
		 * "|" + soDate; if(revenueBySO.containsKey(key)) { Double prevRev =
		 * revenueBySO.get(key); Double currRev = prevRev + revenue;
		 * revenueBySO.put(key, currRev); } else { revenueBySO.put(key, revenue); } //
		 * System.out.println("NANANANANA : " + revenueBySO.toString()); }
		 */

		return revenueBySO;

	}

	public Map<String, Double> getRevenueByDate(Map<String, Double> revenueBySO) throws ParseException {
		Comparator<String> compString = Comparator.comparing(String::toString);

		Map<String, Double> revenueByDates = new TreeMap<>(compString);
		// calculate revenue per date

		for (Map.Entry<String, Double> entry : revenueBySO.entrySet()) {
			System.out.println("Entry dot key : " + entry.getKey());
			System.out.println("Entry dot value : " + entry.getValue());
			String dates = getDate(entry.getKey());
			System.out.println("dates : " + dates);
			// comment because format date changed (submitted_date to creation_date)
			/*
			 * String date = StringUtils.removeSixLastChar(dates);
			 * System.out.println("date : " + date);
			 */
			String formattedDate = DateTimeHelper.formattedDate(dates);
			System.out.println("formatted Date : " + formattedDate);
			Double revenue = entry.getValue();
//			Double revenueBulat = (double) Math.round(revenue);
//			Double revenue = revenueBySO.get(key);
			if (revenueByDates.containsKey(formattedDate)) {
				Double prevRev = revenueByDates.get(formattedDate);
				Double currRev = prevRev + revenue;
				revenueByDates.put(formattedDate, currRev);
			} else {
				revenueByDates.put(formattedDate, revenue);
			}
		}
		System.out.println("YAYAYAYA : " + revenueByDates.toString());
		return revenueByDates;
	}

	private String totalSalesRevenuePerFilter(Map<String, Double> revenueByDates) {
		Double totalRevenue = 0D;

		for (Map.Entry<String, Double> entry : revenueByDates.entrySet()) {
			Double revenue = entry.getValue();
			System.out.println("revenue before : " + revenue);
			System.out.println("total revenue before : " + totalRevenue);
			totalRevenue = totalRevenue + revenue;
			System.out.println("total revenue after : " + totalRevenue);
		}
		System.out.println("Total revenue return : " + totalRevenue);

		DecimalFormat df = new DecimalFormat("#.##");
		System.out.println("HASIL DESIMAL FORMAT : " + df.format(totalRevenue));

		return df.format(totalRevenue);
	}
	
	private String totalScarcityLevelPerFilter(Map<String, Double> valueByFilter) {
		Double totalValue = 0D;
		Double totalAverageValue = 0D;
		Integer sizeMap = valueByFilter.size();

		for (Map.Entry<String, Double> entry : valueByFilter.entrySet()) {
			Double value = entry.getValue();
			System.out.println("value before : " + value);
			System.out.println("total value before : " + totalValue);
			totalValue = totalValue + value;
			System.out.println("total value after : " + totalValue);
		}
		System.out.println("Total value return : " + totalValue);

		totalAverageValue = totalValue/sizeMap;
		System.out.println("Total average value return : " + totalAverageValue);
		
		DecimalFormat df = new DecimalFormat("#.##");
		System.out.println("TOTAL VALUE HASIL DESIMAL FORMAT : " + df.format(totalValue));
		System.out.println("TOTAL AVERAGE VALUE HASIL DESIMAL FORMAT : " + df.format(totalAverageValue));

		return df.format(totalAverageValue);
	}

	private String getDate(String key) {
//		String[] parts = key.split("|s");
		List<String> strings = Pattern.compile("\\|").splitAsStream(key).collect(Collectors.toList());
		System.out.println("split nol : " + strings.get(0));
		System.out.println("split satu : " + strings.get(1));
		return strings.get(1);

		/*
		 * String[] parts = StringUtils.split(key,"|");
		 * System.out.println("split nol : " + parts[0]);
		 * System.out.println("split satu : " + parts[1]); return parts[1];
		 */
	}

	private Double calculateRevenue(Integer clientId, Long startTime, Long endTime) {
		List<SalesOrderTransaction> salesOrders = salesOrderRepository.findByDate(clientId, startTime, endTime);
		Map<String, List<SalesOrderTransaction>> soMap = salesOrders.stream()
				.collect(Collectors.groupingBy(SalesOrderTransaction::getSalesOrderNo, Collectors.toList()));

		Double salesOrderTotal = 0D;

		for (Map.Entry<String, List<SalesOrderTransaction>> soEntry : soMap.entrySet()) {
			Map<Integer, List<SalesOrderTransaction>> soiSeqMap = soEntry.getValue().stream()
					.collect(Collectors.groupingBy(SalesOrderTransaction::getSoiSeq, Collectors.toList()));

			Double subtotalAllItem = 0D;

			for (Map.Entry<Integer, List<SalesOrderTransaction>> entry : soiSeqMap.entrySet()) {
				Integer soiSeq = entry.getKey();
				List<SalesOrderTransaction> siuSalesOrders = entry.getValue().stream()
						.filter(a -> "SIU".equals(a.getDiscountLevel())).collect(Collectors.toList());

				BigDecimal siuDiscount = BigDecimal.ZERO;
				Double finalPrice = 0D;
				Integer qty = 0;

				// calculate discount - sub total level
				Pair<Double, Integer> siuAfterDiscountPair = calculateSiuAfterDiscount(siuSalesOrders);
				Double subTotal = siuAfterDiscountPair.getFirst() * siuAfterDiscountPair.getSecond();
				System.out.println("subTotal : " + subTotal);

				List<SalesOrderTransaction> sisSalesOrders = entry.getValue().stream()
						.filter(a -> "SIS".equals(a.getDiscountLevel())).collect(Collectors.toList());

				Double sisAfterDiscount = calculateSisAfterDiscount(subTotal, sisSalesOrders);
				subtotalAllItem = subtotalAllItem + sisAfterDiscount;

			}

			// get sales order discount
			String salesOrderNo = soEntry.getKey();
			List<SalesOrderDiscounts> salesOrderDiscounts = salesOrderRepository.findSODiscount(clientId, startTime,
					endTime);
			// for each, calculate discount based on subtotalAllItem
			Map<Integer, List<SalesOrderDiscounts>> sodDiscountSeqMap = salesOrderDiscounts.stream()
					.collect(Collectors.groupingBy(SalesOrderDiscounts::getSodDiscountSeq, Collectors.toList()));

//			Double salesOrderTotal = 0D;

			for (Map.Entry<Integer, List<SalesOrderDiscounts>> entry : sodDiscountSeqMap.entrySet()) {
				Integer sodDiscountSeq = entry.getKey();

				salesOrderTotal = calculateSOTotalAfterDiscount(subtotalAllItem, salesOrderDiscounts);

//				Pair<String, Double> soTotalFinal = calculateSOTotalAfterDiscount(salesOrderNo, subtotalAllItem, salesOrderDiscounts);
			}
		}
		return salesOrderTotal;
	}

	/*
	 * private Pair<String, Double> calculateSOTotalAfterDiscount(String
	 * salesOrderNo, Double subtotalAllItem, List<SalesOrderDiscounts> list) { //
	 * Double SOTotalFinal = 0D; for (int i = 0; i < list.size(); i++) {
	 * SalesOrderDiscounts a = list.get(i); String type = a.getSodValueType();
	 * 
	 * subtotalAllItem = getAfterDiscount(type, subtotalAllItem, a.getSodValue());
	 * }; return Pair.of(salesOrderNo, subtotalAllItem); }
	 */

	private Double calculateSOTotalAfterDiscount(Double subtotalAllItem, List<SalesOrderDiscounts> list) {
//		Double SOTotalFinal = 0D;
		for (int i = 0; i < list.size(); i++) {
			SalesOrderDiscounts a = list.get(i);
			String type = a.getSodValueType();

			subtotalAllItem = getAfterDiscount(type, subtotalAllItem, a.getSodValue());
		}
		;
		return subtotalAllItem;
	}

	private Double calculateTotalDiscount(Double subtotalAllItem, List<SalesOrderDiscounts> list) {
//		Double SOTotalFinal = 0D;
		Double discount = 0D;
		Double totalDiscount = 0D;
		for (int i = 0; i < list.size(); i++) {
			SalesOrderDiscounts a = list.get(i);
			String type = a.getSodValueType();

			System.out.println("Discount before : " + discount);
			discount = getDiscountLevel3(type, subtotalAllItem, a.getSodValue());
			System.out.println("Discount after getDiscountLevel3 : " + discount);
			System.out.println("SubtotalAllItem before getAfterDiscount : " + subtotalAllItem);
			subtotalAllItem = getAfterDiscount(type, subtotalAllItem, a.getSodValue());
			System.out.println("subtotalAllItem after getAfterDiscount : " + subtotalAllItem);
			System.out.println("totalDiscount before discount++ : " + totalDiscount);
			totalDiscount = totalDiscount + discount;
			System.out.println("totalDiscount after discount++ : " + totalDiscount);
		}
		;
		return totalDiscount;
	}

	private Pair<Double, Integer> calculateSiuAfterDiscount(List<SalesOrderTransaction> list) {
		Double finalPrice = 0D;
		Integer qty = 0;

		System.out.println("size list siu : " + list.size());
		for (int i = 0; i < list.size(); i++) {
			SalesOrderTransaction a = list.get(i);
			String type = a.getSoidValueType();
			System.out.println("SONO : " + a.getSalesOrderNo() + " soid value type siu [" + i + "] : " + type);

			if (i == 0) {
				finalPrice = a.getPrice();
//				qty = a.getQuantity();
				qty = a.getFulfilledQuantity();

				System.out.println("Final price i = 0 : " + finalPrice);
				System.out.println("Quantity i = 0 : " + qty);
			}

			System.out.println("Quantity siu : " + qty);
			System.out.println("Type siu : " + type);
			System.out.println("Final Price before siu : " + finalPrice);
			System.out.println("Soid value siu : " + a.getSoidValueType());
			finalPrice = getAfterDiscount(type, finalPrice, a.getSoidValue());
			System.out.println("Final price after siu : " + finalPrice);
		}
		;

		System.out.println("Final price siu return : " + finalPrice);
		System.out.println("Quantity siu return : " + qty);
		return Pair.of(finalPrice, qty);

	}

	private Double calculateSisAfterDiscount(Double subTotal, List<SalesOrderTransaction> list) {
		System.out.println("size list sis : " + list.size());
		System.out.println("subTotal for SIS : " + subTotal);
		for (int i = 0; i < list.size(); i++) {
			SalesOrderTransaction a = list.get(i);
			String type = a.getSoidValueType();
			System.out.println("SONO : " + a.getSalesOrderNo() + " soid value type sis [" + i + "] : " + type);

			System.out.println("Type sis : " + type);
			System.out.println("subTotal sis before : " + subTotal);
			System.out.println("Soid value sis : " + a.getSoidValueType());
			subTotal = getAfterDiscount(type, subTotal, a.getSoidValue());
			System.out.println("subTotal sis after : " + subTotal);
		}
		;

		System.out.println("subTotal sis return : " + subTotal);
		return subTotal;

	}

	private Double getAfterDiscount(String type, Double number, Double value) {
		System.out.println("Sod value : " + value);
		if ("PER".equals(type)) {
			System.out.println("Number Before : " + number);
			number = number - ((number * value) / 100);
			System.out.println("Number After : " + number);
		} else if ("AMN".equals(type)) {
			System.out.println("Number Before : " + number);
			number = number - value;
			System.out.println("Number After : " + number);
		}

		System.out.println("Number return : " + number);
		return number;
	}

	private Double getDiscountLevel3(String type, Double number, Double value) {
		System.out.println("Soid value : " + value);
		Double discLevel3 = 0D;
		if ("PER".equals(type)) {
			System.out.println("Number Before : " + number);
			discLevel3 = (number * value) / 100;
			System.out.println("Disc level 3 in PER : " + discLevel3);
//			number = number - ((number * value) / 100);
			number = number - discLevel3;
			System.out.println("Number After : " + number);
		} else if ("AMN".equals(type)) {
			System.out.println("Number Before : " + number);
			discLevel3 = value;
			System.out.println("Disc level 3 in AMN : " + discLevel3);
//			number = number - value;
			number = number - discLevel3;
			System.out.println("Number After : " + number);
		}

		System.out.println("Discount return : " + discLevel3);
		System.out.println("Number return : " + number);
		return discLevel3;
	}

	// NEWW
	// maria
	public List<SalesOrderTransaction> calculateFindByDateString(String idClient, String startTime, String endTime)
			throws ParseException {
		// TODO Auto-generated method stub
//			getRevenueBySO(idClient, startTime, endTime);
		getRevenueByDate(getRevenueBySOString(idClient, startTime, endTime));

		String totalRevenue = calculateFindByFilterString(idClient, startTime, endTime);

		System.out.println("Total revenue in calculate find by date : " + totalRevenue);
		return salesOrderRepository.findByClientAndDateString(idClient, startTime, endTime);
	}

	public Map<String, Double> getRevenueBySOString(String clientId, String startTime, String endTime)
			throws ParseException {
		Map<String, Double> revenueBySO = new HashMap<>();
		Map<Integer, Double> revenueByDates = new HashMap<>();

		System.out.println("ID CLIENT SERVICE : " + clientId);
		System.out.println("START TIME SERVICE : " + startTime);
		System.out.println("END TIME SERVICE : " + endTime);
		System.out.println("FIND BY CLIENT AND DATE STRING SIZE : "
				+ salesOrderRepository.findByClientAndDateString(clientId, startTime, endTime).size());

		List<SalesOrderTransaction> salesOrders = salesOrderRepository.findByClientAndDateString(clientId, startTime,
				endTime);

		// Print the name from the list....
		for (SalesOrderTransaction model : salesOrders) {
			System.out.println("SONo from HASIL QUERY : " + model.getSalesOrderNo() + "|" + model.getCreationDate());
		}
		System.out.println("HASIL QUERY FIND BY CLIENT AND DATE : " + salesOrders.toString());

		Map<String, List<SalesOrderTransaction>> soMap = salesOrders.stream()
				.collect(Collectors.groupingBy(SalesOrderTransaction::getSalesOrderNo, Collectors.toList()));

		Double salesOrderTotal = 0D;

		for (Map.Entry<String, List<SalesOrderTransaction>> soEntry : soMap.entrySet()) {
			Map<Integer, List<SalesOrderTransaction>> soiSeqMap = soEntry.getValue().stream()
					.collect(Collectors.groupingBy(SalesOrderTransaction::getSoiSeq, Collectors.toList()));

			Double subtotalAllItem = 0D;

			System.out.println("soiSeqMap size : " + soiSeqMap.size());
			if (soiSeqMap.equals(null) && soiSeqMap.isEmpty()) {
				System.out.println("Soiseqmap null");
			}

			for (Map.Entry<Integer, List<SalesOrderTransaction>> entry : soiSeqMap.entrySet()) {
				Integer soiSeq = entry.getKey();
				System.out.println("soiSeq now: " + soiSeq);
				List<SalesOrderTransaction> siuSalesOrders = entry.getValue().stream()
						.filter(a -> "SIU".equals(a.getDiscountLevel())).collect(Collectors.toList());

				BigDecimal siuDiscount = BigDecimal.ZERO;
				Double finalPrice = 0D;
				Integer qty = 0;
				Double subTotal = 0D;

				System.out.println("siuSalesOrders size : " + siuSalesOrders.size());
				if (!siuSalesOrders.equals(null) && !siuSalesOrders.isEmpty()) {
					// calculate discount - sub total level
					Pair<Double, Integer> siuAfterDiscountPair = calculateSiuAfterDiscount(siuSalesOrders);
					subTotal = siuAfterDiscountPair.getFirst() * siuAfterDiscountPair.getSecond();
				} else {
					System.out.println("siuSalesOrders null");
					SalesOrderTransaction a = entry.getValue().get(0);
					finalPrice = a.getPrice();
//					qty = a.getQuantity();
					qty = a.getFulfilledQuantity();
					System.out.println("finalPrice before cal : " + finalPrice);
					System.out.println("Quantity before cal : " + qty);
					System.out.println("subtotal before cal : " + subTotal);
					subTotal = finalPrice * qty;
					System.out.println("subtotal after cal : " + subTotal);
				}

				List<SalesOrderTransaction> sisSalesOrders = entry.getValue().stream()
						.filter(a -> "SIS".equals(a.getDiscountLevel())).collect(Collectors.toList());

				System.out.println("sisSalesOrders size : " + sisSalesOrders.size());
				if (!sisSalesOrders.equals(null) && !sisSalesOrders.isEmpty()) {
					Double sisAfterDiscount = calculateSisAfterDiscount(subTotal, sisSalesOrders);
					subtotalAllItem = subtotalAllItem + sisAfterDiscount;
				} else {
					System.out.println("sisSalesOrders null");
					System.out.println("subtotalAll item prev : " + subtotalAllItem);
					subtotalAllItem = subtotalAllItem + subTotal;
					System.out.println("subtotalAll item curr : " + subtotalAllItem);
				}
			}

			// get sales order discount
			String salesOrderNo = soEntry.getKey();
			System.out.println("SALES ORDER NUMBER MARIA: " + salesOrderNo);
			System.out.println("Sub total All Item SO [ " + salesOrderNo + " ] : " + subtotalAllItem);
			List<SalesOrderDiscounts> salesOrderDiscounts = salesOrderRepository.findSODiscountSONo(salesOrderNo);
			// for each, calculate discount based on subtotalAllItem

			salesOrderTotal = calculateSOTotalAfterDiscount(subtotalAllItem, salesOrderDiscounts);
			System.out.println("Sales Order Total SO [ " + salesOrderNo + " ] : " + salesOrderTotal);

			// masukin ke format SONumber|SODate, SOTotal
//			SalesOrderTransaction a = entry.getValue().get(0);
			SalesOrderTransaction formatResult = soEntry.getValue().get(0);
			String soDate = formatResult.getCreationDate();
			Double revenues = salesOrderTotal;
			String key = salesOrderNo + "|" + soDate;
			if (revenueBySO.containsKey(key)) {
				Double prevRev = revenueBySO.get(key);
				Double currRev = prevRev + revenues;
				revenueBySO.put(key, currRev);
			} else {
				revenueBySO.put(key, revenues);
			}
			System.out.println("NANANANANA : " + revenueBySO.toString());
		}

//		return soTotalFinal;

		// calculate revenue per sales order
		/*
		 * for(SalesOrderTransaction salesOrder : salesOrders) { Long soDate =
		 * salesOrder.getSubmittedTime(); String soNo = salesOrder.getSalesOrderNo();
		 * Double revenue = salesOrderTotal; // Double revenue =
		 * calculateRevenue(salesOrder.getClientId(), salesOrder.); String key = soNo +
		 * "|" + soDate; if(revenueBySO.containsKey(key)) { Double prevRev =
		 * revenueBySO.get(key); Double currRev = prevRev + revenue;
		 * revenueBySO.put(key, currRev); } else { revenueBySO.put(key, revenue); } //
		 * System.out.println("NANANANANA : " + revenueBySO.toString()); }
		 */

		return getRevenueByDate(revenueBySO);

	}

	public String calculateFindByFilterString(String idClient, String startTime, String endTime) throws ParseException {
		// TODO Auto-generated method stub
//		getRevenueBySO(idClient, startTime, endTime);
//		getRevenueByDate(getRevenueBySO(idClient, startTime, endTime));
		return totalSalesRevenuePerFilter(getRevenueByDate(getRevenueBySOString(idClient, startTime, endTime)));
	}

	/*
	 * public void calculateFindByDateStringPlis(String idClient, String startTime,
	 * String endTime, String viewBy) throws ParseException {
	 * System.out.println("SO BY DATE PLIS"); Map<String, Double> revenueBySO =
	 * getRevenueBySOString(idClient, startTime, endTime); Map<String, Double>
	 * revenueByDate = getRevenueByDate(revenueBySO); //get total revenue Double
	 * totalRevenue = calculateFindByFilterString(idClient, startTime, endTime);
	 * //get xAxist List<String> xAxist = getAxistXOXO(startTime, endTime, viewBy);
	 * //get all revenue by date from all xAxist Map<String, Double>
	 * allRevenueByDate = new TreeMap<String, Double>(); // String date =
	 * revenueByDate.entrySet() for(Map.Entry<String, Double> entry :
	 * revenueByDate.entrySet()) { for(String xAxistt : xAxist) {
	 * if(entry.getKey().equals(xAxistt)) { allRevenueByDate.put(entry.getKey(),
	 * entry.getValue()); } else { allRevenueByDate.put(xAxistt, 0D); } }
	 * 
	 * } System.out.println("All revenue by date : " + allRevenueByDate);
	 * 
	 * }
	 */

	/*
	 * public List<String> getAxistXOXO(String startTime, String endTime, String
	 * viewBy) throws ParseException { String startDate =
	 * removeSixLastChar(startTime); String endDate = removeSixLastChar(endTime);
	 * System.out.println("start date : " + startDate); String formattedStartDate =
	 * formattedDate(startDate); System.out.println("formatted start Date : " +
	 * formattedStartDate);
	 * 
	 * System.out.println("end date : " + endDate); String formattedEndDate =
	 * formattedDate(endDate); System.out.println("formatted end Date : " +
	 * formattedEndDate);
	 * 
	 * System.out.println("GET X AXIST : " + getXAxis(formattedStartDate,
	 * formattedEndDate, viewBy));
	 * 
	 * Map<String, Double> revenueAllDate = new TreeMap<>(); Map<String, Double>
	 * revenueByDate = salesOvertimeService.getRevenueByDate(revenueBySO) return
	 * getXAxis(formattedStartDate, formattedEndDate, viewBy); }
	 */

	////////////// ADD HIREARCHY
	public Map<String, Double> getRevenueBySOStringHirarki(String clientId, String username, String startTime,
			String endTime) throws ParseException {
		Map<String, Double> revenueBySO = new HashMap<>();
		Map<Integer, Double> revenueByDates = new HashMap<>();

		demoTest.test1();
//		getRevenueByProductInsightHirarki(clientId, username, startTime, endTime);

		/// HIRARKI
//		Integer parentAdvocate = salesOrderRepository.getUserTypeId("advo.support");
		Integer parentAdvocate = salesOrderRepository.getUserTypeId(username);
		System.out.println("PARENT ADVOCATE from user type id : " + parentAdvocate);
		String advocateType = salesOrderRepository.getAdvocateType(parentAdvocate, clientId);
		System.out.println("PARENT ADVOCATE TYPE : " + advocateType);

		List<SalesOrderTransaction> salesOrders = new ArrayList<SalesOrderTransaction>();
		List<Integer> clientIdInParentAdvocate = new ArrayList<Integer>();

		if (advocateType.equals(principal)) {
			System.out.println("MASUK KE MODE PRINCIPAL");
//			salesOrders = salesOrderRepository.findByClientAndDateString(clientId, startTime, endTime);
			clientIdInParentAdvocate = salesOrderRepository.findAllAdvocatesIdByClientId(clientId);
			System.out.println("LIST ADVOCATE ID IN MODE PCP : " + clientIdInParentAdvocate.toString());
			System.out.println("LIST ADVOCATE ID IN MODE PCP SIZE : " + clientIdInParentAdvocate.size());

			System.out.println("FIND BY CLIENT AND DATE STRING SIZE Hirarki MODE PCP : " + salesOrderRepository
					.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime, clientIdInParentAdvocate)
					.size());

			salesOrders = salesOrderRepository.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime,
					clientIdInParentAdvocate);

		} else {
			System.out.println("MASUK KE MODE NON PRINCIPAL");

			clientIdInParentAdvocate = getResultListAdvocate(parentAdvocate, clientId);
			System.out.println("LIST CLIENT ID IN PARENT ADVOCATE : " + clientIdInParentAdvocate.toString());
			System.out.println("LIST CLIENT ID IN PARENT ADVOCATE SIZE : " + clientIdInParentAdvocate.size());

			System.out.println("ID CLIENT SERVICE : " + clientId);
			System.out.println("START TIME SERVICE : " + startTime);
			System.out.println("END TIME SERVICE : " + endTime);
			System.out.println("FIND BY CLIENT AND DATE STRING SIZE Hirarki : " + salesOrderRepository
					.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime, clientIdInParentAdvocate).size());

			salesOrders = salesOrderRepository.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime,
					clientIdInParentAdvocate);

		}

		// Print the name from the list....
		for (SalesOrderTransaction model : salesOrders) {
			System.out.println(
					"SONo from HASIL QUERY Hirarki : " + model.getSalesOrderNo() + "|" + model.getCreationDate());
		}
		System.out.println("HASIL QUERY FIND BY CLIENT AND DATE Hirarki : " + salesOrders.toString());

		Map<String, List<SalesOrderTransaction>> soMap = salesOrders.stream()
				.collect(Collectors.groupingBy(SalesOrderTransaction::getSalesOrderNo, Collectors.toList()));

		Double salesOrderTotal = 0D;

		for (Map.Entry<String, List<SalesOrderTransaction>> soEntry : soMap.entrySet()) {
			Map<Integer, List<SalesOrderTransaction>> soiSeqMap = soEntry.getValue().stream()
					.collect(Collectors.groupingBy(SalesOrderTransaction::getSoiSeq, Collectors.toList()));

			Double subtotalAllItem = 0D;

			System.out.println("soiSeqMap size : " + soiSeqMap.size());
			if (soiSeqMap.equals(null) && soiSeqMap.isEmpty()) {
				System.out.println("Soiseqmap null");
			}

			for (Map.Entry<Integer, List<SalesOrderTransaction>> entry : soiSeqMap.entrySet()) {
				Integer soiSeq = entry.getKey();
				System.out.println("soiSeq now: " + soiSeq);
				List<SalesOrderTransaction> siuSalesOrders = entry.getValue().stream()
						.filter(a -> "SIU".equals(a.getDiscountLevel())).collect(Collectors.toList());

				BigDecimal siuDiscount = BigDecimal.ZERO;
				Double finalPrice = 0D;
				Integer qty = 0;
				Double subTotal = 0D;

				System.out.println("siuSalesOrders size : " + siuSalesOrders.size());
				if (!siuSalesOrders.equals(null) && !siuSalesOrders.isEmpty()) {
					// calculate discount - sub total level
					Pair<Double, Integer> siuAfterDiscountPair = calculateSiuAfterDiscount(siuSalesOrders);
					subTotal = siuAfterDiscountPair.getFirst() * siuAfterDiscountPair.getSecond();
				} else {
					System.out.println("siuSalesOrders null");
					SalesOrderTransaction a = entry.getValue().get(0);
					finalPrice = a.getPrice();
//					qty = a.getQuantity();
					qty = a.getFulfilledQuantity();
					System.out.println("finalPrice before cal : " + finalPrice);
					System.out.println("Quantity before cal : " + qty);
					System.out.println("subtotal before cal : " + subTotal);
					subTotal = finalPrice * qty;
					System.out.println("subtotal after cal : " + subTotal);
				}

				List<SalesOrderTransaction> sisSalesOrders = entry.getValue().stream()
						.filter(a -> "SIS".equals(a.getDiscountLevel())).collect(Collectors.toList());

				System.out.println("sisSalesOrders size : " + sisSalesOrders.size());
				if (!sisSalesOrders.equals(null) && !sisSalesOrders.isEmpty()) {
					Double sisAfterDiscount = calculateSisAfterDiscount(subTotal, sisSalesOrders);
					subtotalAllItem = subtotalAllItem + sisAfterDiscount;
				} else {
					System.out.println("sisSalesOrders null");
					System.out.println("subtotalAll item prev : " + subtotalAllItem);
					subtotalAllItem = subtotalAllItem + subTotal;
					System.out.println("subtotalAll item curr : " + subtotalAllItem);
				}
			}

			// get sales order discount
			String salesOrderNo = soEntry.getKey();
			System.out.println("SALES ORDER NUMBER MARIA: " + salesOrderNo);
			System.out.println("Sub total All Item SO [ " + salesOrderNo + " ] : " + subtotalAllItem);
//			List<SalesOrderDiscounts> salesOrderDiscounts = salesOrderRepository.findSODiscountSONo(salesOrderNo);
			List<SalesOrderDiscounts> salesOrderDiscounts = salesOrderRepository
					.findSODiscountSONoHierarchy(salesOrderNo, clientIdInParentAdvocate);
			// for each, calculate discount based on subtotalAllItem

			System.out.println("SOD SIZE : " + salesOrderDiscounts.size());
			salesOrderTotal = calculateSOTotalAfterDiscount(subtotalAllItem, salesOrderDiscounts);
			System.out.println("Sales Order Total SO [ " + salesOrderNo + " ] : " + salesOrderTotal);

			// masukin ke format SONumber|SODate, SOTotal
//			SalesOrderTransaction a = entry.getValue().get(0);
			SalesOrderTransaction formatResult = soEntry.getValue().get(0);
			String soDate = formatResult.getCreationDate();
			Double revenues = salesOrderTotal;
			String key = salesOrderNo + "|" + soDate;
			if (revenueBySO.containsKey(key)) {
				Double prevRev = revenueBySO.get(key);
				Double currRev = prevRev + revenues;
				revenueBySO.put(key, currRev);
			} else {
				revenueBySO.put(key, revenues);
			}
			System.out.println("NANANANANA Hirarki : " + revenueBySO.toString());
		}

		return getRevenueByDate(revenueBySO);

	}

	private List<Integer> getResultListAdvocate(Integer parentAdvocate, String clientId) {
		Deque<Integer> finalQueue = new ArrayDeque<>();
		Deque<Integer> queue = findByParent(parentAdvocate, clientId);

		finalQueue.push(parentAdvocate);

		while (!queue.isEmpty()) {
			Integer value = queue.pop();
			finalQueue.push(value);

			Deque<Integer> byParent = findByParent(value, clientId);
			while (!byParent.isEmpty()) {
				queue.push(byParent.pop());
			}
		}

		// result untuk loop as criteria query param
		List<Integer> result = new ArrayList<>(finalQueue);
		Collections.sort(result);
		System.out.println("List integer result : " + result.toString());
		System.out.println("List integer result size : " + result.size());

		// buang yang perlu
		// result.remove(99978);

		return result;
	}

	private Deque<Integer> findByParent(Integer parentAdvocate, String clientId) {
		Map<Integer, Integer> clientAdvoMap = getClientAdvocateMap(clientId);

		System.out.println("CLIENT ADVO MAP BEFORE : " + clientAdvoMap.toString());

		Deque<Integer> queue = new ArrayDeque<>();

		for (Map.Entry<Integer, Integer> entry : clientAdvoMap.entrySet()) {
			if (entry.getValue().equals(parentAdvocate) && !entry.getKey().equals(parentAdvocate)) {
				queue.push(entry.getKey());
				System.out.println("QUEUE PUSH : " + queue);
			}
		}

		queue.forEach(clientAdvoMap::remove);

		System.out.println("Return QUEUE : " + queue);
		return queue;
	}

	private Map<Integer, Integer> getClientAdvocateMap(String clientId) {
		Map<Integer, Integer> clientAdvocateMap = new HashMap<>();
		List<ClientAdvocates> clientAdvocates = salesOrderRepository.findAllAdvocatesByClientId(clientId);
		System.out.println("CLient advocate size : " + clientAdvocates.size());
		for (ClientAdvocates advocate : clientAdvocates) {
			System.out.println("Client advocate id " + advocate.getAdvocateId() + " has parent advocate "
					+ advocate.getParentAdvocate());
			clientAdvocateMap.put(advocate.getAdvocateId(), advocate.getParentAdvocate());
		}
		System.out.println("From all map client advocate : " + clientAdvocateMap.toString());
		return clientAdvocateMap;
	}

	public String calculateFindByFilterStringHirarki(String idClient, String username, String startTime, String endTime)
			throws ParseException {
		return totalSalesRevenuePerFilter(getRevenueBySOStringHirarki(idClient, username, startTime, endTime));
	}

	// remake for general net per line
	public Map<String, Double> getRevenueByProductInsightHirarki(String clientId, String username, String startTime,
			String endTime, String viewBy) throws ParseException {
		Map<String, Double> subtotalAfterSis = new TreeMap<>();
		Map<String, Double> subtotalAllItemBeforeDisc3 = new TreeMap<>();

		Comparator<String> compString = Comparator.comparing(String::toString);

		Map<String, Double> disc3Perline = new TreeMap<>(compString);
		Map<String, Double> nettPerline = new TreeMap<>(compString);

		/// HIRARKI
		Integer parentAdvocate = salesOrderRepository.getUserTypeId(username);
		System.out.println("PARENT ADVOCATE from user type id : " + parentAdvocate);
		String advocateType = salesOrderRepository.getAdvocateType(parentAdvocate, clientId);
		System.out.println("PARENT ADVOCATE TYPE : " + advocateType);

		List<SalesOrderTransaction> salesOrders = new ArrayList<SalesOrderTransaction>();
		List<Integer> clientIdInParentAdvocate = new ArrayList<Integer>();

		if (advocateType.equals(principal)) {
			System.out.println("MASUK KE MODE PRINCIPAL");
			clientIdInParentAdvocate = salesOrderRepository.findAllAdvocatesIdByClientId(clientId);
			System.out.println("LIST ADVOCATE ID IN MODE PCP : " + clientIdInParentAdvocate.toString());
			System.out.println("LIST ADVOCATE ID IN MODE PCP SIZE : " + clientIdInParentAdvocate.size());

			System.out.println("FIND BY CLIENT AND DATE STRING SIZE Hirarki MODE PCP : " + salesOrderRepository
					.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime, clientIdInParentAdvocate)
					.size());

			salesOrders = salesOrderRepository.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime,
					clientIdInParentAdvocate);

		} else {
			System.out.println("MASUK KE MODE NON PRINCIPAL");

			clientIdInParentAdvocate = getResultListAdvocate(parentAdvocate, clientId);
			System.out.println("LIST CLIENT ID IN PARENT ADVOCATE : " + clientIdInParentAdvocate.toString());
			System.out.println("LIST CLIENT ID IN PARENT ADVOCATE SIZE : " + clientIdInParentAdvocate.size());

			System.out.println("ID CLIENT SERVICE : " + clientId);
			System.out.println("START TIME SERVICE : " + startTime);
			System.out.println("END TIME SERVICE : " + endTime);
			System.out.println("FIND BY CLIENT AND DATE STRING SIZE Hirarki : " + salesOrderRepository
					.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime, clientIdInParentAdvocate)
					.size());

			salesOrders = salesOrderRepository.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime,
					clientIdInParentAdvocate);

		}

		// Print the name from the list....
		for (SalesOrderTransaction model : salesOrders) {
			System.out.println(
					"SONo from HASIL QUERY Hirarki : " + model.getSalesOrderNo() + "|" + model.getCreationDate());
		}

		Map<String, List<SalesOrderTransaction>> soMap = salesOrders.stream()
				.collect(Collectors.groupingBy(SalesOrderTransaction::getSalesOrderNo, Collectors.toList()));

		Double salesOrderTotal = 0D;
		Double discount3Total = 0D;

		for (Map.Entry<String, List<SalesOrderTransaction>> soEntry : soMap.entrySet()) {
			Map<Integer, List<SalesOrderTransaction>> soiSeqMap = soEntry.getValue().stream()
					.collect(Collectors.groupingBy(SalesOrderTransaction::getSoiSeq, Collectors.toList()));

			Double subtotalAllItem = 0D;
			Double subtotalPerProduct = 0D;
			Double sumSubtotalBeforeDisc3 = 0D;
			Double subtotalBeforeDisc3 = 0D;

			System.out.println("soiSeqMap size : " + soiSeqMap.size());
			if (soiSeqMap.equals(null) && soiSeqMap.isEmpty()) {
				System.out.println("Soiseqmap null");
			}

			for (Map.Entry<Integer, List<SalesOrderTransaction>> entry : soiSeqMap.entrySet()) {
				Integer soiSeq = entry.getKey();
				System.out.println("soiSeq now: " + soiSeq);
				List<SalesOrderTransaction> siuSalesOrders = entry.getValue().stream()
						.filter(a -> "SIU".equals(a.getDiscountLevel())).collect(Collectors.toList());

				BigDecimal siuDiscount = BigDecimal.ZERO;
				Double finalPrice = 0D;
				Integer qty = 0;
				Double subTotal = 0D;

				System.out.println("siuSalesOrders size : " + siuSalesOrders.size());
				if (!siuSalesOrders.equals(null) && !siuSalesOrders.isEmpty()) {
					// calculate discount - sub total level
					Pair<Double, Integer> siuAfterDiscountPair = calculateSiuAfterDiscount(siuSalesOrders);
					subTotal = siuAfterDiscountPair.getFirst() * siuAfterDiscountPair.getSecond();
				} else {
					System.out.println("siuSalesOrders null");
					SalesOrderTransaction a = entry.getValue().get(0);
					finalPrice = a.getPrice();
//					qty = a.getQuantity();
					qty = a.getFulfilledQuantity();
					System.out.println("finalPrice before cal : " + finalPrice);
					System.out.println("Quantity before cal : " + qty);
					System.out.println("subtotal before cal siu : " + subTotal);
					subTotal = finalPrice * qty;
					System.out.println("subtotal after cal siu : " + subTotal);
				}

				List<SalesOrderTransaction> sisSalesOrders = entry.getValue().stream()
						.filter(a -> "SIS".equals(a.getDiscountLevel())).collect(Collectors.toList());

				System.out.println("sisSalesOrders size : " + sisSalesOrders.size());
				if (!sisSalesOrders.equals(null) && !sisSalesOrders.isEmpty()) {
					Double sisAfterDiscount = calculateSisAfterDiscount(subTotal, sisSalesOrders);
					subtotalPerProduct = sisAfterDiscount;
					subtotalAllItem = subtotalAllItem + sisAfterDiscount;
				} else {
					System.out.println("sisSalesOrders null");
					System.out.println("subtotalPerProduct item prev sis : " + subtotalPerProduct);
					System.out.println("subtotalAll item prev sis : " + subtotalAllItem);
					subtotalAllItem = subtotalAllItem + subTotal;
					subtotalPerProduct = subTotal;
					System.out.println("subtotalPerProduct item curr sis : " + subtotalPerProduct);
					System.out.println("subtotalAll item curr sis : " + subtotalAllItem);
				}

				String salesOrderNo = soEntry.getKey();
				SalesOrderTransaction formatResult = entry.getValue().get(0);
				String soDate = formatResult.getCreationDate();
				String productCode = formatResult.getProductCode();
				String productGroup = formatResult.getProductGroupName();
				//case if productGroupName blank space ex. SONo SO-NFI-004540-csaserang1-02112019-115147731 Nutrifood
				if(productGroup.isEmpty()) {
					productCode = "NoNameProductGroup";
				}
				String brand = formatResult.getBrandName();
				String key = null;
				
				if(viewBy.equals("SKU")) {
					key = generateKeyProductInsight(salesOrderNo, soDate, productCode);
				}
				else if(viewBy.equals("productGroup")) {
					key = generateKeyProductInsight(salesOrderNo, soDate, productGroup);
				}
				else if(viewBy.equals("brand")) {
					key = generateKeyProductInsight(salesOrderNo, soDate, brand);
				}
				
				System.out.println("Key product insight : " + key);
				
				if (subtotalAfterSis.containsKey(key)) {
					Double prevRev = subtotalAfterSis.get(key);
					Double currRev = prevRev + subtotalPerProduct;
					subtotalAfterSis.put(key, currRev);
					subtotalBeforeDisc3 = currRev;
				} else {
					subtotalAfterSis.put(key, subtotalPerProduct);
					subtotalBeforeDisc3 = subtotalPerProduct;
				}
				System.out.println("Product Insight Subtotal After SIS Hirarki : " + subtotalAfterSis.toString());

			}

			String salesOrderNo = soEntry.getKey();
			SalesOrderTransaction formatResult = soEntry.getValue().get(0);
			String soDate = formatResult.getCreationDate();
			String keyBeforeDist3 = salesOrderNo + "|" + soDate;
			if (subtotalAllItemBeforeDisc3.containsKey(keyBeforeDist3)) {
				Double prevRev = subtotalAllItemBeforeDisc3.get(keyBeforeDist3);
				System.out.println("Prev Rev : " + prevRev);
				Double currRev = prevRev + subtotalAllItem;
				System.out.println("Curr Rev : " + currRev);
				subtotalAllItemBeforeDisc3.put(keyBeforeDist3, currRev);
				sumSubtotalBeforeDisc3 = currRev;
			} else {
				System.out.println("subtotal all item in map : " + subtotalAllItem);
				subtotalAllItemBeforeDisc3.put(keyBeforeDist3, subtotalAllItem);
				sumSubtotalBeforeDisc3 = subtotalAllItem;
			}
			System.out.println("Sales Order : " + salesOrderNo + " has sum subtotal before disc level 3 : "
					+ sumSubtotalBeforeDisc3);
			System.out.println("SubtotalBeforeDISC3 Hirarki : " + subtotalAllItemBeforeDisc3.toString());

			System.out.println("SALES ORDER NUMBER MARIA: " + salesOrderNo);
			System.out.println("Sub total All Item SO [ " + salesOrderNo + " ] : " + sumSubtotalBeforeDisc3);
			List<SalesOrderDiscounts> salesOrderDiscounts = salesOrderRepository
					.findSODiscountSONoHierarchy(salesOrderNo, clientIdInParentAdvocate);

			System.out.println("SOD SIZE : " + salesOrderDiscounts.size());
			salesOrderTotal = calculateSOTotalAfterDiscount(sumSubtotalBeforeDisc3, salesOrderDiscounts);
			System.out.println("Sales Order Total SO [ " + salesOrderNo + " ] : " + salesOrderTotal);

			discount3Total = calculateTotalDiscount(sumSubtotalBeforeDisc3, salesOrderDiscounts);
			System.out.println("discountTotalnya Disc3 " + salesOrderNo + "adalah : " + discount3Total);

//			Comparator<String> compString = Comparator.comparing(String::toString);
//
//			Map<String, Double> disc3Perline = new TreeMap<>(compString);
//			Map<String, Double> nettPerline = new TreeMap<>(compString);

			for (Map.Entry<String, Double> entry : subtotalAfterSis.entrySet()) {
				System.out.println("Entry dot key : " + entry.getKey());
				System.out.println("Entry dot value : " + entry.getValue());

				String keyPerlineSO = entry.getKey();
				Double revenueDisc3Perline = 0D;
				Double revenueNettPerline = 0D;

				if (keyPerlineSO.contains(salesOrderNo)) {
					Double subTotalPerline = entry.getValue();
					System.out.println("subTotalPerline " + keyPerlineSO + " : " + subTotalPerline);
					System.out.println("sumSubtotalBeforeDisc3 " + salesOrderNo + " : " + sumSubtotalBeforeDisc3);
					Double sumSubtotalPerSO = sumSubtotalBeforeDisc3;
					System.out.println("sumSubtotalPerSO : " + sumSubtotalPerSO);

					revenueDisc3Perline = calculateDisc3Perline(subTotalPerline, sumSubtotalPerSO, discount3Total);
					System.out.println("revenueDisc3Perline INVESTIGATE KEYPERLINESO " + keyPerlineSO + " : " + revenueDisc3Perline);
					revenueNettPerline = calculateNettPerlineSO(subTotalPerline, revenueDisc3Perline);
				}

				if (disc3Perline.containsKey(keyPerlineSO)) {
					Double prevRev = disc3Perline.get(keyPerlineSO);
					Double currRev = prevRev + revenueDisc3Perline;
					disc3Perline.put(keyPerlineSO, currRev);
				} else {
					disc3Perline.put(keyPerlineSO, revenueDisc3Perline);
				}

				if (nettPerline.containsKey(keyPerlineSO)) {
					Double prevRevNett = nettPerline.get(keyPerlineSO);
					Double currRevNett = prevRevNett + revenueNettPerline;
					System.out.println("KEY INVESTIGATE KEYNETTPERLINESO " + keyPerlineSO + " : " + currRevNett);
					nettPerline.put(keyPerlineSO, currRevNett);
//					System.out.println("AWALAN NETT PER LINE MAP : " + nettPerline.toString());
				} else {
					System.out.println("KEY INVESTIGATE KEYNETTPERLINESO " + keyPerlineSO + " : " + revenueNettPerline);
					nettPerline.put(keyPerlineSO, revenueNettPerline);
//					if(keyPerlineSO.contains("SO-NFI-004540-csaserang1-02112019-115147731|2019-11-02")) {
//						System.out.println("NETT PER LINE AWALAN MAP : " + nettPerline.toString());
//					}
				}

			}
			System.out.println("DISC3 PER LINE MAP : " + disc3Perline.toString());
			System.out.println("NETT PER LINE MAP : " + nettPerline.toString());
		}

		return getRevenueByProduct(nettPerline);
//		return getRevenueByDate(revenueBySO);

	}

	private String generateKeyProductInsight(String salesOrderNo, String soDate, String viewBy) {
		String keyProductInsight;
		return keyProductInsight = salesOrderNo + "|" + soDate + "|" + viewBy;
//		if(viewBy.equals("SKU")) {
//			System.out.println("Productduct " + salesOrderNo + "|" + soDate + "|" + viewBy);
//			keyProductInsight = salesOrderNo + "|" + soDate + "|" + viewBy;
//		}
	}

	private Map<String, Double> getRevenueByProduct(Map<String, Double> nettPerline) throws ParseException {
		Comparator<String> compString = Comparator.comparing(String::toString);

		Map<String, Double> revenueByProduct = new TreeMap<>(compString);
		// calculate revenue per date

		for (Map.Entry<String, Double> entry : nettPerline.entrySet()) {
			System.out.println("Entry dot key : " + entry.getKey());
			System.out.println("Entry dot value : " + entry.getValue());
//			String products = getProduct(entry.getKey());
			String products = getSplit(entry.getKey(), 2);
			System.out.println("products : " + products);

			/*
			 * String dates = getDate(entry.getKey()); System.out.println("dates : " +
			 * dates); //comment because format date changed (submitted_date to
			 * creation_date)
			 * 
			 * String date = StringUtils.removeSixLastChar(dates);
			 * System.out.println("date : " + date);
			 * 
			 * String formattedDate = DateTimeHelper.formattedDate(dates);
			 * System.out.println("formatted Date : " + formattedDate);
			 */
			Double revenue = entry.getValue();
//			Double revenueBulat = (double) Math.round(revenue);
//			Double revenue = revenueBySO.get(key);
			if (revenueByProduct.containsKey(products)) {
				Double prevRev = revenueByProduct.get(products);
				Double currRev = prevRev + revenue;
				revenueByProduct.put(products, currRev);
			} else {
				revenueByProduct.put(products, revenue);
			}
		}
		System.out.println("REVENUE BY PRODUCT : " + revenueByProduct.toString());
		return revenueByProduct;
	}

	private String getSplit(String key, int i) {
		String[] parts = key.split("|s");
		List<String> strings = Pattern.compile("\\|").splitAsStream(key).collect(Collectors.toList());
//		System.out.println("split nol : " + strings.get(0));
//		System.out.println("split satu : " + strings.get(1));
//		System.out.println("split dua : " + strings.get(2));
		return strings.get(i);
	}

	private String getProduct(String key) {
		String[] parts = key.split("|s");
		List<String> strings = Pattern.compile("\\|").splitAsStream(key).collect(Collectors.toList());
		System.out.println("split nol : " + strings.get(0));
		System.out.println("split satu : " + strings.get(1));
		return strings.get(1);
	}

	private Double calculateNettPerlineSO(Double subTotalPerline, Double revenueDisc3Perline) {
		Double nettPerlineSO = 0D;
		System.out.println("subTotalPerline calculateNettPerlineSO param : " + subTotalPerline);
		System.out.println("revenueDisc3Perline calculateNettPerlineSO param : " + revenueDisc3Perline);
		nettPerlineSO = subTotalPerline - revenueDisc3Perline;
		System.out.println("nettPerlineSO calculateNettPerlineSO return : " + nettPerlineSO);
		return nettPerlineSO;
	}

	private Double calculateDisc3Perline(Double subTotalPerline, Double sumSubtotalPerSO, Double discountTotal) {
		Double disc3Perline = 0D;
		System.out.println("subTotalPerline param : " + subTotalPerline);
		System.out.println("sumSubtotalPerSo param : " + sumSubtotalPerSO);
		System.out.println("discountTotal param : " + discountTotal);
		disc3Perline = ((subTotalPerline / sumSubtotalPerSO)) * discountTotal;
		if(disc3Perline.equals(Double.NaN)) {
			disc3Perline = 0D;
		}
		System.out.println("disc3Perline in calculateDisc3Perline : " + disc3Perline);
		return disc3Perline;
	}

	public String calculateRevenueByProductInsightHirarki(String idClient, String username, String startTime,
			String endTime, String viewBy) throws ParseException {
		return totalSalesRevenuePerFilter(getRevenueByProductInsightHirarki(idClient, username, startTime, endTime, viewBy));
	}
	
	//for product insight qty fulfilled
	public Map<String, Integer> getProductInsightByQtyFulfilledHirarki(String clientId, String username, String startTime,
			String endTime, String viewBy) throws ParseException {
		Map<String, Integer> qtyPerline = new TreeMap<>();

		Comparator<String> compString = Comparator.comparing(String::toString);

		/// HIRARKI
		Integer parentAdvocate = salesOrderRepository.getUserTypeId(username);
		System.out.println("PARENT ADVOCATE from user type id : " + parentAdvocate);
		String advocateType = salesOrderRepository.getAdvocateType(parentAdvocate, clientId);
		System.out.println("PARENT ADVOCATE TYPE : " + advocateType);

		List<SalesOrderTransaction> salesOrders = new ArrayList<SalesOrderTransaction>();
		List<Integer> clientIdInParentAdvocate = new ArrayList<Integer>();

		if (advocateType.equals(principal)) {
			System.out.println("MASUK KE MODE PRINCIPAL");
			clientIdInParentAdvocate = salesOrderRepository.findAllAdvocatesIdByClientId(clientId);
			System.out.println("LIST ADVOCATE ID IN MODE PCP : " + clientIdInParentAdvocate.toString());
			System.out.println("LIST ADVOCATE ID IN MODE PCP SIZE : " + clientIdInParentAdvocate.size());

			System.out.println("FIND BY CLIENT AND DATE STRING SIZE Hirarki MODE PCP : " + salesOrderRepository
					.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime, clientIdInParentAdvocate)
					.size());

			salesOrders = salesOrderRepository.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime,
					clientIdInParentAdvocate);

		} else {
			System.out.println("MASUK KE MODE NON PRINCIPAL");

			clientIdInParentAdvocate = getResultListAdvocate(parentAdvocate, clientId);
			System.out.println("LIST CLIENT ID IN PARENT ADVOCATE : " + clientIdInParentAdvocate.toString());
			System.out.println("LIST CLIENT ID IN PARENT ADVOCATE SIZE : " + clientIdInParentAdvocate.size());

			System.out.println("ID CLIENT SERVICE : " + clientId);
			System.out.println("START TIME SERVICE : " + startTime);
			System.out.println("END TIME SERVICE : " + endTime);
			System.out.println("FIND BY CLIENT AND DATE STRING SIZE Hirarki : " + salesOrderRepository
					.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime, clientIdInParentAdvocate)
					.size());

			salesOrders = salesOrderRepository.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime,
					clientIdInParentAdvocate);

		}

		// Print the name from the list....
		for (SalesOrderTransaction model : salesOrders) {
			System.out.println(
					"SONo from HASIL QUERY Hirarki : " + model.getSalesOrderNo() + "|" + model.getCreationDate());
		}

		Map<String, List<SalesOrderTransaction>> soMap = salesOrders.stream()
				.collect(Collectors.groupingBy(SalesOrderTransaction::getSalesOrderNo, Collectors.toList()));

		for (Map.Entry<String, List<SalesOrderTransaction>> soEntry : soMap.entrySet()) {
			Map<Integer, List<SalesOrderTransaction>> soiSeqMap = soEntry.getValue().stream()
					.collect(Collectors.groupingBy(SalesOrderTransaction::getSoiSeq, Collectors.toList()));

			System.out.println("soiSeqMap size : " + soiSeqMap.size());
			if (soiSeqMap.equals(null) && soiSeqMap.isEmpty()) {
				System.out.println("Soiseqmap null");
			}

			for (Map.Entry<Integer, List<SalesOrderTransaction>> entry : soiSeqMap.entrySet()) {
				Integer soiSeq = entry.getKey();
				System.out.println("soiSeq now: " + soiSeq);

				String salesOrderNo = soEntry.getKey();
				SalesOrderTransaction formatResult = entry.getValue().get(0);
				String soDate = formatResult.getCreationDate();
				String productCode = formatResult.getProductCode();
				String productGroup = formatResult.getProductGroupName();
				Integer qtyFulfilled = formatResult.getFulfilledQuantity();
				//case if productGroupName blank space ex. SONo SO-NFI-004540-csaserang1-02112019-115147731 Nutrifood
				if(productGroup.isEmpty()) {
					productCode = "NoNameProductGroup";
				}
				String brand = formatResult.getBrandName();
				String key = null;
				
				if(viewBy.equals("SKU")) {
					key = generateKeyProductInsight(salesOrderNo, soDate, productCode);
				}
				else if(viewBy.equals("productGroup")) {
					key = generateKeyProductInsight(salesOrderNo, soDate, productGroup);
				}
				else if(viewBy.equals("brand")) {
					key = generateKeyProductInsight(salesOrderNo, soDate, brand);
				}
				
				System.out.println("Key product insight : " + key);
				
				if (qtyPerline.containsKey(key)) {
					Integer prevRev = qtyPerline.get(key);
					Integer currRev = prevRev + qtyFulfilled;
					qtyPerline.put(key, currRev);
//					subtotalBeforeDisc3 = currRev;
				} else {
					qtyPerline.put(key, qtyFulfilled);
//					subtotalBeforeDisc3 = subtotalPerProduct;
				}
				System.out.println("Product Insight Qty Perline Per-SO : " + qtyPerline.toString());

			}

			System.out.println("Product Insight Qty Perline SO : " + qtyPerline.toString());
		}

		return getProductInsightByProduct(qtyPerline);
//		return getRevenueByProduct(nettPerline);

	}

	private Map<String, Integer> getProductInsightByProduct(Map<String, Integer> qtyPerline) throws ParseException {
		Comparator<String> compString = Comparator.comparing(String::toString);

		Map<String, Integer> productInsightByProduct = new TreeMap<>(compString);
		// calculate revenue per date

		for (Map.Entry<String, Integer> entry : qtyPerline.entrySet()) {
			System.out.println("Entry dot key : " + entry.getKey());
			System.out.println("Entry dot value : " + entry.getValue());
//			String products = getProduct(entry.getKey());
			String products = getSplit(entry.getKey(), 2);
			System.out.println("products : " + products);

			/*
			 * String dates = getDate(entry.getKey()); System.out.println("dates : " +
			 * dates); //comment because format date changed (submitted_date to
			 * creation_date)
			 * 
			 * String date = StringUtils.removeSixLastChar(dates);
			 * System.out.println("date : " + date);
			 * 
			 * String formattedDate = DateTimeHelper.formattedDate(dates);
			 * System.out.println("formatted Date : " + formattedDate);
			 */
			Integer qtyFul = entry.getValue();
//			Double revenueBulat = (double) Math.round(revenue);
//			Double revenue = revenueBySO.get(key);
			if (productInsightByProduct.containsKey(products)) {
				Integer prevRev = productInsightByProduct.get(products);
				Integer currRev = prevRev + qtyFul;
				productInsightByProduct.put(products, currRev);
			} else {
				productInsightByProduct.put(products, qtyFul);
			}
		}
		System.out.println("PRODUCT INSIGHT BY PRODUCT ALL CALCULATION : " + productInsightByProduct.toString());
		return productInsightByProduct;
	}
	
	public String calculateProductInsightQtyFulfilledHirarki(String idClient, String username, String startTime,
			String endTime, String viewBy) throws ParseException {
		return totalQtyFulfilledPerFilter(getProductInsightByQtyFulfilledHirarki(idClient, username, startTime, endTime, viewBy));
	}
	
	private String totalQtyFulfilledPerFilter(Map<String, Integer> revenueByDates) {
		Integer totalQtyFulfilled = 0;

		for (Map.Entry<String, Integer> entry : revenueByDates.entrySet()) {
			Integer revenue = entry.getValue();
			System.out.println("qty fulfilled before : " + revenue);
			System.out.println("total qty fulfilled before : " + totalQtyFulfilled);
			totalQtyFulfilled = totalQtyFulfilled + revenue;
			System.out.println("total qty fulfilled after : " + totalQtyFulfilled);
		}
		System.out.println("Total qty fulfilled return : " + totalQtyFulfilled);

//		DecimalFormat df = new DecimalFormat("#.##");
//		System.out.println("HASIL DESIMAL FORMAT : " + df.format(totalQtyFulfilled));

		return Integer.toString(totalQtyFulfilled);
	}
	
	//for product insight scarcity level
	public Map<String, Double> getProductInsightByScarcityLevelHirarki(String clientId, String username, String startTime,
			String endTime, String viewBy) throws ParseException {
		Map<String, Double> qtyPerline = new TreeMap<>();

		Comparator<String> compString = Comparator.comparing(String::toString);

		/// HIRARKI
		Integer parentAdvocate = salesOrderRepository.getUserTypeId(username);
		System.out.println("PARENT ADVOCATE from user type id : " + parentAdvocate);
		String advocateType = salesOrderRepository.getAdvocateType(parentAdvocate, clientId);
		System.out.println("PARENT ADVOCATE TYPE : " + advocateType);

		List<SalesOrderTransaction> salesOrders = new ArrayList<SalesOrderTransaction>();
		List<Integer> clientIdInParentAdvocate = new ArrayList<Integer>();

		if (advocateType.equals(principal)) {
			System.out.println("MASUK KE MODE PRINCIPAL");
			clientIdInParentAdvocate = salesOrderRepository.findAllAdvocatesIdByClientId(clientId);
			System.out.println("LIST ADVOCATE ID IN MODE PCP : " + clientIdInParentAdvocate.toString());
			System.out.println("LIST ADVOCATE ID IN MODE PCP SIZE : " + clientIdInParentAdvocate.size());

			System.out.println("FIND BY CLIENT AND DATE STRING SIZE Hirarki MODE PCP : " + salesOrderRepository
					.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime, clientIdInParentAdvocate)
					.size());

			salesOrders = salesOrderRepository.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime,
					clientIdInParentAdvocate);

		} else {
			System.out.println("MASUK KE MODE NON PRINCIPAL");

			clientIdInParentAdvocate = getResultListAdvocate(parentAdvocate, clientId);
			System.out.println("LIST CLIENT ID IN PARENT ADVOCATE : " + clientIdInParentAdvocate.toString());
			System.out.println("LIST CLIENT ID IN PARENT ADVOCATE SIZE : " + clientIdInParentAdvocate.size());

			System.out.println("ID CLIENT SERVICE : " + clientId);
			System.out.println("START TIME SERVICE : " + startTime);
			System.out.println("END TIME SERVICE : " + endTime);
			System.out.println("FIND BY CLIENT AND DATE STRING SIZE Hirarki : " + salesOrderRepository
					.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime, clientIdInParentAdvocate)
					.size());

			salesOrders = salesOrderRepository.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime,
					clientIdInParentAdvocate);

		}

		// Print the name from the list....
		for (SalesOrderTransaction model : salesOrders) {
			System.out.println(
					"SONo from HASIL QUERY Hirarki : " + model.getSalesOrderNo() + "|" + model.getCreationDate());
		}

		Map<String, List<SalesOrderTransaction>> soMap = salesOrders.stream()
				.collect(Collectors.groupingBy(SalesOrderTransaction::getSalesOrderNo, Collectors.toList()));

		for (Map.Entry<String, List<SalesOrderTransaction>> soEntry : soMap.entrySet()) {
			Map<Integer, List<SalesOrderTransaction>> soiSeqMap = soEntry.getValue().stream()
					.collect(Collectors.groupingBy(SalesOrderTransaction::getSoiSeq, Collectors.toList()));

			System.out.println("soiSeqMap size : " + soiSeqMap.size());
			if (soiSeqMap.equals(null) && soiSeqMap.isEmpty()) {
				System.out.println("Soiseqmap null");
			}

			for (Map.Entry<Integer, List<SalesOrderTransaction>> entry : soiSeqMap.entrySet()) {
				Integer soiSeq = entry.getKey();
				System.out.println("soiSeq now: " + soiSeq);

				String salesOrderNo = soEntry.getKey();
				SalesOrderTransaction formatResult = entry.getValue().get(0);
				String soDate = formatResult.getCreationDate();
				String productCode = formatResult.getProductCode();
				String productGroup = formatResult.getProductGroupName();
				Integer qtyFulfilled = formatResult.getFulfilledQuantity();
				Integer qtyOrdered = formatResult.getQuantity();
				//case if productGroupName blank space ex. SONo SO-NFI-004540-csaserang1-02112019-115147731 Nutrifood
				if(productGroup.isEmpty()) {
					productCode = "NoNameProductGroup";
				}
				String brand = formatResult.getBrandName();
				String key = null;
				
				if(viewBy.equals("SKU")) {
					key = generateKeyProductInsight(salesOrderNo, soDate, productCode);
				}
				else if(viewBy.equals("productGroup")) {
					key = generateKeyProductInsight(salesOrderNo, soDate, productGroup);
				}
				else if(viewBy.equals("brand")) {
					key = generateKeyProductInsight(salesOrderNo, soDate, brand);
				}
				
				System.out.println("Key product insight : " + key);
				
				Double scarcityPerline;
				scarcityPerline = calculateScarcity(qtyOrdered, qtyFulfilled);
				System.out.println("Scarcity perline : " + scarcityPerline);
				
				if (qtyPerline.containsKey(key)) {
					Double prevRev = qtyPerline.get(key);
					Double currRev = prevRev + scarcityPerline;
					qtyPerline.put(key, currRev);
//					subtotalBeforeDisc3 = currRev;
				} else {
					qtyPerline.put(key, scarcityPerline);
//					subtotalBeforeDisc3 = subtotalPerProduct;
				}
				System.out.println("Product Insight Scarcity Level Per-SO : " + qtyPerline.toString());

			}

			System.out.println("Product Insight Scarcity Level SO : " + qtyPerline.toString());
		}

		return getProductInsightScarcityLevelByProduct(qtyPerline);
//		return getRevenueByProduct(nettPerline);

	}

	private Map<String, Double> getProductInsightScarcityLevelByProduct(Map<String, Double> qtyPerline) {
		Comparator<String> compString = Comparator.comparing(String::toString);

		Map<String, Double> productInsightByProduct = new TreeMap<>(compString);
		// calculate revenue per date

		for (Map.Entry<String, Double> entry : qtyPerline.entrySet()) {
			System.out.println("Entry dot key : " + entry.getKey());
			System.out.println("Entry dot value : " + entry.getValue());
//			String products = getProduct(entry.getKey());
			String products = getSplit(entry.getKey(), 2);
			System.out.println("products : " + products);

			/*
			 * String dates = getDate(entry.getKey()); System.out.println("dates : " +
			 * dates); //comment because format date changed (submitted_date to
			 * creation_date)
			 * 
			 * String date = StringUtils.removeSixLastChar(dates);
			 * System.out.println("date : " + date);
			 * 
			 * String formattedDate = DateTimeHelper.formattedDate(dates);
			 * System.out.println("formatted Date : " + formattedDate);
			 */
			Double scarcityLevel = entry.getValue();
//			Double revenueBulat = (double) Math.round(revenue);
//			Double revenue = revenueBySO.get(key);
			if (productInsightByProduct.containsKey(products)) {
				Double prevRev = productInsightByProduct.get(products);
				System.out.println("prevRev product insight scarcity level : " + prevRev);
				System.out.println("scarcityLevel product insight scarcity level : " + scarcityLevel);
				Double currRev = prevRev + scarcityLevel;
				System.out.println("currRev product insight scarcity level : " + currRev);
				productInsightByProduct.put(products, currRev);
			} else {
				productInsightByProduct.put(products, scarcityLevel);
			}
		}
		System.out.println("PRODUCT INSIGHT SCARCITY LEVEL BY PRODUCT ALL CALCULATION : " + productInsightByProduct.toString());
		return productInsightByProduct;
	}

	private Double calculateScarcity(Integer qtyOrdered, Integer qtyFulfilled) {
		System.out.println("qty ordered : " + qtyOrdered);
		System.out.println("qty fulfilled : " + qtyFulfilled);
		Double scarcityLevel;
		Integer substractionOrderedFulfilled;
		substractionOrderedFulfilled = qtyOrdered - qtyFulfilled;
		Double substractionOrderedFulfilledDouble = Double.valueOf(substractionOrderedFulfilled);
		if(substractionOrderedFulfilled==0) {
			System.out.println("scarcityLevel calculateScarcity : 0");
			return scarcityLevel = 0D;
		}else {
			System.out.println("masuk else hitungan");
			System.out.println("substractionOrderedFulfilled : " + substractionOrderedFulfilled);
			scarcityLevel = (substractionOrderedFulfilledDouble/qtyOrdered)*100;
			System.out.println("scarcityLevel : " + scarcityLevel);
			System.out.println("scarcityLevel calculateScarcityPerline : " + scarcityLevel);
			return scarcityLevel;
		}
	}
	
	//for scarcity level -> bring qtyOrdered and qty fulfilled as value map
	public Map<String, Double> getProductInsightByScarcityLevelHirarkiString(String clientId, String username, String startTime,
			String endTime, String viewBy) throws ParseException {
		Map<String, String> qtyPerline = new TreeMap<>();

		Comparator<String> compString = Comparator.comparing(String::toString);

		/// HIRARKI
		Integer parentAdvocate = salesOrderRepository.getUserTypeId(username);
		System.out.println("PARENT ADVOCATE from user type id : " + parentAdvocate);
		String advocateType = salesOrderRepository.getAdvocateType(parentAdvocate, clientId);
		System.out.println("PARENT ADVOCATE TYPE : " + advocateType);

		List<SalesOrderTransaction> salesOrders = new ArrayList<SalesOrderTransaction>();
		List<Integer> clientIdInParentAdvocate = new ArrayList<Integer>();

		if (advocateType.equals(principal)) {
			System.out.println("MASUK KE MODE PRINCIPAL");
			clientIdInParentAdvocate = salesOrderRepository.findAllAdvocatesIdByClientId(clientId);
			System.out.println("LIST ADVOCATE ID IN MODE PCP : " + clientIdInParentAdvocate.toString());
			System.out.println("LIST ADVOCATE ID IN MODE PCP SIZE : " + clientIdInParentAdvocate.size());

			System.out.println("FIND BY CLIENT AND DATE STRING SIZE Hirarki MODE PCP : " + salesOrderRepository
					.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime, clientIdInParentAdvocate)
					.size());

			salesOrders = salesOrderRepository.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime,
					clientIdInParentAdvocate);

		} else {
			System.out.println("MASUK KE MODE NON PRINCIPAL");

			clientIdInParentAdvocate = getResultListAdvocate(parentAdvocate, clientId);
			System.out.println("LIST CLIENT ID IN PARENT ADVOCATE : " + clientIdInParentAdvocate.toString());
			System.out.println("LIST CLIENT ID IN PARENT ADVOCATE SIZE : " + clientIdInParentAdvocate.size());

			System.out.println("ID CLIENT SERVICE : " + clientId);
			System.out.println("START TIME SERVICE : " + startTime);
			System.out.println("END TIME SERVICE : " + endTime);
			System.out.println("FIND BY CLIENT AND DATE STRING SIZE Hirarki : " + salesOrderRepository
					.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime, clientIdInParentAdvocate)
					.size());

			salesOrders = salesOrderRepository.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime,
					clientIdInParentAdvocate);

		}

		// Print the name from the list....
		for (SalesOrderTransaction model : salesOrders) {
			System.out.println(
					"SONo from HASIL QUERY Hirarki : " + model.getSalesOrderNo() + "|" + model.getCreationDate());
		}

		Map<String, List<SalesOrderTransaction>> soMap = salesOrders.stream()
				.collect(Collectors.groupingBy(SalesOrderTransaction::getSalesOrderNo, Collectors.toList()));

		for (Map.Entry<String, List<SalesOrderTransaction>> soEntry : soMap.entrySet()) {
			Map<Integer, List<SalesOrderTransaction>> soiSeqMap = soEntry.getValue().stream()
					.collect(Collectors.groupingBy(SalesOrderTransaction::getSoiSeq, Collectors.toList()));

			System.out.println("soiSeqMap size : " + soiSeqMap.size());
			if (soiSeqMap.equals(null) && soiSeqMap.isEmpty()) {
				System.out.println("Soiseqmap null");
			}

			for (Map.Entry<Integer, List<SalesOrderTransaction>> entry : soiSeqMap.entrySet()) {
				Integer soiSeq = entry.getKey();
				System.out.println("soiSeq now: " + soiSeq);

				String salesOrderNo = soEntry.getKey();
				SalesOrderTransaction formatResult = entry.getValue().get(0);
				String soDate = formatResult.getCreationDate();
				String productCode = formatResult.getProductCode();
				String productGroup = formatResult.getProductGroupName();
				Integer qtyFulfilled,qtyOrdered;
				qtyFulfilled = formatResult.getFulfilledQuantity();
				qtyOrdered = formatResult.getQuantity();
				if(qtyOrdered==qtyFulfilled || qtyFulfilled>qtyOrdered || qtyFulfilled==0) {
					System.out.println("in condition qtyOrdered==qtyFulfilled || qtyFulfilled>qtyOrdered");
					qtyFulfilled = 0;
					qtyOrdered = 0;
				}
				
				String qtyFulfilledString = parseIntToString(qtyFulfilled);
				String qtyOrderedString = parseIntToString(qtyOrdered);
				//case if productGroupName blank space ex. SONo SO-NFI-004540-csaserang1-02112019-115147731 Nutrifood
				if(productGroup.isEmpty()) {
					productCode = "NoNameProductGroup";
				}
				String brand = formatResult.getBrandName();
				String key = null;
				
				if(viewBy.equals("SKU")) {
					key = generateKeyProductInsight(salesOrderNo, soDate, productCode);
				}
				else if(viewBy.equals("productGroup")) {
					key = generateKeyProductInsight(salesOrderNo, soDate, productGroup);
				}
				else if(viewBy.equals("brand")) {
					key = generateKeyProductInsight(salesOrderNo, soDate, brand);
				}
				
				System.out.println("Key product insight : " + key);
				
				/*Double scarcityPerline;
				scarcityPerline = calculateScarcityPerline(qtyOrdered, qtyFulfilled);
				System.out.println("Scarcity perline : " + scarcityPerline);
				*/
				String value = null;
				
				if(!qtyFulfilledString.equals("0") && !qtyOrderedString.equals("0")) {
					if (qtyPerline.containsKey(key)) {
						String qtyOrderBefore = getSplit(qtyPerline.get(key), 0);
						System.out.println("qtyOrderBefore after split : " + qtyOrderBefore);
						String qtyFulfillBefore = getSplit(qtyPerline.get(key), 1);
						System.out.println("qtyFulfillBefore after split : " + qtyFulfillBefore);
						
						Integer prevQtyOrdered = parseStringToInt(qtyOrderBefore);
						Integer prevQtyFulfilled = parseStringToInt(qtyFulfillBefore);
						
						Integer currQtyOrder = prevQtyOrdered + qtyOrdered;
						Integer currQtyFulfilled = prevQtyFulfilled + qtyFulfilled;
						
						String currQtyOrderStr = parseIntToString(currQtyOrder);
						String currQtyFulfilledStr = parseIntToString(currQtyFulfilled);
						value = currQtyOrderStr + "|" + currQtyFulfilledStr;
						
//						Double prevRev = qtyPerline.get(key);
//						Double currRev = prevRev + scarcityPerline;
						qtyPerline.put(key, value);
					} else {
						value = qtyOrderedString + "|" + qtyFulfilledString;
						qtyPerline.put(key, value);
//						qtyPerline.put(key, scarcityPerline);
//						subtotalBeforeDisc3 = subtotalPerProduct;
					}
				}
				
				System.out.println("String Product Insight Scarcity Level Per-SO : " + qtyPerline.toString());

			}

			System.out.println("String Product Insight Scarcity Level SO : " + qtyPerline.toString());
		}

		System.out.println("String Product Insight MARIA Scarcity Level SO : " + qtyPerline.toString());
		
		return getProductInsightScarcityLevelByProductStr(qtyPerline);
//		return getRevenueByProduct(nettPerline);

	}

	private Map<String, Double> getProductInsightScarcityLevelByProductStr(Map<String, String> qtyPerline) {
		Comparator<String> compString = Comparator.comparing(String::toString);

		Map<String, String> productInsightByProduct = new TreeMap<>(compString);
		Map<String, Double> productInsightScarcityLevelByProduct = new TreeMap<>(compString);
		// calculate revenue per date

		for (Map.Entry<String, String> entry : qtyPerline.entrySet()) {
			System.out.println("Entry dot key : " + entry.getKey());
			System.out.println("Entry dot value : " + entry.getValue());
//			String products = getProduct(entry.getKey());
			String products = getSplit(entry.getKey(), 2);
			System.out.println("products : " + products);

			/*
			 * String dates = getDate(entry.getKey()); System.out.println("dates : " +
			 * dates); //comment because format date changed (submitted_date to
			 * creation_date)
			 * 
			 * String date = StringUtils.removeSixLastChar(dates);
			 * System.out.println("date : " + date);
			 * 
			 * String formattedDate = DateTimeHelper.formattedDate(dates);
			 * System.out.println("formatted Date : " + formattedDate);
			 */
			String qtyOrdered = getSplit(entry.getValue(), 0);
			System.out.println("qtyOrdered getProductInsightScarcityLevelByProductStr : " + qtyOrdered);
			
			String qtyFulfilled = getSplit(entry.getValue(), 1);
			System.out.println("qtyFulfilled getProductInsightScarcityLevelByProductStr : " + qtyFulfilled);
			
			Integer qtyFulfilledInt,qtyOrderedInt;
			qtyFulfilledInt = parseStringToInt(qtyFulfilled);
			qtyOrderedInt = parseStringToInt(qtyOrdered);
			
//			Double scarcityLevel = entry.getValue();
//			Double revenueBulat = (double) Math.round(revenue);
//			Double revenue = revenueBySO.get(key);
			
			String value = null;
			
			if (productInsightByProduct.containsKey(products)) {
				System.out.println("Masuk condition contains key product");
				String qtyOrderBefore = getSplit(productInsightByProduct.get(products), 0);
//				String qtyOrderBefore = getSplit(entry.getValue(), 0);
				System.out.println("qtyOrderBefore productInsightByProduct after split : " + qtyOrderBefore);
				String qtyFulfillBefore = getSplit(productInsightByProduct.get(products), 1);
//				String qtyFulfillBefore = getSplit(entry.getValue(), 1);
				System.out.println("qtyFulfillBefore productInsightByProduct after split : " + qtyFulfillBefore);
				
				Integer prevQtyOrdered = parseStringToInt(qtyOrderBefore);
				Integer prevQtyFulfilled = parseStringToInt(qtyFulfillBefore);
				System.out.println("prevQtyOrdered : " + prevQtyOrdered);
				System.out.println("prevQtyFulfilled : " + prevQtyFulfilled);
				
				Integer currQtyOrder = prevQtyOrdered + qtyOrderedInt;
				Integer currQtyFulfilled = prevQtyFulfilled + qtyFulfilledInt;
				System.out.println("currQtyOrder : " + currQtyOrder);
				System.out.println("currQtyFulfilled : " + currQtyFulfilled);
				
				String currQtyOrderStr = parseIntToString(currQtyOrder);
				String currQtyFulfilledStr = parseIntToString(currQtyFulfilled);
				value = currQtyOrderStr + "|" + currQtyFulfilledStr;
				
				System.out.println("VALUEEEEEE KALO ADA KEY SAMA : " + value);
//				Double prevRev = qtyPerline.get(key);
//				Double currRev = prevRev + scarcityPerline;
				productInsightByProduct.put(products, value);
				
				/*
				 * Double prevRev = productInsightByProduct.get(products);
				 * System.out.println("prevRev product insight scarcity level : " + prevRev);
				 * System.out.println("scarcityLevel product insight scarcity level : " +
				 * scarcityLevel); Double currRev = prevRev + scarcityLevel;
				 * System.out.println("currRev product insight scarcity level : " + currRev);
				 * productInsightByProduct.put(products, currRev);
				 */
			} else {
				value = qtyOrdered + "|" + qtyFulfilled;
				productInsightByProduct.put(products, value);
				
//				productInsightByProduct.put(products, scarcityLevel);
			}
		}
		System.out.println("PRODUCT INSIGHT SCARCITY LEVEL MARIA BY PRODUCT ALL CALCULATION : " + productInsightByProduct.toString());
		
		for (Map.Entry<String, String> entry : productInsightByProduct.entrySet()) {
			String keyProduct = entry.getKey();
			System.out.println("key product : " + keyProduct);
			
			String qtyOrderPerProduct = getSplit(entry.getValue(), 0);
			System.out.println("qtyOrderPerProduct productInsightByProduct after split : " + qtyOrderPerProduct);
			String qtyFulfillPerProduct = getSplit(entry.getValue(), 1);
			System.out.println("qtyFulfillPerProduct productInsightByProduct after split : " + qtyFulfillPerProduct);
			
			Integer qtyOrderPerProductInt = parseStringToInt(qtyOrderPerProduct);
			Integer qtyFulfillPerProductInt = parseStringToInt(qtyFulfillPerProduct);
			
			Double scarcityPerProduct;
			scarcityPerProduct = calculateScarcity(qtyOrderPerProductInt, qtyFulfillPerProductInt);
			System.out.println("Scarcity perproduct : " + scarcityPerProduct);
			
			if (productInsightScarcityLevelByProduct.containsKey(keyProduct)) {
				Double prevScarcity = productInsightScarcityLevelByProduct.get(keyProduct);
//				Double prevScarcityDouble = parseStringToDouble(prevScarcity);
				Double currScarcity = prevScarcity + scarcityPerProduct;
				productInsightScarcityLevelByProduct.put(keyProduct, currScarcity);
//				subtotalBeforeDisc3 = currRev;
			} else {
				productInsightScarcityLevelByProduct.put(keyProduct, scarcityPerProduct);
//				subtotalBeforeDisc3 = subtotalPerProduct;
			}
			System.out.println("Product Insight Scarcity Level Per-Product : " + productInsightScarcityLevelByProduct.toString());

		}
		
		return productInsightScarcityLevelByProduct;
	}
	
	public String calculateProductInsightScarcityLevelHirarki(String idClient, String username, String startTime,
			String endTime, String viewBy) throws ParseException {
		return totalScarcityLevelPerFilter(getProductInsightByScarcityLevelHirarkiString(idClient, username, startTime, endTime, viewBy));
	}
	
	//customerOverview 
	public Map<String, Integer> getCustomerOverview(String clientId, String username, String startTime,
			String endTime) throws ParseException {
		
		String startDateTime = startTime + timeToStart;
		String endDateTime = endTime + timeToEnd;
		
		System.out.println("startDateTime : " + startDateTime);
		System.out.println("endDateTime : " + endDateTime);
		
		String formattedStartDate = DateTimeHelper.formattedDate(startTime);
		System.out.println("formattedStartDate : " + formattedStartDate);
		String formattedEndDate = DateTimeHelper.formattedDate(endTime);
		System.out.println("formattedEndDate : " + formattedEndDate);

		/// HIRARKI
		Integer parentAdvocate = salesOrderRepository.getUserTypeId(username);
		System.out.println("PARENT ADVOCATE from user type id : " + parentAdvocate);
		String advocateType = salesOrderRepository.getAdvocateType(parentAdvocate, clientId);
		System.out.println("PARENT ADVOCATE TYPE : " + advocateType);

		List<Integer> advocateIdInParentAdvocate = new ArrayList<Integer>();
		List<Integer> salesGroupIdInClientId = new ArrayList<Integer>();
		List<Integer> allAdvIdInSalesGroupClientId = new ArrayList<Integer>();
		
		Map<String, Integer> customerOverviewResult = new TreeMap<>();
		
		Integer registeredStore, visitedStore, productiveStore;

		if (advocateType.equals(principal)) {
			System.out.println("MASUK KE MODE PRINCIPAL");
			advocateIdInParentAdvocate = salesOrderRepository.findAllAdvocatesInAdvocateTypeSTRByClientId(clientId);
			System.out.println("LIST ADVOCATE ID IN MODE PCP : " + advocateIdInParentAdvocate.toString());
			System.out.println("LIST ADVOCATE ID IN MODE PCP SIZE : " + advocateIdInParentAdvocate.size());

//			System.out.println("FIND BY CLIENT AND DATE STRING SIZE Hirarki MODE PCP : " + salesOrderRepository
//					.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime, advocateIdInParentAdvocate)
//					.size());
			
//			registeredStore = salesOrderRepository.totalRegisteredStore(clientId, advocateIdInParentAdvocate);
//			System.out.println("REGISTERED STORE : " + registeredStore);
//			visitedStore = salesOrderRepository.totalVisitedStore(clientId, advocateIdInParentAdvocate);
//			System.out.println("ClientIDnya ini : " + clientId);
//			productiveStore = salesOrderRepository.totalProductiveStore(clientId, advocateIdInParentAdvocate);
//			System.out.println("PRODUCTIVE STORE : " + productiveStore);
			registeredStore = salesOrderRepository.totalRegisteredStore(clientId, endDateTime,
					advocateIdInParentAdvocate);
			visitedStore = salesOrderRepository.totalVisitedStore(clientId, startTime, endDateTime,
					advocateIdInParentAdvocate);
			productiveStore = salesOrderRepository.totalProductiveStore(clientId, formattedStartDate, formattedEndDate,
					advocateIdInParentAdvocate);
			
//			salesOrders = salesOrderRepository.findProductInSOByClientAndDateHierarchy(clientId, startTime, endTime,
//					advocateIdInParentAdvocate);

		} else {
			System.out.println("MASUK KE MODE NON PRINCIPAL");

//			advocateIdInParentAdvocate = getResultListAdvocate(parentAdvocate, clientId);
			advocateIdInParentAdvocate = getResultListAdvocateSTR(parentAdvocate, clientId);
			System.out.println("LIST ADVOCATE ID IN PARENT ADVOCATE : " + advocateIdInParentAdvocate.toString());
			System.out.println("LIST ADVOCATE ID IN PARENT ADVOCATE SIZE : " + advocateIdInParentAdvocate.size());

			System.out.println("ID CLIENT SERVICE : " + clientId);
			System.out.println("START TIME SERVICE : " + startDateTime);
			System.out.println("END TIME SERVICE : " + endDateTime);
			
			salesGroupIdInClientId = salesOrderRepository.getSalesGroupId(advocateIdInParentAdvocate);
			allAdvIdInSalesGroupClientId = salesOrderRepository.getAllAdvIdInSalesGroupClientId(salesGroupIdInClientId);
			
//			registeredStore = salesOrderRepository.totalRegisteredStore(clientId, advocateIdInParentAdvocate);
//			visitedStore = salesOrderRepository.totalVisitedStore(clientId, advocateIdInParentAdvocate);
//			productiveStore = salesOrderRepository.totalProductiveStore(clientId, advocateIdInParentAdvocate);
			
			registeredStore = salesOrderRepository.totalRegisteredStore(clientId, endDateTime,
					allAdvIdInSalesGroupClientId);
			visitedStore = salesOrderRepository.totalVisitedStore(clientId, startTime, endDateTime,
					allAdvIdInSalesGroupClientId);
			productiveStore = salesOrderRepository.totalProductiveStore(clientId, formattedStartDate, formattedEndDate,
					allAdvIdInSalesGroupClientId);
		}
		
		customerOverviewResult.put("Registered Store", registeredStore);
		customerOverviewResult.put("Visited Store", visitedStore);
		customerOverviewResult.put("Productive Store", productiveStore);
		
		System.out.println("INI CUSTOMER OVERVIEW RESULT : " + customerOverviewResult.toString());
		getCustomerOverviewWorkgroup(clientId, username, startTime, endTime);
		return customerOverviewResult;
	}

	private List<Integer> getResultListAdvocateSTR(Integer parentAdvocate, String clientId) {
		Deque<Integer> finalQueue = new ArrayDeque<>();
		Deque<Integer> queue = findByParentSTR(parentAdvocate, clientId);

		finalQueue.push(parentAdvocate);

		while (!queue.isEmpty()) {
			Integer value = queue.pop();
			finalQueue.push(value);

			Deque<Integer> byParent = findByParent(value, clientId);
			while (!byParent.isEmpty()) {
				queue.push(byParent.pop());
			}
		}

		// result untuk loop as criteria query param
		List<Integer> result = new ArrayList<>(finalQueue);
		Collections.sort(result);
		System.out.println("List integer result : " + result.toString());
		System.out.println("List integer result size : " + result.size());

		// buang yang perlu
		// result.remove(99978);

		return result;
	}

	private Deque<Integer> findByParentSTR(Integer parentAdvocate, String clientId) {
		Map<Integer, Integer> clientAdvoMap = getClientAdvocateMapSTR(clientId);

		System.out.println("CLIENT ADVO MAP BEFORE : " + clientAdvoMap.toString());

		Deque<Integer> queue = new ArrayDeque<>();

		for (Map.Entry<Integer, Integer> entry : clientAdvoMap.entrySet()) {
			if (entry.getValue().equals(parentAdvocate) && !entry.getKey().equals(parentAdvocate)) {
				queue.push(entry.getKey());
				System.out.println("QUEUE PUSH : " + queue);
			}
		}

		queue.forEach(clientAdvoMap::remove);

		System.out.println("Return QUEUE : " + queue);
		return queue;
	}

	private Map<Integer, Integer> getClientAdvocateMapSTR(String clientId) {
		Map<Integer, Integer> clientAdvocateMap = new HashMap<>();
		List<ClientAdvocates> clientAdvocates = salesOrderRepository.findAllAdvocatesIdInAdvocateTypeSTRByClientId(clientId);
		System.out.println("CLient advocate size : " + clientAdvocates.size());
		for (ClientAdvocates advocate : clientAdvocates) {
			System.out.println("Client advocate id " + advocate.getAdvocateId() + " has parent advocate "
					+ advocate.getParentAdvocate());
			clientAdvocateMap.put(advocate.getAdvocateId(), advocate.getParentAdvocate());
		}
		System.out.println("From all map client advocate : " + clientAdvocateMap.toString());
		return clientAdvocateMap;
	}
	
	//customerOverview Workgroup
	public Map<String, String> getCustomerOverviewWorkgroup(String clientId, String username, String startTime,
			String endTime) throws ParseException {
		
		String startDateTime = startTime + timeToStart;
		String endDateTime = endTime + timeToEnd;
		
		System.out.println("startDateTime : " + startDateTime);
		System.out.println("endDateTime : " + endDateTime);
		
		String formattedStartDate = DateTimeHelper.formattedDate(startTime);
		System.out.println("formattedStartDate : " + formattedStartDate);
		String formattedEndDate = DateTimeHelper.formattedDate(endTime);
		System.out.println("formattedEndDate : " + formattedEndDate);

		/// HIRARKI
		Integer parentAdvocate = salesOrderRepository.getUserTypeId(username);
		System.out.println("PARENT ADVOCATE from user type id : " + parentAdvocate);
		String advocateType = salesOrderRepository.getAdvocateType(parentAdvocate, clientId);
		System.out.println("PARENT ADVOCATE TYPE : " + advocateType);

		List<Integer> advocateIdInParentAdvocate = new ArrayList<Integer>();
		List<Integer> salesGroupIdInClientId = new ArrayList<Integer>();
		List<Integer> allAdvIdInSalesGroupClientId = new ArrayList<Integer>();
		
		Map<String, Integer> customerOverviewResult = new TreeMap<>();
		
		Map<String, String> customerOverviewWorkgroup = new TreeMap<>();
		Map<String, String> registeredStoreWorkgroup = new TreeMap<>();
		Map<String, String> visitedStoreWorkgroup = new TreeMap<>();
		Map<String, String> productiveStoreWorkgroup = new TreeMap<>();
		
		List<String> provinceList = new ArrayList<String>();
		List<String> salesGroupList = new ArrayList<String>();
		List<RegisteredStoreWorkgroup> registeredStores = new ArrayList<RegisteredStoreWorkgroup>();
		List<VisitedStoreWorkgroup> visitedStores = new ArrayList<VisitedStoreWorkgroup>();
		List<ProductiveStoreWorkgroup> productiveStores = new ArrayList<ProductiveStoreWorkgroup>();
		
		provinceList = salesOrderRepository.findAllProvinceName();
		salesGroupList = salesOrderRepository.findAllSalesGroupName(clientId);
		
		for(String salesGroup : salesGroupList) {
			customerOverviewWorkgroup.put(salesGroup, "0|0|0");
		}
		
		System.out.println("INITIAL customerOverviewWorkgroup : " + customerOverviewWorkgroup.toString());
		
		if (advocateType.equals(principal)) {
		System.out.println("MASUK KE MODE PRINCIPAL");
		advocateIdInParentAdvocate = salesOrderRepository.findAllAdvocatesInAdvocateTypeSTRByClientId(clientId);
		System.out.println("LIST ADVOCATE ID IN MODE PCP : " + advocateIdInParentAdvocate.toString());
		System.out.println("LIST ADVOCATE ID IN MODE PCP SIZE : " + advocateIdInParentAdvocate.size());

		registeredStores = salesOrderRepository.totalRegisteredStoreWorkgroup(clientId, endDateTime,
				advocateIdInParentAdvocate);
		visitedStores = salesOrderRepository.totalVisitedStoreWorkgroup(clientId, startTime, endDateTime,
				advocateIdInParentAdvocate);
		productiveStores = salesOrderRepository.totalProductiveStoreWorkgroup(clientId, formattedStartDate, formattedEndDate,
				advocateIdInParentAdvocate);
		
	} else {
		System.out.println("MASUK KE MODE NON PRINCIPAL");

		advocateIdInParentAdvocate = getResultListAdvocateSTR(parentAdvocate, clientId);
		System.out.println("LIST ADVOCATE ID IN PARENT ADVOCATE : " + advocateIdInParentAdvocate.toString());
		System.out.println("LIST ADVOCATE ID IN PARENT ADVOCATE SIZE : " + advocateIdInParentAdvocate.size());

		System.out.println("ID CLIENT SERVICE : " + clientId);
		System.out.println("START TIME SERVICE : " + startDateTime);
		System.out.println("END TIME SERVICE : " + endDateTime);
		
		salesGroupIdInClientId = salesOrderRepository.getSalesGroupId(advocateIdInParentAdvocate);
		allAdvIdInSalesGroupClientId = salesOrderRepository.getAllAdvIdInSalesGroupClientId(salesGroupIdInClientId);
		
		registeredStores = salesOrderRepository.totalRegisteredStoreWorkgroup(clientId, endDateTime,
				allAdvIdInSalesGroupClientId);
		visitedStores = salesOrderRepository.totalVisitedStoreWorkgroup(clientId, startTime, endDateTime,
				allAdvIdInSalesGroupClientId);
		productiveStores = salesOrderRepository.totalProductiveStoreWorkgroup(clientId, formattedStartDate, formattedEndDate,
				allAdvIdInSalesGroupClientId);
	}
		for(RegisteredStoreWorkgroup registeredStore : registeredStores) {
			registeredStoreWorkgroup.put(registeredStore.getSalesGroupName(), parseIntToString(registeredStore.getRegisteredStore()));
		}
		
		System.out.println("registeredStoreWorkgroup : " + registeredStoreWorkgroup.toString());
		
		for(VisitedStoreWorkgroup visitedStore : visitedStores) {
			visitedStoreWorkgroup.put(visitedStore.getSalesGroupName(), parseIntToString(visitedStore.getVisitedStore()));
		}
		
		System.out.println("visitedStoreWorkgroup : " + visitedStoreWorkgroup.toString());
		
		for(ProductiveStoreWorkgroup productiveStore : productiveStores) {
			productiveStoreWorkgroup.put(productiveStore.getSalesGroupName(), parseIntToString(productiveStore.getProductiveStore()));
		}
		
		System.out.println("productiveStoreWorkgroup : " + productiveStoreWorkgroup.toString());
		
		
		for (String key : customerOverviewWorkgroup.keySet()) {
			String valueResult = customerOverviewWorkgroup.get(key);
			String registeredResult = getSplit(valueResult, 0);
			String visitedResult = getSplit(valueResult, 1);
			String productiveResult = getSplit(valueResult, 2);
			
			String value;
			
			if(registeredStoreWorkgroup.containsKey(key)) {
				registeredResult = registeredStoreWorkgroup.get(key);
			}
			
			if(visitedStoreWorkgroup.containsKey(key)) {
				visitedResult = visitedStoreWorkgroup.get(key);
			}
			
			if(productiveStoreWorkgroup.containsKey(key)) {
				productiveResult = productiveStoreWorkgroup.get(key);
			}
			
			valueResult = registeredResult + "|" + visitedResult + "|" + productiveResult;
			
			customerOverviewWorkgroup.replace(key, customerOverviewWorkgroup.get(key), valueResult);
		}
		
		System.out.println("after merge customerOverviewWorkgroup : " + customerOverviewWorkgroup.toString());
		
		return customerOverviewWorkgroup;
	}

	private Double parseStringToDouble(String yourString) {
		return Double.parseDouble(yourString);
	}

	private String parseIntToString(Integer yourInt) {
		return String.valueOf(yourInt);
	}

	private Integer parseStringToInt(String yourString) {
		return Integer.parseInt(yourString);
	}

	public Map<String, String> getCustomerOverviewArea(String clientId, String username, String startTime,
			String endTime) throws ParseException {
		String startDateTime = startTime + timeToStart;
		String endDateTime = endTime + timeToEnd;
		
		System.out.println("startDateTime : " + startDateTime);
		System.out.println("endDateTime : " + endDateTime);
		
		String formattedStartDate = DateTimeHelper.formattedDate(startTime);
		System.out.println("formattedStartDate : " + formattedStartDate);
		String formattedEndDate = DateTimeHelper.formattedDate(endTime);
		System.out.println("formattedEndDate : " + formattedEndDate);

		/// HIRARKI
		Integer parentAdvocate = salesOrderRepository.getUserTypeId(username);
		System.out.println("PARENT ADVOCATE from user type id : " + parentAdvocate);
		String advocateType = salesOrderRepository.getAdvocateType(parentAdvocate, clientId);
		System.out.println("PARENT ADVOCATE TYPE : " + advocateType);

		List<Integer> advocateIdInParentAdvocate = new ArrayList<Integer>();
		List<Integer> salesGroupIdInClientId = new ArrayList<Integer>();
		List<Integer> allAdvIdInSalesGroupClientId = new ArrayList<Integer>();
		
		Map<String, Integer> customerOverviewResult = new TreeMap<>();
		
		Map<String, String> customerOverviewArea = new TreeMap<>();
		Map<String, String> registeredStoreArea = new TreeMap<>();
		Map<String, String> visitedStoreArea = new TreeMap<>();
		Map<String, String> productiveStoreArea = new TreeMap<>();
		
		List<String> provinceList = new ArrayList<String>();
		List<RegisteredStoreArea> registeredStores = new ArrayList<RegisteredStoreArea>();
		List<VisitedStoreArea> visitedStores = new ArrayList<VisitedStoreArea>();
		List<ProductiveStoreArea> productiveStores = new ArrayList<ProductiveStoreArea>();
		
		provinceList = salesOrderRepository.findAllProvinceName();
		
		for(String province : provinceList) {
			customerOverviewArea.put(province, "0|0|0");
		}
		
		System.out.println("INITIAL customerOverviewArea : " + customerOverviewArea.toString());
		
		if (advocateType.equals(principal)) {
		System.out.println("MASUK KE MODE PRINCIPAL");
		advocateIdInParentAdvocate = salesOrderRepository.findAllAdvocatesInAdvocateTypeSTRByClientId(clientId);
		System.out.println("LIST ADVOCATE ID IN MODE PCP : " + advocateIdInParentAdvocate.toString());
		System.out.println("LIST ADVOCATE ID IN MODE PCP SIZE : " + advocateIdInParentAdvocate.size());

		registeredStores = salesOrderRepository.totalRegisteredStoreArea(clientId, endDateTime,
				advocateIdInParentAdvocate);
		visitedStores = salesOrderRepository.totalVisitedStoreArea(clientId, startTime, endDateTime,
				advocateIdInParentAdvocate);
		productiveStores = salesOrderRepository.totalProductiveStoreArea(clientId, formattedStartDate, formattedEndDate,
				advocateIdInParentAdvocate);
		
	} else {
		System.out.println("MASUK KE MODE NON PRINCIPAL");

		advocateIdInParentAdvocate = getResultListAdvocateSTR(parentAdvocate, clientId);
		System.out.println("LIST ADVOCATE ID IN PARENT ADVOCATE : " + advocateIdInParentAdvocate.toString());
		System.out.println("LIST ADVOCATE ID IN PARENT ADVOCATE SIZE : " + advocateIdInParentAdvocate.size());

		System.out.println("ID CLIENT SERVICE : " + clientId);
		System.out.println("START TIME SERVICE : " + startDateTime);
		System.out.println("END TIME SERVICE : " + endDateTime);
		
		salesGroupIdInClientId = salesOrderRepository.getSalesGroupId(advocateIdInParentAdvocate);
		allAdvIdInSalesGroupClientId = salesOrderRepository.getAllAdvIdInSalesGroupClientId(salesGroupIdInClientId);
		
		registeredStores = salesOrderRepository.totalRegisteredStoreArea(clientId, endDateTime,
				allAdvIdInSalesGroupClientId);
		visitedStores = salesOrderRepository.totalVisitedStoreArea(clientId, startTime, endDateTime,
				allAdvIdInSalesGroupClientId);
		productiveStores = salesOrderRepository.totalProductiveStoreArea(clientId, formattedStartDate, formattedEndDate,
				allAdvIdInSalesGroupClientId);
	}
		for(RegisteredStoreArea registeredStore : registeredStores) {
			registeredStoreArea.put(registeredStore.getProvinceName(), parseIntToString(registeredStore.getRegisteredStore()));
		}
		
		System.out.println("registeredStoreArea : " + registeredStoreArea.toString());
		
		for(VisitedStoreArea visitedStore : visitedStores) {
			visitedStoreArea.put(visitedStore.getProvinceName(), parseIntToString(visitedStore.getVisitedStore()));
		}
		
		System.out.println("visitedStoreArea : " + visitedStoreArea.toString());
		
		for(ProductiveStoreArea productiveStore : productiveStores) {
			productiveStoreArea.put(productiveStore.getProvinceName(), parseIntToString(productiveStore.getProductiveStore()));
		}
		
		System.out.println("productiveStoreArea : " + productiveStoreArea.toString());
		
		
		for (String key : customerOverviewArea.keySet()) {
			String valueResult = customerOverviewArea.get(key);
			String registeredResult = getSplit(valueResult, 0);
			String visitedResult = getSplit(valueResult, 1);
			String productiveResult = getSplit(valueResult, 2);
			
			if(registeredStoreArea.containsKey(key)) {
				registeredResult = registeredStoreArea.get(key);
			}
			
			if(visitedStoreArea.containsKey(key)) {
				visitedResult = visitedStoreArea.get(key);
			}
			
			if(productiveStoreArea.containsKey(key)) {
				productiveResult = productiveStoreArea.get(key);
			}
			
			valueResult = registeredResult + "|" + visitedResult + "|" + productiveResult;
			
			customerOverviewArea.replace(key, customerOverviewArea.get(key), valueResult);
		}
		
		System.out.println("after merge customerOverviewArea : " + customerOverviewArea.toString());
		
		return customerOverviewArea;
	}
}