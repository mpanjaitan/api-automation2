package starter.salesorder;

public interface ClientAdvocates {

	/*
	 * SELECT client_id as clientId, advocate_id as advocateId, advocate_type as
	 * advocateType, parent_advocate as parentAdvocate, role as role, status as
	 * status FROM adv_identity.client_advocates where client_id = 1 and
	 * advocate_type != 'STR' and status = 'ACT';
	 */
	
	Integer getClientId();
	Integer getAdvocateId();
	String getAdvocateType();
	Integer getParentAdvocate();
	String getRole();
	String getStatus();
}
