package starter.salesorder;

public interface VisitedStoreArea {

	String getProvinceName();
	Integer getVisitedStore();
}
