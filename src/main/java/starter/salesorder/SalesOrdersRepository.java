package starter.salesorder;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SalesOrdersRepository extends JpaRepository<SalesOrders, String> {

	/*@Query(value="SELECT so.sales_order_no AS salesOrderNo, so.client_id AS clientId, so.status AS status, STR_TO_DATE(so.creation_date,'%d-%m-%Y') AS creationDate "
			+ "from sales_orders so "
			+ "where so.sales_order_no=:soNumber", nativeQuery = true)
	SalesOrdersNew findBySONo(@Param("soNumber") String soNumber);*/
	
	@Query(value = "SELECT so.sales_order_no AS salesOrderNo, so.client_id AS clientId, so.status AS status, STR_TO_DATE(so.creation_date,'%d-%m-%Y') as creationDate,"
			+ "soi.seq AS soiSeq, soi.quantity AS quantity, soi.price AS price, soi.is_active AS isActive,"
			+ "soid.sales_order_item_seq AS salesOrderItemSeq, soid.discount_level AS discountLevel, soid.discount_seq AS soidDiscountSeq, soid.value AS soidValue, soid.value_type AS soidValueType, soid.is_active as soidIsActive,"
			+ "sod.discount_seq AS sodDiscountSeq, sod.value AS sodValue, sod.value_type AS sodValueType, sod.is_active as sodIsActive"
			+ " FROM adv_item.sales_orders so"
			+ " INNER JOIN adv_item.sales_order_items soi ON so.sales_order_no = soi.sales_order_no"
			+ " LEFT JOIN adv_item.sales_order_discounts sod ON sod.sales_order_no = so.sales_order_no"
			+ " LEFT JOIN adv_item.sales_order_item_discounts soid ON soid.sales_order_no = soi.sales_order_no"
			+ " AND soid.sales_order_item_seq = soi.seq"
			+ " WHERE so.client_id = 8333"
			+ " AND STR_TO_DATE(so.creation_date,'%d-%m-%Y') between '2019-01-01' and '2019-01-30'"
			+ " AND so.status IN ('FUL', 'PRE', 'PUN')"
			+ " AND soi.is_active = 'Y'", nativeQuery = true)
	List <SalesOrderTransaction> findByDefaultDate();
	
	@Query(value = "SELECT so.sales_order_no AS salesOrderNo, so.client_id AS clientId, so.status AS status, STR_TO_DATE(so.creation_date,'%d-%m-%Y') as creationDate,"
			+ "soi.seq AS soiSeq, soi.quantity AS quantity, soi.price AS price, soi.is_active AS isActive,"
			+ "soid.sales_order_item_seq AS salesOrderItemSeq, soid.discount_level AS discountLevel, soid.discount_seq AS soidDiscountSeq, soid.value AS soidValue, soid.value_type AS soidValueType, soid.is_active as soidIsActive,"
			+ "sod.discount_seq AS sodDiscountSeq, sod.value AS sodValue, sod.value_type AS sodValueType, sod.is_active as sodIsActive"
			+ " FROM adv_item.sales_orders so"
			+ " INNER JOIN adv_item.sales_order_items soi ON so.sales_order_no = soi.sales_order_no"
			+ " LEFT JOIN adv_item.sales_order_discounts sod ON sod.sales_order_no = so.sales_order_no"
			+ " LEFT JOIN adv_item.sales_order_item_discounts soid ON soid.sales_order_no = soi.sales_order_no"
			+ " AND soid.sales_order_item_seq = soi.seq"
			+ " WHERE so.client_id = :idClient"
			+ " AND STR_TO_DATE(so.creation_date,'%d-%m-%Y') between :startTime and :endTime"
			+ " AND so.status IN ('FUL', 'PRE', 'PUN')"
			+ " AND soi.is_active = 'Y'", nativeQuery = true)
	List <SalesOrderTransaction> findByDate(@Param("idClient") Integer idClient, @Param("startTime") Long startTime, @Param("endTime") Long endTime);
	
	@Query(value = "SELECT so.sales_order_no AS salesOrderNo, so.client_id AS clientId, so.status AS status, STR_TO_DATE(so.creation_date,'%d-%m-%Y') as creationDate, so.sales_channel as salesChannel,"
			+ "soi.seq AS soiSeq, soi.quantity AS quantity, soi.price AS price, soi.is_active AS isActive,"
			+ "soid.sales_order_item_seq AS salesOrderItemSeq, soid.discount_level AS discountLevel, soid.discount_seq AS soidDiscountSeq, soid.value AS soidValue, soid.value_type AS soidValueType, soid.is_active as soidIsActive"
			+ " FROM adv_item.sales_orders so"
			+ " INNER JOIN adv_item.sales_order_items soi ON so.sales_order_no = soi.sales_order_no"
			+ " LEFT JOIN adv_item.sales_order_item_discounts soid ON soid.sales_order_no = soi.sales_order_no"
			+ " AND soid.sales_order_item_seq = soi.seq"
			+ " WHERE so.client_id = :idClient"
			+ " AND STR_TO_DATE(so.creation_date,'%d-%m-%Y') between :startTime and :endTime"
			+ " AND so.status IN ('FUL', 'PRE', 'PUN')"
			+ " AND soi.is_active = 'Y'"
			+ " AND so.sales_channel IN ('WEB', 'VIS')"
			+ " ORDER BY so.sales_order_no, soi.seq,"
			+ " case when discount_level = 'SIU' then 0 when discount_level = 'SIS' then 1 else 2 end,"
			+ " soid.discount_seq", nativeQuery = true)
	List <SalesOrderTransaction> findByClientAndDate(@Param("idClient") Integer idClient, @Param("startTime") Long startTime, @Param("endTime") Long endTime);
	
	//maria
	@Query(value = "SELECT so.sales_order_no AS salesOrderNo, so.client_id AS clientId, so.status AS status, STR_TO_DATE(so.creation_date,'%d-%m-%Y') as creationDate, so.sales_channel as salesChannel,"
			+ "soi.seq AS soiSeq, soi.quantity AS quantity, soi.fulfilled_quantity AS fulfilledQuantity, soi.price AS price, soi.is_active AS isActive,"
			+ "soid.sales_order_item_seq AS salesOrderItemSeq, soid.discount_level AS discountLevel, soid.discount_seq AS soidDiscountSeq, soid.value AS soidValue, soid.value_type AS soidValueType, soid.is_active as soidIsActive"
			+ " FROM adv_item.sales_orders so"
			+ " INNER JOIN adv_item.sales_order_items soi ON so.sales_order_no = soi.sales_order_no"
			+ " LEFT JOIN adv_item.sales_order_item_discounts soid ON soid.sales_order_no = soi.sales_order_no"
			+ " AND soid.sales_order_item_seq = soi.seq"
			+ " WHERE so.client_id = :idClient"
			+ " AND STR_TO_DATE(so.creation_date,'%d-%m-%Y') between :startTime and :endTime"
			+ " AND so.status IN ('FUL', 'PRE', 'PUN')"
			+ " AND soi.is_active = 'Y'"
			+ " AND so.sales_channel IN ('WEB', 'VIS', 'SOR', 'BUP')"
			+ " ORDER BY so.sales_order_no, soi.seq,"
			+ " case when discount_level = 'SIU' then 0 when discount_level = 'SIS' then 1 else 2 end,"
			+ " soid.discount_seq", nativeQuery = true)
	List <SalesOrderTransaction> findByClientAndDateString(@Param("idClient") String idClient, @Param("startTime") String startTime, @Param("endTime") String endTime);
	
	@Query(value = "SELECT so.sales_order_no AS salesOrderNo, so.client_id AS clientId, so.status AS status, STR_TO_DATE(so.creation_date,'%d-%m-%Y') as creationDate, so.sales_channel as salesChannel,"
			+ "sod.discount_seq AS sodDiscountSeq, sod.value AS sodValue, sod.value_type AS sodValueType, sod.is_active as sodIsActive"
			+ " FROM adv_item.sales_orders so"
			+ " LEFT JOIN adv_item.sales_order_discounts sod ON sod.sales_order_no = so.sales_order_no"
			+ " WHERE so.client_id = :idClient"
			+ " AND STR_TO_DATE(so.creation_date,'%d-%m-%Y') between :startTime and :endTime"
			+ " AND so.status IN ('FUL', 'PRE', 'PUN')"
			+ " AND sod.is_active = 'Y'"
			+ " AND so.sales_channel IN ('WEB', 'VIS')"
			+ " ORDER BY so.sales_order_no, sod.discount_seq", nativeQuery = true)
	List <SalesOrderDiscounts> findSODiscount(@Param("idClient") Integer idClient, @Param("startTime") Long startTime, @Param("endTime") Long endTime);
	
	
	@Query(value = "SELECT soi.sales_order_no AS salesOrderNo, soi.seq AS soiSeq, soi.quantity AS quantity, soi.price AS price, soi.discount AS discount, soi.is_active AS isActive"
			+ " FROM adv_item.sales_order_items soi"
			+ " WHERE soi.sales_order_no = :soNumber"
			+ " AND soi.is_active = 'Y'", nativeQuery = true)
	List <SalesOrderItem> findBySONo(@Param("soNumber") Integer soNumber);

	@Query(value = "SELECT so.sales_order_no AS salesOrderNo, so.client_id AS clientId, so.status AS status, STR_TO_DATE(so.creation_date,'%d-%m-%Y') as creationDate, so.discount as globalSODiscount, so.tax as globalSOTax"
			+ " FROM adv_item.sales_orders so"
			+ " WHERE so.client_id = :idClient"
			+ " AND STR_TO_DATE(so.creation_date,'%d-%m-%Y') between :startTime and :endTime"
			+ " AND so.status IN ('FUL', 'PRE', 'PUN')", nativeQuery = true)
	List<SalesOrder> getSalesOrdersByClientAndDate(@Param("idClient") Integer idClient, @Param("startTime") Long startTime, @Param("endTime") Long endTime);

	@Query(value="SELECT distinct (soi.sales_order_no) AS salesOrderNo, soi.quantity AS quantity, soi.price AS price, soi.is_active AS isActive" + 
			" FROM adv_item.sales_order_items soi JOIN sales_orders so ON so.sales_order_no = soi.sales_order_no" + 
			" WHERE soi.client_id = :idClient" + 
			" AND soi.loki_index between :startTime and :endTime" + 
			" AND so.status IN ('FUL', 'PRE', 'PUN')", nativeQuery = true)
	List<String> getDistinctSalesOrderNumberByClientAndDate(@Param("idClient") Integer idClient, @Param("startTime") Long startTime, @Param("endTime") Long endTime);
	
	@Query(value = "SELECT so.sales_order_no AS salesOrderNo, so.client_id AS clientId, so.status AS status, STR_TO_DATE(so.creation_date,'%d-%m-%Y') as creationDate, so.discount as globalSODiscount, so.tax as globalSOTax"
			+ " FROM adv_item.sales_orders so"
			+ " WHERE so.sales_order_no = :salesOrderNo", nativeQuery = true)
	List<SalesOrder> findSalesOrderBySalesOrderNumber(@Param("salesOrderNo") String salesOrderNo);

	@Query(value = "SELECT so.sales_order_no AS salesOrderNo, so.client_id AS clientId, so.status AS status, STR_TO_DATE(so.creation_date,'%d-%m-%Y') as creationDate,"
			+ "sod.discount_seq AS sodDiscountSeq, sod.value AS sodValue, sod.value_type AS sodValueType, sod.is_active as sodIsActive"
			+ " FROM adv_item.sales_orders so"
			+ " LEFT JOIN adv_item.sales_order_discounts sod ON sod.sales_order_no = so.sales_order_no"
			+ " WHERE so.sales_order_no = :salesOrderNo"
			+ " AND so.status IN ('FUL', 'PRE', 'PUN')"
			+ " AND so.sales_channel IN ('WEB', 'VIS', 'SOR', 'BUP')"
			+ " AND sod.is_active = 'Y'"
			+ " ORDER BY so.sales_order_no, sod.discount_seq", nativeQuery = true)
	List<SalesOrderDiscounts> findSODiscountSONo(@Param("salesOrderNo") String salesOrderNo);
	
	//add hierarchy
	@Query(value = "SELECT client_id as clientId, advocate_id as advocateId, advocate_type as advocateType, parent_advocate as parentAdvocate, role as role, status as status"
			+ " FROM adv_identity.client_advocates"
			+ " where client_id = :idClient"
			+ " and advocate_type != 'STR'"
			+ " and status = 'ACT'", nativeQuery = true)
	List <ClientAdvocates> findAllAdvocatesByClientId(@Param("idClient") String idClient);
	
	@Query(value = "SELECT user_type_id"
			+ " FROM adv_identity.user_logins"
			+ " WHERE username = :userLogin and is_active = 'Y'", nativeQuery = true)
	Integer getUserTypeId(@Param("userLogin") String userLogin);
	
	@Query(value = "SELECT so.sales_order_no AS salesOrderNo, so.client_id AS clientId, so.status AS status, STR_TO_DATE(so.creation_date,'%d-%m-%Y') as creationDate, so.sales_channel as salesChannel,"
			+ "soi.seq AS soiSeq, soi.quantity AS quantity, soi.fulfilled_quantity AS fulfilledQuantity, soi.price AS price, soi.is_active AS isActive,"
			+ "soid.sales_order_item_seq AS salesOrderItemSeq, soid.discount_level AS discountLevel, soid.discount_seq AS soidDiscountSeq, soid.value AS soidValue, soid.value_type AS soidValueType, soid.is_active as soidIsActive"
			+ " FROM adv_item.sales_orders so"
			+ " INNER JOIN adv_item.sales_order_items soi ON so.sales_order_no = soi.sales_order_no"
			+ " LEFT JOIN adv_item.sales_order_item_discounts soid ON soid.sales_order_no = soi.sales_order_no"
			+ " AND soid.sales_order_item_seq = soi.seq"
			+ " WHERE so.client_id = :idClient"
			+ " AND STR_TO_DATE(so.creation_date,'%d-%m-%Y') between :startTime and :endTime"
			+ " AND so.status IN ('FUL', 'PRE', 'PUN')"
			+ " AND soi.is_active = 'Y'"
			+ " AND so.sales_channel IN ('WEB', 'VIS', 'SOR', 'BUP')"
			+ " AND so.sales_id IN :listSalesId"
			+ " ORDER BY so.sales_order_no, soi.seq,"
			+ " case when discount_level = 'SIU' then 0 when discount_level = 'SIS' then 1 else 2 end,"
			+ " soid.discount_seq", nativeQuery = true)
	List <SalesOrderTransaction> findByClientAndDateStringHierarchy(@Param("idClient") String idClient, @Param("startTime") String startTime, @Param("endTime") String endTime, @Param("listSalesId") List<Integer> listSalesId);
	
	@Query(value = "SELECT so.sales_order_no AS salesOrderNo, so.client_id AS clientId, so.status AS status, STR_TO_DATE(so.creation_date,'%d-%m-%Y') as creationDate,"
			+ "sod.discount_seq AS sodDiscountSeq, sod.value AS sodValue, sod.value_type AS sodValueType, sod.is_active as sodIsActive"
			+ " FROM adv_item.sales_orders so"
			+ " LEFT JOIN adv_item.sales_order_discounts sod ON sod.sales_order_no = so.sales_order_no"
			+ " WHERE so.sales_order_no = :salesOrderNo"
			+ " AND so.status IN ('FUL', 'PRE', 'PUN')"
			+ " AND so.sales_channel IN ('WEB', 'VIS', 'SOR', 'BUP')"
			+ " AND sod.is_active = 'Y'"
			+ " AND so.sales_id IN :listSalesId"
			+ " ORDER BY so.sales_order_no, sod.discount_seq", nativeQuery = true)
	List<SalesOrderDiscounts> findSODiscountSONoHierarchy(@Param("salesOrderNo") String salesOrderNo, @Param("listSalesId") List<Integer> listSalesId);

	@Query(value = "SELECT advocate_type"
			+ " FROM adv_identity.client_advocates"
			+ " WHERE advocate_id = :parentAdvocate"
			+ " and client_id = :idClient", nativeQuery = true)
	String getAdvocateType(@Param("parentAdvocate") Integer parentAdvocate, @Param("idClient") String idClient);
	
	@Query(value = "SELECT advocate_id as advocateId"
			+ " FROM adv_identity.client_advocates"
			+ " where client_id = :idClient"
			+ " and advocate_type != 'STR'", nativeQuery = true)
//			+ " and status IN ('ACT', 'SUS')", nativeQuery = true)
	List <Integer> findAllAdvocatesIdByClientId(@Param("idClient") String idClient);
	
	//alternate findByClientAndDateStringHierarchy
	@Query(value = "SELECT so.sales_order_no AS salesOrderNo, so.client_id AS clientId, so.status AS status, STR_TO_DATE(so.creation_date,'%d-%m-%Y') as creationDate, so.sales_channel as salesChannel,"
			+ "soi.seq AS soiSeq, soi.product_code AS productCode, soi.quantity AS quantity, soi.fulfilled_quantity AS fulfilledQuantity, soi.price AS price, soi.is_active AS isActive,"
			+ "cp.product_name AS productName, cp.product_group_id AS productGroupId, cp.brand_id AS brandId,"
			+ "cpg.product_group_name AS productGroupName,"
			+ "cb.brand_name AS brandName,"
			+ "soid.sales_order_item_seq AS salesOrderItemSeq, soid.discount_level AS discountLevel, soid.discount_seq AS soidDiscountSeq, soid.value AS soidValue, soid.value_type AS soidValueType, soid.is_active as soidIsActive"
			+ " FROM adv_item.sales_orders so"
			+ " INNER JOIN adv_item.sales_order_items soi ON so.sales_order_no = soi.sales_order_no"
			+ " INNER JOIN adv_item.client_products cp ON cp.product_code = soi.product_code"
			+ " INNER JOIN adv_item.client_product_groups cpg ON cpg.product_group_id = cp.product_group_id"
			+ " INNER JOIN adv_item.client_brands cb ON cb.brand_id = cp.brand_id"
			+ " LEFT JOIN adv_item.sales_order_item_discounts soid ON soid.sales_order_no = soi.sales_order_no"
			+ " AND soid.sales_order_item_seq = soi.seq"
			+ " WHERE so.client_id = :idClient"
			+ " AND STR_TO_DATE(so.creation_date,'%d-%m-%Y') between :startTime and :endTime"
			+ " AND so.status IN ('FUL', 'PRE', 'PUN')"
			+ " AND soi.is_active = 'Y'"
			+ " AND so.sales_channel IN ('WEB', 'VIS', 'SOR', 'BUP')"
			+ " AND so.sales_id IN :listSalesId"
			+ " AND cp.client_id = :idClient"
			+ " AND cp.is_active = 'Y'"
			+ " AND cpg.product_group_name <>'CLIENT_MERCHANDISE'"
			+ " ORDER BY so.sales_order_no, soi.seq,"
			+ " case when discount_level = 'SIU' then 0 when discount_level = 'SIS' then 1 else 2 end,"
			+ " soid.discount_seq", nativeQuery = true)
	List <SalesOrderTransaction> findProductInSOByClientAndDateHierarchy(@Param("idClient") String idClient, @Param("startTime") String startTime, @Param("endTime") String endTime, @Param("listSalesId") List<Integer> listSalesId);

	@Query(value = "SELECT COUNT(DISTINCT advocate_id)"
			+ " FROM adv_identity.client_advocates"
			+ " WHERE client_id = :idClient"
			+ " AND advocate_type = 'STR'"
			+ " AND status = 'ACT'"
			+ " AND creation_time <= :endTime"
//			+ " AND creation_time <= '2019-12-30 23:59:59'"
//			+ " AND creation_time <= '2019-11-21 23:59:59'"
			+ " AND advocate_id IN :listAdvocateId", nativeQuery = true)
//	Integer totalRegisteredStore(@Param("idClient") String idClient, @Param("listAdvocateId") List<Integer> listAdvocateId);
	Integer totalRegisteredStore(@Param("idClient") String idClient, @Param("endTime") String endTime,
			@Param("listAdvocateId") List<Integer> listAdvocateId);

	@Query(value = "SELECT COUNT(DISTINCT v.visitee_id)"
			+ " FROM adv_identity.visit_activities va"
			+ " INNER JOIN adv_identity.visits v"
			+ " ON va.visit_id = v.visit_id"
			+ " WHERE v.client_id = :idClient"
//			+ " AND va.creation_time between '2019-12-01 00:00:00' and '2019-12-30 23:59:59'"
			+ " AND va.creation_time between :startTime and :endTime"
//			+ " AND va.creation_time >= :startTime"
//			+ " AND va.creation_time <= :endTime"
			+ " AND v.visitee_id IN :listAdvocateId", nativeQuery = true)
//	Integer totalVisitedStore(@Param("idClient") String idClient, @Param("listAdvocateId") List<Integer> listAdvocateId);
	Integer totalVisitedStore(@Param("idClient") String idClient, @Param("startTime") String startTime, @Param("endTime") String endTime,
			@Param("listAdvocateId") List<Integer> listAdvocateId);

	@Query(value = "SELECT COUNT(DISTINCT so.customer_id) AS total_productive_store"
			+ " FROM adv_item.sales_orders so"
			+ " INNER JOIN adv_identity.client_advocates ca"
			+ " ON so.customer_id = ca.advocate_id"
			+ " and so.client_id = ca.client_id"
			+ " INNER JOIN adv_item.sales_order_items soi"
			+ " ON so.sales_order_no = soi.sales_order_no"
			+ " WHERE so.client_id = :idClient"
			+ " AND str_to_date(so.creation_date, '%d-%m-%Y') between str_to_date(:startTime, '%d-%m-%Y') and str_to_date(:endTime, '%d-%m-%Y')"
			+ " AND soi.is_active = 'Y'"
			+ " AND advocate_id IN :listAdvocateId", nativeQuery = true)
//	Integer totalProductiveStore(@Param("idClient") String idClient, @Param("listAdvocateId") List<Integer> listAdvocateId);
	Integer totalProductiveStore(@Param("idClient") String idClient, @Param("startTime") String startTime, @Param("endTime") String endTime,
			@Param("listAdvocateId") List<Integer> listAdvocateId);

	@Query(value = "SELECT sales_group_id"
			+ " FROM adv_identity.client_advocates"
			+ " WHERE advocate_id IN :listAdvocateId,", nativeQuery = true)
	List<Integer> getSalesGroupId(@Param("listAdvocateId") List<Integer> listAdvocateId);

	@Query(value = "SELECT ca.advocate_id"
			+ " FROM adv_identity.client_advocates ca"
			+ " WHERE ca.client_id = :clientId"
			+ " AND ca.sales_group_id IN (:listSalesGroupId)", nativeQuery = true)
	List<Integer> getAllAdvIdInSalesGroupClientId(@Param("listSalesGroupId") List<Integer> listSalesGroupId);
	
	@Query(value = "SELECT advocate_id as advocateId"
			+ " FROM adv_identity.client_advocates"
			+ " where client_id = :idClient"
			+ " and advocate_type = 'STR'", nativeQuery = true)
//			+ " and status IN ('ACT', 'SUS')", nativeQuery = true)
	List <Integer> findAllAdvocatesInAdvocateTypeSTRByClientId(@Param("idClient") String idClient);

	@Query(value = "SELECT client_id as clientId, advocate_id as advocateId, advocate_type as advocateType, parent_advocate as parentAdvocate, role as role, status as status"
			+ " FROM adv_identity.client_advocates"
			+ " where client_id = :idClient"
			+ " and advocate_type = 'STR'"
			+ " and status = 'ACT'", nativeQuery = true)
	List <ClientAdvocates> findAllAdvocatesIdInAdvocateTypeSTRByClientId(@Param("idClient") String idClient);
	
	@Query(value = "SELECT sg.sales_group_name AS salesGroupName, COUNT(DISTINCT ca.advocate_id) AS registeredStore"
			+ " FROM adv_identity.client_advocates ca"
			+ " INNER JOIN adv_identity.sales_groups sg"
			+ " ON sg.sales_group_id = ca.sales_group_id"
			+ " WHERE ca.client_id = :idClient"
			+ " AND ca.advocate_type = 'STR'"
			+ " AND ca.status = 'ACT'"
			+ " AND ca.creation_time <= :endTime"
//			+ " AND creation_time <= '2019-12-30 23:59:59'"
//			+ " AND creation_time <= '2019-11-21 23:59:59'"
			+ " AND ca.advocate_id IN :listAdvocateId"
			+ " GROUP BY sg.sales_group_name", nativeQuery = true)
//	Integer totalRegisteredStore(@Param("idClient") String idClient, @Param("listAdvocateId") List<Integer> listAdvocateId);
	List<RegisteredStoreWorkgroup> totalRegisteredStoreWorkgroup(@Param("idClient") String idClient, @Param("endTime") String endTime,
			@Param("listAdvocateId") List<Integer> listAdvocateId);

	@Query(value = "SELECT sg.sales_group_name AS salesGroupName, COUNT(DISTINCT v.visitee_id) AS visitedStore"
			+ " FROM adv_identity.visit_activities va"
			+ " INNER JOIN adv_identity.visits v"
			+ " ON va.visit_id = v.visit_id"
			+ " INNER JOIN adv_identity.client_advocates ca"
			+ " ON ca.advocate_id = v.visitee_id"
			+ " INNER JOIN adv_identity.sales_groups sg"
			+ " ON sg.sales_group_id = ca.sales_group_id"
			+ " WHERE v.client_id = :idClient"
//			+ " AND va.creation_time between '2019-12-01 00:00:00' and '2019-12-30 23:59:59'"
			+ " AND va.creation_time between :startTime and :endTime"
//			+ " AND va.creation_time >= :startTime"
//			+ " AND va.creation_time <= :endTime"
			+ " AND v.visitee_id IN :listAdvocateId"
			+ " GROUP BY sg.sales_group_name", nativeQuery = true)
//	Integer totalVisitedStore(@Param("idClient") String idClient, @Param("listAdvocateId") List<Integer> listAdvocateId);
	List<VisitedStoreWorkgroup> totalVisitedStoreWorkgroup(@Param("idClient") String idClient, @Param("startTime") String startTime, @Param("endTime") String endTime,
			@Param("listAdvocateId") List<Integer> listAdvocateId);
	
	@Query(value = "SELECT sg.sales_group_name AS salesGroupName, COUNT(DISTINCT so.customer_id) AS productiveStore"
			+ " FROM adv_item.sales_orders so"
			+ " INNER JOIN adv_identity.client_advocates ca"
			+ " ON so.customer_id = ca.advocate_id"
			+ " and so.client_id = ca.client_id"
			+ " INNER JOIN adv_identity.sales_groups sg"
			+ " ON sg.sales_group_id = ca.sales_group_id"
			+ " INNER JOIN adv_item.sales_order_items soi"
			+ " ON so.sales_order_no = soi.sales_order_no"
			+ " WHERE so.client_id = :idClient"
			+ " AND str_to_date(so.creation_date, '%d-%m-%Y') between str_to_date(:startTime, '%d-%m-%Y') and str_to_date(:endTime, '%d-%m-%Y')"
			+ " AND soi.is_active = 'Y'"
			+ " AND ca.advocate_id IN :listAdvocateId"
			+ " GROUP BY sg.sales_group_name", nativeQuery = true)
//	Integer totalProductiveStore(@Param("idClient") String idClient, @Param("listAdvocateId") List<Integer> listAdvocateId);
	List<ProductiveStoreWorkgroup> totalProductiveStoreWorkgroup(@Param("idClient") String idClient, @Param("startTime") String startTime, @Param("endTime") String endTime,
			@Param("listAdvocateId") List<Integer> listAdvocateId);
	
	@Query(value = "SELECT pr.province_name FROM adv_identity.PROVINCES pr", nativeQuery = true)
	List <String> findAllProvinceName();
	
	@Query(value = "SELECT distinct sg.sales_group_name FROM adv_identity.sales_groups sg"
			+ " INNER JOIN adv_identity.client_advocates ca"
			+ " ON sg.sales_group_id = ca.sales_group_id"
			+ " WHERE ca.client_id = :idClient", nativeQuery = true)
	List <String> findAllSalesGroupName(@Param("idClient") String idClient);
	
	@Query(value = "SELECT pr.province_name AS provinceName, COUNT(DISTINCT ca.advocate_id) AS registeredStore"
			+ " FROM adv_identity.client_advocates ca"
			+ " INNER JOIN adv_identity.advocate_address aa"
			+ " ON ca.advocate_id = aa.advocate_id"
			+ " INNER JOIN adv_identity.PROVINCES pr"
			+ " ON pr.province_code = aa.province"
			+ " WHERE ca.client_id = :idClient"
			+ " AND ca.advocate_type = 'STR'"
			+ " AND ca.status = 'ACT'"
			+ " AND ca.creation_time <= :endTime"
//			+ " AND creation_time <= '2019-12-30 23:59:59'"
//			+ " AND creation_time <= '2019-11-21 23:59:59'"
			+ " AND ca.advocate_id IN :listAdvocateId"
			+ " GROUP BY pr.province_name", nativeQuery = true)
//	Integer totalRegisteredStore(@Param("idClient") String idClient, @Param("listAdvocateId") List<Integer> listAdvocateId);
	List<RegisteredStoreArea> totalRegisteredStoreArea(@Param("idClient") String idClient, @Param("endTime") String endTime,
			@Param("listAdvocateId") List<Integer> listAdvocateId);
	
	@Query(value = "SELECT pr.province_name AS provinceName, COUNT(DISTINCT v.visitee_id) AS visitedStore"
			+ " FROM adv_identity.visit_activities va"
			+ " INNER JOIN adv_identity.visits v"
			+ " ON va.visit_id = v.visit_id"
			+ " INNER JOIN adv_identity.advocate_address aa"
			+ " ON v.visitee_id = aa.advocate_id"
			+ " INNER JOIN adv_identity.PROVINCES pr"
			+ " ON pr.province_code = aa.province"
			+ " WHERE v.client_id = :idClient"
//			+ " AND va.creation_time between '2019-12-01 00:00:00' and '2019-12-30 23:59:59'"
			+ " AND va.creation_time between :startTime and :endTime"
//			+ " AND va.creation_time >= :startTime"
//			+ " AND va.creation_time <= :endTime"
			+ " AND v.visitee_id IN :listAdvocateId"
			+ " GROUP BY pr.province_name", nativeQuery = true)
//	Integer totalVisitedStore(@Param("idClient") String idClient, @Param("listAdvocateId") List<Integer> listAdvocateId);
	List<VisitedStoreArea> totalVisitedStoreArea(@Param("idClient") String idClient, @Param("startTime") String startTime, @Param("endTime") String endTime,
			@Param("listAdvocateId") List<Integer> listAdvocateId);
	
	@Query(value = "SELECT pr.province_name AS provinceName, COUNT(DISTINCT so.customer_id) AS productiveStore"
			+ " FROM adv_item.sales_orders so"
			+ " INNER JOIN adv_identity.client_advocates ca"
			+ " ON so.customer_id = ca.advocate_id"
			+ " AND so.client_id = ca.client_id"
			+ " INNER JOIN adv_item.sales_order_items soi"
			+ " ON so.sales_order_no = soi.sales_order_no"
			+ " INNER JOIN adv_identity.advocate_address aa"
			+ " ON ca.client_id = aa.advocate_id"
			+ " INNER JOIN adv_identity.PROVINCES pr"
			+ " ON pr.province_code = aa.province"
			+ " WHERE so.client_id = :idClient"
			+ " AND str_to_date(so.creation_date, '%d-%m-%Y') between str_to_date(:startTime, '%d-%m-%Y') and str_to_date(:endTime, '%d-%m-%Y')"
			+ " AND soi.is_active = 'Y'"
			+ " AND ca.advocate_id IN :listAdvocateId"
			+ " GROUP BY pr.province_name", nativeQuery = true)
//	Integer totalProductiveStore(@Param("idClient") String idClient, @Param("listAdvocateId") List<Integer> listAdvocateId);
	List<ProductiveStoreArea> totalProductiveStoreArea(@Param("idClient") String idClient, @Param("startTime") String startTime, @Param("endTime") String endTime,
			@Param("listAdvocateId") List<Integer> listAdvocateId);
}
