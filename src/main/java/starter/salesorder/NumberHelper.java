package starter.salesorder;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * A class for number conversion, formatting etc.
 * 
 * @author andrea
 *
 */

public class NumberHelper {

	/**
	 * format double value into specified decimal places
	 * 
	 * @param value (double), and the decimal places (int)
	 * e.g. value: 29.752, places: 2
	 * @return double 29.75
	 **/
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	public static double round(BigDecimal value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    value = value.setScale(places, RoundingMode.HALF_UP);
	    return value.intValue();
	}
	
	public static String convertIntToStringSeparator(int number){
		try {
			NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
		    String numberAsString = numberFormat.format(number);
		    return numberAsString;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ""+number;
		
	}
	
	/**
	 * format double value into integer
	 * 
	 * @param value (double)
	 * e.g. value: 29.752
	 * @return integer 30
	 **/
	
	public static int roundToInteger(double value) {
	    
		return (int) Math.round(value);
		
	}
	

	public static String doubleMeterToFormatKm(double amount){
		DecimalFormat formatter = new DecimalFormat("#,###.##");
		if(amount>=1000){
			System.out.println(amount/1000);
			return formatter.format(amount/1000).toString()+" km";
		}else{
			return formatter.format(amount).toString()+" m";
		}
	}
		
	public static boolean isNumeric(String str) {

		try {
			if (StringUtils.isNullOrEmpty(str)) {
				return false;
			}
			// if not valid, it will throw NumberFormatException
			Double.parseDouble(str);

			return true;

		} catch (Exception e) {
			return false;
		}
	}
	
}
