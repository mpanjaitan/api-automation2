package starter;

import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class Login {

	private static String envUrl;
	
	Login (String environment){
		EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();
    	// ---add for using full properties
    	String urlRun = environment;

    	String urlRunValue = variables.getProperty(urlRun);
    	Login.envUrl = urlRunValue;
    	
    	System.out.println("environment in login class: " + envUrl);
    	
	}
	
	public static String getEnvUrl() {
        return envUrl;
    }
}
