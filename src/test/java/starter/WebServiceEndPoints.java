package starter;

public enum WebServiceEndPoints {
    STATUS("http://localhost:8080/api/status"),
    TRADE("http://localhost:8080/api/trade"),
    STAFFLIST("https://sfa-api-integ.advotics.com/web/staff"),
    SALESORDER("https://sfa-api-integ.advotics.com/web/salesOrder"),
    SALESORDERBD("http://localhost:8080/soNew/all"),
    SALESORDERBYDATE("http://localhost:8080/soByDate"),
    SALESORDERBYDATESTRING("http://localhost:8080/soByDateString"),
    SALESORDERBYDATESTRINGPLIS("http://localhost:8080/soByDateStringPlis"),
    REVENUEBYSOANDDATE("http://localhost:8080/soRevenueBySOAndDate"),
    REVENUETOTALBYSOANDDATE("http://localhost:8080/soRevenueTotalBySOAndDate"),
    REVENUEBYPRODUCTINSIGHT("http://localhost:8080/soRevenueByProductInsight"),
    REVENUETOTALBYPRODUCTINSIGHT("http://localhost:8080/soRevenueTotalByProductInsight"),
    PRODUCTINSIGHTQTYFULFILLED("http://localhost:8080/soProductInsightQtyFulfilled"),
    TOTALBYPRODUCTINSIGHTQTYFULFILLED("http://localhost:8080/soTotalByProductInsightQtyFulfilled"),
    PRODUCTINSIGHTSCARCITYLEVEL("http://localhost:8080/soProductInsightScarcityLevel"),
    TOTALBYPRODUCTINSIGHTSCARCITYLEVEL("http://localhost:8080/soTotalByProductInsightScarcityLevel"),
    CUSTOMEROVERVIEW("http://localhost:8080/dashboardCustomerOverview"),
    CUSTOMEROVERVIEWWORKGROUP("http://localhost:8080/dashboardCustomerOverviewWorkgroup"),
    CUSTOMEROVERVIEWAREA("http://localhost:8080/dashboardCustomerOverviewArea"),
    //for sales overtime widget
    SALESOVERTIMEWIDGET("https://sfa-api-integ.advotics.com/web/getWidgetData"),
    SALESOVERTIMEWIDGETSBX("https://sfa-api-sbx.advotics.com/web/getWidgetData"),
    SALESOVERTIMEWIDGETPROD("https://sfa-api.advotics.com/web/getWidgetData"),
    WIDGETDATAINTEG("https://sfa-api-integ.advotics.com/web/getWidgetData"),
    WIDGETDATASBX("https://sfa-api-sbx.advotics.com/web/getWidgetData"),
	WIDGETDATAPROD("https://sfa-api.advotics.com/web/getWidgetData");

    private final String url;

    WebServiceEndPoints(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
