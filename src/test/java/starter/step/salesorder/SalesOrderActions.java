package starter.step.salesorder;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import starter.WebServiceEndPoints;
import starter.salesorder.DateTimeHelper;
import starter.salesorder.SalesOvertimeBE;
import starter.salesorder.SalesOvertimeCalculatorService;
import starter.salesorder.SalesOvertimeChartBuilder;
import starter.salesorder.StringUtils;

public class SalesOrderActions {
	
	String envUrl;
	
//	@Autowired
	SalesOrdersResponse soResponse = new SalesOrdersResponse();
	
	@Autowired
	SalesOvertimeCalculatorService salesOvertimeService; // = new SalesOvertimeCalculatorService();
	
	private String SKU = "SKU";
	private String productGroup = "productGroup";
	private String brand = "brand";

	/*SalesOvertimeChartBuilder salesOvertimeChartBuilder;
	StringUtils stringUtils;
	DateTimeHelper dateTimeHelper;*/
	
	@Step
	public void showSOByDefaultDate() {
		SerenityRest.given()
		.contentType("application/json")
		.when().get(WebServiceEndPoints.SALESORDERBD.getUrl());
	}

	@Step("Save SO Number")
	public void saveSONumber(String soNumber) {
//    	String accessToken = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0aW5nc3RsIiwiYXBwS2V5IjoiZjNhODM2NGFhZmNlOGZkZmEyMjQyZGIyYjZjY2UwMDEiLCJjbGllbnRJZCI6MSwidXNlcklkIjo4MTIsImNvbXBhbnlJZCI6MTEsImFkdm9jYXRlSWQiOiI5OTk3NyIsImxvZ2luRXZlbnRJZCI6MjkzNzM0LCJzY29wZSI6IkRTViBQTVYgVk1WIE5TUiBTTlYgU1NWIENIQyBTT1YgU09FIFNJViBTSUUgUENWIERPViBET0UgU1ZWIFNWRSBVTUUgVU1WIExQRSBGUlYgTFBWIENJViBGUkUgVVJFIFNUViBTVEUgU1JWIFNSRSBTREUgQ1VFIENVViBDU1YgQ0FEIE1JViBHQVYgRDAxIEQwMiBEMDMgRDA0IEQwNSBEMDYgTUdEIE1HUiBSMDEgUjAyIFIwMyBCVVYgQlVFIFIwNyBCMDIgQjA0IExUViBMVFYgQjAxIFIwNiBTTzIgQU1FIEFNUyBBTVYgQjA3IFdQTyBEMDcgU1YyIEJCQSBETzIgVE1WIFJNViBCMDMgQjEzIFNWUiBTT1IgU09GIEIxNSBCMTYgUjE2IDFJQSBBUFYgSU5WIElTSSBJU0wgUENWIFBDRSBCMDUgQjE3IENQViBCMDkgSUNWIDFQTCBTUFYgQ0FEIENIQyBETzIgRE9FIERPViBEU1YgRlJFIEZSViBMUEUgTFBWIE1JViBOU1IgUENWIFBNViBSMDcgU0lFIFNJViBTTlYgU09FIFNPViBTUEEgU1BFIFNSRSBTUlYgU1NWIFNURSBTVFYgU1YyIFNWRSBTVlYgVU1FIFVNViBVUkUgVk1WIFZTTyBXUE8gMUlBIEFNRSBBTVMgQU1WIEFQViBCMDcgQ0FEIENIQyBDUEUgQ1BWIERPMiBET0UgRE9WIERTViBGUkUgRlJWIElOViBJU0kgSVNMIExQRSBMUFYgTFRWIE1JViBOU1IgUENWIFBNViBQUkMgUjA3IFJNViBTSUUgU0lWIFNOViBTT0UgU09WIFNSRSBTUlYgU1NWIFNURSBTVFYgU1YyIFNWRSBTVlIgU1ZWIFRNViBVTUUgVU1WIFVSRSBWTVYgV1BPIFdSViBSMjAgMUlBIDFQVCBBTFYgQU1FIEFNUyBBTVYgQVBWIEIwMSBCMDIgQjAzIEIwNCBCMDcgQjA5IEIxMiBCMTMgQjE1IEIxNiBCQkEgQlVFIEJVViBDQUQgQ0hDIENQRSBDUFYgQ1VFIENVViBEMDEgRDAyIEQwMyBEMDQgRDA1IEQwNiBETzIgRE9FIERPViBEU1YgRlJFIEZSViBHQVYgSU5WIElTSSBJU0wgS1JWIExQRSBMUFYgTFRWIE1HRCBNR1IgTUlWIE5TUiBQQ0UgUENWIFBNViBQUkMgUjAxIFIwMiBSMDMgUjA2IFIwNyBSMTYgUk1WIFNERSBTRFYgU0lFIFNJViBTTlYgU09FIFNPRiBTT1IgU09WIFNSRSBTUlYgU1NWIFNURSBTVFYgU1YyIFNWRSBTVlIgU1ZWIFVNRSBVTVYgVVJFIFZNViBXUE8gV1JWIFIxMSBSMDEgUjAxIFIxNSBEMTQgQ1ZMIENVQSBDRUEgQ0xBIEFDTCBBQ1YgSVNBIENNViBGUkEgTEZBIEwxQSBMM0EgTDJBIExUUiBMT0EgTE9CIExNQSBQUkUgUEVBIFBTViBQUlYgUExFIFBMTyBMT1MgT1VUIEhNViBQTEEgUlRWIFNWRCBTVkQgU1ZEIFNWRCBCMTkgQjIxIEIyMiBCMjAgQjA5IDFQVCBQSkUgUjEyIFIyMSBSMTUgUjIyIiwid29ya0dyb3VwSWQiOjEsImV4cCI6MTU3MTkzNjM5OX0.cCbaSDsc8X9_VTRFtEu10Qs3aNt7yw2uRVJzw7hQsaSYl37RGJdjRKVDhnsv0T8jtr4qYsKCzD4M1PuS5nAtuw";
    	String accessToken = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0aW5nc3RsIiwiYXBwS2V5IjoiZjNhODM2NGFhZmNlOGZkZmEyMjQyZGIyYjZjY2UwMDEiLCJjbGllbnRJZCI6MSwidXNlcklkIjo4MTIsImNvbXBhbnlJZCI6MTEsImFkdm9jYXRlSWQiOiI5OTk3NyIsImxvZ2luRXZlbnRJZCI6ODYyNTkxLCJzY29wZSI6IkRTViBQTVYgVk1WIE5TUiBTTlYgU1NWIENIQyBTT1YgU0lWIFNJRSBQQ1YgRE9WIERPRSBTVkUgVU1FIFVNViBMUEUgRlJWIExQViBDSVYgRlJFIFVSRSBTVFYgU1RFIFNSViBTUkUgU0RFIENVRSBDVVYgQ1NWIENBRCBNSVYgR0FWIEQwMSBEMDIgRDAzIEQwNCBEMDUgRDA2IE1HRCBNR1IgUjAxIFIwMiBSMDMgQlVWIEJVRSBSMDcgQjAyIEIwNCBMVFYgTFRWIEIwMSBSMDYgU08yIEFNRSBBTVMgQU1WIEIwNyBXUE8gRDA3IFNWMiBCQkEgU08yIFIxMyBTT1IgU09GIERPMiBCMDMgS1JWIENMQSBDRUEgUjEyIEIxMSBCMDkgQjEyIFNWUiBCMTMgQjE1IFIxNiBCMTYgUk1WIFRNViBCMTcgQjA1IDFJQSBBUFYgSU5WIElTSSBJU0wgUENWIFBDRSBDUEUgQ1BWIFBSQyBTUEUgU1BWIDFQTCBSMjAgRDE0IFJUViBTVkQgQjIyIDFQVCBQSlYgUEpFIFNQQSBSMjEgQjE4IElDViBCMTggUjIyIiwid29ya0dyb3VwSWQiOjEsImV4cCI6MTU3MzU3Nzk5OX0.jz8IrcYUomM3-ZdKuUdcc3PsuZD7pT06n14VUsL0POUJ4bRo33_serrLo3rXp2AV7gJaRU2As50qyFOmH2_oDg";
    	
    	Map<String, String> params = new HashMap<>();
    	params.put("action", "detail");
		params.put("orderNo", "MzE3MS0zOTAyNTEtNTEzODEyLTE1MzUyMzQwMw==");
		params.put("signature", "ce842df395f379022a8d1ecb0fef838e");
		
		SerenityRest.given().auth().oauth2(accessToken)
		.contentType("application/json")
		.and().params(params)
		.when().get(WebServiceEndPoints.SALESORDER.getUrl());
		
		/*List<SalesOrderTransaction> salesOrders = repository.findByDefaultDate();
    	
    	System.out.println("salesOrders count: " + salesOrders.size());*/
	}

	@Step
	public void getItemValid(String item) {
		System.out.println("itemnya adalah: " + SerenityRest.then().extract().body().jsonPath().get("items.unitDiscountList.value"));
	}

	@Step
	public String setEnvironment(String environment) {
    	EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();
    	// ---add for using full properties
    	String urlRun = environment;

    	String urlRunValue = variables.getProperty(urlRun);
    	envUrl = urlRunValue;
    	
    	System.out.println("environment: " + envUrl);
    	
    	return envUrl;
	}

	@Step
	public void showSOInRangeTime(String idClient, String startTime, String endTime) {
		Map<String, String> params = new HashMap<>();
		params.put("idClient", idClient);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		SerenityRest.given()
		.contentType("application/json")
		.and().params(params)
		.when().get(WebServiceEndPoints.SALESORDERBYDATE.getUrl());
	}

	@Step("get Sales Revenue")
	public void getSalesRevenue(String startTime, String endTime) {
//    	String accessToken = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0aW5nc3RsIiwiYXBwS2V5IjoiZjNhODM2NGFhZmNlOGZkZmEyMjQyZGIyYjZjY2UwMDEiLCJjbGllbnRJZCI6MSwidXNlcklkIjo4MTIsImNvbXBhbnlJZCI6MTEsImFkdm9jYXRlSWQiOiI5OTk3NyIsImxvZ2luRXZlbnRJZCI6MjkzNzM0LCJzY29wZSI6IkRTViBQTVYgVk1WIE5TUiBTTlYgU1NWIENIQyBTT1YgU09FIFNJViBTSUUgUENWIERPViBET0UgU1ZWIFNWRSBVTUUgVU1WIExQRSBGUlYgTFBWIENJViBGUkUgVVJFIFNUViBTVEUgU1JWIFNSRSBTREUgQ1VFIENVViBDU1YgQ0FEIE1JViBHQVYgRDAxIEQwMiBEMDMgRDA0IEQwNSBEMDYgTUdEIE1HUiBSMDEgUjAyIFIwMyBCVVYgQlVFIFIwNyBCMDIgQjA0IExUViBMVFYgQjAxIFIwNiBTTzIgQU1FIEFNUyBBTVYgQjA3IFdQTyBEMDcgU1YyIEJCQSBETzIgVE1WIFJNViBCMDMgQjEzIFNWUiBTT1IgU09GIEIxNSBCMTYgUjE2IDFJQSBBUFYgSU5WIElTSSBJU0wgUENWIFBDRSBCMDUgQjE3IENQViBCMDkgSUNWIDFQTCBTUFYgQ0FEIENIQyBETzIgRE9FIERPViBEU1YgRlJFIEZSViBMUEUgTFBWIE1JViBOU1IgUENWIFBNViBSMDcgU0lFIFNJViBTTlYgU09FIFNPViBTUEEgU1BFIFNSRSBTUlYgU1NWIFNURSBTVFYgU1YyIFNWRSBTVlYgVU1FIFVNViBVUkUgVk1WIFZTTyBXUE8gMUlBIEFNRSBBTVMgQU1WIEFQViBCMDcgQ0FEIENIQyBDUEUgQ1BWIERPMiBET0UgRE9WIERTViBGUkUgRlJWIElOViBJU0kgSVNMIExQRSBMUFYgTFRWIE1JViBOU1IgUENWIFBNViBQUkMgUjA3IFJNViBTSUUgU0lWIFNOViBTT0UgU09WIFNSRSBTUlYgU1NWIFNURSBTVFYgU1YyIFNWRSBTVlIgU1ZWIFRNViBVTUUgVU1WIFVSRSBWTVYgV1BPIFdSViBSMjAgMUlBIDFQVCBBTFYgQU1FIEFNUyBBTVYgQVBWIEIwMSBCMDIgQjAzIEIwNCBCMDcgQjA5IEIxMiBCMTMgQjE1IEIxNiBCQkEgQlVFIEJVViBDQUQgQ0hDIENQRSBDUFYgQ1VFIENVViBEMDEgRDAyIEQwMyBEMDQgRDA1IEQwNiBETzIgRE9FIERPViBEU1YgRlJFIEZSViBHQVYgSU5WIElTSSBJU0wgS1JWIExQRSBMUFYgTFRWIE1HRCBNR1IgTUlWIE5TUiBQQ0UgUENWIFBNViBQUkMgUjAxIFIwMiBSMDMgUjA2IFIwNyBSMTYgUk1WIFNERSBTRFYgU0lFIFNJViBTTlYgU09FIFNPRiBTT1IgU09WIFNSRSBTUlYgU1NWIFNURSBTVFYgU1YyIFNWRSBTVlIgU1ZWIFVNRSBVTVYgVVJFIFZNViBXUE8gV1JWIFIxMSBSMDEgUjAxIFIxNSBEMTQgQ1ZMIENVQSBDRUEgQ0xBIEFDTCBBQ1YgSVNBIENNViBGUkEgTEZBIEwxQSBMM0EgTDJBIExUUiBMT0EgTE9CIExNQSBQUkUgUEVBIFBTViBQUlYgUExFIFBMTyBMT1MgT1VUIEhNViBQTEEgUlRWIFNWRCBTVkQgU1ZEIFNWRCBCMTkgQjIxIEIyMiBCMjAgQjA5IDFQVCBQSkUgUjEyIFIyMSBSMTUgUjIyIiwid29ya0dyb3VwSWQiOjEsImV4cCI6MTU3MTkzNjM5OX0.cCbaSDsc8X9_VTRFtEu10Qs3aNt7yw2uRVJzw7hQsaSYl37RGJdjRKVDhnsv0T8jtr4qYsKCzD4M1PuS5nAtuw";
    	String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0aW5nc3RsIiwiYXBwS2V5IjoiOTg3ZjRhMjc2Y2VkNDU0NjE4ZmRlYjNjYWNlNjhkNGJmYzViOGFlMiIsImNsaWVudElkIjoxLCJ1c2VySWQiOjgxMiwiY29tcGFueUlkIjoxMSwiYWR2b2NhdGVJZCI6Ijk5OTc3IiwibG9naW5FdmVudElkIjoyOTUwNDUsInNjb3BlIjoiRFNWIFBNViBWTVYgTlNSIFNOViBTU1YgQ0hDIFNPViBTT0UgU0lWIFNJRSBQQ1YgRE9WIERPRSBTVlYgU1ZFIFVNRSBVTVYgTFBFIEZSViBMUFYgQ0lWIEZSRSBVUkUgU1RWIFNURSBTUlYgU1JFIFNERSBDVUUgQ1VWIENTViBDQUQgTUlWIEdBViBEMDEgRDAyIEQwMyBEMDQgRDA1IEQwNiBNR0QgTUdSIFIwMSBSMDIgUjAzIEJVViBCVUUgUjA3IEIwMiBCMDQgTFRWIExUViBCMDEgUjA2IFNPMiBBTUUgQU1TIEFNViBCMDcgV1BPIEQwNyBTVjIgQkJBIERPMiBUTVYgUk1WIEIwMyBCMTMgU1ZSIFNPUiBTT0YgQjE1IEIxNiBSMTYgMUlBIEFQViBJTlYgSVNJIElTTCBQQ1YgUENFIEIwNSBCMTcgQ1BWIEIwOSBJQ1YgMVBMIFNQViBDQUQgQ0hDIERPMiBET0UgRE9WIERTViBGUkUgRlJWIExQRSBMUFYgTUlWIE5TUiBQQ1YgUE1WIFIwNyBTSUUgU0lWIFNOViBTT0UgU09WIFNQQSBTUEUgU1JFIFNSViBTU1YgU1RFIFNUViBTVjIgU1ZFIFNWViBVTUUgVU1WIFVSRSBWTVYgVlNPIFdQTyAxSUEgQU1FIEFNUyBBTVYgQVBWIEIwNyBDQUQgQ0hDIENQRSBDUFYgRE8yIERPRSBET1YgRFNWIEZSRSBGUlYgSU5WIElTSSBJU0wgTFBFIExQViBMVFYgTUlWIE5TUiBQQ1YgUE1WIFBSQyBSMDcgUk1WIFNJRSBTSVYgU05WIFNPRSBTT1YgU1JFIFNSViBTU1YgU1RFIFNUViBTVjIgU1ZFIFNWUiBTVlYgVE1WIFVNRSBVTVYgVVJFIFZNViBXUE8gV1JWIFIyMCAxSUEgMVBUIEFMViBBTUUgQU1TIEFNViBBUFYgQjAxIEIwMiBCMDMgQjA0IEIwNyBCMDkgQjEyIEIxMyBCMTUgQjE2IEJCQSBCVUUgQlVWIENBRCBDSEMgQ1BFIENQViBDVUUgQ1VWIEQwMSBEMDIgRDAzIEQwNCBEMDUgRDA2IERPMiBET0UgRE9WIERTViBGUkUgRlJWIEdBViBJTlYgSVNJIElTTCBLUlYgTFBFIExQViBMVFYgTUdEIE1HUiBNSVYgTlNSIFBDRSBQQ1YgUE1WIFBSQyBSMDEgUjAyIFIwMyBSMDYgUjA3IFIxNiBSTVYgU0RFIFNEViBTSUUgU0lWIFNOViBTT0UgU09GIFNPUiBTT1YgU1JFIFNSViBTU1YgU1RFIFNUViBTVjIgU1ZFIFNWUiBTVlYgVU1FIFVNViBVUkUgVk1WIFdQTyBXUlYgUjExIFIwMSBSMDEgUjE1IEQxNCBDVkwgQ1VBIENFQSBDTEEgQUNMIEFDViBJU0EgQ01WIEZSQSBMRkEgTDFBIEwzQSBMMkEgTFRSIExPQSBMT0IgTE1BIFBSRSBQRUEgUFNWIFBSViBQTEUgUExPIExPUyBPVVQgSE1WIFBMQSBSVFYgU1ZEIFNWRCBTVkQgU1ZEIEIxOSBCMjEgQjIyIEIyMCBCMDkgMVBUIFBKRSBSMTIgUjIxIFIxNSBSMjIgVE5UIiwid29ya0dyb3VwSWQiOjEsImV4cCI6MTU3MzY2NDM5OX0.t2KnN-_Pe7kpN0SmcIT2h1OEFc6vAf7Iv25yeGaB6_UIxd2ZvVNqNt85LRyIe7n8cIRNmO7-FC116bwvf1GQ5A";
    	
    	Map<String, String> params = new HashMap<>();
    	params.put("action", "detail");
		params.put("orderNo", "MzE3MS0zOTAyNTEtNTEzODEyLTE1MzUyMzQwMw==");
		params.put("signature", "ce842df395f379022a8d1ecb0fef838e");
		
		SerenityRest.given().auth().oauth2(accessToken)
		.contentType("application/json")
		.and().params(params)
		.when().get(WebServiceEndPoints.SALESORDER.getUrl());
		
		/*List<SalesOrderTransaction> salesOrders = repository.findByDefaultDate();
    	
    	System.out.println("salesOrders count: " + salesOrders.size());*/
	}

	@Step
	public void hitBESalesOvertime(String viewBy, String compareBy, String idClient, String startTime, String endTime, String environment) {
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjI5NzYyNCwic2NvcGUiOiJVUkUgQ0hDIERTViBLUlYgTlNSIFNOViBTUkUgU1JWIFNTViBTVEUgU1RWIFVNRSBVTVYgVk1WIFNPRSBTT1YgQUNMIEFDViBBTFYgQ0VBIENMQSBDTVYgQ1NWIENVQSBDVUUgQ1VWIENWTCBET0UgRE9WIEZSQSBITVYgTDFBIEwyQSBMM0EgTEZBIExNQSBMT0EgTE9CIE1HRCBPVVQgUENFIFBDViBQRUEgUExBIFBMRSBQTE8gUE1WIFBSRSBQUlYgUFNWIFNERSBTRFYgU0lFIFNJViBTVkUgU1ZWIENJViBGUkUgRlJWIExQRSBMUFYgQ0FEIE1JViBEMDEgRDAyIEQwMyBEMDQgRDA1IEQwNiBNR0QgTUdSIFIwMSBSMDIgUjAzIFIwNyBMVFYgR0FWIEJVRSBCMDIgQU1FIEFNViBCQkEgQjA3IFNWMiBXUE8gUjA2IEFNUyBCMDQgQjAxIFNPMiBCVVYgU1ZSIFIxNiAxSUEgQVBWIElOViBJU0kgSVNMIFBDViBQQ0UgQ1BFIENQViBQUkMgV1JWIFNPRiAxUEwgU1BWIFIxMSBSTVYgVE1WIFNWRCBEMDcgSUNWIERPMiBJU0EgQjEzIEIxMiBCMTYgQjE1IEIwMyBCMjAgQjE3IEIyMiBCMDUgTFRSIExPUyBTUEEgU1BFIFJUViBTT1IgUjE1IFZTTyBETE8iLCJ3b3JrR3JvdXBJZCI6MSwiZXhwIjoxNTc1OTEwNzk5fQ.oPQONDABeXpbECWhsojA9knUdVso0YgPmqzEG_aC26wTConpANs5Uo1WEEOjBLa_CvnQ_c3V4kOSWdPHE-wAMg";
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjMwNzc3MCwic2NvcGUiOiJVUkUgQ0hDIERTViBLUlYgTlNSIFNOViBTUkUgU1JWIFNTViBTVEUgU1RWIFVNRSBVTVYgVk1WIFNPRSBTT1YgQUNMIEFDViBBTFYgQ0VBIENMQSBDTVYgQ1NWIENVQSBDVUUgQ1VWIENWTCBET0UgRE9WIEZSQSBITVYgTDFBIEwyQSBMM0EgTEZBIExNQSBMT0EgTE9CIE1HRCBPVVQgUENFIFBDViBQRUEgUExBIFBMRSBQTE8gUE1WIFBSRSBQUlYgUFNWIFNERSBTRFYgU0lFIFNJViBTVkUgU1ZWIENJViBGUkUgRlJWIExQRSBMUFYgQ0FEIE1JViBEMDEgRDAyIEQwMyBEMDQgRDA1IEQwNiBNR0QgTUdSIFIwMSBSMDIgUjAzIFIwNyBMVFYgR0FWIEJVRSBCMDIgQU1FIEFNViBCQkEgQjA3IFNWMiBXUE8gUjA2IEFNUyBCMDQgQjAxIFNPMiBCVVYgU1ZSIFIxNiBSTVYgVE1WIFNQViBSMjAgRDE0IFNWRCBSMjIgSUNWIEJDTSBBUFYiLCJ3b3JrR3JvdXBJZCI6MSwiZXhwIjoxNTc1OTEwNzk5fQ.iQS453ZY6ZIwsQ4dAbb5Wg0Axtpictz2vNT48olts_R36zXVAVw7361nOBbd_ijkDj9CfNVN2il_oK3_QwkA-Q";
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjk3ODI5OSwic2NvcGUiOiJVUkUgQ0hDIERTViBLUlYgTlNSIFNOViBTUkUgU1JWIFNTViBTVEUgU1RWIFVNRSBVTVYgVk1WIFNPRSBTT1YgQUNMIEFDViBBTFYgQ0VBIENMQSBDTVYgQ1NWIENVQSBDVUUgQ1VWIENWTCBET0UgRE9WIEZSQSBITVYgTDFBIEwyQSBMM0EgTEZBIExNQSBMT0EgTE9CIE1HRCBPVVQgUENFIFBDViBQRUEgUExBIFBMRSBQTE8gUE1WIFBSRSBQUlYgUFNWIFNERSBTRFYgU0lFIFNJViBTVkUgU1YyIENJViBGUkUgRlJWIExQRSBMUFYgQ0FEIE1JViBEMDEgRDAyIEQwMyBEMDQgRDA1IEQwNiBNR0QgTUdSIFIwMSBSMDIgUjAzIFIwNyBMVFYgU1YyIFNPMiBTT1IgU09GIFNWUiBSMTYgRE8yIEFNUyBBTUUgQU1WIEIwNCBCMTMgQjEyIEJVRSBCMTUgQjAxIEIxMSBCMDIgQjA5IEJVViBCMDcgQkJBIEQwNyBHQVYgUjA2IFNQQSBTUEUgMVBMIFNQViBJU0kgVlNPIFdQTyBSMjAgU1ZEIEFQViBQQ0UgUENWIElTQSBCMjIgQjE4IEJCQSBJQ1YgSVNMIENQRSBDUFYgUFJDIElOViBQSkUgUEpWIFIxMSBSVFYgVlBKIEIxNiBCQ00iLCJ3b3JrR3JvdXBJZCI6MSwiZXhwIjoxNTc2ODYxMTk5fQ.EGjqKNvEoy1dh0TaU8whD-Slc8DNrGS5RTgeKoL2DFWRYX1pqfz_ZTFo7yf037dRJQc9FHhCagzvu28Dd7NfbA";
//		prod RAS advo support
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZHZvLnN1cHBvcnQiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEwMDAzNiwidXNlcklkIjo0NTU1LCJjb21wYW55SWQiOjEwMDAzNjEsImFkdm9jYXRlSWQiOiIzNzA0NjEiLCJsb2dpbkV2ZW50SWQiOjk4NTc3Miwic2NvcGUiOiJCVUUgQlVWIENBRCBDSEMgQ0lWIENVRSBDVVYgRE9FIERPViBEU1YgRlJFIEZSViBHQVYgTFBWIE1HUiBNSVYgTlNSIFBDViBETzIgUjAxIFIwMiBSMDcgU0lFIFNJViBTTlYgU09WIFNSRSBTUlYgU1NWIFNURSBTVFYgU1ZFIFNWMiBVTUUgVU1WIFVSRSBWTVYgQjA0IFBNViBQTVYgTFRWIEIwMSBDVVYgQ1VWIEIwMiBHQVYgUjAxIENBRCBNSVYgUjEyIEIwMyBCMDYgRlJFIERPViBNSVYgQU1FIEFNUyBBTVYgU1YyIEdBViBTVjIgU1YyIEZSViBSMDcgUjEzIEIwNyBTVlIgQjEzIEIwOSBQTVYgTFRWIEIxMiBHQVYgQjE1IEIxNiBCMDUgRE9FIFBKViBQSkUgUk1WIFNWViBCMTQgQjE3IFZTTyBBUFYgMVBUIFIyMSBBUlAgR1JQIFZNRCBWUk4gVlJQIEFSUCBHUlAgUk1WIFZNRCBWUk4gVlJQIEQxNCBJQ1YgQkJBIEQwMSBMUFYgTFBFIENQRSBDUFYgUFJDIFZQSiBEMTYgRDE3IFNQViBSMDMgRDE0Iiwid29ya0dyb3VwSWQiOjQzOCwiZXhwIjoxNTc3MTIwMzk5fQ.efFGze-S3HzC7N12X-05v8e5BPhd6vG8ytT99-dNphS9Erx0ZXJL5odaJ5Y0rwr5oKV7c6mOiTGjxEjApYA4uw";
		//prod federal advo cs
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZHZvLmNzIiwiYXBwS2V5IjoiOTg3ZjRhMjc2Y2VkNDU0NjE4ZmRlYjNjYWNlNjhkNGJmYzViOGFlMiIsImNsaWVudElkIjo4MzMzLCJ1c2VySWQiOjc5MSwiY29tcGFueUlkIjo4MzMzMDEsImFkdm9jYXRlSWQiOiIyMCIsImxvZ2luRXZlbnRJZCI6OTQ2NjQ5LCJzY29wZSI6IkRTViBQTVYgVk1WIE5TUiBTTlYgU1NWIENIQyBTT1YgU09FIFNJViBTSUUgUENWIERPViBET0UgU1ZFIFVNRSBVTVYgQ0FEIE1JViBHQVYgRDAxIEQwMiBEMDMgRDA0IEQwNSBEMDYgTUdEIE1HUiBSMDEgUjAyIFIwMyBDU1YgTFRWIEJCQSBTTzIgU1YyIFNWUiBTVkQgQUxWIEFURSBBVFYgQjA0IEJDTSBDTVAgQ1RFIENUViBDVVYgUVJHIFIwNCBSMDUgUjA2IFIwNyBSMDggUjEwIFJXViBTQ0UgU0NWIFNRUiBTVEUgU1RWIFVSRSBSMTYgUjEyIFdQTyIsIndvcmtHcm91cElkIjoxNiwiZXhwIjoxNTc1OTk3MTk5fQ.l6a-FgpXO_P-lFWqlaXBXYv9U8X0kskzOAK3Ru9BA5li4NUmkaFEVCFyZ75ZVIudpBP5XKJdMMOPU1HSlmbrZA";
		//prod nutrifood nf.superuser
		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJuZi5zdXBlcnVzZXIiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjg2ODMsInVzZXJJZCI6NTY4NiwiY29tcGFueUlkIjo4NjgzMDEsImFkdm9jYXRlSWQiOiI1NDE1MzciLCJsb2dpbkV2ZW50SWQiOjk4NjMxMywic2NvcGUiOiJBTFYgQ0FEIENIQyBDSVYgQ1VWIERPMiBET0UgS1JWIE1HRCBNSVYgUE1WIFJXRSBSV1YgU0RWIFNOViBTTzIgU09FIFNSRSBTUlYgU1NWIFNURSBTVFYgU1ZFIFVNRSBVTVYgVVJFIFZNViBCVUUgQlVWIEIwMiBCMDQgTFRSIExUViBTT0UgU09WIFIwMSBSMDIgUjAzIFIwNCBSMDUgUjA2IFIwNyBSMDggUjA5IFIxMCBSMTEgUjEyIEdBViBCMDEgQjAxIEIwMiBCMDMgQjA0IEIwNiBCMDcgQlVFIEJVViBSMDEgUjAyIFIwMyBSMDQgUjA1IFIwNiBSMDcgUjA4IFIwOSBSMTAgUjExIFIxMiBMVFIgTFRWIEdBViBEMDEgRDAxIEQwMiBEMDMgRDA0IEQwNSBEMDYgRDA3IEQwOCBEMDkgRDEwIEQxMSBEMTIgRlJWIExQViBMUEUgRE9WIFZTTyBET1YgQkJBIFNPMiBTT1IgU09GIENTViBTVjIgU1ZSIFIxNiBCMTUgQjE1IE1HUiBCMDkgU1ZEIiwid29ya0dyb3VwSWQiOjIxNywiZXhwIjoxNTc3MTIwMzk5fQ.O8cxXCdeOOW20JQAL0XDm2UR8PXNHl_2i-uJBrXP-GVVNDjBjqMV7MqLuq0p4TMbIADJJpdYFgVI1TUQjPhdqw";
		//prod federal advo.support
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZHZvLnN1cHBvcnQiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjgzMzMsInVzZXJJZCI6NDU1NSwiY29tcGFueUlkIjo4MzMzNDMsImFkdm9jYXRlSWQiOiIzNzA0NjEiLCJsb2dpbkV2ZW50SWQiOjk4NTY1NCwic2NvcGUiOiJCVUUgQlVWIENBRCBDSEMgQ0lWIENVRSBDVVYgRE9FIERPViBEU1YgRlJFIEZSViBHQVYgTFBWIE1HUiBNSVYgTlNSIFBDViBETzIgUjAxIFIwMiBSMDcgU0lFIFNJViBTTlYgU09WIFNSRSBTUlYgU1NWIFNURSBTVFYgU1ZFIFNWMiBVTUUgVU1WIFVSRSBWTVYgQjA0IFBNViBQTVYgTFRWIEIwMSBDVVYgQ1VWIEIwMiBHQVYgUjAxIENBRCBNSVYgUjEyIEIwMyBCMDYgRlJFIERPViBNSVYgQU1FIEFNUyBBTVYgU1YyIEdBViBTVjIgU1YyIEZSViBSMDcgUjEzIEIwNyBTVlIgQjEzIEIwOSBQTVYgTFRWIEIxMiBHQVYgQjE1IEIxNiBCMDUgRE9FIFBKViBQSkUgUk1WIFNWViBCMTQgQjE3IFZTTyBBUFYgMVBUIFIyMSBBUlAgR1JQIFZNRCBWUk4gVlJQIEFSUCBHUlAgUk1WIFZNRCBWUk4gVlJQIEQxNCBJQ1YgQkJBIEQwMSBMUFYgTFBFIENQRSBDUFYgUFJDIFZQSiBEMTYgRDE3IFNQViBSMDMgRDE0Iiwid29ya0dyb3VwSWQiOjU3LCJleHAiOjE1NzcxMjAzOTl9.Tm0NN7j6u3px0EZ36trOG_4U3vHlIYQV9LKaPbQAkPd0Iwip1Mnd0YoJAcEDVwqRSEA18OdUGAVY5sNE3-62Yw";
		
		
		Map<String, String> params = new HashMap<>();
		/*params.put("action", "widgetSalesOverTime");
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		params.put("viewBy", viewBy);
		params.put("compareBy", compareBy);*/

		/*//20190901 - 20190908
		params.put("action", "widgetSalesOverTime");
		params.put("compareBy", "bm9uZQ==");
		params.put("endTime", "MDgtMDktMjAxOQ==");
		params.put("signature", "aadf13fea8e83d49cdc9649ba9de0a57");
		params.put("startTime", "MDEtMDktMjAxOQ==");
		params.put("viewBy", "ZGF5");*/
		
		//20191001 - 20191008
		/*
		 * params.put("action", "widgetSalesOverTime"); params.put("compareBy",
		 * "bm9uZQ=="); params.put("endTime", "MDgtMTAtMjAxOQ==");
		 * params.put("signature", "5416592e7924a96a7f963e2af18c868c");
		 * params.put("startTime", "MDEtMTAtMjAxOQ=="); params.put("viewBy", "ZGF5");
		 */
		
		/*
		 * //20191101 - 20191119 params.put("action", "widgetSalesOverTime");
		 * params.put("compareBy", "bm9uZQ=="); params.put("endTime",
		 * "MTktMTEtMjAxOQ=="); params.put("signature",
		 * "f95e3d6792e40ad5b9055d67d5874d8b"); params.put("startTime",
		 * "MDEtMTEtMjAxOQ=="); params.put("viewBy", "ZGF5");
		 */
		
		//20191101 - 20191120
//		params.put("action", "widgetSalesOverTime");
//		params.put("compareBy", "bm9uZQ==");
//		params.put("endTime", "MjAtMTEtMjAxOQ==");
//		params.put("signature", "b3db129a9a927c5da508296fe76369a1");
//		params.put("startTime", "MDEtMTEtMjAxOQ==");
//		params.put("viewBy", "ZGF5");
		
		//20191201 - 20191209
//		params.put("action", "widgetSalesOverTime");
//		params.put("compareBy", "bm9uZQ==");
//		params.put("endTime", "MDktMTItMjAxOQ==");
//		params.put("signature", "08221d9a4134cbb0acc166ae68783a37");
//		params.put("startTime", "MDEtMTItMjAxOQ==");
//		params.put("viewBy", "ZGF5");
		
		//20191201 - 20191210
//		params.put("action", "widgetSalesOverTime");
//		params.put("compareBy", "bm9uZQ==");
//		params.put("endTime", "MTAtMTItMjAxOQ==");
//		params.put("signature", "380f059d07f7bd858ec9e8b0ceab6e2d");
//		params.put("startTime", "MDEtMTItMjAxOQ==");
//		params.put("viewBy", "ZGF5");
		
		//20191205 - 20191205
//	    params.put("action", "widgetSalesOverTime");
//		params.put("compareBy", "bm9uZQ==");
//		params.put("endTime", "MDUtMTItMjAxOQ==");
//		params.put("signature", "e3fce80bf1a29d54e189bbf701f097ad");
//		params.put("startTime", "MDUtMTItMjAxOQ==");
//		params.put("viewBy", "ZGF5");
		
		//20191101 - 20191107
//	    params.put("action", "widgetSalesOverTime");
//		params.put("compareBy", "bm9uZQ==");
//		params.put("endTime", "MDctMTEtMjAxOQ==");
//		params.put("signature", "f2ca2b4cbfc24fe8242d121f9e472733");
//		params.put("startTime", "MDEtMTEtMjAxOQ==");
//		params.put("viewBy", "ZGF5");
		
		//20191101 - 20191111
//		params.put("action", "widgetSalesOverTime");
//		params.put("compareBy", "bm9uZQ==");
//		params.put("endTime", "MTEtMTEtMjAxOQ==");
//		params.put("signature", "8e07c66fa0476a8c4e18767ab34a6c19");
//		params.put("startTime", "MDEtMTEtMjAxOQ==");
//		params.put("viewBy", "ZGF5");
		
		//20191101 - 20191105
//		params.put("action", "widgetSalesOverTime");
//		params.put("compareBy", "bm9uZQ==");
//		params.put("endTime", "MDUtMTEtMjAxOQ==");
//		params.put("signature", "a6141a62766ef716ba23dd5666fc3fbe");
//		params.put("startTime", "MDEtMTEtMjAxOQ==");
//		params.put("viewBy", "ZGF5");
		
		//20191102 - 20191103
//		params.put("action", "widgetSalesOverTime");
//		params.put("compareBy", "bm9uZQ==");
//		params.put("endTime", "MDMtMTEtMjAxOQ==");
//		params.put("signature", "dc93db8590b48b131ab4cf2823b5ef0b");
//		params.put("startTime", "MDItMTEtMjAxOQ==");
//		params.put("viewBy", "ZGF5");
		
		//20191105 - 20191112
		params.put("action", "widgetSalesOverTime");
		params.put("compareBy", "bm9uZQ==");
		params.put("endTime", "MTItMTEtMjAxOQ==");
		params.put("signature", "d85c5055b0141dc05db65ccb8619a091");
		params.put("startTime", "MDUtMTEtMjAxOQ==");
		params.put("viewBy", "ZGF5");
		
//		"viewBy": "ZGF5",
//	    "compareBy": "bm9uZQ==",
//	    "startTime": "MDItMTEtMjAxOQ==",
//	    "endTime": "MDMtMTEtMjAxOQ==",
//	    "signature": "dc93db8590b48b131ab4cf2823b5ef0b"

		String path = getPathUrlEnvironmentWidgetData(environment);;
		
		SerenityRest.given().auth().oauth2(accessToken)
		.contentType("application/json")
		.and().params(params)
		.when().get(path);
		
//		SerenityRest.given().auth().oauth2(accessToken)
//		.contentType("application/json")
//		.and().params(params)
////		.when().get(WebServiceEndPoints.SALESOVERTIMEWIDGET.getUrl());
////		.when().get(WebServiceEndPoints.SALESOVERTIMEWIDGETSBX.getUrl());
//		.when().get(WebServiceEndPoints.SALESOVERTIMEWIDGETPROD.getUrl());
		
		/*List<SalesOvertimeBE> soBERes = SerenityRest.given().auth().oauth2(accessToken)
				.contentType("application/json")
				.and().params(params)
				.when().get(WebServiceEndPoints.SALESOVERTIMEWIDGET.getUrl()).then().extract().body().jsonPath().getList(".", SalesOvertimeBE.class);*/
	}

	@Step
	public Map<String, String> getListSalesOvertime() {
		List<HashMap<String, String>> salesOvertimes = SalesOrdersResponse.balikan();
		
		/*if(salesOvertimes != null && !salesOvertimes.isEmpty()) {
			for(SalesOvertimeBE sale : salesOvertimes) {
				actualResponse.put(sale.getValueLabel(), sale.getValue());
			}
		}*/
		
		//actualResponse = SalesOrdersResponse.balikan().stream().collect(Collectors.toMap(SalesOvertimeBE::getValueLabel, SalesOvertimeBE::getValue));
//		System.out.println("DARI SALES ORDER RESPONSE CLASS DI ACTION : " + salesOvertimes);
		Map<String, String> value = new TreeMap<String, String>();
		for (HashMap<String, String> sale: salesOvertimes) {
			value.put(sale.getOrDefault("valueLabel", "0"), String.valueOf(sale.getOrDefault("value", "0")));
		}
		
		System.out.println("RESULT: " + value);
		return value;
	}

	@Step
	public List<String> getAxist(String startTime, String endTime, String viewBy) throws ParseException {
		/*
		 * String startDate = StringUtils.removeSixLastChar(startTime); String endDate =
		 * StringUtils.removeSixLastChar(endTime); System.out.println("start date : " +
		 * startDate); String formattedStartDate =
		 * DateTimeHelper.formattedDate(startDate);
		 * System.out.println("formatted start Date : " + formattedStartDate);
		 * 
		 * System.out.println("end date : " + endDate); String formattedEndDate =
		 * DateTimeHelper.formattedDate(endDate);
		 * System.out.println("formatted end Date : " + formattedEndDate);
		 */
		
		System.out.println("GET X AXIST : " + SalesOvertimeChartBuilder.getXAxis(startTime, endTime, viewBy));
		
		/*Map<String, Double> revenueAllDate = new TreeMap<>();
		Map<String, Double> revenueByDate = salesOvertimeService.getRevenueByDate(revenueBySO)*/
//		return SalesOvertimeChartBuilder.getXAxis(formattedStartDate, formattedEndDate, viewBy);
		return SalesOvertimeChartBuilder.getXAxis(startTime, endTime, viewBy);
	}
	
	//NEWW
	//maria
	@Step
	public void showSOInRangeTimeString(String idClient, String startTime, String endTime) throws JsonParseException, JsonMappingException, IOException {
		 Map<String, String> params = new HashMap<>();
		params.put("idClient", idClient);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		
		Response response  = SerenityRest.given()
		.contentType("application/json")
		.and().params(params)
		.when().get(WebServiceEndPoints.SALESORDERBYDATESTRING.getUrl())
		.then().contentType(ContentType.JSON)
		.extract().response();
		
		System.out.println("TEST RESPONSEEEEE : " + response.asString());
		
		/*ObjectMapper mapper = new ObjectMapper();
		Map<String, Double> res = mapper.readValue(response.asString(), Map.class);*/
	}

	@Step
	public Map<String, String> getListRevenueSalesOvertime(String idClient, String username, String startTime, String endTime, String viewBy) throws ParseException, JsonParseException, JsonMappingException, IOException {
		System.out.println("ID CLIENT : " + idClient);
		System.out.println("START TIME : " + startTime);
		System.out.println("END TIME : " + endTime);
		
		Map<String, String> params = new HashMap<>();
		params.put("idClient", idClient);
		params.put("username", username);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		
		//get revenue by so
		Response response  = SerenityRest.given()
				.and().params(params)
				.contentType("application/json")
				.when().get(WebServiceEndPoints.REVENUEBYSOANDDATE.getUrl());
		
		System.out.println("MARIA TEST RESPONSEEEEE BY SO : " + response.asString());
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Double> revenueBySO = mapper.readValue(response.asString(), Map.class);
		
		System.out.println("revenueBySO MARIA : " + revenueBySO);
		
		//get xAxist
				List<String> xAxist = getAxist(startTime, endTime, viewBy);
				System.out.println("xAxist list : " + xAxist);
				//get all revenue by date from all xAxist
				Map<String, String> allRevenueByDate = new TreeMap<String, String>();
//				String date = revenueByDate.entrySet()
				
//				for(Map.Entry<String, String> entry : revenueBySO.entrySet()) {
					for(String xAxistt : xAxist) {
//						for(Map.Entry<String, String> entry : revenueBySO.entrySet()) {
						System.out.println("CEK AXIST : " + xAxistt);
						System.out.println("CEK AXIST BOOLEAN : " + revenueBySO.containsKey(xAxistt));
						if(revenueBySO.containsKey(xAxistt)) {
//							allRevenueByDate.put(entry.getKey(), entry.getValue()); revenueBySO.getOrDefault(revenueBySO.get(xAxistt), revenueBySO.values()
							String mValueString = Double.toString((Double) revenueBySO.get(xAxistt));
							System.out.println("KEY NOW : " + xAxistt);
							System.out.println("VALUE NOW : " + mValueString);
							allRevenueByDate.put(xAxistt, mValueString);
						}
						else {
							allRevenueByDate.put(xAxistt, "0.0");
						}
//					}
					
				}
				System.out.println("All revenue by date : " + allRevenueByDate);
				
		return allRevenueByDate;
		
		
//		salesOvertimeService.getRevenueBySOString(idClient, startTime, endTime);
		/*Map<String, Double> revenueByDate = salesOvertimeService.getRevenueByDate(revenueBySO);REVENUEBYDATE
		//get total revenue
		Double totalRevenue = salesOvertimeService.calculateFindByFilterString(idClient, startTime, endTime);
		//get xAxist
		List<String> xAxist = getAxist(startTime, endTime, viewBy);
		//get all revenue by date from all xAxist
		Map<String, Double> allRevenueByDate = new TreeMap<String, Double>();
//		String date = revenueByDate.entrySet()
		
		for(Map.Entry<String, Double> entry : revenueByDate.entrySet()) {
			for(String xAxistt : xAxist) {
				if(entry.getKey().equals(xAxistt)) {
					allRevenueByDate.put(entry.getKey(), entry.getValue());
				}
				else {
					allRevenueByDate.put(xAxistt, 0D);
				}
			}
			
		}
		System.out.println("All revenue by date : " + allRevenueByDate);*/
	}
	
	//NEWW
	//maria
	@Step
	public String getListSalesOvertimeStringDouble() {
		Map<String, String> salesOvertimes = SalesOrdersResponse.returned();
		
		String totalSOMARIA = null;
		totalSOMARIA = salesOvertimes.get("totalSales");
		System.out.println("TOTAL SALES MARIAAAAA : " + totalSOMARIA);
		Double totalSOMariaDouble = 0.0D;
		totalSOMariaDouble = Double.valueOf(totalSOMARIA);
		System.out.println("totalSOMariaDouble after convert : " + totalSOMariaDouble);
		
		DecimalFormat df = new DecimalFormat("#.##");
		String totalSOAfterDecimalFormat = null;
		totalSOAfterDecimalFormat = df.format(totalSOMariaDouble);
		System.out.println("totalSOMariaDouble HASIL DESIMAL FORMAT : " + totalSOAfterDecimalFormat);
		
		/*if(salesOvertimes != null && !salesOvertimes.isEmpty()) {
			for(SalesOvertimeBE sale : salesOvertimes) {
				actualResponse.put(sale.getValueLabel(), sale.getValue());
			}
		}*/
		
		//actualResponse = SalesOrdersResponse.balikan().stream().collect(Collectors.toMap(SalesOvertimeBE::getValueLabel, SalesOvertimeBE::getValue));
//		System.out.println("DARI SALES ORDER RESPONSE CLASS DI ACTION : " + salesOvertimes);
		/*Map<String, Double> value = new TreeMap<String, Double>();
		for (Map<String, String> sale: salesOvertimes) {
			value.put(sale.getOrDefault(key, defaultValue), sale.getValue());
		}
		
		System.out.println("RESULT: " + value);
		return value;*/
				return totalSOAfterDecimalFormat;
	}

	@Step
	public boolean compareBEAndServiceResult(Map<String, String> listSalesOvertimeBE,
			Map<String, String> listSalesOvertimeServiceResult) {
		System.out.println("LIST FROM BE : " + listSalesOvertimeBE);
		System.out.println("LIST FROM SERVICE RESULT : " + listSalesOvertimeServiceResult);
		System.out.println("LIST FROM BE SIZE : " + listSalesOvertimeBE.size());
		System.out.println("LIST FROM SERVICE RESULT SIZE : " + listSalesOvertimeServiceResult.size());
		System.out.println("COMPARE RESULT : " + listSalesOvertimeBE.equals(listSalesOvertimeServiceResult));
		return listSalesOvertimeBE.equals(listSalesOvertimeServiceResult);
	}
	
	private static Double getAllignBEService(Double value2Service, Double value1BE) {
		Double percentageAlign = 0.0D;
		System.out.println("value 2 service : " + value2Service);
		System.out.println("value 2 BE : " + value1BE);
		percentageAlign = calculatePercentageAllign(value2Service, value1BE);
		System.out.println("percentageAlign in getAllignBEService : " + percentageAlign);
		if(percentageAlign.equals(Double.NaN)) {
			System.out.println("Percentage Align NaN");
			percentageAlign = 100.0;
		}
		if(percentageAlign.equals(Double.POSITIVE_INFINITY)) {
			System.out.println("Percentage Align Infinity");
			percentageAlign = 0.0;
		}
		BigDecimal bd = new BigDecimal(percentageAlign).setScale(2, RoundingMode.HALF_UP);
        double newInput = bd.doubleValue();
//        System.out.println("PERCENTAGE ALIGN : " + newInput + "%");
        
        boolean align = false;
        
//        if(newInput<= 99.00 || newInput >= 101.0) {
//        	System.out.println("MASUK ALIGN FALSE");
//        	align = false;
////        	return align = false;
//        }
//        else {
//        	System.out.println("MASUK ALIGN TRUE");
//        	align = true;
////        	return align = true;
//        }
        return newInput;
	}

	private static Double calculatePercentageAllign(Double value2Service, Double value1BE) {
		return (value2Service/value1BE)*100;
	}

	@Step
	public String getRevenueTotalSalesOvertime(String idClient, String username, String startTime, String endTime,
			String viewBy) {
		System.out.println("ID CLIENT : " + idClient);
		System.out.println("START TIME : " + startTime);
		System.out.println("END TIME : " + endTime);
		
		Map<String, String> params = new HashMap<>();
		params.put("idClient", idClient);
		params.put("username", username);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		
		//get revenue by so
		Response response  = SerenityRest.given()
				.and().params(params)
				.contentType("application/json")
				.when().get(WebServiceEndPoints.REVENUETOTALBYSOANDDATE.getUrl());
		
		System.out.println("getRevenueTotalSalesOvertime HASILNYA IN STRING : " + response.asString());
		
		return response.asString();
	}

	@Step
	public boolean compareBEAndServiceResultPercentage(String salesOvertimeTotalBE, String salesOvertimeTotalService, String viewBy) {
		Double salesOvertimeTotalBEDouble = 0.0D;
		salesOvertimeTotalBEDouble = Double.parseDouble(salesOvertimeTotalBE);
		System.out.println("Revenue Total from BE " + viewBy + " : " + salesOvertimeTotalBEDouble);
		
		Double salesOvertimeTotalServiceDouble = 0.0D;
		salesOvertimeTotalServiceDouble = Double.valueOf(salesOvertimeTotalService);
		System.out.println("Revenue Total from Service " + viewBy + " : "+ salesOvertimeTotalServiceDouble);
		
		Double percentageAlign = 0.0D;
		percentageAlign = (salesOvertimeTotalServiceDouble/salesOvertimeTotalBEDouble) * 100;
		
		BigDecimal bd = new BigDecimal(percentageAlign).setScale(2, RoundingMode.HALF_UP);
        double newInput = bd.doubleValue();
        System.out.println("PERCENTAGE ALIGN : " + newInput + "%");
        
        boolean align = false;
        
        if(newInput<= 95.00 || newInput >= 100.0) {
        	System.out.println("MASUK ALIGN FALSE");
        	return align = false;
        }
        else {
        	System.out.println("MASUK ALIGN TRUE");
        	return align = true;
        }
        
//        return align;
	}

	@Step
	public void hitBEProductInsight(String viewBy, String unit, String idClient, String startTime, String endTime, String environment) {
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjI5NzYyNCwic2NvcGUiOiJVUkUgQ0hDIERTViBLUlYgTlNSIFNOViBTUkUgU1JWIFNTViBTVEUgU1RWIFVNRSBVTVYgVk1WIFNPRSBTT1YgQUNMIEFDViBBTFYgQ0VBIENMQSBDTVYgQ1NWIENVQSBDVUUgQ1VWIENWTCBET0UgRE9WIEZSQSBITVYgTDFBIEwyQSBMM0EgTEZBIExNQSBMT0EgTE9CIE1HRCBPVVQgUENFIFBDViBQRUEgUExBIFBMRSBQTE8gUE1WIFBSRSBQUlYgUFNWIFNERSBTRFYgU0lFIFNJViBTVkUgU1ZWIENJViBGUkUgRlJWIExQRSBMUFYgQ0FEIE1JViBEMDEgRDAyIEQwMyBEMDQgRDA1IEQwNiBNR0QgTUdSIFIwMSBSMDIgUjAzIFIwNyBMVFYgR0FWIEJVRSBCMDIgQU1FIEFNViBCQkEgQjA3IFNWMiBXUE8gUjA2IEFNUyBCMDQgQjAxIFNPMiBCVVYgU1ZSIFIxNiAxSUEgQVBWIElOViBJU0kgSVNMIFBDViBQQ0UgQ1BFIENQViBQUkMgV1JWIFNPRiAxUEwgU1BWIFIxMSBSTVYgVE1WIFNWRCBEMDcgSUNWIERPMiBJU0EgQjEzIEIxMiBCMTYgQjE1IEIwMyBCMjAgQjE3IEIyMiBCMDUgTFRSIExPUyBTUEEgU1BFIFJUViBTT1IgUjE1IFZTTyBETE8iLCJ3b3JrR3JvdXBJZCI6MSwiZXhwIjoxNTc1OTEwNzk5fQ.oPQONDABeXpbECWhsojA9knUdVso0YgPmqzEG_aC26wTConpANs5Uo1WEEOjBLa_CvnQ_c3V4kOSWdPHE-wAMg";
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjMwNzc3MCwic2NvcGUiOiJVUkUgQ0hDIERTViBLUlYgTlNSIFNOViBTUkUgU1JWIFNTViBTVEUgU1RWIFVNRSBVTVYgVk1WIFNPRSBTT1YgQUNMIEFDViBBTFYgQ0VBIENMQSBDTVYgQ1NWIENVQSBDVUUgQ1VWIENWTCBET0UgRE9WIEZSQSBITVYgTDFBIEwyQSBMM0EgTEZBIExNQSBMT0EgTE9CIE1HRCBPVVQgUENFIFBDViBQRUEgUExBIFBMRSBQTE8gUE1WIFBSRSBQUlYgUFNWIFNERSBTRFYgU0lFIFNJViBTVkUgU1ZWIENJViBGUkUgRlJWIExQRSBMUFYgQ0FEIE1JViBEMDEgRDAyIEQwMyBEMDQgRDA1IEQwNiBNR0QgTUdSIFIwMSBSMDIgUjAzIFIwNyBMVFYgR0FWIEJVRSBCMDIgQU1FIEFNViBCQkEgQjA3IFNWMiBXUE8gUjA2IEFNUyBCMDQgQjAxIFNPMiBCVVYgU1ZSIFIxNiBSTVYgVE1WIFNQViBSMjAgRDE0IFNWRCBSMjIgSUNWIEJDTSBBUFYiLCJ3b3JrR3JvdXBJZCI6MSwiZXhwIjoxNTc1OTEwNzk5fQ.iQS453ZY6ZIwsQ4dAbb5Wg0Axtpictz2vNT48olts_R36zXVAVw7361nOBbd_ijkDj9CfNVN2il_oK3_QwkA-Q";
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjk3ODI5OSwic2NvcGUiOiJVUkUgQ0hDIERTViBLUlYgTlNSIFNOViBTUkUgU1JWIFNTViBTVEUgU1RWIFVNRSBVTVYgVk1WIFNPRSBTT1YgQUNMIEFDViBBTFYgQ0VBIENMQSBDTVYgQ1NWIENVQSBDVUUgQ1VWIENWTCBET0UgRE9WIEZSQSBITVYgTDFBIEwyQSBMM0EgTEZBIExNQSBMT0EgTE9CIE1HRCBPVVQgUENFIFBDViBQRUEgUExBIFBMRSBQTE8gUE1WIFBSRSBQUlYgUFNWIFNERSBTRFYgU0lFIFNJViBTVkUgU1YyIENJViBGUkUgRlJWIExQRSBMUFYgQ0FEIE1JViBEMDEgRDAyIEQwMyBEMDQgRDA1IEQwNiBNR0QgTUdSIFIwMSBSMDIgUjAzIFIwNyBMVFYgU1YyIFNPMiBTT1IgU09GIFNWUiBSMTYgRE8yIEFNUyBBTUUgQU1WIEIwNCBCMTMgQjEyIEJVRSBCMTUgQjAxIEIxMSBCMDIgQjA5IEJVViBCMDcgQkJBIEQwNyBHQVYgUjA2IFNQQSBTUEUgMVBMIFNQViBJU0kgVlNPIFdQTyBSMjAgU1ZEIEFQViBQQ0UgUENWIElTQSBCMjIgQjE4IEJCQSBJQ1YgSVNMIENQRSBDUFYgUFJDIElOViBQSkUgUEpWIFIxMSBSVFYgVlBKIEIxNiBCQ00iLCJ3b3JrR3JvdXBJZCI6MSwiZXhwIjoxNTc2ODYxMTk5fQ.EGjqKNvEoy1dh0TaU8whD-Slc8DNrGS5RTgeKoL2DFWRYX1pqfz_ZTFo7yf037dRJQc9FHhCagzvu28Dd7NfbA";
//		prod RAS advo support
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZHZvLnN1cHBvcnQiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEwMDAzNiwidXNlcklkIjo0NTU1LCJjb21wYW55SWQiOjEwMDAzNjEsImFkdm9jYXRlSWQiOiIzNzA0NjEiLCJsb2dpbkV2ZW50SWQiOjk4NTc3Miwic2NvcGUiOiJCVUUgQlVWIENBRCBDSEMgQ0lWIENVRSBDVVYgRE9FIERPViBEU1YgRlJFIEZSViBHQVYgTFBWIE1HUiBNSVYgTlNSIFBDViBETzIgUjAxIFIwMiBSMDcgU0lFIFNJViBTTlYgU09WIFNSRSBTUlYgU1NWIFNURSBTVFYgU1ZFIFNWMiBVTUUgVU1WIFVSRSBWTVYgQjA0IFBNViBQTVYgTFRWIEIwMSBDVVYgQ1VWIEIwMiBHQVYgUjAxIENBRCBNSVYgUjEyIEIwMyBCMDYgRlJFIERPViBNSVYgQU1FIEFNUyBBTVYgU1YyIEdBViBTVjIgU1YyIEZSViBSMDcgUjEzIEIwNyBTVlIgQjEzIEIwOSBQTVYgTFRWIEIxMiBHQVYgQjE1IEIxNiBCMDUgRE9FIFBKViBQSkUgUk1WIFNWViBCMTQgQjE3IFZTTyBBUFYgMVBUIFIyMSBBUlAgR1JQIFZNRCBWUk4gVlJQIEFSUCBHUlAgUk1WIFZNRCBWUk4gVlJQIEQxNCBJQ1YgQkJBIEQwMSBMUFYgTFBFIENQRSBDUFYgUFJDIFZQSiBEMTYgRDE3IFNQViBSMDMgRDE0Iiwid29ya0dyb3VwSWQiOjQzOCwiZXhwIjoxNTc3MTIwMzk5fQ.efFGze-S3HzC7N12X-05v8e5BPhd6vG8ytT99-dNphS9Erx0ZXJL5odaJ5Y0rwr5oKV7c6mOiTGjxEjApYA4uw";
		//prod federal advo cs (old) -> advo support (new)
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZHZvLnN1cHBvcnQiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjgzMzMsInVzZXJJZCI6NDU1NSwiY29tcGFueUlkIjo4MzMzNDMsImFkdm9jYXRlSWQiOiIzNzA0NjEiLCJsb2dpbkV2ZW50SWQiOjk2Nzk1Miwic2NvcGUiOiJCVUUgQlVWIENBRCBDSEMgQ0lWIENTViBDVUUgQ1VWIERPRSBET1YgRFNWIEZSRSBGUlYgR0FWIExQViBNR1IgTUlWIE5TUiBQQ1YgRE8yIFIwMSBSMDIgUjA3IFNJRSBTSVYgU05WIFNPViBTUkUgU1JWIFNTViBTVEUgU1RWIFNWRSBTVjIgVU1FIFVNViBVUkUgVk1WIEIwNCBQTVYgUE1WIExUViBCMDEgQ1VWIENVViBCMDIgR0FWIFIwMSBDQUQgTUlWIFIxMiBCMDMgQjA2IEZSRSBET1YgTUlWIEFNRSBBTVMgQU1WIFNWMiBHQVYgU1YyIFNWMiBGUlYgUjA3IFIxMyBCMDcgU1ZSIEIxMyBCMDkgUE1WIExUViBCMTIgR0FWIEIxNSBXUE8gQjE2IEIwNSBET0UgUEpWIFBKRSBSTVYgU1ZWIEIxNCBCMTcgVlNPIEFQViAxUFQgUjIxIEFSUCBHUlAgVk1EIFZSTiBWUlAgQVJQIEdSUCBSTVYgVk1EIFZSTiBWUlAgRDE0IElDViBCQkEgRDAxIExQViBMUEUgQ1BFIENQViBQUkMgVlBKIEQxNiBEMTcgU1BWIFIwMyBEMTQiLCJ3b3JrR3JvdXBJZCI6NTcsImV4cCI6MTU3NjYwMTk5OX0.roBXsDhh8nnf63pmmnVoIgtgKOQkls5bSOJbSA0dj6LfCTeroH3j9aDORmbrve28MPEm5xXHdZxwnZAtYTLc1Q";
		//prod nutrifood nf.superuser
		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJuZi5zdXBlcnVzZXIiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjg2ODMsInVzZXJJZCI6NTY4NiwiY29tcGFueUlkIjo4NjgzMDEsImFkdm9jYXRlSWQiOiI1NDE1MzciLCJsb2dpbkV2ZW50SWQiOjk4NjMxMywic2NvcGUiOiJBTFYgQ0FEIENIQyBDSVYgQ1VWIERPMiBET0UgS1JWIE1HRCBNSVYgUE1WIFJXRSBSV1YgU0RWIFNOViBTTzIgU09FIFNSRSBTUlYgU1NWIFNURSBTVFYgU1ZFIFVNRSBVTVYgVVJFIFZNViBCVUUgQlVWIEIwMiBCMDQgTFRSIExUViBTT0UgU09WIFIwMSBSMDIgUjAzIFIwNCBSMDUgUjA2IFIwNyBSMDggUjA5IFIxMCBSMTEgUjEyIEdBViBCMDEgQjAxIEIwMiBCMDMgQjA0IEIwNiBCMDcgQlVFIEJVViBSMDEgUjAyIFIwMyBSMDQgUjA1IFIwNiBSMDcgUjA4IFIwOSBSMTAgUjExIFIxMiBMVFIgTFRWIEdBViBEMDEgRDAxIEQwMiBEMDMgRDA0IEQwNSBEMDYgRDA3IEQwOCBEMDkgRDEwIEQxMSBEMTIgRlJWIExQViBMUEUgRE9WIFZTTyBET1YgQkJBIFNPMiBTT1IgU09GIENTViBTVjIgU1ZSIFIxNiBCMTUgQjE1IE1HUiBCMDkgU1ZEIiwid29ya0dyb3VwSWQiOjIxNywiZXhwIjoxNTc3MTIwMzk5fQ.O8cxXCdeOOW20JQAL0XDm2UR8PXNHl_2i-uJBrXP-GVVNDjBjqMV7MqLuq0p4TMbIADJJpdYFgVI1TUQjPhdqw";
		//prod federal advo.support
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZHZvLnN1cHBvcnQiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjgzMzMsInVzZXJJZCI6NDU1NSwiY29tcGFueUlkIjo4MzMzNDMsImFkdm9jYXRlSWQiOiIzNzA0NjEiLCJsb2dpbkV2ZW50SWQiOjk4NTY1NCwic2NvcGUiOiJCVUUgQlVWIENBRCBDSEMgQ0lWIENVRSBDVVYgRE9FIERPViBEU1YgRlJFIEZSViBHQVYgTFBWIE1HUiBNSVYgTlNSIFBDViBETzIgUjAxIFIwMiBSMDcgU0lFIFNJViBTTlYgU09WIFNSRSBTUlYgU1NWIFNURSBTVFYgU1ZFIFNWMiBVTUUgVU1WIFVSRSBWTVYgQjA0IFBNViBQTVYgTFRWIEIwMSBDVVYgQ1VWIEIwMiBHQVYgUjAxIENBRCBNSVYgUjEyIEIwMyBCMDYgRlJFIERPViBNSVYgQU1FIEFNUyBBTVYgU1YyIEdBViBTVjIgU1YyIEZSViBSMDcgUjEzIEIwNyBTVlIgQjEzIEIwOSBQTVYgTFRWIEIxMiBHQVYgQjE1IEIxNiBCMDUgRE9FIFBKViBQSkUgUk1WIFNWViBCMTQgQjE3IFZTTyBBUFYgMVBUIFIyMSBBUlAgR1JQIFZNRCBWUk4gVlJQIEFSUCBHUlAgUk1WIFZNRCBWUk4gVlJQIEQxNCBJQ1YgQkJBIEQwMSBMUFYgTFBFIENQRSBDUFYgUFJDIFZQSiBEMTYgRDE3IFNQViBSMDMgRDE0Iiwid29ya0dyb3VwSWQiOjU3LCJleHAiOjE1NzcxMjAzOTl9.Tm0NN7j6u3px0EZ36trOG_4U3vHlIYQV9LKaPbQAkPd0Iwip1Mnd0YoJAcEDVwqRSEA18OdUGAVY5sNE3-62Yw";
		
		
		Map<String, String> params = new HashMap<>();
		
		if(viewBy.equals(productGroup)) {
			//20191101 - 20191107 - Product Group - SalesRevenue
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "c2FsZXNSZXZlbnVl");
//			params.put("endTime", "MDctMTEtMjAxOQ==");
//			params.put("signature", "002fe9f8eab1f264f7d59794b9b9df6c");
//			params.put("startTime", "MDEtMTEtMjAxOQ==");
//			params.put("viewBy", "cHJvZHVjdEdyb3Vw");
			
			//20191105 - 20191112 - Product Group - SalesRevenue
			params.put("action", "widgetProductInsight");
			params.put("unit", "c2FsZXNSZXZlbnVl");
			params.put("endTime", "MTItMTEtMjAxOQ==");
			params.put("signature", "ac02f6923ecccee020f77b61a4d127bb");
			params.put("startTime", "MDUtMTEtMjAxOQ==");
			params.put("viewBy", "cHJvZHVjdEdyb3Vw");
			
			//20191101 - 20191105 - Product Group - SalesRevenue
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "c2FsZXNSZXZlbnVl");
//			params.put("endTime", "MDUtMTEtMjAxOQ==");
//			params.put("signature", "79fad44426c1fc14fd63fbbcac47e37d");
//			params.put("startTime", "MDEtMTEtMjAxOQ==");
//			params.put("viewBy", "cHJvZHVjdEdyb3Vw");
			
			//20191102 - 20191103 - Product Group - SalesRevenue
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "c2FsZXNSZXZlbnVl");
//			params.put("endTime", "MDMtMTEtMjAxOQ==");
//			params.put("signature", "20713b97e774c88eb2607cd42118377b");
//			params.put("startTime", "MDItMTEtMjAxOQ==");
//			params.put("viewBy", "cHJvZHVjdEdyb3Vw");
			
//			"viewBy": "cHJvZHVjdEdyb3Vw",
//		    "unit": "c2FsZXNSZXZlbnVl",
//		    "startTime": "MDItMTEtMjAxOQ==",
//		    "endTime": "MDMtMTEtMjAxOQ==",
//		    "signature": "20713b97e774c88eb2607cd42118377b"

		} 
		else if(viewBy.equals(SKU)) {
			//20191101 - 20191107 - SKU - SalesRevenue
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "c2FsZXNSZXZlbnVl");
//			params.put("endTime", "MDctMTEtMjAxOQ==");
//			params.put("signature", "3c76d297f62c4e2cfcecf9acfd6a50a8");
//			params.put("startTime", "MDEtMTEtMjAxOQ==");
//			params.put("viewBy", "U0tV");
			
			//20191101 - 20191111 - SKU - SalesRevenue
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "c2FsZXNSZXZlbnVl");
//			params.put("endTime", "MTEtMTEtMjAxOQ==");
//			params.put("signature", "675d8f8049c75b025898f3bf06e7f6d2");
//			params.put("startTime", "MDEtMTEtMjAxOQ==");
//			params.put("viewBy", "U0tV");
			
			//20191101 - 20191105 - SKU - SalesRevenue
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "c2FsZXNSZXZlbnVl");
//			params.put("endTime", "MDUtMTEtMjAxOQ==");
//			params.put("signature", "55fd5f20988e587af05af25d39c251dd");
//			params.put("startTime", "MDEtMTEtMjAxOQ==");
//			params.put("viewBy", "U0tV");
			
			//20191105 - 20191112 - SKU - SalesRevenue
			params.put("action", "widgetProductInsight");
			params.put("unit", "c2FsZXNSZXZlbnVl");
			params.put("endTime", "MTItMTEtMjAxOQ==");
			params.put("signature", "3c1096041c2f19497ec630071a88fbd6");
			params.put("startTime", "MDUtMTEtMjAxOQ==");
			params.put("viewBy", "U0tV");
			
			//20191102 - 20191103 - SKU - SalesRevenue
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "c2FsZXNSZXZlbnVl");
//			params.put("endTime", "MDMtMTEtMjAxOQ==");
//			params.put("signature", "ae57b1e2cbd73b247c2c2586ad252274");
//			params.put("startTime", "MDItMTEtMjAxOQ==");
//			params.put("viewBy", "U0tV");
			
//			"viewBy": "U0tV",
//		    "unit": "c2FsZXNSZXZlbnVl",
//		    "startTime": "MDItMTEtMjAxOQ==",
//		    "endTime": "MDMtMTEtMjAxOQ==",
//		    "signature": "ae57b1e2cbd73b247c2c2586ad252274"

		} 
		else if(viewBy.equals(brand)) {
			//20191101 - 20191107 - Brand - SalesRevenue
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "c2FsZXNSZXZlbnVl");
//			params.put("endTime", "MDctMTEtMjAxOQ==");
//			params.put("signature", "570741e9abacfa62a64aa061941ced3b");
//			params.put("startTime", "MDEtMTEtMjAxOQ==");
//			params.put("viewBy", "YnJhbmQ=");
			
			//20191105 - 20191112 - Brand - SalesRevenue
			params.put("action", "widgetProductInsight");
			params.put("unit", "c2FsZXNSZXZlbnVl");
			params.put("endTime", "MTItMTEtMjAxOQ==");
			params.put("signature", "6c70e899a64452a9e79b7626d9e7bd92");
			params.put("startTime", "MDUtMTEtMjAxOQ==");
			params.put("viewBy", "YnJhbmQ=");
			
			//20191101 - 20191105 - Brand - SalesRevenue
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "c2FsZXNSZXZlbnVl");
//			params.put("endTime", "MDUtMTEtMjAxOQ==");
//			params.put("signature", "c2da89edf9ce7bb1a8c0c0ba466f58df");
//			params.put("startTime", "MDEtMTEtMjAxOQ==");
//			params.put("viewBy", "YnJhbmQ=");
			
			//20191102 - 20191103 - Brand - SalesRevenue
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "c2FsZXNSZXZlbnVl");
//			params.put("endTime", "MDMtMTEtMjAxOQ==");
//			params.put("signature", "cc510a8c81afcb96b427afbde0ab8516");
//			params.put("startTime", "MDItMTEtMjAxOQ==");
//			params.put("viewBy", "YnJhbmQ=");
			
//			"viewBy": "YnJhbmQ=",
//		    "unit": "c2FsZXNSZXZlbnVl",
//		    "startTime": "MDItMTEtMjAxOQ==",
//		    "endTime": "MDMtMTEtMjAxOQ==",
//		    "signature": "cc510a8c81afcb96b427afbde0ab8516"

		}
		else {
			//20191101 - 20191107 - Product Group - SalesRevenue
			params.put("action", "widgetProductInsight");
			params.put("unit", "c2FsZXNSZXZlbnVl");
			params.put("endTime", "MDctMTEtMjAxOQ==");
			params.put("signature", "002fe9f8eab1f264f7d59794b9b9df6c");
			params.put("startTime", "MDEtMTEtMjAxOQ==");
			params.put("viewBy", "cHJvZHVjdEdyb3Vw");
		}
	    	
		String path = getPathUrlEnvironmentWidgetData(environment);;
		
		SerenityRest.given().auth().oauth2(accessToken)
		.contentType("application/json")
		.and().params(params)
		.when().get(path);
		
		
	}

	private String getPathUrlEnvironmentWidgetData(String environment) {
		if(environment.equals("env-production")) {
			return WebServiceEndPoints.WIDGETDATAPROD.getUrl();
		}
		else if(environment.equals("env-sandbox")) {
			return WebServiceEndPoints.WIDGETDATASBX.getUrl();
		}
		else if(environment.equals("env-integration")) {
			return WebServiceEndPoints.WIDGETDATAINTEG.getUrl();
		}
		else {
			return WebServiceEndPoints.WIDGETDATAPROD.getUrl();
		}
	}

	@Step
	public Map<String, String> getListProductInsight() {
		List<HashMap<String, String>> productInsights = SalesOrdersResponse.responseBEProductInsight();
		
		Map<String, String> value = new TreeMap<String, String>();
		for (HashMap<String, String> productInsight: productInsights) {
			value.put(productInsight.getOrDefault("name", "0"), String.valueOf(productInsight.getOrDefault("value", "0")));
		}
		
		System.out.println("RESULT: " + value);
		return value;
	}

	@Step
	public String getListProductInsightStringDouble() {
		Map<String, String> salesRevenue = SalesOrdersResponse.returned();
		
		String totalRevenue = null;
		totalRevenue = salesRevenue.get("measurementValue");
		System.out.println("TOTAL REVENUE PRODUCT INSIGHT : " + totalRevenue);
		Double totalRevenueDouble = 0.0D;
		totalRevenueDouble = Double.valueOf(totalRevenue);
		System.out.println("totalRevenueDouble after convert : " + totalRevenueDouble);
		
		DecimalFormat df = new DecimalFormat("#.##");
		String totalRevenueAfterDecimalFormat = null;
		totalRevenueAfterDecimalFormat = df.format(totalRevenueDouble);
		System.out.println("totalRevenueAfterDecimalFormat HASIL DESIMAL FORMAT : " + totalRevenueAfterDecimalFormat);
		
		return totalRevenueAfterDecimalFormat;
	}

	@Step
	public Map<String, String> getListRevenueProductInsight(String idClient, String username, String startTime,
			String endTime, String viewBy) throws JsonParseException, JsonMappingException, IOException, ParseException {
		System.out.println("ID CLIENT : " + idClient);
		System.out.println("START TIME : " + startTime);
		System.out.println("END TIME : " + endTime);
		
		Map<String, String> params = new HashMap<>();
		params.put("idClient", idClient);
		params.put("username", username);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		params.put("viewBy", viewBy);
		
		//get revenue by so
		Response response  = SerenityRest.given()
				.and().params(params)
				.contentType("application/json")
				.when().get(WebServiceEndPoints.REVENUEBYPRODUCTINSIGHT.getUrl());
		
		System.out.println("MARIA TEST RESPONSEEEEE BY SO : " + response.asString());
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Double> revenueByProduct = mapper.readValue(response.asString(), Map.class);
		
		System.out.println("revenueByProduct MARIA : " + revenueByProduct);
		
		Map<String, String> allRevenueByProduct = new TreeMap<String, String>();

		for (Map.Entry<String, Double> entry : revenueByProduct.entrySet()) {
			System.out.println("Entry dot key : " + entry.getKey());
			System.out.println("Entry dot value : " + entry.getValue());
			String products = entry.getKey();
			System.out.println("products : " + products);

			Double revenue = entry.getValue();
			if (!allRevenueByProduct.containsKey(products)) {
				String mValueString = Double.toString((Double) revenueByProduct.get(products));
				System.out.println("KEY NOW : " + products);
				System.out.println("VALUE NOW : " + mValueString);
				allRevenueByProduct.put(products, mValueString);
			} else {
				allRevenueByProduct.put(products, "0.0");
			}
			
				System.out.println("All revenue by product : " + allRevenueByProduct);
		}		
		return allRevenueByProduct;
		
	}

	@Step
	public String getRevenueTotalProductInsight(String idClient, String username, String startTime, String endTime,
			String viewBy) {
		
		Map<String, String> params = new HashMap<>();
		params.put("idClient", idClient);
		params.put("username", username);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		params.put("viewBy", viewBy);
		
		//get revenue by so
		Response response  = SerenityRest.given()
				.and().params(params)
				.contentType("application/json")
				.when().get(WebServiceEndPoints.REVENUETOTALBYPRODUCTINSIGHT.getUrl());
		
		System.out.println("getRevenueTotalProductInsight HASILNYA IN STRING : " + response.asString());
		
		return response.asString();
	}

	public Map<String, String> getAllignPercentageCompareMapBEAndServiceResult(Map<String, String> mapBE,
			Map<String, String> mapServiceResult, String viewBy) {
		
		Comparator<String> compString = Comparator.comparing(String::toString);

		Map<String, String> allignPerDetail = new TreeMap<>(compString);
		
		for (String key : mapServiceResult.keySet()) {
			String value2 = mapServiceResult.get(key);
			String value1;
			if(mapBE.containsKey(key)) {
				value1 = mapBE.get(key);
			}
			else {
				value1 = "0.0";
			}
			
			Double value1BE = 0.0D;
			value1BE = Double.parseDouble(value1);
			
			Double value2Service = 0.0D;
			value2Service = Double.parseDouble(value2);
			
//			boolean align = false;
			Double align = 0D;
			align = getAllignBEService(value2Service, value1BE);
			
			if(allignPerDetail.containsKey(key)) {
				Double currAllign = align;
				allignPerDetail.put(key, currAllign + "%");
			}else {
				allignPerDetail.put(key, align + "%");
			}
		}
		
		//if other keys exist in BE but not exist in Service Result
		for (String key : mapBE.keySet()) {
			if(!allignPerDetail.containsKey(key)) {
				allignPerDetail.put(key, "0.0%");
			}
		}
		System.out.println("PERCENTAGE ALLIGN ALL DETAIL : " + viewBy + " : " + allignPerDetail.toString());
		return allignPerDetail;
	}
	
	@Step
	public void hitBEProductInsightQtyFulfilled(String viewBy, String unit, String idClient, String startTime, String endTime, String environment) {
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjI5NzYyNCwic2NvcGUiOiJVUkUgQ0hDIERTViBLUlYgTlNSIFNOViBTUkUgU1JWIFNTViBTVEUgU1RWIFVNRSBVTVYgVk1WIFNPRSBTT1YgQUNMIEFDViBBTFYgQ0VBIENMQSBDTVYgQ1NWIENVQSBDVUUgQ1VWIENWTCBET0UgRE9WIEZSQSBITVYgTDFBIEwyQSBMM0EgTEZBIExNQSBMT0EgTE9CIE1HRCBPVVQgUENFIFBDViBQRUEgUExBIFBMRSBQTE8gUE1WIFBSRSBQUlYgUFNWIFNERSBTRFYgU0lFIFNJViBTVkUgU1ZWIENJViBGUkUgRlJWIExQRSBMUFYgQ0FEIE1JViBEMDEgRDAyIEQwMyBEMDQgRDA1IEQwNiBNR0QgTUdSIFIwMSBSMDIgUjAzIFIwNyBMVFYgR0FWIEJVRSBCMDIgQU1FIEFNViBCQkEgQjA3IFNWMiBXUE8gUjA2IEFNUyBCMDQgQjAxIFNPMiBCVVYgU1ZSIFIxNiAxSUEgQVBWIElOViBJU0kgSVNMIFBDViBQQ0UgQ1BFIENQViBQUkMgV1JWIFNPRiAxUEwgU1BWIFIxMSBSTVYgVE1WIFNWRCBEMDcgSUNWIERPMiBJU0EgQjEzIEIxMiBCMTYgQjE1IEIwMyBCMjAgQjE3IEIyMiBCMDUgTFRSIExPUyBTUEEgU1BFIFJUViBTT1IgUjE1IFZTTyBETE8iLCJ3b3JrR3JvdXBJZCI6MSwiZXhwIjoxNTc1OTEwNzk5fQ.oPQONDABeXpbECWhsojA9knUdVso0YgPmqzEG_aC26wTConpANs5Uo1WEEOjBLa_CvnQ_c3V4kOSWdPHE-wAMg";
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjMwNzc3MCwic2NvcGUiOiJVUkUgQ0hDIERTViBLUlYgTlNSIFNOViBTUkUgU1JWIFNTViBTVEUgU1RWIFVNRSBVTVYgVk1WIFNPRSBTT1YgQUNMIEFDViBBTFYgQ0VBIENMQSBDTVYgQ1NWIENVQSBDVUUgQ1VWIENWTCBET0UgRE9WIEZSQSBITVYgTDFBIEwyQSBMM0EgTEZBIExNQSBMT0EgTE9CIE1HRCBPVVQgUENFIFBDViBQRUEgUExBIFBMRSBQTE8gUE1WIFBSRSBQUlYgUFNWIFNERSBTRFYgU0lFIFNJViBTVkUgU1ZWIENJViBGUkUgRlJWIExQRSBMUFYgQ0FEIE1JViBEMDEgRDAyIEQwMyBEMDQgRDA1IEQwNiBNR0QgTUdSIFIwMSBSMDIgUjAzIFIwNyBMVFYgR0FWIEJVRSBCMDIgQU1FIEFNViBCQkEgQjA3IFNWMiBXUE8gUjA2IEFNUyBCMDQgQjAxIFNPMiBCVVYgU1ZSIFIxNiBSTVYgVE1WIFNQViBSMjAgRDE0IFNWRCBSMjIgSUNWIEJDTSBBUFYiLCJ3b3JrR3JvdXBJZCI6MSwiZXhwIjoxNTc1OTEwNzk5fQ.iQS453ZY6ZIwsQ4dAbb5Wg0Axtpictz2vNT48olts_R36zXVAVw7361nOBbd_ijkDj9CfNVN2il_oK3_QwkA-Q";
		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjEwMTc5MTcsInNjb3BlIjoiVVJFIENIQyBEU1YgS1JWIE5TUiBTTlYgU1JFIFNSViBTU1YgU1RFIFNUViBVTUUgVU1WIFZNViBTT0UgU09WIEFDTCBBQ1YgQUxWIENFQSBDTEEgQ01WIENTViBDVUEgQ1VFIENVViBDVkwgRE9FIERPViBGUkEgSE1WIEwxQSBMMkEgTDNBIExGQSBMTUEgTE9BIExPQiBNR0QgT1VUIFBDRSBQQ1YgUEVBIFBMQSBQTEUgUExPIFBNViBQUkUgUFJWIFBTViBTREUgU0RWIFNJRSBTSVYgU1ZFIFNWMiBDSVYgRlJFIEZSViBMUEUgTFBWIENBRCBNSVYgRDAxIEQwMiBEMDMgRDA0IEQwNSBEMDYgTUdEIE1HUiBSMDEgUjAyIFIwMyBSMDcgTFRWIFNWMiBTTzIgU09SIFNPRiBTVlIgUjE2IERPMiBBTVMgQU1FIEFNViBCMDQgQjEzIEIxMiBCVUUgQjE1IEIwMSBCMTEgQjAyIEIwOSBCVVYgQjA3IEJCQSBEMDcgR0FWIFIwNiBTUEEgU1BFIDFQTCBTUFYgSVNJIFZTTyBXUE8gUjIwIFNWRCBBUFYgUENFIFBDViBJU0EgQjIyIEIxOCBCQkEgSUNWIElTTCBDUEUgQ1BWIFBSQyBJTlYgUEpFIFBKViBSMTEgUlRWIFZQSiBCMTYgQkNNIiwid29ya0dyb3VwSWQiOjEsImV4cCI6MTU3ODQxNjM5OX0.PwgnUYNa01Sck0Hi0Wws0tsZXDL6pXQgfh_VoRoD0SfrZHJgYbFOaeTW3D6hIw8zCERiMnFyFnAa4mZXih-Eug";
//		prod RAS advo support
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZHZvLnN1cHBvcnQiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEwMDAzNiwidXNlcklkIjo0NTU1LCJjb21wYW55SWQiOjEwMDAzNjEsImFkdm9jYXRlSWQiOiIzNzA0NjEiLCJsb2dpbkV2ZW50SWQiOjk4NTc3Miwic2NvcGUiOiJCVUUgQlVWIENBRCBDSEMgQ0lWIENVRSBDVVYgRE9FIERPViBEU1YgRlJFIEZSViBHQVYgTFBWIE1HUiBNSVYgTlNSIFBDViBETzIgUjAxIFIwMiBSMDcgU0lFIFNJViBTTlYgU09WIFNSRSBTUlYgU1NWIFNURSBTVFYgU1ZFIFNWMiBVTUUgVU1WIFVSRSBWTVYgQjA0IFBNViBQTVYgTFRWIEIwMSBDVVYgQ1VWIEIwMiBHQVYgUjAxIENBRCBNSVYgUjEyIEIwMyBCMDYgRlJFIERPViBNSVYgQU1FIEFNUyBBTVYgU1YyIEdBViBTVjIgU1YyIEZSViBSMDcgUjEzIEIwNyBTVlIgQjEzIEIwOSBQTVYgTFRWIEIxMiBHQVYgQjE1IEIxNiBCMDUgRE9FIFBKViBQSkUgUk1WIFNWViBCMTQgQjE3IFZTTyBBUFYgMVBUIFIyMSBBUlAgR1JQIFZNRCBWUk4gVlJQIEFSUCBHUlAgUk1WIFZNRCBWUk4gVlJQIEQxNCBJQ1YgQkJBIEQwMSBMUFYgTFBFIENQRSBDUFYgUFJDIFZQSiBEMTYgRDE3IFNQViBSMDMgRDE0Iiwid29ya0dyb3VwSWQiOjQzOCwiZXhwIjoxNTc3MTIwMzk5fQ.efFGze-S3HzC7N12X-05v8e5BPhd6vG8ytT99-dNphS9Erx0ZXJL5odaJ5Y0rwr5oKV7c6mOiTGjxEjApYA4uw";
		//prod federal advo cs (old) -> advo support (new)
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZHZvLnN1cHBvcnQiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjgzMzMsInVzZXJJZCI6NDU1NSwiY29tcGFueUlkIjo4MzMzNDMsImFkdm9jYXRlSWQiOiIzNzA0NjEiLCJsb2dpbkV2ZW50SWQiOjk2Nzk1Miwic2NvcGUiOiJCVUUgQlVWIENBRCBDSEMgQ0lWIENTViBDVUUgQ1VWIERPRSBET1YgRFNWIEZSRSBGUlYgR0FWIExQViBNR1IgTUlWIE5TUiBQQ1YgRE8yIFIwMSBSMDIgUjA3IFNJRSBTSVYgU05WIFNPViBTUkUgU1JWIFNTViBTVEUgU1RWIFNWRSBTVjIgVU1FIFVNViBVUkUgVk1WIEIwNCBQTVYgUE1WIExUViBCMDEgQ1VWIENVViBCMDIgR0FWIFIwMSBDQUQgTUlWIFIxMiBCMDMgQjA2IEZSRSBET1YgTUlWIEFNRSBBTVMgQU1WIFNWMiBHQVYgU1YyIFNWMiBGUlYgUjA3IFIxMyBCMDcgU1ZSIEIxMyBCMDkgUE1WIExUViBCMTIgR0FWIEIxNSBXUE8gQjE2IEIwNSBET0UgUEpWIFBKRSBSTVYgU1ZWIEIxNCBCMTcgVlNPIEFQViAxUFQgUjIxIEFSUCBHUlAgVk1EIFZSTiBWUlAgQVJQIEdSUCBSTVYgVk1EIFZSTiBWUlAgRDE0IElDViBCQkEgRDAxIExQViBMUEUgQ1BFIENQViBQUkMgVlBKIEQxNiBEMTcgU1BWIFIwMyBEMTQiLCJ3b3JrR3JvdXBJZCI6NTcsImV4cCI6MTU3NjYwMTk5OX0.roBXsDhh8nnf63pmmnVoIgtgKOQkls5bSOJbSA0dj6LfCTeroH3j9aDORmbrve28MPEm5xXHdZxwnZAtYTLc1Q";
		//prod nutrifood nf.superuser
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJuZi5zdXBlcnVzZXIiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjg2ODMsInVzZXJJZCI6NTY4NiwiY29tcGFueUlkIjo4NjgzMDEsImFkdm9jYXRlSWQiOiI1NDE1MzciLCJsb2dpbkV2ZW50SWQiOjk4NjMxMywic2NvcGUiOiJBTFYgQ0FEIENIQyBDSVYgQ1VWIERPMiBET0UgS1JWIE1HRCBNSVYgUE1WIFJXRSBSV1YgU0RWIFNOViBTTzIgU09FIFNSRSBTUlYgU1NWIFNURSBTVFYgU1ZFIFVNRSBVTVYgVVJFIFZNViBCVUUgQlVWIEIwMiBCMDQgTFRSIExUViBTT0UgU09WIFIwMSBSMDIgUjAzIFIwNCBSMDUgUjA2IFIwNyBSMDggUjA5IFIxMCBSMTEgUjEyIEdBViBCMDEgQjAxIEIwMiBCMDMgQjA0IEIwNiBCMDcgQlVFIEJVViBSMDEgUjAyIFIwMyBSMDQgUjA1IFIwNiBSMDcgUjA4IFIwOSBSMTAgUjExIFIxMiBMVFIgTFRWIEdBViBEMDEgRDAxIEQwMiBEMDMgRDA0IEQwNSBEMDYgRDA3IEQwOCBEMDkgRDEwIEQxMSBEMTIgRlJWIExQViBMUEUgRE9WIFZTTyBET1YgQkJBIFNPMiBTT1IgU09GIENTViBTVjIgU1ZSIFIxNiBCMTUgQjE1IE1HUiBCMDkgU1ZEIiwid29ya0dyb3VwSWQiOjIxNywiZXhwIjoxNTc3MTIwMzk5fQ.O8cxXCdeOOW20JQAL0XDm2UR8PXNHl_2i-uJBrXP-GVVNDjBjqMV7MqLuq0p4TMbIADJJpdYFgVI1TUQjPhdqw";
		//prod federal advo.support
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZHZvLnN1cHBvcnQiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjgzMzMsInVzZXJJZCI6NDU1NSwiY29tcGFueUlkIjo4MzMzNDMsImFkdm9jYXRlSWQiOiIzNzA0NjEiLCJsb2dpbkV2ZW50SWQiOjk4NTY1NCwic2NvcGUiOiJCVUUgQlVWIENBRCBDSEMgQ0lWIENVRSBDVVYgRE9FIERPViBEU1YgRlJFIEZSViBHQVYgTFBWIE1HUiBNSVYgTlNSIFBDViBETzIgUjAxIFIwMiBSMDcgU0lFIFNJViBTTlYgU09WIFNSRSBTUlYgU1NWIFNURSBTVFYgU1ZFIFNWMiBVTUUgVU1WIFVSRSBWTVYgQjA0IFBNViBQTVYgTFRWIEIwMSBDVVYgQ1VWIEIwMiBHQVYgUjAxIENBRCBNSVYgUjEyIEIwMyBCMDYgRlJFIERPViBNSVYgQU1FIEFNUyBBTVYgU1YyIEdBViBTVjIgU1YyIEZSViBSMDcgUjEzIEIwNyBTVlIgQjEzIEIwOSBQTVYgTFRWIEIxMiBHQVYgQjE1IEIxNiBCMDUgRE9FIFBKViBQSkUgUk1WIFNWViBCMTQgQjE3IFZTTyBBUFYgMVBUIFIyMSBBUlAgR1JQIFZNRCBWUk4gVlJQIEFSUCBHUlAgUk1WIFZNRCBWUk4gVlJQIEQxNCBJQ1YgQkJBIEQwMSBMUFYgTFBFIENQRSBDUFYgUFJDIFZQSiBEMTYgRDE3IFNQViBSMDMgRDE0Iiwid29ya0dyb3VwSWQiOjU3LCJleHAiOjE1NzcxMjAzOTl9.Tm0NN7j6u3px0EZ36trOG_4U3vHlIYQV9LKaPbQAkPd0Iwip1Mnd0YoJAcEDVwqRSEA18OdUGAVY5sNE3-62Yw";
		
		
		Map<String, String> params = new HashMap<>();
		
		if(viewBy.equals(productGroup)) {
			//20191101 - 20191107 - Product Group - Qty Fulfilled
			params.put("action", "widgetProductInsight");
			params.put("unit", "ZnVsZmlsbGVkUXVhbnRpdHk=");
			params.put("endTime", "MDctMTEtMjAxOQ==");
			params.put("signature", "b412eaa1ae331c82cde539029801db3e");
			params.put("startTime", "MDEtMTEtMjAxOQ==");
			params.put("viewBy", "cHJvZHVjdEdyb3Vw");
			
			//20191105 - 20191112 - Product Group - Qty Fulfilled
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "ZnVsZmlsbGVkUXVhbnRpdHk=");
//			params.put("endTime", "MTItMTEtMjAxOQ==");
//			params.put("signature", "f3e1a8b0dbafd75761e5676b396b1a8a");
//			params.put("startTime", "MDUtMTEtMjAxOQ==");
//			params.put("viewBy", "cHJvZHVjdEdyb3Vw");
			
			//20191101 - 20191105 - Product Group - Qty Fulfilled
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "ZnVsZmlsbGVkUXVhbnRpdHk=");
//			params.put("endTime", "MDUtMTEtMjAxOQ==");
//			params.put("signature", "bc15c5ec8c3e31e9c8ef38644dba43eb");
//			params.put("startTime", "MDEtMTEtMjAxOQ==");
//			params.put("viewBy", "cHJvZHVjdEdyb3Vw");
			
			//20191102 - 20191103 - Product Group - Qty Fulfilled
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "ZnVsZmlsbGVkUXVhbnRpdHk=");
//			params.put("endTime", "MDMtMTEtMjAxOQ==");
//			params.put("signature", "91d86524123e4730a2a74764601c912a");
//			params.put("startTime", "MDItMTEtMjAxOQ==");
//			params.put("viewBy", "cHJvZHVjdEdyb3Vw");
			
//			"viewBy": "cHJvZHVjdEdyb3Vw",
//		    "unit": "ZnVsZmlsbGVkUXVhbnRpdHk=",
//		    "startTime": "MDItMTEtMjAxOQ==",
//		    "endTime": "MDMtMTEtMjAxOQ==",
//		    "signature": "91d86524123e4730a2a74764601c912a"

		} 
		else if(viewBy.equals(SKU)) {
			//20191101 - 20191107 - SKU - Qty Fulfilled
			params.put("action", "widgetProductInsight");
			params.put("unit", "ZnVsZmlsbGVkUXVhbnRpdHk=");
			params.put("endTime", "MDctMTEtMjAxOQ==");
			params.put("signature", "1c66a3f262e61e601e2d9b0c9601152a");
			params.put("startTime", "MDEtMTEtMjAxOQ==");
			params.put("viewBy", "U0tV");
			
			//20191105 - 20191112 - SKU - Qty Fulfilled
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "ZnVsZmlsbGVkUXVhbnRpdHk=");
//			params.put("endTime", "MTItMTEtMjAxOQ==");
//			params.put("signature", "9e8215ab5b6d71b5a80275d40dd24bec");
//			params.put("startTime", "MDUtMTEtMjAxOQ==");
//			params.put("viewBy", "U0tV");
			
			//20191101 - 20191105 - SKU - Qty Fulfilled
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "ZnVsZmlsbGVkUXVhbnRpdHk=");
//			params.put("endTime", "MDUtMTEtMjAxOQ==");
//			params.put("signature", "d5ae5d8683d224403c5d3b52bc8932b5");
//			params.put("startTime", "MDEtMTEtMjAxOQ==");
//			params.put("viewBy", "U0tV");
			
			//20191102 - 20191103 - SKU - Qty Fulfilled
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "ZnVsZmlsbGVkUXVhbnRpdHk=");
//			params.put("endTime", "MDMtMTEtMjAxOQ==");
//			params.put("signature", "63bf406ad5598d51fc813b9e3fe51620");
//			params.put("startTime", "MDItMTEtMjAxOQ==");
//			params.put("viewBy", "U0tV");
			
//			 "viewBy": "U0tV",
//			    "unit": "ZnVsZmlsbGVkUXVhbnRpdHk=",
//			    "startTime": "MDItMTEtMjAxOQ==",
//			    "endTime": "MDMtMTEtMjAxOQ==",
//			    "signature": "63bf406ad5598d51fc813b9e3fe51620"

		} 
		else if(viewBy.equals(brand)) {
			//20191101 - 20191107 - Brand - Qty Fulfilled
			params.put("action", "widgetProductInsight");
			params.put("unit", "ZnVsZmlsbGVkUXVhbnRpdHk=");
			params.put("endTime", "MDctMTEtMjAxOQ==");
			params.put("signature", "420798b2922cc6a3691da1ce11fd932c");
			params.put("startTime", "MDEtMTEtMjAxOQ==");
			params.put("viewBy", "YnJhbmQ=");
			
			//20191105 - 20191112 - Brand - Qty Fulfilled
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "ZnVsZmlsbGVkUXVhbnRpdHk=");
//			params.put("endTime", "MTItMTEtMjAxOQ==");
//			params.put("signature", "71a0948296708d402b6fcbd6f24b76bd");
//			params.put("startTime", "MDUtMTEtMjAxOQ==");
//			params.put("viewBy", "YnJhbmQ=");
			
			//20191101 - 20191105 - Brand - Qty Fulfilled
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "ZnVsZmlsbGVkUXVhbnRpdHk=");
//			params.put("endTime", "MDUtMTEtMjAxOQ==");
//			params.put("signature", "0ef6790a952af3b1066d63b26b0747a6");
//			params.put("startTime", "MDEtMTEtMjAxOQ==");
//			params.put("viewBy", "YnJhbmQ=");
			
			//20191102 - 20191103 - Brand - Qty Fulfilled
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "ZnVsZmlsbGVkUXVhbnRpdHk=");
//			params.put("endTime", "MDMtMTEtMjAxOQ==");
//			params.put("signature", "dcf68993c72223ff8e82671d41149d3d");
//			params.put("startTime", "MDItMTEtMjAxOQ==");
//			params.put("viewBy", "YnJhbmQ=");
			
//			"viewBy": "YnJhbmQ=",
//		    "unit": "ZnVsZmlsbGVkUXVhbnRpdHk=",
//		    "startTime": "MDItMTEtMjAxOQ==",
//		    "endTime": "MDMtMTEtMjAxOQ==",
//		    "signature": "dcf68993c72223ff8e82671d41149d3d"

		}
		else {
			//20191101 - 20191107 - Product Group - Qty Fulfilled
			params.put("action", "widgetProductInsight");
			params.put("unit", "ZnVsZmlsbGVkUXVhbnRpdHk=");
			params.put("endTime", "MDctMTEtMjAxOQ==");
			params.put("signature", "b412eaa1ae331c82cde539029801db3e");
			params.put("startTime", "MDEtMTEtMjAxOQ==");
			params.put("viewBy", "cHJvZHVjdEdyb3Vw");
			
		}
	    	
		String path = getPathUrlEnvironmentWidgetData(environment);;
		
		SerenityRest.given().auth().oauth2(accessToken)
		.contentType("application/json")
		.and().params(params)
		.when().get(path);
	}
	
	@Step
	public String getListProductInsightQtyFulfilledString() {
		Map<String, String> salesRevenue = SalesOrdersResponse.returned();
		
		String totalQtyFulfilled = null;
		totalQtyFulfilled = salesRevenue.get("measurementValue");
		System.out.println("TOTAL REVENUE PRODUCT INSIGHT QTY FULFILLED : " + totalQtyFulfilled);
//		Integer totalQtyFulfilled = 0;
//		totalQtyFulfilled = In
//		
//		Double totalRevenueDouble = 0.0D;
//		totalRevenueDouble = Double.valueOf(totalRevenue);
//		System.out.println("totalRevenueDouble after convert : " + totalRevenueDouble);
//		
//		DecimalFormat df = new DecimalFormat("#.##");
//		String totalRevenueAfterDecimalFormat = null;
//		totalRevenueAfterDecimalFormat = df.format(totalRevenueDouble);
//		System.out.println("totalRevenueAfterDecimalFormat HASIL DESIMAL FORMAT : " + totalRevenueAfterDecimalFormat);
//		
//		return totalRevenueAfterDecimalFormat;
		return totalQtyFulfilled;
	}
	
	@Step
	public Map<String, String> getListProductInsightQtyFulfilled(String idClient, String username, String startTime,
			String endTime, String viewBy) throws JsonParseException, JsonMappingException, IOException, ParseException {
		System.out.println("ID CLIENT : " + idClient);
		System.out.println("START TIME : " + startTime);
		System.out.println("END TIME : " + endTime);
		
		Map<String, String> params = new HashMap<>();
		params.put("idClient", idClient);
		params.put("username", username);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		params.put("viewBy", viewBy);
		
		//get product insight by qty fulfilled
		Response response  = SerenityRest.given()
				.and().params(params)
				.contentType("application/json")
				.when().get(WebServiceEndPoints.PRODUCTINSIGHTQTYFULFILLED.getUrl());
		
		System.out.println("MARIA TEST RESPONSEEEEE BY QTY FULFILLED : " + response.asString());
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Integer> productInsightQtyFulfilled = mapper.readValue(response.asString(), Map.class);
		
		System.out.println("productInsightQtyFulfilled MARIA : " + productInsightQtyFulfilled);
		
		Map<String, String> allProductInsightByQtyFulfilled = new TreeMap<String, String>();

		for (Map.Entry<String, Integer> entry : productInsightQtyFulfilled.entrySet()) {
			System.out.println("Entry dot key : " + entry.getKey());
			System.out.println("Entry dot value : " + entry.getValue());
			String products = entry.getKey();
			System.out.println("products : " + products);

//			Integer qtyFulfilled = entry.getValue();
			if (!allProductInsightByQtyFulfilled.containsKey(products)) {
				String mValueString = Integer.toString((Integer) productInsightQtyFulfilled.get(products));
				System.out.println("KEY NOW : " + products);
				System.out.println("VALUE NOW : " + mValueString);
				allProductInsightByQtyFulfilled.put(products, mValueString);
			} else {
				allProductInsightByQtyFulfilled.put(products, "0");
			}
			
				System.out.println("All product insight by qty fulfilled : " + allProductInsightByQtyFulfilled);
		}		
		return allProductInsightByQtyFulfilled;
		
	}
	
	@Step
	public String getTotalProductInsightQtyFulfilled(String idClient, String username, String startTime, String endTime,
			String viewBy) {
		
		Map<String, String> params = new HashMap<>();
		params.put("idClient", idClient);
		params.put("username", username);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		params.put("viewBy", viewBy);
		
		//get revenue by so
		Response response  = SerenityRest.given()
				.and().params(params)
				.contentType("application/json")
				.when().get(WebServiceEndPoints.TOTALBYPRODUCTINSIGHTQTYFULFILLED.getUrl());
		
		System.out.println("getTotalProductInsightQtyFulfilled HASILNYA IN STRING : " + response.asString());
		
		return response.asString();
	}
	
	public Map<String, String> getAllignPercentageCompareMapBEAndServiceResultInt(Map<String, String> mapBE,
			Map<String, String> mapServiceResult, String viewBy) {
		
		Comparator<String> compString = Comparator.comparing(String::toString);

		Map<String, String> allignPerDetail = new TreeMap<>(compString);
		
		for (String key : mapServiceResult.keySet()) {
			String value2 = mapServiceResult.get(key);
			String value1;
			if(mapBE.containsKey(key)) {
				value1 = mapBE.get(key);
			}
			else {
				value1 = "0";
			}
			
			Integer value1BE = 0;
			value1BE = Integer.parseInt(value1);
			
			Integer value2Service = 0;
			value2Service = Integer.parseInt(value2);
			
//			boolean align = false;
			Integer align = 0;
			align = getAllignBEServiceInt(value2Service, value1BE);
			
			if(allignPerDetail.containsKey(key)) {
				Integer currAllign = align;
				allignPerDetail.put(key, currAllign + "%");
			}else {
				allignPerDetail.put(key, align + "%");
			}
		}
		
		//if other keys exist in BE but not exist in Service Result
		for (String key : mapBE.keySet()) {
			if(!allignPerDetail.containsKey(key)) {
				allignPerDetail.put(key, "0.0%");
			}
		}
		System.out.println("PERCENTAGE ALLIGN ALL DETAIL INT : " + viewBy + " : " + allignPerDetail.toString());
		return allignPerDetail;
	}
	
	private static Integer getAllignBEServiceInt(Integer value2Service, Integer value1BE) {
		Integer percentageAlign = 0;
		System.out.println("value 2 service : " + value2Service);
		System.out.println("value 2 BE : " + value1BE);
		percentageAlign = calculatePercentageAllignInt(value2Service, value1BE);
		System.out.println("percentageAlign in getAllignBEService : " + percentageAlign);
		if(percentageAlign.equals(Double.NaN)) {
			System.out.println("Percentage Align NaN");
			percentageAlign = 100;
		}
		if(percentageAlign.equals(Double.POSITIVE_INFINITY)) {
			System.out.println("Percentage Align Infinity");
			percentageAlign = 0;
		}
//		BigDecimal bd = new BigDecimal(percentageAlign).setScale(2, RoundingMode.HALF_UP);
//        Integer newInput = bd.ro;
//        System.out.println("PERCENTAGE ALIGN : " + newInput + "%");
        
        boolean align = false;
        
//        if(newInput<= 99.00 || newInput >= 101.0) {
//        	System.out.println("MASUK ALIGN FALSE");
//        	align = false;
////        	return align = false;
//        }
//        else {
//        	System.out.println("MASUK ALIGN TRUE");
//        	align = true;
////        	return align = true;
//        }
        return percentageAlign;
//        return newInput;
	}
	
	private static Integer calculatePercentageAllignInt(Integer value2Service, Integer value1BE) {
		Integer calculateResult;
		if(value1BE == 0 || value2Service == 0) {
			calculateResult = 0;
		}
		else {
			calculateResult = (value2Service/value1BE)*100;
		}
		return calculateResult;
	}

	@Step
	public void hitBEProductInsightScarcityLevel(String viewBy, String unit, String idClient, String startTime,
			String endTime, String environment) {
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjI5NzYyNCwic2NvcGUiOiJVUkUgQ0hDIERTViBLUlYgTlNSIFNOViBTUkUgU1JWIFNTViBTVEUgU1RWIFVNRSBVTVYgVk1WIFNPRSBTT1YgQUNMIEFDViBBTFYgQ0VBIENMQSBDTVYgQ1NWIENVQSBDVUUgQ1VWIENWTCBET0UgRE9WIEZSQSBITVYgTDFBIEwyQSBMM0EgTEZBIExNQSBMT0EgTE9CIE1HRCBPVVQgUENFIFBDViBQRUEgUExBIFBMRSBQTE8gUE1WIFBSRSBQUlYgUFNWIFNERSBTRFYgU0lFIFNJViBTVkUgU1ZWIENJViBGUkUgRlJWIExQRSBMUFYgQ0FEIE1JViBEMDEgRDAyIEQwMyBEMDQgRDA1IEQwNiBNR0QgTUdSIFIwMSBSMDIgUjAzIFIwNyBMVFYgR0FWIEJVRSBCMDIgQU1FIEFNViBCQkEgQjA3IFNWMiBXUE8gUjA2IEFNUyBCMDQgQjAxIFNPMiBCVVYgU1ZSIFIxNiAxSUEgQVBWIElOViBJU0kgSVNMIFBDViBQQ0UgQ1BFIENQViBQUkMgV1JWIFNPRiAxUEwgU1BWIFIxMSBSTVYgVE1WIFNWRCBEMDcgSUNWIERPMiBJU0EgQjEzIEIxMiBCMTYgQjE1IEIwMyBCMjAgQjE3IEIyMiBCMDUgTFRSIExPUyBTUEEgU1BFIFJUViBTT1IgUjE1IFZTTyBETE8iLCJ3b3JrR3JvdXBJZCI6MSwiZXhwIjoxNTc1OTEwNzk5fQ.oPQONDABeXpbECWhsojA9knUdVso0YgPmqzEG_aC26wTConpANs5Uo1WEEOjBLa_CvnQ_c3V4kOSWdPHE-wAMg";
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjMwNzc3MCwic2NvcGUiOiJVUkUgQ0hDIERTViBLUlYgTlNSIFNOViBTUkUgU1JWIFNTViBTVEUgU1RWIFVNRSBVTVYgVk1WIFNPRSBTT1YgQUNMIEFDViBBTFYgQ0VBIENMQSBDTVYgQ1NWIENVQSBDVUUgQ1VWIENWTCBET0UgRE9WIEZSQSBITVYgTDFBIEwyQSBMM0EgTEZBIExNQSBMT0EgTE9CIE1HRCBPVVQgUENFIFBDViBQRUEgUExBIFBMRSBQTE8gUE1WIFBSRSBQUlYgUFNWIFNERSBTRFYgU0lFIFNJViBTVkUgU1ZWIENJViBGUkUgRlJWIExQRSBMUFYgQ0FEIE1JViBEMDEgRDAyIEQwMyBEMDQgRDA1IEQwNiBNR0QgTUdSIFIwMSBSMDIgUjAzIFIwNyBMVFYgR0FWIEJVRSBCMDIgQU1FIEFNViBCQkEgQjA3IFNWMiBXUE8gUjA2IEFNUyBCMDQgQjAxIFNPMiBCVVYgU1ZSIFIxNiBSTVYgVE1WIFNQViBSMjAgRDE0IFNWRCBSMjIgSUNWIEJDTSBBUFYiLCJ3b3JrR3JvdXBJZCI6MSwiZXhwIjoxNTc1OTEwNzk5fQ.iQS453ZY6ZIwsQ4dAbb5Wg0Axtpictz2vNT48olts_R36zXVAVw7361nOBbd_ijkDj9CfNVN2il_oK3_QwkA-Q";
		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjEwMjkyMzksInNjb3BlIjoiVVJFIENIQyBEU1YgS1JWIE5TUiBTTlYgU1JFIFNSViBTU1YgU1RFIFNUViBVTUUgVU1WIFZNViBTT0UgU09WIEFDTCBBQ1YgQUxWIENFQSBDTEEgQ01WIENTViBDVUEgQ1VFIENVViBDVkwgRE9FIERPViBGUkEgSE1WIEwxQSBMMkEgTDNBIExGQSBMTUEgTE9BIExPQiBNR0QgT1VUIFBDRSBQQ1YgUEVBIFBMQSBQTEUgUExPIFBNViBQUkUgUFJWIFBTViBTREUgU0RWIFNJRSBTSVYgU1ZFIFNWMiBDSVYgRlJFIEZSViBMUEUgTFBWIENBRCBNSVYgRDAxIEQwMiBEMDMgRDA0IEQwNSBEMDYgTUdEIE1HUiBSMDEgUjAyIFIwMyBSMDcgTFRWIFNWMiBTTzIgU09SIFNPRiBTVlIgUjE2IERPMiBBTVMgQU1FIEFNViBCMDQgQjEzIEIxMiBCVUUgQjE1IEIwMSBCMTEgQjAyIEIwOSBCVVYgQjA3IEJCQSBEMDcgR0FWIFIwNiBTUEEgU1BFIDFQTCBTUFYgSVNJIFZTTyBXUE8gUjIwIFNWRCBBUFYgUENFIFBDViBJU0EgQjIyIEIxOCBCQkEgSUNWIElTTCBDUEUgQ1BWIFBSQyBJTlYgUEpFIFBKViBSMTEgUlRWIFZQSiBCMTYgQkNNIiwid29ya0dyb3VwSWQiOjEsImV4cCI6MTU3ODY3NTU5OX0.OQM2K48ck52-Wt3ZMBk1wPdoeGjvRgvdvuWan3TcenrOpkCtKJ8aigOr_DX0JxRuf5fSpGsUB3GHCP8XbjrdOg";
//		prod RAS advo support
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZHZvLnN1cHBvcnQiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEwMDAzNiwidXNlcklkIjo0NTU1LCJjb21wYW55SWQiOjEwMDAzNjEsImFkdm9jYXRlSWQiOiIzNzA0NjEiLCJsb2dpbkV2ZW50SWQiOjEwMTg3NzYsInNjb3BlIjoiQlVFIEJVViBDQUQgQ0hDIENJViBDVUUgQ1VWIERPRSBET1YgRFNWIEZSRSBGUlYgR0FWIExQViBNR1IgTUlWIE5TUiBQQ1YgRE8yIFIwMSBSMDIgUjA3IFNJRSBTSVYgU05WIFNPViBTUkUgU1JWIFNTViBTVEUgU1RWIFNWRSBTVjIgVU1FIFVNViBVUkUgVk1WIEIwNCBQTVYgUE1WIExUViBCMDEgQ1VWIENVViBCMDIgR0FWIFIwMSBDQUQgTUlWIFIxMiBCMDMgQjA2IEZSRSBET1YgTUlWIEFNRSBBTVMgQU1WIFNWMiBHQVYgU1YyIFNWMiBGUlYgUjA3IFIxMyBCMDcgU1ZSIEIxMyBCMDkgUE1WIExUViBCMTIgR0FWIEIxNSBCMTYgQjA1IERPRSBQSlYgUEpFIFJNViBTVlYgQjE0IEIxNyBWU08gQVBWIDFQVCBSMjEgQVJQIEdSUCBWTUQgVlJOIFZSUCBBUlAgR1JQIFJNViBWTUQgVlJOIFZSUCBEMTQgSUNWIEJCQSBEMDEgTFBWIExQRSBDUEUgQ1BWIFBSQyBWUEogRDE2IEQxNyBTUFYgUjAzIEQxNCBSMjIiLCJ3b3JrR3JvdXBJZCI6NDM4LCJleHAiOjE1Nzg0MTYzOTl9.nqkUEccuq1v2E_CJ_i7Ixbtd3pgpvV9fCYHnjNZSRdVJMNEtFQKSEpw8ADbrcUJ58dcq3lum29YEps3UoMt79Q";
		//prod federal advo cs (old) -> advo support (new)
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZHZvLnN1cHBvcnQiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjgzMzMsInVzZXJJZCI6NDU1NSwiY29tcGFueUlkIjo4MzMzNDMsImFkdm9jYXRlSWQiOiIzNzA0NjEiLCJsb2dpbkV2ZW50SWQiOjk2Nzk1Miwic2NvcGUiOiJCVUUgQlVWIENBRCBDSEMgQ0lWIENTViBDVUUgQ1VWIERPRSBET1YgRFNWIEZSRSBGUlYgR0FWIExQViBNR1IgTUlWIE5TUiBQQ1YgRE8yIFIwMSBSMDIgUjA3IFNJRSBTSVYgU05WIFNPViBTUkUgU1JWIFNTViBTVEUgU1RWIFNWRSBTVjIgVU1FIFVNViBVUkUgVk1WIEIwNCBQTVYgUE1WIExUViBCMDEgQ1VWIENVViBCMDIgR0FWIFIwMSBDQUQgTUlWIFIxMiBCMDMgQjA2IEZSRSBET1YgTUlWIEFNRSBBTVMgQU1WIFNWMiBHQVYgU1YyIFNWMiBGUlYgUjA3IFIxMyBCMDcgU1ZSIEIxMyBCMDkgUE1WIExUViBCMTIgR0FWIEIxNSBXUE8gQjE2IEIwNSBET0UgUEpWIFBKRSBSTVYgU1ZWIEIxNCBCMTcgVlNPIEFQViAxUFQgUjIxIEFSUCBHUlAgVk1EIFZSTiBWUlAgQVJQIEdSUCBSTVYgVk1EIFZSTiBWUlAgRDE0IElDViBCQkEgRDAxIExQViBMUEUgQ1BFIENQViBQUkMgVlBKIEQxNiBEMTcgU1BWIFIwMyBEMTQiLCJ3b3JrR3JvdXBJZCI6NTcsImV4cCI6MTU3NjYwMTk5OX0.roBXsDhh8nnf63pmmnVoIgtgKOQkls5bSOJbSA0dj6LfCTeroH3j9aDORmbrve28MPEm5xXHdZxwnZAtYTLc1Q";
		//prod nutrifood nf.superuser
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJuZi5zdXBlcnVzZXIiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjg2ODMsInVzZXJJZCI6NTY4NiwiY29tcGFueUlkIjo4NjgzMDEsImFkdm9jYXRlSWQiOiI1NDE1MzciLCJsb2dpbkV2ZW50SWQiOjEwMjI0NjYsInNjb3BlIjoiQUxWIENBRCBDSEMgQ0lWIENVViBETzIgRE9FIEtSViBNR0QgTUlWIFBNViBSV0UgUldWIFNEViBTTlYgU08yIFNPRSBTUkUgU1JWIFNTViBTVEUgU1RWIFNWRSBVTUUgVU1WIFVSRSBWTVYgQlVFIEJVViBCMDIgQjA0IExUUiBMVFYgU09FIFNPViBSMDEgUjAyIFIwMyBSMDQgUjA1IFIwNiBSMDcgUjA4IFIwOSBSMTAgUjExIFIxMiBHQVYgQjAxIEIwMSBCMDIgQjAzIEIwNCBCMDYgQjA3IEJVRSBCVVYgUjAxIFIwMiBSMDMgUjA0IFIwNSBSMDYgUjA3IFIwOCBSMDkgUjEwIFIxMSBSMTIgTFRSIExUViBHQVYgRDAxIEQwMSBEMDIgRDAzIEQwNCBEMDUgRDA2IEQwNyBEMDggRDA5IEQxMCBEMTEgRDEyIEZSViBMUFYgTFBFIERPViBWU08gRE9WIEJCQSBTTzIgU09SIFNPRiBDU1YgU1YyIFNWUiBSMTYgQjE1IEIxNSBNR1IgQjA5IFNWRCIsIndvcmtHcm91cElkIjoyMTcsImV4cCI6MTU3ODUwMjc5OX0.u4JJFiM29cDc-pO6MzOsQpZwFu02aXqUDxY9u197IGhJ4YjXhgYn6o55-2FnDpWarAHkbdM3fTKrsyVWUlmRVA";
		//prod federal advo.support
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZHZvLnN1cHBvcnQiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjgzMzMsInVzZXJJZCI6NDU1NSwiY29tcGFueUlkIjo4MzMzNDMsImFkdm9jYXRlSWQiOiIzNzA0NjEiLCJsb2dpbkV2ZW50SWQiOjEwMTg3NzYsInNjb3BlIjoiQlVFIEJVViBDQUQgQ0hDIENJViBDVUUgQ1VWIERPRSBET1YgRFNWIEZSRSBGUlYgR0FWIExQViBNR1IgTUlWIE5TUiBQQ1YgRE8yIFIwMSBSMDIgUjA3IFNJRSBTSVYgU05WIFNPViBTUkUgU1JWIFNTViBTVEUgU1RWIFNWRSBTVjIgVU1FIFVNViBVUkUgVk1WIEIwNCBQTVYgUE1WIExUViBCMDEgQ1VWIENVViBCMDIgR0FWIFIwMSBDQUQgTUlWIFIxMiBCMDMgQjA2IEZSRSBET1YgTUlWIEFNRSBBTVMgQU1WIFNWMiBHQVYgU1YyIFNWMiBGUlYgUjA3IFIxMyBCMDcgU1ZSIEIxMyBCMDkgUE1WIExUViBCMTIgR0FWIEIxNSBCMTYgQjA1IERPRSBQSlYgUEpFIFJNViBTVlYgQjE0IEIxNyBWU08gQVBWIDFQVCBSMjEgQVJQIEdSUCBWTUQgVlJOIFZSUCBBUlAgR1JQIFJNViBWTUQgVlJOIFZSUCBEMTQgSUNWIEJCQSBEMDEgTFBWIExQRSBDUEUgQ1BWIFBSQyBWUEogRDE2IEQxNyBTUFYgUjAzIEQxNCBSMjIiLCJ3b3JrR3JvdXBJZCI6NTcsImV4cCI6MTU3ODQxNjM5OX0.x8irYIig9lCincqSyf0ijGrGmsLYIs8GkblDmG64DE8aZlsywzp7hlE2UCdvDpxfeoxL47--EckCJZH5pgYtPw";
		
		
		Map<String, String> params = new HashMap<>();
		
		if(viewBy.equals(productGroup)) {
			//20191101 - 20191107 - Product Group - Scarcity Level
			params.put("action", "widgetProductInsight");
			params.put("unit", "c2NhcmNpdHlMZXZlbA==");
			params.put("endTime", "MDctMTEtMjAxOQ==");
			params.put("signature", "7cc4480604770a89ae0033def8dbd1f0");
			params.put("startTime", "MDEtMTEtMjAxOQ==");
			params.put("viewBy", "cHJvZHVjdEdyb3Vw");
			
			//20191105 - 20191112 - Product Group - Scarcity Level
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "c2NhcmNpdHlMZXZlbA==");
//			params.put("endTime", "MTItMTEtMjAxOQ==");
//			params.put("signature", "a35400b4f631fbe83fc9421f05694683");
//			params.put("startTime", "MDUtMTEtMjAxOQ==");
//			params.put("viewBy", "cHJvZHVjdEdyb3Vw");
			
			//20191101 - 20191105 - Product Group - Scarcity Level
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "c2NhcmNpdHlMZXZlbA==");
//			params.put("endTime", "MDUtMTEtMjAxOQ==");
//			params.put("signature", "633657348d9a914bf6af1844a6019960");
//			params.put("startTime", "MDEtMTEtMjAxOQ==");
//			params.put("viewBy", "cHJvZHVjdEdyb3Vw");
			
			//20191102 - 20191103 - Product Group - Scarcity Level
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "c2NhcmNpdHlMZXZlbA==");
//			params.put("endTime", "MDMtMTEtMjAxOQ==");
//			params.put("signature", "0491875df8778db851d292efa63b8657");
//			params.put("startTime", "MDItMTEtMjAxOQ==");
//			params.put("viewBy", "cHJvZHVjdEdyb3Vw");
			
//			"viewBy": "cHJvZHVjdEdyb3Vw",
//		    "unit": "c2NhcmNpdHlMZXZlbA==",
//		    "startTime": "MDItMTEtMjAxOQ==",
//		    "endTime": "MDMtMTEtMjAxOQ==",
//		    "signature": "0491875df8778db851d292efa63b8657"
		} 
		else if(viewBy.equals(SKU)) {
			//20191101 - 20191107 - SKU - Scarcity Level
			params.put("action", "widgetProductInsight");
			params.put("unit", "c2NhcmNpdHlMZXZlbA==");
			params.put("endTime", "MDctMTEtMjAxOQ==");
			params.put("signature", "21066eae0d3ab0ba7be0618fd5499169");
			params.put("startTime", "MDEtMTEtMjAxOQ==");
			params.put("viewBy", "U0tV");
			
			//20191105 - 20191112 - SKU - Scarcity Level
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "c2NhcmNpdHlMZXZlbA==");
//			params.put("endTime", "MTItMTEtMjAxOQ==");
//			params.put("signature", "eab3d5ab64840a62c83d13c134893d67");
//			params.put("startTime", "MDUtMTEtMjAxOQ==");
//			params.put("viewBy", "U0tV");
			
			//20191101 - 20191105 - SKU - Scarcity Level
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "c2NhcmNpdHlMZXZlbA==");
//			params.put("endTime", "MDUtMTEtMjAxOQ==");
//			params.put("signature", "8818d52b7d7a4a8c9a246f4a28a288b9");
//			params.put("startTime", "MDEtMTEtMjAxOQ==");
//			params.put("viewBy", "U0tV");
			
			//20191102 - 20191103 - SKU - Scarcity Level
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "c2NhcmNpdHlMZXZlbA==");
//			params.put("endTime", "MDMtMTEtMjAxOQ==");
//			params.put("signature", "dc3930acf79dbb7df1e54a6df0287e31");
//			params.put("startTime", "MDItMTEtMjAxOQ==");
//			params.put("viewBy", "U0tV");
			
//			"viewBy": "U0tV",
//		    "unit": "c2NhcmNpdHlMZXZlbA==",
//		    "startTime": "MDItMTEtMjAxOQ==",
//		    "endTime": "MDMtMTEtMjAxOQ==",
//		    "signature": "dc3930acf79dbb7df1e54a6df0287e31"

		} 
		else if(viewBy.equals(brand)) {
			//20191101 - 20191107 - Brand - Scarcity Level
			params.put("action", "widgetProductInsight");
			params.put("unit", "c2NhcmNpdHlMZXZlbA==");
			params.put("endTime", "MDctMTEtMjAxOQ==");
			params.put("signature", "38b7c8c17ac4c306ba15bd76ef6acd78");
			params.put("startTime", "MDEtMTEtMjAxOQ==");
			params.put("viewBy", "YnJhbmQ=");
			
			//20191105 - 20191112 - Brand - Scarcity Level
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "c2NhcmNpdHlMZXZlbA==");
//			params.put("endTime", "MTItMTEtMjAxOQ==");
//			params.put("signature", "9b59157c1c4c8eabd5a393325a9ac34a");
//			params.put("startTime", "MDUtMTEtMjAxOQ==");
//			params.put("viewBy", "YnJhbmQ=");
			
			//20191101 - 20191105 - Brand - Scarcity Level
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "c2NhcmNpdHlMZXZlbA==");
//			params.put("endTime", "MDUtMTEtMjAxOQ==");
//			params.put("signature", "064c00b72a48926faeb82fccbc45dbbf");
//			params.put("startTime", "MDEtMTEtMjAxOQ==");
//			params.put("viewBy", "YnJhbmQ=");
			
			//20191102 - 20191103 - Brand - Scarcity Level
//			params.put("action", "widgetProductInsight");
//			params.put("unit", "c2NhcmNpdHlMZXZlbA==");
//			params.put("endTime", "MDMtMTEtMjAxOQ==");
//			params.put("signature", "4b33bd77f8a566c7a520a4987967d3aa");
//			params.put("startTime", "MDItMTEtMjAxOQ==");
//			params.put("viewBy", "YnJhbmQ=");
			
//			"viewBy": "YnJhbmQ=",
//		    "unit": "c2NhcmNpdHlMZXZlbA==",
//		    "startTime": "MDItMTEtMjAxOQ==",
//		    "endTime": "MDMtMTEtMjAxOQ==",
//		    "signature": "4b33bd77f8a566c7a520a4987967d3aa"
		}
		else {
			//20191101 - 20191107 - Product Group - Scarcity Level
			params.put("action", "widgetProductInsight");
			params.put("unit", "c2NhcmNpdHlMZXZlbA==");
			params.put("endTime", "MDctMTEtMjAxOQ==");
			params.put("signature", "7cc4480604770a89ae0033def8dbd1f0");
			params.put("startTime", "MDEtMTEtMjAxOQ==");
			params.put("viewBy", "cHJvZHVjdEdyb3Vw");
			
		}
	    	
		String path = getPathUrlEnvironmentWidgetData(environment);;
		
		SerenityRest.given().auth().oauth2(accessToken)
		.contentType("application/json")
		.and().params(params)
		.when().get(path);		
		
	}

	@Step
	public Map<String, String> getListProductInsightScarcityLevel(String idClient, String username, String startTime,
			String endTime, String viewBy) throws JsonParseException, JsonMappingException, IOException {
		System.out.println("ID CLIENT : " + idClient);
		System.out.println("START TIME : " + startTime);
		System.out.println("END TIME : " + endTime);
		
		Map<String, String> params = new HashMap<>();
		params.put("idClient", idClient);
		params.put("username", username);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		params.put("viewBy", viewBy);
		
		Response response  = SerenityRest.given()
				.and().params(params)
				.contentType("application/json")
				.when().get(WebServiceEndPoints.PRODUCTINSIGHTSCARCITYLEVEL.getUrl());
		
		System.out.println("MARIA TEST RESPONSEEEEE BY SCARCITY LEVEL : " + response.asString());
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Double> scarcityLevelByProduct = mapper.readValue(response.asString(), Map.class);
		
		System.out.println("product insight scarcity level MARIA : " + scarcityLevelByProduct);
		
		Map<String, String> allScarcityLevelByProduct = new TreeMap<String, String>();

		for (Map.Entry<String, Double> entry : scarcityLevelByProduct.entrySet()) {
			System.out.println("Entry dot key : " + entry.getKey());
			System.out.println("Entry dot value : " + entry.getValue());
			String products = entry.getKey();
			System.out.println("products : " + products);

			Double revenue = entry.getValue();
			if (!allScarcityLevelByProduct.containsKey(products)) {
				String mValueString = Double.toString((Double) scarcityLevelByProduct.get(products));
				System.out.println("KEY NOW : " + products);
				System.out.println("VALUE NOW : " + mValueString);
				allScarcityLevelByProduct.put(products, mValueString);
			} else {
				allScarcityLevelByProduct.put(products, "0.0");
			}
			
				System.out.println("All scarcity level by product : " + allScarcityLevelByProduct);
		}		
		return allScarcityLevelByProduct;
	}

	@Step
	public String getTotalProductInsightScarcityLevel(String idClient, String username, String startTime,
			String endTime, String viewBy) {
		Map<String, String> params = new HashMap<>();
		params.put("idClient", idClient);
		params.put("username", username);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		params.put("viewBy", viewBy);
		
		Response response  = SerenityRest.given()
				.and().params(params)
				.contentType("application/json")
				.when().get(WebServiceEndPoints.TOTALBYPRODUCTINSIGHTSCARCITYLEVEL.getUrl());
		
		System.out.println("getTotalProductInsightScarcityLevel HASILNYA IN STRING : " + response.asString());
		
		return response.asString();
	}
	
	@Step
	public void hitBECustomerOverview(String idClient, String startTime,
			String endTime, String environment) {
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjI5NzYyNCwic2NvcGUiOiJVUkUgQ0hDIERTViBLUlYgTlNSIFNOViBTUkUgU1JWIFNTViBTVEUgU1RWIFVNRSBVTVYgVk1WIFNPRSBTT1YgQUNMIEFDViBBTFYgQ0VBIENMQSBDTVYgQ1NWIENVQSBDVUUgQ1VWIENWTCBET0UgRE9WIEZSQSBITVYgTDFBIEwyQSBMM0EgTEZBIExNQSBMT0EgTE9CIE1HRCBPVVQgUENFIFBDViBQRUEgUExBIFBMRSBQTE8gUE1WIFBSRSBQUlYgUFNWIFNERSBTRFYgU0lFIFNJViBTVkUgU1ZWIENJViBGUkUgRlJWIExQRSBMUFYgQ0FEIE1JViBEMDEgRDAyIEQwMyBEMDQgRDA1IEQwNiBNR0QgTUdSIFIwMSBSMDIgUjAzIFIwNyBMVFYgR0FWIEJVRSBCMDIgQU1FIEFNViBCQkEgQjA3IFNWMiBXUE8gUjA2IEFNUyBCMDQgQjAxIFNPMiBCVVYgU1ZSIFIxNiAxSUEgQVBWIElOViBJU0kgSVNMIFBDViBQQ0UgQ1BFIENQViBQUkMgV1JWIFNPRiAxUEwgU1BWIFIxMSBSTVYgVE1WIFNWRCBEMDcgSUNWIERPMiBJU0EgQjEzIEIxMiBCMTYgQjE1IEIwMyBCMjAgQjE3IEIyMiBCMDUgTFRSIExPUyBTUEEgU1BFIFJUViBTT1IgUjE1IFZTTyBETE8iLCJ3b3JrR3JvdXBJZCI6MSwiZXhwIjoxNTc1OTEwNzk5fQ.oPQONDABeXpbECWhsojA9knUdVso0YgPmqzEG_aC26wTConpANs5Uo1WEEOjBLa_CvnQ_c3V4kOSWdPHE-wAMg";
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjMwNzc3MCwic2NvcGUiOiJVUkUgQ0hDIERTViBLUlYgTlNSIFNOViBTUkUgU1JWIFNTViBTVEUgU1RWIFVNRSBVTVYgVk1WIFNPRSBTT1YgQUNMIEFDViBBTFYgQ0VBIENMQSBDTVYgQ1NWIENVQSBDVUUgQ1VWIENWTCBET0UgRE9WIEZSQSBITVYgTDFBIEwyQSBMM0EgTEZBIExNQSBMT0EgTE9CIE1HRCBPVVQgUENFIFBDViBQRUEgUExBIFBMRSBQTE8gUE1WIFBSRSBQUlYgUFNWIFNERSBTRFYgU0lFIFNJViBTVkUgU1ZWIENJViBGUkUgRlJWIExQRSBMUFYgQ0FEIE1JViBEMDEgRDAyIEQwMyBEMDQgRDA1IEQwNiBNR0QgTUdSIFIwMSBSMDIgUjAzIFIwNyBMVFYgR0FWIEJVRSBCMDIgQU1FIEFNViBCQkEgQjA3IFNWMiBXUE8gUjA2IEFNUyBCMDQgQjAxIFNPMiBCVVYgU1ZSIFIxNiBSTVYgVE1WIFNQViBSMjAgRDE0IFNWRCBSMjIgSUNWIEJDTSBBUFYiLCJ3b3JrR3JvdXBJZCI6MSwiZXhwIjoxNTc1OTEwNzk5fQ.iQS453ZY6ZIwsQ4dAbb5Wg0Axtpictz2vNT48olts_R36zXVAVw7361nOBbd_ijkDj9CfNVN2il_oK3_QwkA-Q";
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjEwNjUzNzAsInNjb3BlIjoiVVJFIENIQyBEU1YgS1JWIE5TUiBTTlYgU1JFIFNSViBTU1YgU1RFIFNUViBVTUUgVU1WIFZNViBTT0UgU09WIEFDTCBBQ1YgQUxWIENFQSBDTEEgQ01WIENTViBDVUEgQ1VFIENVViBDVkwgRE9FIERPViBGUkEgSE1WIEwxQSBMMkEgTDNBIExGQSBMTUEgTE9BIExPQiBNR0QgT1VUIFBDRSBQQ1YgUEVBIFBMQSBQTEUgUExPIFBNViBQUkUgUFJWIFBTViBTREUgU0RWIFNJRSBTSVYgU1ZFIFNWMiBDSVYgRlJFIEZSViBMUEUgTFBWIENBRCBNSVYgRDAxIEQwMiBEMDMgRDA0IEQwNSBEMDYgTUdEIE1HUiBSMDEgUjAyIFIwMyBSMDcgTFRWIFNWMiBTTzIgU09SIFNPRiBTVlIgUjE2IERPMiBBTVMgQU1FIEFNViBCMDQgQjEzIEIxMiBCVUUgQjE1IEIwMSBCMTEgQjAyIEIwOSBCVVYgQjA3IEJCQSBEMDcgR0FWIFIwNiBTUEEgU1BFIDFQTCBTUFYgSVNJIFZTTyBXUE8gUjIwIFNWRCBBUFYgUENFIFBDViBJU0EgQjIyIEIxOCBCQkEgSUNWIElTTCBDUEUgQ1BWIFBSQyBJTlYgUEpFIFBKViBSMTEgUlRWIFZQSiBCMTYgQkNNIiwid29ya0dyb3VwSWQiOjEsImV4cCI6MTU3OTc5ODc5OX0._4zELtx0LFkFZIk0N0jyhR_uQkLsevcH8efEepwzkQpbBmIYtsupYHWiXEHm55X4xm4PaQgwJDclGop52IWGjg";
//		prod RAS advo support
		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZHZvLnN1cHBvcnQiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEwMDAzNiwidXNlcklkIjo0NTU1LCJjb21wYW55SWQiOjEwMDAzNjEsImFkdm9jYXRlSWQiOiIzNzA0NjEiLCJsb2dpbkV2ZW50SWQiOjEwNjkxMjIsInNjb3BlIjoiQUxWIEFURSBBVFYgQlVFIEJVViBDQUQgQ0hDIENJViBDTVAgQ1NWIENVRSBDVVYgRDAxIEQwMiBEMDMgRDA0IEQwNSBEMDYgRE9FIERPViBEU1YgRlJFIEZSViBHQVYgS1JWIExQViBNR0QgTUdSIE1JViBOU1IgUENWIERPMiBSMDEgUjAyIFIwMyBSMDQgUjA1IFIwNiBSMDcgUldWIFNJRSBTSVYgU05WIFNPMiBTTzIgU09WIFNSRSBTUlYgU1NWIFNURSBTVFYgU1ZFIFNWMiBVTUUgVU1WIFVSRSBWTVYgQjA0IFBNViBQTVYgTFRWIEIwMSBCQ00gQkNNIENVViBDVVYgQjAyIEdBViBCVVYgQlVFIEIwNCBEMDIgUjAxIFIwMyBEMDEgQ0FEIE1JViBCMDQgUjEyIEJCQSBCMDMgQjA2IFIwOCBGUkUgRE9WIE1JViBBTUUgQU1TIEFNViBTVjIgR0FWIFNPMiBTVjIgU1YyIEZSViBTT1IgU09GIFIwNyBSMTMgU09FIEJCQSBCMDcgU1ZSIFIxMCBCMTMgQjA5IFBNViBMVFYgQkJBIEIxMiBSMTYgR0FWIEIxNSBXUE8gQjE2IEIwNSBET0UgUEpWIFZBTSBTVkQgUEpFIFJNViBEMTQgU1ZWIEIxNCBCMTcgVlNPIEFQViAxUFQgUjIxIEFSUCBHUlAgVk1EIFZSTiBWUlAgQVJQIEdSUCBSTVYgVk1EIFZSTiBWUlAgRDE0IElDViBCQkEgRDAxIExQViBMUEUgQ1BFIENQViBQUkMgVlBKIEQxNiBEMTcgU1BWIFIwMyBEMTQgUjIyIEIyMSBFTFYgVE1WIiwid29ya0dyb3VwSWQiOjQzOCwiZXhwIjoxNTc5ODg1MTk5fQ.pSdyVk6R05UWywctOmQWAnQT_jzWLmpPyZNZjwRuU-bt7_4skt75IERpaX3d5VDXJ-hD4b40JqHMxAuDQL5u7Q";
		//prod nutrifood nf.superuser
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJuZi5zdXBlcnVzZXIiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjg2ODMsInVzZXJJZCI6NTY4NiwiY29tcGFueUlkIjo4NjgzMDEsImFkdm9jYXRlSWQiOiI1NDE1MzciLCJsb2dpbkV2ZW50SWQiOjEwNjkwNjAsInNjb3BlIjoiQUxWIENBRCBDSEMgQ0lWIENVViBETzIgRE9FIEtSViBNR0QgTUlWIFBNViBSV0UgUldWIFNEViBTTlYgU08yIFNPRSBTUkUgU1JWIFNTViBTVEUgU1RWIFNWRSBVTUUgVU1WIFVSRSBWTVYgQlVFIEJVViBCMDIgQjA0IExUUiBMVFYgU09FIFNPViBSMDEgUjAyIFIwMyBSMDQgUjA1IFIwNiBSMDcgUjA4IFIwOSBSMTAgUjExIFIxMiBHQVYgQjAxIEIwMSBCMDIgQjAzIEIwNCBCMDYgQjA3IEJVRSBCVVYgUjAxIFIwMiBSMDMgUjA0IFIwNSBSMDYgUjA3IFIwOCBSMDkgUjEwIFIxMSBSMTIgTFRSIExUViBHQVYgRDAxIEQwMSBEMDIgRDAzIEQwNCBEMDUgRDA2IEQwNyBEMDggRDA5IEQxMCBEMTEgRDEyIEZSViBMUFYgTFBFIERPViBWU08gRE9WIEJCQSBTTzIgU09SIFNPRiBDU1YgU1YyIFNWUiBSMTYgQjE1IEIxNSBNR1IgQjA5IFNWRCIsIndvcmtHcm91cElkIjoyMTcsImV4cCI6MTU3OTg4NTE5OX0.oGve2IN6NFDoDeuutpvc6HK1nIUE3sTrT4kPQQ5gBcNrlrZXNCZJaOpYRTDmWQ6O9WxUf4C-4NSu6P-P5qTFtA";
		//prod federal advo.support
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZHZvLnN1cHBvcnQiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjgzMzMsInVzZXJJZCI6NDU1NSwiY29tcGFueUlkIjo4MzMzNDMsImFkdm9jYXRlSWQiOiIzNzA0NjEiLCJsb2dpbkV2ZW50SWQiOjEwNjkxMjIsInNjb3BlIjoiQUxWIEFURSBBVFYgQlVFIEJVViBDQUQgQ0hDIENJViBDTVAgQ1NWIENVRSBDVVYgRDAxIEQwMiBEMDMgRDA0IEQwNSBEMDYgRE9FIERPViBEU1YgRlJFIEZSViBHQVYgS1JWIExQViBNR0QgTUdSIE1JViBOU1IgUENWIERPMiBSMDEgUjAyIFIwMyBSMDQgUjA1IFIwNiBSMDcgUldWIFNJRSBTSVYgU05WIFNPMiBTTzIgU09WIFNSRSBTUlYgU1NWIFNURSBTVFYgU1ZFIFNWMiBVTUUgVU1WIFVSRSBWTVYgQjA0IFBNViBQTVYgTFRWIEIwMSBCQ00gQkNNIENVViBDVVYgQjAyIEdBViBCVVYgQlVFIEIwNCBEMDIgUjAxIFIwMyBEMDEgQ0FEIE1JViBCMDQgUjEyIEJCQSBCMDMgQjA2IFIwOCBGUkUgRE9WIE1JViBBTUUgQU1TIEFNViBTVjIgR0FWIFNPMiBTVjIgU1YyIEZSViBTT1IgU09GIFIwNyBSMTMgU09FIEJCQSBCMDcgU1ZSIFIxMCBCMTMgQjA5IFBNViBMVFYgQkJBIEIxMiBSMTYgR0FWIEIxNSBXUE8gQjE2IEIwNSBET0UgUEpWIFZBTSBTVkQgUEpFIFJNViBEMTQgU1ZWIEIxNCBCMTcgVlNPIEFQViAxUFQgUjIxIEFSUCBHUlAgVk1EIFZSTiBWUlAgQVJQIEdSUCBSTVYgVk1EIFZSTiBWUlAgRDE0IElDViBCQkEgRDAxIExQViBMUEUgQ1BFIENQViBQUkMgVlBKIEQxNiBEMTcgU1BWIFIwMyBEMTQgUjIyIEIyMSBFTFYgVE1WIiwid29ya0dyb3VwSWQiOjU3LCJleHAiOjE1Nzk4ODUxOTl9.sa5rY2etaA_Idf_5IF_q6KNhOYCiCVcf4qKijZX7JopdHI9_zdlRTurSlflh30XRRjXgwLmaiNFsjdLs9xyLYw";
		
		
		Map<String, String> params = new HashMap<>();
		//20191101 - 20191121 - Customer Overtime
//		params.put("action", "widgetCustomerOverview");
//		params.put("endTime", "MjEtMTEtMjAxOQ==");
//		params.put("signature", "8f04307328250d890a3c2ddc05a7ff55");
//		params.put("startTime", "MDEtMTEtMjAxOQ==");
		
		//20191101 - 20191107 - Customer Overtime
//		params.put("action", "widgetCustomerOverview");
//		params.put("endTime", "MDctMTEtMjAxOQ==");
//		params.put("signature", "9fb8be500b75179187023e0b16e23b41");
//		params.put("startTime", "MDEtMTEtMjAxOQ==");
		
		//20191201 - 20191230 - Customer Overview
		params.put("action", "widgetCustomerOverview");
		params.put("endTime", "MzAtMTItMjAxOQ==");
		params.put("signature", "177d47973ba82670efc0899676a7d2e0");
		params.put("startTime", "MDEtMTItMjAxOQ==");
		
//		"startTime": "MDEtMTItMjAxOQ==",
//	    "endTime": "MzAtMTItMjAxOQ==",
//	    "signature": "177d47973ba82670efc0899676a7d2e0"

		String path = getPathUrlEnvironmentWidgetData(environment);;
		
		SerenityRest.given().auth().oauth2(accessToken)
		.contentType("application/json")
		.and().params(params)
		.when().get(path);		
		
	}

	public Map<String, String> getListCustomerOverview() {
		List<HashMap<String, String>> customerOverviews = SalesOrdersResponse.balikanCustomerOverview();
		
		Map<String, String> value = new TreeMap<String, String>();
		for (HashMap<String, String> customerOverview: customerOverviews) {
			value.put(customerOverview.getOrDefault("name", "0"), String.valueOf(customerOverview.getOrDefault("value", "0")));
		}
		
		System.out.println("RESULT: " + value);
		return value;
	}

	public Map<String, String> getListCustomerOverviewService(String idClient, String username, String startTime,
			String endTime) throws JsonParseException, JsonMappingException, IOException {
		System.out.println("ID CLIENT : " + idClient);
		System.out.println("START TIME : " + startTime);
		System.out.println("END TIME : " + endTime);
		
		Map<String, String> params = new HashMap<>();
		params.put("idClient", idClient);
		params.put("username", username);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		
		Response response  = SerenityRest.given()
				.and().params(params)
				.contentType("application/json")
				.when().get(WebServiceEndPoints.CUSTOMEROVERVIEW.getUrl());
		
		System.out.println("MARIA TEST RESPONSEEEEE BY CUSTOMER OVERVIEW WORKGROUP : " + response.asString());
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Integer> customerOverview = mapper.readValue(response.asString(), Map.class);
		
		System.out.println("customer overview workgroup MARIA : " + customerOverview);
		
		Map<String, String> allCustomerOverview = new TreeMap<String, String>();

		for (Map.Entry<String, Integer> entry : customerOverview.entrySet()) {
			System.out.println("Entry dot key : " + entry.getKey());
			System.out.println("Entry dot value : " + entry.getValue());
			String customerOverviewWorkgroupLabel = entry.getKey();
			System.out.println("customerOverviewType : " + customerOverviewWorkgroupLabel);

			if (!allCustomerOverview.containsKey(customerOverviewWorkgroupLabel)) {
				Integer mValueString = customerOverview.get(customerOverviewWorkgroupLabel);
				System.out.println("KEY NOW : " + customerOverviewWorkgroupLabel);
				System.out.println("VALUE NOW : " + mValueString);
				allCustomerOverview.put(customerOverviewWorkgroupLabel, String.valueOf(mValueString));
			} else {
				allCustomerOverview.put(customerOverviewWorkgroupLabel, "0");
			}
			
				System.out.println("All customer overview allCustomerOverview : " + allCustomerOverview);
		}		
		return allCustomerOverview;
	}

	public Map<String, String> getAllignPercentageCompareMapBEAndServiceResultCustomerOverview(
			Map<String, String> listCustomerOverviewBE, Map<String, String> listCustomerOverviewServiceResult) {
		Comparator<String> compString = Comparator.comparing(String::toString);

		Map<String, String> allignPerDetail = new TreeMap<>(compString);
		
		for (String key : listCustomerOverviewServiceResult.keySet()) {
			String value2 = listCustomerOverviewServiceResult.get(key);
//			String value2Registered, value2Visited, value2Productive;
//			
//			value2Registered = StringUtils.getSplit(value2, 0);
//			value2Visited = StringUtils.getSplit(value2, 1);
//			value2Productive = StringUtils.getSplit(value2, 2);
			
			String value1;
//			, value1Registered, value1Visited, value1Productive;
			if(listCustomerOverviewBE.containsKey(key)) {
				value1 = listCustomerOverviewBE.get(key);
			}
			else {
				value1 = "0.0";
			}
//			value1Registered = StringUtils.getSplit(value1, 0);
//			value1Visited = StringUtils.getSplit(value1, 1);
//			value1Productive = StringUtils.getSplit(value1, 2);
			
			//parse to Double
//			Double value1RegisteredBE, value1VisitedBE, value1ProductiveBE = 0.0D;
//			value1RegisteredBE = Double.parseDouble(value1Registered);
//			value1VisitedBE = Double.parseDouble(value1Visited);
//			value1ProductiveBE = Double.parseDouble(value1Productive);
//			
//			Double value2RegisteredService, value2VisitedService, value2ProductiveService = 0.0D;
//			value2RegisteredService = Double.parseDouble(value2Registered);
//			value2VisitedService = Double.parseDouble(value2Visited);
//			value2ProductiveService = Double.parseDouble(value2Productive);
			
			Double value1BE = 0.0D;
			value1BE = Double.parseDouble(value1);
			
			Double value2Service = 0.0D;
			value2Service = Double.parseDouble(value2);
			
			Double alignStore = 0D;
			alignStore = getAllignBEService(value2Service, value1BE);
			
			if(allignPerDetail.containsKey(key)) {
				Double currAllign = alignStore;
				allignPerDetail.put(key, currAllign + "%");
			}else {
				allignPerDetail.put(key, alignStore + "%");
			}
			
////			boolean align = false;
//			Double alignRegisteredStore, alignVisitedStore, alignProductiveStore = 0D;
//			alignRegisteredStore = getAllignBEService(value2RegisteredService, value1RegisteredBE);
//			alignVisitedStore = getAllignBEService(value2VisitedService, value1VisitedBE);
//			alignProductiveStore = getAllignBEService(value2ProductiveService, value1ProductiveBE);
//			
//			allignPerDetail.put(key, alignRegisteredStore + "% | " + alignVisitedStore + "% | " + alignProductiveStore);
//			
////			if(allignPerDetail.containsKey(key)) {
////				Double currAllign = alignRegisteredStore;
////				allignPerDetail.put(key, currAllign + "%");
////			}else {
////				allignPerDetail.put(key, alignRegisteredStore + "%");
////			}
		}
		
		//if other keys exist in BE but not exist in Service Result
		for (String key : listCustomerOverviewBE.keySet()) {
			if(!allignPerDetail.containsKey(key)) {
				allignPerDetail.put(key, "0.0%");
			}
		}
		System.out.println("PERCENTAGE ALLIGN ALL DETAIL Cust Overview : " + allignPerDetail.toString());
		return allignPerDetail;
	}

	@Step
	public void hitBECustomerOverviewByWorkgroup(String idClient, String startTime, String endTime,
			String environment) {
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjI5NzYyNCwic2NvcGUiOiJVUkUgQ0hDIERTViBLUlYgTlNSIFNOViBTUkUgU1JWIFNTViBTVEUgU1RWIFVNRSBVTVYgVk1WIFNPRSBTT1YgQUNMIEFDViBBTFYgQ0VBIENMQSBDTVYgQ1NWIENVQSBDVUUgQ1VWIENWTCBET0UgRE9WIEZSQSBITVYgTDFBIEwyQSBMM0EgTEZBIExNQSBMT0EgTE9CIE1HRCBPVVQgUENFIFBDViBQRUEgUExBIFBMRSBQTE8gUE1WIFBSRSBQUlYgUFNWIFNERSBTRFYgU0lFIFNJViBTVkUgU1ZWIENJViBGUkUgRlJWIExQRSBMUFYgQ0FEIE1JViBEMDEgRDAyIEQwMyBEMDQgRDA1IEQwNiBNR0QgTUdSIFIwMSBSMDIgUjAzIFIwNyBMVFYgR0FWIEJVRSBCMDIgQU1FIEFNViBCQkEgQjA3IFNWMiBXUE8gUjA2IEFNUyBCMDQgQjAxIFNPMiBCVVYgU1ZSIFIxNiAxSUEgQVBWIElOViBJU0kgSVNMIFBDViBQQ0UgQ1BFIENQViBQUkMgV1JWIFNPRiAxUEwgU1BWIFIxMSBSTVYgVE1WIFNWRCBEMDcgSUNWIERPMiBJU0EgQjEzIEIxMiBCMTYgQjE1IEIwMyBCMjAgQjE3IEIyMiBCMDUgTFRSIExPUyBTUEEgU1BFIFJUViBTT1IgUjE1IFZTTyBETE8iLCJ3b3JrR3JvdXBJZCI6MSwiZXhwIjoxNTc1OTEwNzk5fQ.oPQONDABeXpbECWhsojA9knUdVso0YgPmqzEG_aC26wTConpANs5Uo1WEEOjBLa_CvnQ_c3V4kOSWdPHE-wAMg";
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjMwNzc3MCwic2NvcGUiOiJVUkUgQ0hDIERTViBLUlYgTlNSIFNOViBTUkUgU1JWIFNTViBTVEUgU1RWIFVNRSBVTVYgVk1WIFNPRSBTT1YgQUNMIEFDViBBTFYgQ0VBIENMQSBDTVYgQ1NWIENVQSBDVUUgQ1VWIENWTCBET0UgRE9WIEZSQSBITVYgTDFBIEwyQSBMM0EgTEZBIExNQSBMT0EgTE9CIE1HRCBPVVQgUENFIFBDViBQRUEgUExBIFBMRSBQTE8gUE1WIFBSRSBQUlYgUFNWIFNERSBTRFYgU0lFIFNJViBTVkUgU1ZWIENJViBGUkUgRlJWIExQRSBMUFYgQ0FEIE1JViBEMDEgRDAyIEQwMyBEMDQgRDA1IEQwNiBNR0QgTUdSIFIwMSBSMDIgUjAzIFIwNyBMVFYgR0FWIEJVRSBCMDIgQU1FIEFNViBCQkEgQjA3IFNWMiBXUE8gUjA2IEFNUyBCMDQgQjAxIFNPMiBCVVYgU1ZSIFIxNiBSTVYgVE1WIFNQViBSMjAgRDE0IFNWRCBSMjIgSUNWIEJDTSBBUFYiLCJ3b3JrR3JvdXBJZCI6MSwiZXhwIjoxNTc1OTEwNzk5fQ.iQS453ZY6ZIwsQ4dAbb5Wg0Axtpictz2vNT48olts_R36zXVAVw7361nOBbd_ijkDj9CfNVN2il_oK3_QwkA-Q";
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjEwNjUzNzAsInNjb3BlIjoiVVJFIENIQyBEU1YgS1JWIE5TUiBTTlYgU1JFIFNSViBTU1YgU1RFIFNUViBVTUUgVU1WIFZNViBTT0UgU09WIEFDTCBBQ1YgQUxWIENFQSBDTEEgQ01WIENTViBDVUEgQ1VFIENVViBDVkwgRE9FIERPViBGUkEgSE1WIEwxQSBMMkEgTDNBIExGQSBMTUEgTE9BIExPQiBNR0QgT1VUIFBDRSBQQ1YgUEVBIFBMQSBQTEUgUExPIFBNViBQUkUgUFJWIFBTViBTREUgU0RWIFNJRSBTSVYgU1ZFIFNWMiBDSVYgRlJFIEZSViBMUEUgTFBWIENBRCBNSVYgRDAxIEQwMiBEMDMgRDA0IEQwNSBEMDYgTUdEIE1HUiBSMDEgUjAyIFIwMyBSMDcgTFRWIFNWMiBTTzIgU09SIFNPRiBTVlIgUjE2IERPMiBBTVMgQU1FIEFNViBCMDQgQjEzIEIxMiBCVUUgQjE1IEIwMSBCMTEgQjAyIEIwOSBCVVYgQjA3IEJCQSBEMDcgR0FWIFIwNiBTUEEgU1BFIDFQTCBTUFYgSVNJIFZTTyBXUE8gUjIwIFNWRCBBUFYgUENFIFBDViBJU0EgQjIyIEIxOCBCQkEgSUNWIElTTCBDUEUgQ1BWIFBSQyBJTlYgUEpFIFBKViBSMTEgUlRWIFZQSiBCMTYgQkNNIiwid29ya0dyb3VwSWQiOjEsImV4cCI6MTU3OTc5ODc5OX0._4zELtx0LFkFZIk0N0jyhR_uQkLsevcH8efEepwzkQpbBmIYtsupYHWiXEHm55X4xm4PaQgwJDclGop52IWGjg";
//		prod RAS advo support
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZHZvLnN1cHBvcnQiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEwMDAzNiwidXNlcklkIjo0NTU1LCJjb21wYW55SWQiOjEwMDAzNjEsImFkdm9jYXRlSWQiOiIzNzA0NjEiLCJsb2dpbkV2ZW50SWQiOjEwNjkxMjIsInNjb3BlIjoiQUxWIEFURSBBVFYgQlVFIEJVViBDQUQgQ0hDIENJViBDTVAgQ1NWIENVRSBDVVYgRDAxIEQwMiBEMDMgRDA0IEQwNSBEMDYgRE9FIERPViBEU1YgRlJFIEZSViBHQVYgS1JWIExQViBNR0QgTUdSIE1JViBOU1IgUENWIERPMiBSMDEgUjAyIFIwMyBSMDQgUjA1IFIwNiBSMDcgUldWIFNJRSBTSVYgU05WIFNPMiBTTzIgU09WIFNSRSBTUlYgU1NWIFNURSBTVFYgU1ZFIFNWMiBVTUUgVU1WIFVSRSBWTVYgQjA0IFBNViBQTVYgTFRWIEIwMSBCQ00gQkNNIENVViBDVVYgQjAyIEdBViBCVVYgQlVFIEIwNCBEMDIgUjAxIFIwMyBEMDEgQ0FEIE1JViBCMDQgUjEyIEJCQSBCMDMgQjA2IFIwOCBGUkUgRE9WIE1JViBBTUUgQU1TIEFNViBTVjIgR0FWIFNPMiBTVjIgU1YyIEZSViBTT1IgU09GIFIwNyBSMTMgU09FIEJCQSBCMDcgU1ZSIFIxMCBCMTMgQjA5IFBNViBMVFYgQkJBIEIxMiBSMTYgR0FWIEIxNSBXUE8gQjE2IEIwNSBET0UgUEpWIFZBTSBTVkQgUEpFIFJNViBEMTQgU1ZWIEIxNCBCMTcgVlNPIEFQViAxUFQgUjIxIEFSUCBHUlAgVk1EIFZSTiBWUlAgQVJQIEdSUCBSTVYgVk1EIFZSTiBWUlAgRDE0IElDViBCQkEgRDAxIExQViBMUEUgQ1BFIENQViBQUkMgVlBKIEQxNiBEMTcgU1BWIFIwMyBEMTQgUjIyIEIyMSBFTFYgVE1WIiwid29ya0dyb3VwSWQiOjQzOCwiZXhwIjoxNTc5ODg1MTk5fQ.pSdyVk6R05UWywctOmQWAnQT_jzWLmpPyZNZjwRuU-bt7_4skt75IERpaX3d5VDXJ-hD4b40JqHMxAuDQL5u7Q";
		//prod nutrifood nf.superuser
		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJuZi5zdXBlcnVzZXIiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjg2ODMsInVzZXJJZCI6NTY4NiwiY29tcGFueUlkIjo4NjgzMDEsImFkdm9jYXRlSWQiOiI1NDE1MzciLCJsb2dpbkV2ZW50SWQiOjEwNjkwNjAsInNjb3BlIjoiQUxWIENBRCBDSEMgQ0lWIENVViBETzIgRE9FIEtSViBNR0QgTUlWIFBNViBSV0UgUldWIFNEViBTTlYgU08yIFNPRSBTUkUgU1JWIFNTViBTVEUgU1RWIFNWRSBVTUUgVU1WIFVSRSBWTVYgQlVFIEJVViBCMDIgQjA0IExUUiBMVFYgU09FIFNPViBSMDEgUjAyIFIwMyBSMDQgUjA1IFIwNiBSMDcgUjA4IFIwOSBSMTAgUjExIFIxMiBHQVYgQjAxIEIwMSBCMDIgQjAzIEIwNCBCMDYgQjA3IEJVRSBCVVYgUjAxIFIwMiBSMDMgUjA0IFIwNSBSMDYgUjA3IFIwOCBSMDkgUjEwIFIxMSBSMTIgTFRSIExUViBHQVYgRDAxIEQwMSBEMDIgRDAzIEQwNCBEMDUgRDA2IEQwNyBEMDggRDA5IEQxMCBEMTEgRDEyIEZSViBMUFYgTFBFIERPViBWU08gRE9WIEJCQSBTTzIgU09SIFNPRiBDU1YgU1YyIFNWUiBSMTYgQjE1IEIxNSBNR1IgQjA5IFNWRCIsIndvcmtHcm91cElkIjoyMTcsImV4cCI6MTU3OTg4NTE5OX0.oGve2IN6NFDoDeuutpvc6HK1nIUE3sTrT4kPQQ5gBcNrlrZXNCZJaOpYRTDmWQ6O9WxUf4C-4NSu6P-P5qTFtA";
		//prod federal advo.support
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZHZvLnN1cHBvcnQiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjgzMzMsInVzZXJJZCI6NDU1NSwiY29tcGFueUlkIjo4MzMzNDMsImFkdm9jYXRlSWQiOiIzNzA0NjEiLCJsb2dpbkV2ZW50SWQiOjEwNjkxMjIsInNjb3BlIjoiQUxWIEFURSBBVFYgQlVFIEJVViBDQUQgQ0hDIENJViBDTVAgQ1NWIENVRSBDVVYgRDAxIEQwMiBEMDMgRDA0IEQwNSBEMDYgRE9FIERPViBEU1YgRlJFIEZSViBHQVYgS1JWIExQViBNR0QgTUdSIE1JViBOU1IgUENWIERPMiBSMDEgUjAyIFIwMyBSMDQgUjA1IFIwNiBSMDcgUldWIFNJRSBTSVYgU05WIFNPMiBTTzIgU09WIFNSRSBTUlYgU1NWIFNURSBTVFYgU1ZFIFNWMiBVTUUgVU1WIFVSRSBWTVYgQjA0IFBNViBQTVYgTFRWIEIwMSBCQ00gQkNNIENVViBDVVYgQjAyIEdBViBCVVYgQlVFIEIwNCBEMDIgUjAxIFIwMyBEMDEgQ0FEIE1JViBCMDQgUjEyIEJCQSBCMDMgQjA2IFIwOCBGUkUgRE9WIE1JViBBTUUgQU1TIEFNViBTVjIgR0FWIFNPMiBTVjIgU1YyIEZSViBTT1IgU09GIFIwNyBSMTMgU09FIEJCQSBCMDcgU1ZSIFIxMCBCMTMgQjA5IFBNViBMVFYgQkJBIEIxMiBSMTYgR0FWIEIxNSBXUE8gQjE2IEIwNSBET0UgUEpWIFZBTSBTVkQgUEpFIFJNViBEMTQgU1ZWIEIxNCBCMTcgVlNPIEFQViAxUFQgUjIxIEFSUCBHUlAgVk1EIFZSTiBWUlAgQVJQIEdSUCBSTVYgVk1EIFZSTiBWUlAgRDE0IElDViBCQkEgRDAxIExQViBMUEUgQ1BFIENQViBQUkMgVlBKIEQxNiBEMTcgU1BWIFIwMyBEMTQgUjIyIEIyMSBFTFYgVE1WIiwid29ya0dyb3VwSWQiOjU3LCJleHAiOjE1Nzk4ODUxOTl9.sa5rY2etaA_Idf_5IF_q6KNhOYCiCVcf4qKijZX7JopdHI9_zdlRTurSlflh30XRRjXgwLmaiNFsjdLs9xyLYw";
		
		
		Map<String, String> params = new HashMap<>();
		//20191201 - 20191230 - Customer Overview Workgroup
		params.put("action", "widgetCustomerOverviewDetail");
		params.put("startTime", "MDEtMTItMjAxOQ==");
		params.put("endTime", "MzAtMTItMjAxOQ==");
		params.put("viewBy", "V29ya2dyb3Vw");
		params.put("signature", "543c2345f14163257b6a041352bd79e8");
		
//		"startTime": "MDEtMTItMjAxOQ==",
//	    "endTime": "MzAtMTItMjAxOQ==",
//	    "viewBy": "V29ya2dyb3Vw",
//	    "signature": "543c2345f14163257b6a041352bd79e8"
	    	
		String path = getPathUrlEnvironmentWidgetData(environment);;
		
		SerenityRest.given().auth().oauth2(accessToken)
		.contentType("application/json")
		.and().params(params)
		.when().get(path);		
		
	}

	@Step
	public void hitBECustomerOverviewByArea(String idClient, String startTime, String endTime, String environment) {
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjI5NzYyNCwic2NvcGUiOiJVUkUgQ0hDIERTViBLUlYgTlNSIFNOViBTUkUgU1JWIFNTViBTVEUgU1RWIFVNRSBVTVYgVk1WIFNPRSBTT1YgQUNMIEFDViBBTFYgQ0VBIENMQSBDTVYgQ1NWIENVQSBDVUUgQ1VWIENWTCBET0UgRE9WIEZSQSBITVYgTDFBIEwyQSBMM0EgTEZBIExNQSBMT0EgTE9CIE1HRCBPVVQgUENFIFBDViBQRUEgUExBIFBMRSBQTE8gUE1WIFBSRSBQUlYgUFNWIFNERSBTRFYgU0lFIFNJViBTVkUgU1ZWIENJViBGUkUgRlJWIExQRSBMUFYgQ0FEIE1JViBEMDEgRDAyIEQwMyBEMDQgRDA1IEQwNiBNR0QgTUdSIFIwMSBSMDIgUjAzIFIwNyBMVFYgR0FWIEJVRSBCMDIgQU1FIEFNViBCQkEgQjA3IFNWMiBXUE8gUjA2IEFNUyBCMDQgQjAxIFNPMiBCVVYgU1ZSIFIxNiAxSUEgQVBWIElOViBJU0kgSVNMIFBDViBQQ0UgQ1BFIENQViBQUkMgV1JWIFNPRiAxUEwgU1BWIFIxMSBSTVYgVE1WIFNWRCBEMDcgSUNWIERPMiBJU0EgQjEzIEIxMiBCMTYgQjE1IEIwMyBCMjAgQjE3IEIyMiBCMDUgTFRSIExPUyBTUEEgU1BFIFJUViBTT1IgUjE1IFZTTyBETE8iLCJ3b3JrR3JvdXBJZCI6MSwiZXhwIjoxNTc1OTEwNzk5fQ.oPQONDABeXpbECWhsojA9knUdVso0YgPmqzEG_aC26wTConpANs5Uo1WEEOjBLa_CvnQ_c3V4kOSWdPHE-wAMg";
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjMwNzc3MCwic2NvcGUiOiJVUkUgQ0hDIERTViBLUlYgTlNSIFNOViBTUkUgU1JWIFNTViBTVEUgU1RWIFVNRSBVTVYgVk1WIFNPRSBTT1YgQUNMIEFDViBBTFYgQ0VBIENMQSBDTVYgQ1NWIENVQSBDVUUgQ1VWIENWTCBET0UgRE9WIEZSQSBITVYgTDFBIEwyQSBMM0EgTEZBIExNQSBMT0EgTE9CIE1HRCBPVVQgUENFIFBDViBQRUEgUExBIFBMRSBQTE8gUE1WIFBSRSBQUlYgUFNWIFNERSBTRFYgU0lFIFNJViBTVkUgU1ZWIENJViBGUkUgRlJWIExQRSBMUFYgQ0FEIE1JViBEMDEgRDAyIEQwMyBEMDQgRDA1IEQwNiBNR0QgTUdSIFIwMSBSMDIgUjAzIFIwNyBMVFYgR0FWIEJVRSBCMDIgQU1FIEFNViBCQkEgQjA3IFNWMiBXUE8gUjA2IEFNUyBCMDQgQjAxIFNPMiBCVVYgU1ZSIFIxNiBSTVYgVE1WIFNQViBSMjAgRDE0IFNWRCBSMjIgSUNWIEJDTSBBUFYiLCJ3b3JrR3JvdXBJZCI6MSwiZXhwIjoxNTc1OTEwNzk5fQ.iQS453ZY6ZIwsQ4dAbb5Wg0Axtpictz2vNT48olts_R36zXVAVw7361nOBbd_ijkDj9CfNVN2il_oK3_QwkA-Q";
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWl0dHkiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEsInVzZXJJZCI6ODgzLCJjb21wYW55SWQiOjExLCJhZHZvY2F0ZUlkIjoiOTk5NzgiLCJsb2dpbkV2ZW50SWQiOjEwNjUzNzAsInNjb3BlIjoiVVJFIENIQyBEU1YgS1JWIE5TUiBTTlYgU1JFIFNSViBTU1YgU1RFIFNUViBVTUUgVU1WIFZNViBTT0UgU09WIEFDTCBBQ1YgQUxWIENFQSBDTEEgQ01WIENTViBDVUEgQ1VFIENVViBDVkwgRE9FIERPViBGUkEgSE1WIEwxQSBMMkEgTDNBIExGQSBMTUEgTE9BIExPQiBNR0QgT1VUIFBDRSBQQ1YgUEVBIFBMQSBQTEUgUExPIFBNViBQUkUgUFJWIFBTViBTREUgU0RWIFNJRSBTSVYgU1ZFIFNWMiBDSVYgRlJFIEZSViBMUEUgTFBWIENBRCBNSVYgRDAxIEQwMiBEMDMgRDA0IEQwNSBEMDYgTUdEIE1HUiBSMDEgUjAyIFIwMyBSMDcgTFRWIFNWMiBTTzIgU09SIFNPRiBTVlIgUjE2IERPMiBBTVMgQU1FIEFNViBCMDQgQjEzIEIxMiBCVUUgQjE1IEIwMSBCMTEgQjAyIEIwOSBCVVYgQjA3IEJCQSBEMDcgR0FWIFIwNiBTUEEgU1BFIDFQTCBTUFYgSVNJIFZTTyBXUE8gUjIwIFNWRCBBUFYgUENFIFBDViBJU0EgQjIyIEIxOCBCQkEgSUNWIElTTCBDUEUgQ1BWIFBSQyBJTlYgUEpFIFBKViBSMTEgUlRWIFZQSiBCMTYgQkNNIiwid29ya0dyb3VwSWQiOjEsImV4cCI6MTU3OTc5ODc5OX0._4zELtx0LFkFZIk0N0jyhR_uQkLsevcH8efEepwzkQpbBmIYtsupYHWiXEHm55X4xm4PaQgwJDclGop52IWGjg";
//		prod RAS advo support
		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZHZvLnN1cHBvcnQiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjEwMDAzNiwidXNlcklkIjo0NTU1LCJjb21wYW55SWQiOjEwMDAzNjEsImFkdm9jYXRlSWQiOiIzNzA0NjEiLCJsb2dpbkV2ZW50SWQiOjEwNjkxMjIsInNjb3BlIjoiQUxWIEFURSBBVFYgQlVFIEJVViBDQUQgQ0hDIENJViBDTVAgQ1NWIENVRSBDVVYgRDAxIEQwMiBEMDMgRDA0IEQwNSBEMDYgRE9FIERPViBEU1YgRlJFIEZSViBHQVYgS1JWIExQViBNR0QgTUdSIE1JViBOU1IgUENWIERPMiBSMDEgUjAyIFIwMyBSMDQgUjA1IFIwNiBSMDcgUldWIFNJRSBTSVYgU05WIFNPMiBTTzIgU09WIFNSRSBTUlYgU1NWIFNURSBTVFYgU1ZFIFNWMiBVTUUgVU1WIFVSRSBWTVYgQjA0IFBNViBQTVYgTFRWIEIwMSBCQ00gQkNNIENVViBDVVYgQjAyIEdBViBCVVYgQlVFIEIwNCBEMDIgUjAxIFIwMyBEMDEgQ0FEIE1JViBCMDQgUjEyIEJCQSBCMDMgQjA2IFIwOCBGUkUgRE9WIE1JViBBTUUgQU1TIEFNViBTVjIgR0FWIFNPMiBTVjIgU1YyIEZSViBTT1IgU09GIFIwNyBSMTMgU09FIEJCQSBCMDcgU1ZSIFIxMCBCMTMgQjA5IFBNViBMVFYgQkJBIEIxMiBSMTYgR0FWIEIxNSBXUE8gQjE2IEIwNSBET0UgUEpWIFZBTSBTVkQgUEpFIFJNViBEMTQgU1ZWIEIxNCBCMTcgVlNPIEFQViAxUFQgUjIxIEFSUCBHUlAgVk1EIFZSTiBWUlAgQVJQIEdSUCBSTVYgVk1EIFZSTiBWUlAgRDE0IElDViBCQkEgRDAxIExQViBMUEUgQ1BFIENQViBQUkMgVlBKIEQxNiBEMTcgU1BWIFIwMyBEMTQgUjIyIEIyMSBFTFYgVE1WIiwid29ya0dyb3VwSWQiOjQzOCwiZXhwIjoxNTc5ODg1MTk5fQ.pSdyVk6R05UWywctOmQWAnQT_jzWLmpPyZNZjwRuU-bt7_4skt75IERpaX3d5VDXJ-hD4b40JqHMxAuDQL5u7Q";
		//prod nutrifood nf.superuser
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJuZi5zdXBlcnVzZXIiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjg2ODMsInVzZXJJZCI6NTY4NiwiY29tcGFueUlkIjo4NjgzMDEsImFkdm9jYXRlSWQiOiI1NDE1MzciLCJsb2dpbkV2ZW50SWQiOjEwNjkwNjAsInNjb3BlIjoiQUxWIENBRCBDSEMgQ0lWIENVViBETzIgRE9FIEtSViBNR0QgTUlWIFBNViBSV0UgUldWIFNEViBTTlYgU08yIFNPRSBTUkUgU1JWIFNTViBTVEUgU1RWIFNWRSBVTUUgVU1WIFVSRSBWTVYgQlVFIEJVViBCMDIgQjA0IExUUiBMVFYgU09FIFNPViBSMDEgUjAyIFIwMyBSMDQgUjA1IFIwNiBSMDcgUjA4IFIwOSBSMTAgUjExIFIxMiBHQVYgQjAxIEIwMSBCMDIgQjAzIEIwNCBCMDYgQjA3IEJVRSBCVVYgUjAxIFIwMiBSMDMgUjA0IFIwNSBSMDYgUjA3IFIwOCBSMDkgUjEwIFIxMSBSMTIgTFRSIExUViBHQVYgRDAxIEQwMSBEMDIgRDAzIEQwNCBEMDUgRDA2IEQwNyBEMDggRDA5IEQxMCBEMTEgRDEyIEZSViBMUFYgTFBFIERPViBWU08gRE9WIEJCQSBTTzIgU09SIFNPRiBDU1YgU1YyIFNWUiBSMTYgQjE1IEIxNSBNR1IgQjA5IFNWRCIsIndvcmtHcm91cElkIjoyMTcsImV4cCI6MTU3OTg4NTE5OX0.oGve2IN6NFDoDeuutpvc6HK1nIUE3sTrT4kPQQ5gBcNrlrZXNCZJaOpYRTDmWQ6O9WxUf4C-4NSu6P-P5qTFtA";
		//prod federal advo.support
//		String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZHZvLnN1cHBvcnQiLCJhcHBLZXkiOiI5ODdmNGEyNzZjZWQ0NTQ2MThmZGViM2NhY2U2OGQ0YmZjNWI4YWUyIiwiY2xpZW50SWQiOjgzMzMsInVzZXJJZCI6NDU1NSwiY29tcGFueUlkIjo4MzMzNDMsImFkdm9jYXRlSWQiOiIzNzA0NjEiLCJsb2dpbkV2ZW50SWQiOjEwNjkxMjIsInNjb3BlIjoiQUxWIEFURSBBVFYgQlVFIEJVViBDQUQgQ0hDIENJViBDTVAgQ1NWIENVRSBDVVYgRDAxIEQwMiBEMDMgRDA0IEQwNSBEMDYgRE9FIERPViBEU1YgRlJFIEZSViBHQVYgS1JWIExQViBNR0QgTUdSIE1JViBOU1IgUENWIERPMiBSMDEgUjAyIFIwMyBSMDQgUjA1IFIwNiBSMDcgUldWIFNJRSBTSVYgU05WIFNPMiBTTzIgU09WIFNSRSBTUlYgU1NWIFNURSBTVFYgU1ZFIFNWMiBVTUUgVU1WIFVSRSBWTVYgQjA0IFBNViBQTVYgTFRWIEIwMSBCQ00gQkNNIENVViBDVVYgQjAyIEdBViBCVVYgQlVFIEIwNCBEMDIgUjAxIFIwMyBEMDEgQ0FEIE1JViBCMDQgUjEyIEJCQSBCMDMgQjA2IFIwOCBGUkUgRE9WIE1JViBBTUUgQU1TIEFNViBTVjIgR0FWIFNPMiBTVjIgU1YyIEZSViBTT1IgU09GIFIwNyBSMTMgU09FIEJCQSBCMDcgU1ZSIFIxMCBCMTMgQjA5IFBNViBMVFYgQkJBIEIxMiBSMTYgR0FWIEIxNSBXUE8gQjE2IEIwNSBET0UgUEpWIFZBTSBTVkQgUEpFIFJNViBEMTQgU1ZWIEIxNCBCMTcgVlNPIEFQViAxUFQgUjIxIEFSUCBHUlAgVk1EIFZSTiBWUlAgQVJQIEdSUCBSTVYgVk1EIFZSTiBWUlAgRDE0IElDViBCQkEgRDAxIExQViBMUEUgQ1BFIENQViBQUkMgVlBKIEQxNiBEMTcgU1BWIFIwMyBEMTQgUjIyIEIyMSBFTFYgVE1WIiwid29ya0dyb3VwSWQiOjU3LCJleHAiOjE1Nzk4ODUxOTl9.sa5rY2etaA_Idf_5IF_q6KNhOYCiCVcf4qKijZX7JopdHI9_zdlRTurSlflh30XRRjXgwLmaiNFsjdLs9xyLYw";
		
		
		Map<String, String> params = new HashMap<>();
		//20191201 - 20191230 - Customer Overview Area
		params.put("action", "widgetCustomerOverviewDetail");
		params.put("startTime", "MDEtMTItMjAxOQ==");
		params.put("endTime", "MzAtMTItMjAxOQ==");
		params.put("viewBy", "QXJlYQ==");
		params.put("signature", "00fcb889972f2786add5be703a31925c");
		
//		"startTime": "MDEtMTItMjAxOQ==",
//	    "endTime": "MzAtMTItMjAxOQ==",
//	    "viewBy": "QXJlYQ==",
//	    "signature": "00fcb889972f2786add5be703a31925c"

		String path = getPathUrlEnvironmentWidgetData(environment);;
		
		SerenityRest.given().auth().oauth2(accessToken)
		.contentType("application/json")
		.and().params(params)
		.when().get(path);	
	}

	public Map<String, String> getListCustomerOverviewArea() {
		List<HashMap<String, String>> customerOverviewAreas = SalesOrdersResponse.balikanCustomerOverviewArea();
		
		Map<String, String> value = new TreeMap<String, String>();
		for (HashMap<String, String> customerOverview: customerOverviewAreas) {
			value.put(customerOverview.getOrDefault("label", "0"), (String.valueOf(customerOverview.getOrDefault("registered", "0")) + "|" + String.valueOf(customerOverview.getOrDefault("visited", "0")) + "|" + String.valueOf(customerOverview.getOrDefault("productive", "0"))));
		}
		
		System.out.println("RESULT: " + value);
		return value;
	}

	public Map<String, String> getListCustomerOverviewAreaService(String idClient, String username, String startTime,
			String endTime) throws JsonParseException, JsonMappingException, IOException {
		System.out.println("ID CLIENT : " + idClient);
		System.out.println("START TIME : " + startTime);
		System.out.println("END TIME : " + endTime);
		
		Map<String, String> params = new HashMap<>();
		params.put("idClient", idClient);
		params.put("username", username);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		
		Response response  = SerenityRest.given()
				.and().params(params)
				.contentType("application/json")
				.when().get(WebServiceEndPoints.CUSTOMEROVERVIEWAREA.getUrl());
		
		System.out.println("MARIA TEST RESPONSEEEEE BY CUSTOMER OVERVIEW AREA : " + response.asString());
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> customerOverview = mapper.readValue(response.asString(), Map.class);
		
		System.out.println("customer overview workgroup MARIA : " + customerOverview);
		
		Map<String, String> allCustomerOverview = new TreeMap<String, String>();

		//kyaknya ini nnti bisa dihapus, gak guna krna udah Map String, String
		for (Map.Entry<String, String> entry : customerOverview.entrySet()) {
			System.out.println("Entry dot key : " + entry.getKey());
			System.out.println("Entry dot value : " + entry.getValue());
			String customerOverviewWorkgroupLabel = entry.getKey();
			System.out.println("customerOverviewType : " + customerOverviewWorkgroupLabel);

			if (!allCustomerOverview.containsKey(customerOverviewWorkgroupLabel)) {
				String mValueString = customerOverview.get(customerOverviewWorkgroupLabel);
				System.out.println("KEY NOW : " + customerOverviewWorkgroupLabel);
				System.out.println("VALUE NOW : " + mValueString);
				allCustomerOverview.put(customerOverviewWorkgroupLabel, mValueString);
			} else {
				allCustomerOverview.put(customerOverviewWorkgroupLabel, "0");
			}
			
				System.out.println("All customer overview allCustomerOverview : " + allCustomerOverview);
		}		
		return allCustomerOverview;
	}

	@Step
	public Map<String, String> getListCustomerOverviewDetail() {
		List<HashMap<String, String>> customerOverviews = SalesOrdersResponse.balikanCustomerOverviewDetail();
		
		Map<String, String> value = new TreeMap<String, String>();
		for (HashMap<String, String> customerOverview: customerOverviews) {
			value.put(customerOverview.getOrDefault("label", "0"), String.valueOf(customerOverview.getOrDefault("registered", "0")) + "|" + String.valueOf(customerOverview.getOrDefault("visited", "0")) + "|" + String.valueOf(customerOverview.getOrDefault("productive", "0")));
		}
		
		System.out.println("RESULT getListCustomerOverviewDetail : " + value);
		return value;
	}

	public Map<String, String> getListCustomerOverviewWorkgroupService(String idClient, String username,
			String startTime, String endTime) throws JsonParseException, JsonMappingException, IOException {
		System.out.println("ID CLIENT : " + idClient);
		System.out.println("START TIME : " + startTime);
		System.out.println("END TIME : " + endTime);
		
		Map<String, String> params = new HashMap<>();
		params.put("idClient", idClient);
		params.put("username", username);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		
		Response response  = SerenityRest.given()
				.and().params(params)
				.contentType("application/json")
				.when().get(WebServiceEndPoints.CUSTOMEROVERVIEWWORKGROUP.getUrl());
		
		System.out.println("MARIA TEST RESPONSEEEEE BY CUSTOMER OVERVIEW WORKGROUP : " + response.asString());
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> customerOverview = mapper.readValue(response.asString(), Map.class);
		
		System.out.println("customer overview workgroup MARIA : " + customerOverview);
		
		Map<String, String> allCustomerOverview = new TreeMap<String, String>();

		//kyaknya ini nnti bisa dihapus, gak guna krna udah Map String, String
		for (Map.Entry<String, String> entry : customerOverview.entrySet()) {
			System.out.println("Entry dot key : " + entry.getKey());
			System.out.println("Entry dot value : " + entry.getValue());
			String customerOverviewWorkgroupLabel = entry.getKey();
			System.out.println("customerOverviewType : " + customerOverviewWorkgroupLabel);

			if (!allCustomerOverview.containsKey(customerOverviewWorkgroupLabel)) {
				String mValueString = customerOverview.get(customerOverviewWorkgroupLabel);
				System.out.println("KEY NOW : " + customerOverviewWorkgroupLabel);
				System.out.println("VALUE NOW : " + mValueString);
				allCustomerOverview.put(customerOverviewWorkgroupLabel, mValueString);
			} else {
				allCustomerOverview.put(customerOverviewWorkgroupLabel, "0");
			}
			
				System.out.println("All customer overview allCustomerOverview : " + allCustomerOverview);
		}		
		return allCustomerOverview;
	}

	@Step
	public Map<String, String> getAllignPercentageCompareMapBEAndServiceResultCustomerOverviewDetail(
			Map<String, String> listCustomerOverviewBE, Map<String, String> listCustomerOverviewServiceResult) {
		Comparator<String> compString = Comparator.comparing(String::toString);

		Map<String, String> allignPerDetail = new TreeMap<>(compString);
		
		for (String key : listCustomerOverviewServiceResult.keySet()) {
			String value2 = listCustomerOverviewServiceResult.get(key);
			String value2Registered, value2Visited, value2Productive;
			
			value2Registered = StringUtils.getSplit(value2, 0);
			value2Visited = StringUtils.getSplit(value2, 1);
			value2Productive = StringUtils.getSplit(value2, 2);
			
			String value1, value1Registered, value1Visited, value1Productive;
			if(listCustomerOverviewBE.containsKey(key)) {
				value1 = listCustomerOverviewBE.get(key);
			}
			else {
				value1 = "0.0|0.0|0.0";
			}
			value1Registered = StringUtils.getSplit(value1, 0);
			value1Visited = StringUtils.getSplit(value1, 1);
			value1Productive = StringUtils.getSplit(value1, 2);
			
			//parse to Double
			Double value1RegisteredBE, value1VisitedBE, value1ProductiveBE = 0.0D;
			value1RegisteredBE = Double.parseDouble(value1Registered);
			value1VisitedBE = Double.parseDouble(value1Visited);
			value1ProductiveBE = Double.parseDouble(value1Productive);
			
			Double value2RegisteredService, value2VisitedService, value2ProductiveService = 0.0D;
			value2RegisteredService = Double.parseDouble(value2Registered);
			value2VisitedService = Double.parseDouble(value2Visited);
			value2ProductiveService = Double.parseDouble(value2Productive);
			
//			Double value1BE = 0.0D;
//			value1BE = Double.parseDouble(value1);
//			
//			Double value2Service = 0.0D;
//			value2Service = Double.parseDouble(value2);
			
//			boolean align = false;
			Double alignRegisteredStore, alignVisitedStore, alignProductiveStore = 0D;
			alignRegisteredStore = getAllignBEService(value2RegisteredService, value1RegisteredBE);
			alignVisitedStore = getAllignBEService(value2VisitedService, value1VisitedBE);
			alignProductiveStore = getAllignBEService(value2ProductiveService, value1ProductiveBE);
			
			allignPerDetail.put(key, alignRegisteredStore + "% | " + alignVisitedStore + "% | " + alignProductiveStore + "%");
			
//			if(allignPerDetail.containsKey(key)) {
//				Double currAllign = alignRegisteredStore;
//				allignPerDetail.put(key, currAllign + "%");
//			}else {
//				allignPerDetail.put(key, alignRegisteredStore + "%");
//			}
		}
		
		//if other keys exist in BE but not exist in Service Result
		for (String key : listCustomerOverviewBE.keySet()) {
			if(!allignPerDetail.containsKey(key)) {
				allignPerDetail.put(key, "0.0% | 0.0% | 0.0%");
			}
		}
		System.out.println("PERCENTAGE ALLIGN ALL DETAIL : " + allignPerDetail.toString());
		return allignPerDetail;
	}

	public Map<String, String> getListCustomerOverviewDetailArea() {
		List<HashMap<String, String>> customerOverviews = SalesOrdersResponse.balikanCustomerOverviewDetail();
		
		Map<String, String> value = new TreeMap<String, String>();
		for (HashMap<String, String> customerOverview: customerOverviews) {
			if(customerOverview.getOrDefault("label", "0").equals("Jakarta")){
				value.put("DKI JAKARTA", String.valueOf(customerOverview.getOrDefault("registered", "0")) + "|" + String.valueOf(customerOverview.getOrDefault("visited", "0")) + "|" + String.valueOf(customerOverview.getOrDefault("productive", "0")));
			}
			else if(customerOverview.getOrDefault("label", "0").equals("Yogyakarta")) {
				value.put("DI YOGYAKARTA", String.valueOf(customerOverview.getOrDefault("registered", "0")) + "|" + String.valueOf(customerOverview.getOrDefault("visited", "0")) + "|" + String.valueOf(customerOverview.getOrDefault("productive", "0")));
			}
			else {
				value.put(customerOverview.getOrDefault("label", "0").toUpperCase(), String.valueOf(customerOverview.getOrDefault("registered", "0")) + "|" + String.valueOf(customerOverview.getOrDefault("visited", "0")) + "|" + String.valueOf(customerOverview.getOrDefault("productive", "0")));
			}
		}
		
		System.out.println("RESULT getListCustomerOverviewDetailArea : " + value);
		return value;
	}
}
