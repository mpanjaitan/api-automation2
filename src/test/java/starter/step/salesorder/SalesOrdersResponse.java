package starter.step.salesorder;

import static java.util.stream.Collectors.toMap;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import net.serenitybdd.rest.SerenityRest;
import starter.salesorder.SalesOvertimeBE;

public class SalesOrdersResponse {
	public static Map<String, String> returned() {
	       return mapOfStringsFrom(SerenityRest.lastResponse().getBody().as(Map.class));
	    }

	private static Map<String,String> mapOfStringsFrom(Map<String, Object> map) {
        return map.entrySet()
                .stream()
                .collect(toMap(Map.Entry::getKey,
                        entry -> entry.getValue().toString()));
    }
	
	/*public List<SalesOvertimeBE> balikan(){
		List <SalesOvertimeBE> salesOvertimeBEs = SerenityRest.lastResponse().getBody().jsonPath().getList("chart.series");
		System.out.println("DARI SALES ORDERS RESPONSE CLASS : " + salesOvertimeBEs);
		return Arrays.asList(SerenityRest.lastResponse().getBody().as(SalesOvertimeBE.class));
	}*/
	
	public static List<HashMap<String, String>> balikan(){
		System.out.println("GET SALE ORDERS");
        List<List<HashMap<String, String>>> salesOvertimeBEs = SerenityRest.lastResponse().getBody().jsonPath().get("chart.series.");
        
        System.out.println("SALES OVERTIME BE PAGI : " + salesOvertimeBEs);
        for(List<HashMap<String, String>> sale : salesOvertimeBEs) {
        	System.out.println("SALE : " + sale.getClass());
        	return sale;
        }

        return new ArrayList<HashMap<String, String>>();
    }
	
	//NEWW
	//maria
	public static List<SalesOvertimeBE> balikanStringDouble(){
		System.out.println("GET SALE ORDERS");
        List<List<HashMap<String, String>>> salesOvertimeBEs = SerenityRest.lastResponse().getBody().jsonPath().get("chart.series.");
        
        List<List<SalesOvertimeBE>> salesOvertimeBEsYA = SerenityRest.lastResponse().getBody().jsonPath().get("chart.series.");
        
        System.out.println("SALES OVERTIME BE PAGI : " + salesOvertimeBEs);
        for(List<SalesOvertimeBE> sale : salesOvertimeBEsYA) {
        	System.out.println("SALE : " + sale.getClass());
        	return sale;
        }
		return null;

//        return sale;
    }
	
	public static List<HashMap<String, String>> responseBEProductInsight(){
		System.out.println("GET PRODUCT INSIGHT");
        List<HashMap<String, String>> productInsightBEs = SerenityRest.lastResponse().getBody().jsonPath().get("series.");
        
        System.out.println("PRODUCT INSIGHT BE : " + productInsightBEs);
//        for(HashMap<String, String> sale : productInsightBEs) {
//        	System.out.println("SALE : " + sale.getClass());
//        	return sale;
//        }

        return productInsightBEs;
//        return new ArrayList<HashMap<String, String>>();
    }
	
	public static List<HashMap<String, String>> balikanCustomerOverview(){
		System.out.println("GET CUSTOMER OVERVIEW");
        List<List<HashMap<String, String>>> customerOverviewBEs = SerenityRest.lastResponse().getBody().jsonPath().get("series.data.");
        
        System.out.println("CUSTOMER OVERVIEW BE : " + customerOverviewBEs);
        for(List<HashMap<String, String>> customerOverview : customerOverviewBEs) {
        	System.out.println("CUSTOVERVIEW : " + customerOverview.getClass());
        	return customerOverview;
        }

        return new ArrayList<HashMap<String, String>>();
    }

	public static List<HashMap<String, String>> balikanCustomerOverviewArea() {
		System.out.println("GET CUSTOMER OVERVIEW AREA");
        List<HashMap<String, String>> customerOverviewAreaBEs = SerenityRest.lastResponse().getBody().jsonPath().get("data.");
        
        System.out.println("CUSTOMER OVERVIEW AREA BE : " + customerOverviewAreaBEs);
//        for(HashMap<String, String> sale : productInsightBEs) {
//        	System.out.println("SALE : " + sale.getClass());
//        	return sale;
//        }

        return customerOverviewAreaBEs;
	}

	public static List<HashMap<String, String>> balikanCustomerOverviewDetail() {
		System.out.println("GET CUSTOMER OVERVIEW DETAIL");
        List<HashMap<String, String>> customerOverviewDetailBEs = SerenityRest.lastResponse().getBody().jsonPath().get("data.");
        
        System.out.println("CUSTOMER OVERVIEW DETAIL BE : " + customerOverviewDetailBEs);
//        for(HashMap<String, String> sale : productInsightBEs) {
//        	System.out.println("SALE : " + sale.getClass());
//        	return sale;
//        }

        return customerOverviewDetailBEs;
	}
}


/*package starter.step.salesorder;

import static java.util.stream.Collectors.toMap;

import java.util.List;
import java.util.Map;

import net.serenitybdd.rest.SerenityRest;
import starter.WebServiceEndPoints;
import starter.salesorder.SalesOvertimeBE;

public class SalesOrdersResponse {
	public Map<String, String> returned() {
	       return mapOfStringsFrom(SerenityRest.lastResponse().getBody().as(Map.class));
	    }

	private Map<String,String> mapOfStringsFrom(Map<String, Object> map) {
        return map.entrySet()
                .stream()
                .collect(toMap(Map.Entry::getKey,
                        entry -> entry.getValue().toString()));
    }
	
	public List<SalesOvertimeBE> balikan(){
		List <SalesOvertimeBE> salesOvertimeBEs = SerenityRest.lastResponse().getBody().jsonPath().getList(".", SalesOvertimeBE.class);
		System.out.println("DARI SALES ORDERS RESPONSE CLASS : " + salesOvertimeBEs);
		return SerenityRest.lastResponse().getBody().jsonPath().getList(".", SalesOvertimeBE.class);
	}
}
*/