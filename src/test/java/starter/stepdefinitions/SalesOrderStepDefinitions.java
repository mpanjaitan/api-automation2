package starter.stepdefinitions;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;

import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;
import starter.step.salesorder.SalesOrderActions;

public class SalesOrderStepDefinitions {

	@Steps
	private SalesOrderActions salesOrderActions;
	
	/*@Steps
	SalesOrdersResponse salesOrdersResponse;*/
//    TradeResponse theTradeDetails;
	
	@Given("I request detail SO with number '(.*)'")
    public void i_have_so_number(String soNumber) {
		salesOrderActions.saveSONumber(soNumber);
    }
    
    @Then("I should get response status code '(.*)'")
    public void i_should_get_response_status_code(int statusCode) {
    	
    	/*List<SalesOrderTransaction> salesOrders = repository.findByDefaultDate();
    	
    	System.out.println("salesOrders count: " + salesOrders.size());*/
    	restAssuredThat(response -> response.statusCode(statusCode));
    }
    
    @And("I should get SO status '(.*)'")
    public void i_should_get_SO_status(String SOStatus) {
    	restAssuredThat(response -> response.body("salesOrderStatusName", equalTo(SOStatus)));
    }
    
    @And("I should get item '(.*)'")
    public void i_should_get_item(String item) {
    	salesOrderActions.getItemValid(item);
    }
    
    @Given("I have environment '(.*)'")
    public void i_have_environment(String environment) {
    	salesOrderActions.setEnvironment(environment);
    }
    
	@Given("I find SO by default date")
    public void i_find_so_by_default_date() {
    	salesOrderActions.showSOByDefaultDate();
//    	Map<String, String> actualResponse = salesOrdersResponse.returned();
    }
	
	@And("I find SO with client '(.*)' in range time '(.*)' and '(.*)'")
	public void i_find_so_with_client_in_range_time(String idClient, String startTime, String endTime) {
		salesOrderActions.showSOInRangeTime(idClient, startTime, endTime);
	}
	
	@And("As '(.*)' in '(.*)' I view Sales Overtime by '(.*)' and compare by '(.*)' with client '(.*)' in range time '(.*)' and '(.*)'")
	public void i_view_sales_overtime_by_detail(String username, String environment, String viewBy, String compareBy, String idClient, String startTime, String endTime) throws ParseException, JsonParseException, JsonMappingException, IOException {
		salesOrderActions.hitBESalesOvertime(viewBy, compareBy, idClient, startTime, endTime, environment);
		Map<String, String> listSalesOvertimeBE = salesOrderActions.getListSalesOvertime();
		String salesOvertimeTotalBE = salesOrderActions.getListSalesOvertimeStringDouble();
//		salesOrderActions.getAxist(startTime, endTime, viewBy);
//		salesOrderActions.showSOInRangeTimeString(idClient, startTime, endTime);
		Map<String, String> listSalesOvertimeServiceResult = salesOrderActions.getListRevenueSalesOvertime(idClient, username, startTime, endTime, viewBy);
		String SalesOvertimeTotalService = salesOrderActions.getRevenueTotalSalesOvertime(idClient, username, startTime, endTime, viewBy);
		boolean resultCompare = salesOrderActions.compareBEAndServiceResult(listSalesOvertimeBE, listSalesOvertimeServiceResult);
		System.out.println("INI HASIL BOOLEAN COMPARE RESULT : " + resultCompare);
		salesOrderActions.getAllignPercentageCompareMapBEAndServiceResult(listSalesOvertimeBE, listSalesOvertimeServiceResult , viewBy);
		salesOrderActions.compareBEAndServiceResultPercentage(salesOvertimeTotalBE, SalesOvertimeTotalService, viewBy);
	}
	
	@And("As '(.*)' in '(.*)' I view Product Insight by '(.*)' and unit '(.*)' with client '(.*)' in range time '(.*)' and '(.*)'")
	public void i_view_product_insight_by_detail(String username, String environment, String viewBy, String unit, String idClient, String startTime, String endTime) throws ParseException, JsonParseException, JsonMappingException, IOException {
		salesOrderActions.hitBEProductInsight(viewBy, unit, idClient, startTime, endTime, environment);
		Map<String, String> listProductInsightBE = salesOrderActions.getListProductInsight();
		String productInsightTotalBE = salesOrderActions.getListProductInsightStringDouble();
//		salesOrderActions.getAxist(startTime, endTime, viewBy);
//		salesOrderActions.showSOInRangeTimeString(idClient, startTime, endTime);
		Map<String, String> listProductInsightServiceResult = salesOrderActions.getListRevenueProductInsight(idClient, username, startTime, endTime, viewBy);
		String productInsightTotalService = salesOrderActions.getRevenueTotalProductInsight(idClient, username, startTime, endTime, viewBy);
		boolean resultCompare = salesOrderActions.compareBEAndServiceResult(listProductInsightBE, listProductInsightServiceResult);
		System.out.println("INI HASIL BOOLEAN COMPARE RESULT : " + resultCompare);
		salesOrderActions.getAllignPercentageCompareMapBEAndServiceResult(listProductInsightBE, listProductInsightServiceResult , viewBy);
		salesOrderActions.compareBEAndServiceResultPercentage(productInsightTotalBE, productInsightTotalService, viewBy);
	}
	
	@And("As '(.*)' in '(.*)' I view Product Insight Quantity Fulfilled by '(.*)' and unit '(.*)' with client '(.*)' in range time '(.*)' and '(.*)'")
	public void i_view_product_insight_qty_fulfilled_by_detail(String username, String environment, String viewBy, String unit, String idClient, String startTime, String endTime) throws ParseException, JsonParseException, JsonMappingException, IOException {
		salesOrderActions.hitBEProductInsightQtyFulfilled(viewBy, unit, idClient, startTime, endTime, environment);
		Map<String, String> listProductInsightBE = salesOrderActions.getListProductInsight();
		String productInsightTotalBE = salesOrderActions.getListProductInsightQtyFulfilledString();
//		salesOrderActions.getAxist(startTime, endTime, viewBy);
//		salesOrderActions.showSOInRangeTimeString(idClient, startTime, endTime);
		Map<String, String> listProductInsightServiceResult = salesOrderActions.getListProductInsightQtyFulfilled(idClient, username, startTime, endTime, viewBy);
		String productInsightTotalService = salesOrderActions.getTotalProductInsightQtyFulfilled(idClient, username, startTime, endTime, viewBy);
		boolean resultCompare = salesOrderActions.compareBEAndServiceResult(listProductInsightBE, listProductInsightServiceResult);
		System.out.println("INI HASIL BOOLEAN COMPARE RESULT : " + resultCompare);
		salesOrderActions.getAllignPercentageCompareMapBEAndServiceResultInt(listProductInsightBE, listProductInsightServiceResult , viewBy);
		salesOrderActions.compareBEAndServiceResultPercentage(productInsightTotalBE, productInsightTotalService, viewBy);
	}
	
	@And("As '(.*)' in '(.*)' I view Product Insight Scarcity Level by '(.*)' and unit '(.*)' with client '(.*)' in range time '(.*)' and '(.*)'")
	public void i_view_product_insight_scarcity_level_by_detail(String username, String environment, String viewBy, String unit, String idClient, String startTime, String endTime) throws ParseException, JsonParseException, JsonMappingException, IOException {
		salesOrderActions.hitBEProductInsightScarcityLevel(viewBy, unit, idClient, startTime, endTime, environment);
		Map<String, String> listProductInsightBE = salesOrderActions.getListProductInsight();
		String productInsightTotalBE = salesOrderActions.getListProductInsightStringDouble();
//		salesOrderActions.getAxist(startTime, endTime, viewBy);
//		salesOrderActions.showSOInRangeTimeString(idClient, startTime, endTime);
		Map<String, String> listProductInsightServiceResult = salesOrderActions.getListProductInsightScarcityLevel(idClient, username, startTime, endTime, viewBy);
		String productInsightTotalService = salesOrderActions.getTotalProductInsightScarcityLevel(idClient, username, startTime, endTime, viewBy);
		boolean resultCompare = salesOrderActions.compareBEAndServiceResult(listProductInsightBE, listProductInsightServiceResult);
		System.out.println("INI HASIL BOOLEAN COMPARE RESULT : " + resultCompare);
		salesOrderActions.getAllignPercentageCompareMapBEAndServiceResult(listProductInsightBE, listProductInsightServiceResult , viewBy);
		salesOrderActions.compareBEAndServiceResultPercentage(productInsightTotalBE, productInsightTotalService, viewBy);
	}
	
	@And("As '(.*)' in '(.*)' I view Customer Overview with client '(.*)' in range time '(.*)' and '(.*)'")
	public void i_view_customer_overview_by_detail(String username, String environment, String idClient, String startTime, String endTime) throws ParseException, JsonParseException, JsonMappingException, IOException {
		salesOrderActions.hitBECustomerOverview(idClient, startTime, endTime, environment);
		Map<String, String> listCustomerOverviewBE = salesOrderActions.getListCustomerOverview();
//		salesOrderActions.getAxist(startTime, endTime, viewBy);
//		salesOrderActions.showSOInRangeTimeString(idClient, startTime, endTime);
		Map<String, String> listCustomerOverviewServiceResult = salesOrderActions.getListCustomerOverviewService(idClient, username, startTime, endTime);
		boolean resultCompare = salesOrderActions.compareBEAndServiceResult(listCustomerOverviewBE, listCustomerOverviewServiceResult);
		System.out.println("INI HASIL BOOLEAN COMPARE RESULT : " + resultCompare);
		salesOrderActions.getAllignPercentageCompareMapBEAndServiceResultCustomerOverview(listCustomerOverviewBE, listCustomerOverviewServiceResult);
	}
	
	@And("As '(.*)' in '(.*)' I view Customer Overview Detail by Workgroup with client '(.*)' in range time '(.*)' and '(.*)'")
	public void i_view_customer_overview_by_detail_workgroup(String username, String environment, String idClient, String startTime, String endTime) throws ParseException, JsonParseException, JsonMappingException, IOException {
		salesOrderActions.hitBECustomerOverviewByWorkgroup(idClient, startTime, endTime, environment);
		Map<String, String> listCustomerOverviewWorkgroupBE = salesOrderActions.getListCustomerOverviewDetail();
//		salesOrderActions.getAxist(startTime, endTime, viewBy);
//		salesOrderActions.showSOInRangeTimeString(idClient, startTime, endTime);
		Map<String, String> listCustomerOverviewServiceResult = salesOrderActions.getListCustomerOverviewWorkgroupService(idClient, username, startTime, endTime);
		boolean resultCompare = salesOrderActions.compareBEAndServiceResult(listCustomerOverviewWorkgroupBE, listCustomerOverviewServiceResult);
		System.out.println("INI HASIL BOOLEAN COMPARE RESULT : " + resultCompare);
		salesOrderActions.getAllignPercentageCompareMapBEAndServiceResultCustomerOverviewDetail(listCustomerOverviewWorkgroupBE, listCustomerOverviewServiceResult);
	}
	
	@And("As '(.*)' in '(.*)' I view Customer Overview Detail by Area with client '(.*)' in range time '(.*)' and '(.*)'")
	public void i_view_customer_overview_by_detail_area(String username, String environment, String idClient, String startTime, String endTime) throws ParseException, JsonParseException, JsonMappingException, IOException {
		salesOrderActions.hitBECustomerOverviewByArea(idClient, startTime, endTime, environment);
		Map<String, String> listCustomerOverviewAreaBE = salesOrderActions.getListCustomerOverviewDetailArea();
//		salesOrderActions.getAxist(startTime, endTime, viewBy);
//		salesOrderActions.showSOInRangeTimeString(idClient, startTime, endTime);
		Map<String, String> listCustomerOverviewAreaServiceResult = salesOrderActions.getListCustomerOverviewAreaService(idClient, username, startTime, endTime);
		boolean resultCompare = salesOrderActions.compareBEAndServiceResult(listCustomerOverviewAreaBE, listCustomerOverviewAreaServiceResult);
		System.out.println("INI HASIL BOOLEAN COMPARE RESULT : " + resultCompare);
		salesOrderActions.getAllignPercentageCompareMapBEAndServiceResultCustomerOverviewDetail(listCustomerOverviewAreaBE, listCustomerOverviewAreaServiceResult);
	}
	
	//NEWW
	//maria
	@And("I find SO with client '(.*)' in range time '(.*)' and '(.*)' string")
	public void i_find_so_with_client_in_range_time_String(String idClient, String startTime, String endTime) throws JsonParseException, JsonMappingException, IOException {
		salesOrderActions.showSOInRangeTimeString(idClient, startTime, endTime);
	}
}
