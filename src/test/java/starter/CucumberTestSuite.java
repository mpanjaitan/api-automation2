package starter;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import starter.salesorder.SalesOrdersRepository;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        features = "classpath:features"
)
@ContextConfiguration(classes = {SalesOrdersRepository.class})
public class CucumberTestSuite {}
