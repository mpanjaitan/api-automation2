Feature: Lookup a activity in sales order menu
  In order to know about widget data
  As an user
  I want to see details alligment between result from Backend and database OLTP

#Author		: Maria Magdalena Panjaitan
#User Story	: WORK_WEB_SO
#Cib-ID		: XXXX
  
#  Scenario: Get Allignment Percentage Sales Overtime
#    Given I find SO by default date
#    And As 'smitty' in 'env-production' I view Sales Overtime by 'day' and compare by 'none' with client '1' in range time '2019-11-01' and '2019-11-07'
#	And As 'nf.superuser' in 'env-production' I view Sales Overtime by 'day' and compare by 'none' with client '8683' in range time '2019-11-05' and '2019-11-12'
#    And As 'nf.superuser' in 'env-production' I view Sales Overtime by 'day' and compare by 'none' with client '8683' in range time '2019-12-01' and '2019-12-10'
#    And As 'advo.support' in 'env-production' I view Sales Overtime by 'day' and compare by 'none' with client '8333' in range time '2019-11-01' and '2019-11-05'
#    And As 'advo.support' in 'env-production' I view Sales Overtime by 'day' and compare by 'none' with client '8333' in range time '2019-12-01' and '2019-12-09'
#    And As 'advo.support' in 'env-production' I view Sales Overtime by 'day' and compare by 'none' with client '100036' in range time '2019-11-02' and '2019-11-03'
#    And As 'advo.cs' in 'env-production' I view Sales Overtime by 'day' and compare by 'none' with client '8333' in range time '2019-11-01' and '2019-11-20'
   
  #Advotics
#  Scenario: Get Allignment Percentage Product Insight ~ Sales Revenue - Advotics
#    Given I find SO by default date
#    And As 'smitty' in 'env-production' I view Product Insight by 'SKU' and unit 'salesRevenue' with client '1' in range time '2019-11-01' and '2019-11-07'
#    And As 'smitty' in 'env-production' I view Product Insight by 'productGroup' and unit 'salesRevenue' with client '1' in range time '2019-11-01' and '2019-11-07'
#    And As 'smitty' in 'env-production' I view Product Insight by 'brand' and unit 'salesRevenue' with client '1' in range time '2019-11-01' and '2019-11-07'

  #Federal
#  Scenario: Get Allignment Percentage Product Insight ~ Sales Revenue - Federal
#    Given I find SO by default date
#    And As 'advo.support' in 'env-production' I view Product Insight by 'SKU' and unit 'salesRevenue' with client '8333' in range time '2019-11-01' and '2019-11-05'
#    And As 'advo.support' in 'env-production' I view Product Insight by 'productGroup' and unit 'salesRevenue' with client '8333' in range time '2019-11-01' and '2019-11-05'
#    And As 'advo.support' in 'env-production' I view Product Insight by 'brand' and unit 'salesRevenue' with client '8333' in range time '2019-11-01' and '2019-11-05'
  
  #RAS 
#  Scenario: Get Allignment Percentage Product Insight ~ Sales Revenue - RAS
#    Given I find SO by default date
#    And As 'advo.support' in 'env-production' I view Product Insight by 'SKU' and unit 'salesRevenue' with client '100036' in range time '2019-11-02' and '2019-11-03'
#    And As 'advo.support' in 'env-production' I view Product Insight by 'productGroup' and unit 'salesRevenue' with client '100036' in range time '2019-11-02' and '2019-11-03'
#    And As 'advo.support' in 'env-production' I view Product Insight by 'brand' and unit 'salesRevenue' with client '100036' in range time '2019-11-02' and '2019-11-03'
  
  #Nutrifood SO-NFI-004540-csaserang1-02112019-115147731|2019-11-02|Nutr
#  Scenario: Get Allignment Percentage Product Insight ~ Sales Revenue - Nutrifood
#    Given I find SO by default date
#    And As 'nf.superuser' in 'env-production' I view Product Insight by 'SKU' and unit 'salesRevenue' with client '8683' in range time '2019-11-05' and '2019-11-12'
#    And As 'nf.superuser' in 'env-production' I view Product Insight by 'productGroup' and unit 'salesRevenue' with client '8683' in range time '2019-11-05' and '2019-11-12'
#    And As 'nf.superuser' in 'env-production' I view Product Insight by 'brand' and unit 'salesRevenue' with client '8683' in range time '2019-11-05' and '2019-11-12'

#Advotics - Product Insight - Qty Fulfilled 
#  Scenario: Get Allignment Percentage Product Insight ~ Qty Fulfilled - Advotics
#    Given I find SO by default date
#    And As 'smitty' in 'env-production' I view Product Insight Quantity Fulfilled by 'SKU' and unit 'fulfilledQuantity' with client '1' in range time '2019-11-01' and '2019-11-07'
#    And As 'smitty' in 'env-production' I view Product Insight Quantity Fulfilled by 'productGroup' and unit 'fulfilledQuantity' with client '1' in range time '2019-11-01' and '2019-11-07'
#    And As 'smitty' in 'env-production' I view Product Insight Quantity Fulfilled by 'brand' and unit 'fulfilledQuantity' with client '1' in range time '2019-11-01' and '2019-11-07'

  #Nutrifood - Product Insight - Qty Fulfilled
#  Scenario: Get Allignment Percentage Product Insight ~ Qty Fulfilled - Advotics
#    And As 'nf.superuser' in 'env-production' I view Product Insight Quantity Fulfilled by 'SKU' and unit 'fulfilledQuantity' with client '8683' in range time '2019-11-05' and '2019-11-12'
#    And As 'nf.superuser' in 'env-production' I view Product Insight Quantity Fulfilled by 'productGroup' and unit 'fulfilledQuantity' with client '8683' in range time '2019-11-05' and '2019-11-12'
#    And As 'nf.superuser' in 'env-production' I view Product Insight Quantity Fulfilled by 'brand' and unit 'fulfilledQuantity' with client '8683' in range time '2019-11-05' and '2019-11-12'

  #Federal - Product Insight - Qty Fulfilled
#  Scenario: Get Allignment Percentage Product Insight ~ Qty Fulfilled - Advotics
#    And As 'advo.support' in 'env-production' I view Product Insight Quantity Fulfilled by 'SKU' and unit 'fulfilledQuantity' with client '8333' in range time '2019-11-01' and '2019-11-05'
#    And As 'advo.support' in 'env-production' I view Product Insight Quantity Fulfilled by 'productGroup' and unit 'fulfilledQuantity' with client '8333' in range time '2019-11-01' and '2019-11-05'
#    And As 'advo.support' in 'env-production' I view Product Insight Quantity Fulfilled by 'brand' and unit 'fulfilledQuantity' with client '8333' in range time '2019-11-01' and '2019-11-05'

  #RAS - Product Insight - Qty Fulfilled
#  Scenario: Get Allignment Percentage Product Insight ~ Qty Fulfilled - RAS
#    And As 'advo.support' in 'env-production' I view Product Insight Quantity Fulfilled by 'SKU' and unit 'fulfilledQuantity' with client '100036' in range time '2019-11-02' and '2019-11-03'
#    And As 'advo.support' in 'env-production' I view Product Insight Quantity Fulfilled by 'productGroup' and unit 'fulfilledQuantity' with client '100036' in range time '2019-11-02' and '2019-11-03'
#    And As 'advo.support' in 'env-production' I view Product Insight Quantity Fulfilled by 'brand' and unit 'fulfilledQuantity' with client '100036' in range time '2019-11-02' and '2019-11-03'

   #Advotics - Product Insight - Scarcity Level
#   Scenario: Get Allignment Percentage Product Insight ~ Scarcity Level - Advotics
#    And As 'smitty' in 'env-production' I view Product Insight Scarcity Level by 'SKU' and unit 'scarcityLevel' with client '1' in range time '2019-11-01' and '2019-11-07'
#    And As 'smitty' in 'env-production' I view Product Insight Scarcity Level by 'productGroup' and unit 'scarcityLevel' with client '1' in range time '2019-11-01' and '2019-11-07'
#    And As 'smitty' in 'env-production' I view Product Insight Scarcity Level by 'brand' and unit 'scarcityLevel' with client '1' in range time '2019-11-01' and '2019-11-07'
  
   #Nutrifood - Product Insight - Scarcity Level
#   Scenario: Get Allignment Percentage Product Insight ~ Scarcity Level - Nutrifood
#    And As 'nf.superuser' in 'env-production' I view Product Insight Scarcity Level by 'SKU' and unit 'scarcityLevel' with client '8683' in range time '2019-11-05' and '2019-11-12'
#    And As 'nf.superuser' in 'env-production' I view Product Insight Scarcity Level by 'productGroup' and unit 'scarcityLevel' with client '8683' in range time '2019-11-05' and '2019-11-12'
#    And As 'nf.superuser' in 'env-production' I view Product Insight Scarcity Level by 'brand' and unit 'scarcityLevel' with client '8683' in range time '2019-11-05' and '2019-11-12'
  
   #Federal - Product Insight - Scarcity Level
#   Scenario: Get Allignment Percentage Product Insight ~ Scarcity Level - Federal
#    And As 'advo.support' in 'env-production' I view Product Insight Scarcity Level by 'SKU' and unit 'scarcityLevel' with client '8333' in range time '2019-11-01' and '2019-11-05'
#    And As 'advo.support' in 'env-production' I view Product Insight Scarcity Level by 'productGroup' and unit 'scarcityLevel' with client '8333' in range time '2019-11-01' and '2019-11-05'
#    And As 'advo.support' in 'env-production' I view Product Insight Scarcity Level by 'brand' and unit 'scarcityLevel' with client '8333' in range time '2019-11-01' and '2019-11-05'
  
  #RAS - Product Insight - Scarcity Level
#   Scenario: Get Allignment Percentage Product Insight ~ Scarcity Level - RAS
#    And As 'advo.support' in 'env-production' I view Product Insight Scarcity Level by 'SKU' and unit 'scarcityLevel' with client '100036' in range time '2019-11-02' and '2019-11-03'
#    And As 'advo.support' in 'env-production' I view Product Insight Scarcity Level by 'productGroup' and unit 'scarcityLevel' with client '100036' in range time '2019-11-02' and '2019-11-03'
#    And As 'advo.support' in 'env-production' I view Product Insight Scarcity Level by 'brand' and unit 'scarcityLevel' with client '100036' in range time '2019-11-02' and '2019-11-03'

   #Advotics - Customer Overtime
#   Scenario: Get Allignment Percentage Customer Overtime - Advotics
#    And As 'smitty' in 'env-production' I view Customer Overview with client '1' in range time '2019-12-01' and '2019-12-30'
#    And As 'smitty' in 'env-production' I view Customer Overview Detail by Workgroup with client '1' in range time '2019-12-01' and '2019-12-30'
#    And As 'smitty' in 'env-production' I view Customer Overview Detail by Area with client '1' in range time '2019-12-01' and '2019-12-30'
  
  #Federal - Customer Overtime
#   Scenario: Get Allignment Percentage Customer Overtime - Federal
#    And As 'advo.support' in 'env-production' I view Customer Overview with client '8333' in range time '2019-12-01' and '2019-12-30'
#    And As 'advo.support' in 'env-production' I view Customer Overview Detail by Workgroup with client '8333' in range time '2019-12-01' and '2019-12-30'
#    And As 'advo.support' in 'env-production' I view Customer Overview Detail by Area with client '8333' in range time '2019-12-01' and '2019-12-30'
  
  #Nutrifood - Customer Overtime
   Scenario: Get Allignment Percentage Customer Overtime - Nutrifood
#    And As 'nf.superuser' in 'env-production' I view Customer Overview with client '8683' in range time '2019-12-01' and '2019-12-30'
    And As 'nf.superuser' in 'env-production' I view Customer Overview Detail by Workgroup with client '8683' in range time '2019-12-01' and '2019-12-30'
#    And As 'nf.superuser' in 'env-production' I view Customer Overview Detail by Area with client '8683' in range time '2019-12-01' and '2019-12-30'
  
  #RAS - Customer Overtime
#   Scenario: Get Allignment Percentage Customer Overtime - RAS
#    And As 'advo.support' in 'env-production' I view Customer Overview with client '100036' in range time '2019-12-01' and '2019-12-30'
#    And As 'advo.support' in 'env-production' I view Customer Overview Detail by Workgroup with client '100036' in range time '2019-12-01' and '2019-12-30'
#    And As 'advo.support' in 'env-production' I view Customer Overview Detail by Area with client '100036' in range time '2019-12-01' and '2019-12-30'
  
  #TC_ID		: WORK_WEB_SO_1
  #Sales Order	: View Sales Order List -> using data table
#  Scenario: Get SO detail
#    Given I have environment 'advowork-integration'
#    When I request detail SO with number '3507-99976-29178-123469'
#    Then I should get response status code '200'
#    And I should get SO status 'Fulfilled'
#    And I should get item 'ABC-mama'

  
#  Scenario: Get SO by SONo
#    Given I find SO by default date
##    And I find SO with client '1' in range time '20191110230000' and '20191110234000'
##    And I find SO with client '1' in range time '20190901000000' and '20190908000000'
##    And I find SO with client '1' in range time '20191001000000' and '20191008000000'
##    And I find SO with client '1' in range time '20190901000000' and '20190908000000' string
#    And As 'smitty' in 'env-production' I view Sales Overtime by 'day' and compare by 'none' with client '1' in range time '2019-11-01' and '2019-11-20'
#    And As 'smitty' in 'env-production' I view Product Insight by 'SKU' and unit 'salesRevenue' with client '1' in range time '2019-11-01' and '2019-11-07'
#    And As 'smitty' in 'env-production' I view Product Insight by 'productGroup' and unit 'salesRevenue' with client '1' in range time '2019-11-01' and '2019-11-07'
#    And As 'smitty' in 'env-production' I view Product Insight by 'brand' and unit 'salesRevenue' with client '1' in range time '2019-11-01' and '2019-11-07'
##    And As 'nf.superuser' I view Sales Overtime by 'day' and compare by 'none' with client '8683' in range time '2019-12-01' and '2019-12-09'
##    And As 'nf.superuser' I view Sales Overtime by 'day' and compare by 'none' with client '8683' in range time '2019-12-01' and '2019-12-10'
##    And As 'advo.support' I view Sales Overtime by 'day' and compare by 'none' with client '8333' in range time '2019-12-01' and '2019-12-10'
#    And As 'advo.support' I view Sales Overtime by 'day' and compare by 'none' with client '8333' in range time '2019-12-01' and '2019-12-09'
#    And As 'advo.support' I view Sales Overtime by 'day' and compare by 'none' with client '100036' in range time '2019-11-01' and '2019-11-02'
#    And As 'advo.cs' I view Sales Overtime by 'day' and compare by 'none' with client '8333' in range time '2019-11-01' and '2019-11-20'
#    And I find Sales Overtime with detail below:
#    |	clientId	|	startDate		|		endDate		|	viewBy	| comparedBy	|
#    |	1			|	20190901000000	|	20190908000000	|	day		|	none		|
#    And I find SO with client '1' in range time '20191030000000' and '20191102000000'
#    And I find SO with client '8333' in range time '20190901000000' and '20190908000000'
#    When I request detail SO with number '3507-99976-29178-123469'
#    Then I should get response status code '200'
#    And I should get SO status 'Fulfilled'
#    And I should get item 'ABC-mama'